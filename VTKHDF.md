**Auteur** : *Jacques-Bernard Lekien*, jacques-bernard.lekien@cea.fr

**Projet gitlab.com** : *pykdi*, *6 mai 2024*.

# Le format VTK HDF

Le chemin pour aller jusqu'à la version 0.4.0 n'a pas été le long fleuve
tranquille que l'on pensait.
L'objet de ce document, c'est de remonter les écueils rencontrés  ou
d'engager des réflexions sur des choix sémantiques fait par Kitware.

## Pérennité

Le principal point délicat sur l'emploi d'une bibliothèque E/S non maison,
comme peut l'être Hercule, c'est la pérennité des bases ainsi produites.

Quelle garantie a-t-on qu'une base produire en HDF5 soit relisible dans
plusieurs années ?

<span style="color: #e65b4c">
Primordial, la mise en place de tests de non-régression exécutés
régulièrement pour accéder à d'anciennes bases avec une validation
fine que ce soit du point de vue de HDF comme de ParaView/VTK à travers
l'analyse et la visualistion des données.
</span>

En effet, le choix de la sémantique VTK nous lit indiscutablement au
bon fonctionnement du lecteur VTK HDF de ParaView/VTK ainsi qu'aux
fonctions avancées qu'il serait en mesure de proposer.

<span style="color: #e65b4c">
Aujourd'hui, constaté jusqu'à la version 0.4.0, l'option `MergeParts`
activée du lecteur de base VTK HDF fonctionne très bien afin de
proposer une seule partition par serveur, alors qu'au contraire
la désactivation de cette option ne fonctionne pas.
Heureusement, nous conseillons l'activation de cette option afin que
l'application de filtres puissent donner toujours un résultat correct.
</span>

<span style="color: #e65b4c">
A défaut d'avoir fait le test puisque cette fonctionnalité n'a pas
d'intérêt pour nous, on peut néanmoins se demander si en l'état ce
lecteur est toujours capable de charger des partitions représentant
des maillages structurés afin de décrire un maillage structuré plus
grand. Même si là aussi, il est conseillé alors de décrire un
unique maillage structuré qui devrait alors être automatiquement
distribué sur les serveurs à la lecture de ces données.
</span>

A savoir que ce format VTK HDF est en cours de définition par Kitware.
C'est la raison pour laquelle l'usage que nous en faisons se limite
pour le moment à la représentation de maillages non structurés
distribués en partitions elles-mêmes non structurés.

## HDF

### h5py

Le choix du module Python s'est porté sur `h5py` qui propose une API plus
compacte que son l'équivalent en C++ en proposant des fonctions intégrées
[PythonAndHDF52013].

<span style="color: #e65b4c">
Comment décrire un tableau mono-dimensionné de taille non limitée en `h5py` ?
</span>

La seule difficulté rencontrée est apparue lorsque j'ai voulu décrire des
tableaux mono-dimensionnés de taille non limitée, les exemples  portés
toujours sur des tableaux multi-dimensionnés.

La philosophie de l'ouvrage telle qu'il est présentée laisse à penser  que
l'on utilise la première dimension comme variant temporel et la seconde
dimension pour décrire la donnée.

Au final, le problème n'est pas lié à l'API mais à l'interpréteur Python qui
n'a pas le même comportement entre le tuple ainsi décrit `(None)` qui est
finalement équialent à `None` et le tuple `(None,)` qui ne l'est pas.

La syntaxe d'appel pour créer un tel tableau est donc la suivante, à la
virgule près (sans faire de jeu de mots) :

```python
dset = fw[parent_key].create_dataset(name=child_key,
                                     shape=(1,),
                                     dtype=dtype,
                                     maxshape=(None,),
                                     chunks=True)
```

### Régles d'utilisation

En C++, le préfixe des noms des fonctions indique leur périmètre :
- h5f... pour les manipulations de fichier
- h5g... pour les opérations sur les groupes
- h5s... pour les opérations sur les dataspaces
- h5d... pour les opérations sur les datasets

La philosophie d'emploi de l'API est un peu archaïque puisque, on le verra
plus tard, il faut dissocier la phase de création de la base de celle
de la complétion.

Ceci est vrai en fait pour tous les éléments d'une base HDF.

Ainsi, le code se décompose toujours en deux parties :
- est-ce que l'objet existe ?
- si non, alors je le crée.

Voici un exemple pour un groupe :
```python
        try:
            fw[key_group]
        except KeyError:
            fw.create_group(key_group, ...)
```
ou encore pour un dataset :
```python
    try:
        dset = fw[key_full]
    except KeyError:
        dset = fw[key_parent].create_dataset(name=key_dataset, ...)
```

On aurait pu imaginer une API qui supporte l'appel récurrent de `create_`
avec un mode `force` si on souhaitait créer un nouveau élément.

Tout cela allourdit inutilement le code.

<span style="color: #e65b4c">
Normalement, l'option `track_order` peut être fixé au niveau du File pour tous les
éléments `dataset`/`group`/`attribute`, même si dans notre cas, seul l'aspect `group`
nous intéresse. Le fait de le préciser à la création (w) ou à la complétion (a),
le fait de retirer cet option lors du create_group rend illisible la base HDF
par ParaView.

### h5dump

La commande `h5dump` comme `h5diff` ne sont pas suffisemment discréminent
pour identifier une différence entre deux bases.

C'est tout particulièrement le cas lorsqu'on emploie ou non le paramètre `track_order`
lors de la définition d'un groupe à travers l'API d'écriture :
```python
fw.create_group(group_key, track_order=True)
```
Les deux bases sont alors équivalentes du point de vue de ces outils, mais
ne l'est pas du point de vue du lecteur VTK HDF dans ParaView qui refuse
d'ouvrir une base sans la définition de ce paramètre à vrai.

<span style="color: #e65b4c">
Attention à bien employer le paramètre `track_order=True`.
</span>

Ce comportement est probablement à lier avec l'emploi de `SoftLink` :
```python
fw[link_internal_key] = h5py.SoftLink(source_key)
```

### Compression

Non abordé par la 0.4.0, l'aspect ***compression*** proposé par HDF5 nécessite des
expérimentations fines afin de  pouvoir juger du gain en stockage vs de la perte en
temps elapse à l'écriture comme à la lecture.
La nécessité de picorer dans un tableau ne va, a priori, pas en faveur de cette option.
Néanmoins, elle pourrait être considéré intéressante si le gain en stockage était
trés important sur des tableaux, a priori, très peu utilisé ou pour des simulations
très grosses.

La documentation [PythonAndHDF52013] indique un facteur de compression de l'ordre de
10 à 20% (noisy data or random float data), ce qui est conforme aux expérimentations du
début du siècle en Hercule sur des données codes (plutôt 7 - 12% à vrai dire), sur un
tableau comportant un million de valeurs pour un surcoût elapse d'usage de 6 à 15!

A priori, le gain ici n'en vaut pas la chandelle d'autant que le CDC (Centre De Calcul)
recommande de ne pas compresser les données afin de favoriser la compression pour le
stockage longue durée.

Bien sûr, la question reste ouverte si la sémantique des données permet un facteur de
compression bien plus important comme cela peut être le cas pour des données TB-AMR
[ThèseLoïcStrafella2023].

### Checksum

Non abordé par la 0.4.0, l'aspect ***checksum*** via Fletcher32 est possible au
niveau d'un tableau :
```python
dset = fw[parent_key].create_dataset(name=child_key,
                                     shape=(1000,),
                                     fletcher32=True)
```
C'est un algorithme équivalent, très rapide et tout aussi performant que j'ai
développé pour Hercule.

A savoir que le checksum est calculé et stocké par chunk.

Reste à en déterminer le niveau de pertinence vs le rapport coût / gain d'une
telle fonctionnalité.

### Latitude à utiliser plusieurs fichiers

Le principe de HDF est de pouvoir décrire une base à travers un unique fichier même
si celui-ci décrit une simulation parallèle.

L'emploi de tableaux avec une dimension non-limité amène HDF5 à définir des morceaux
de tableau (chunk). Dans la version 0.4.0, l'activation est décrite par le paramètre
`chunks=True` qui est d'ailleurs automatiquement activé à partir du moment que l'on
définit une dimension non-limité.

Non testé dans la version 0.4.0, il existe néanmoins la possibilité de décrire les 
données dans plusieurs fichiers mais il restera un fichier qui jouera le rôle d'entête :

- un tableau peut être complétement décrit dans un autre fichier :
```python
with h5py.File('data_file.vtkhdf', 'w') as f1:
    f1.create_group('source_key')

with f2 = h5py.File('header_file.vtkhdf', 'w')
    f2['link_external_key'] = h5py.ExternalLink('file_with_resource.hdf5', 'source_key')
```
Ici le fichier `header_file.vtkhdf` indique que la clef `link_external_key` est
réellement défini dans le fichier `data_file.vtkhdf`.

<span style="color: #e65b4c">
A vérifier le comportement si le fichier `data_file.vtkhdf` n'existe pas. Est-ce qu'il
est possible d'ouvrir quand même le fichier `header_file.vtkhdf`' pour accéder aux
autres données ? Si c'est le cas, cela pourrait être un moyen de produire une base
avec des données dont l'intérêt serait immédiat... puis d'en réduire la taille
en supprimant les fichiers de données... optionnels.
</span>

- un tableau peut être poursuivi dans un autre fichier à l'aide des `VirtualSource`
et  `VirtualLayout`.
Cette description n'est pas disponible dans [PythonAndHDF52013] mais dans la
documentation https://docs.h5py.org/en/stable/vds.html.
Il semble que ce mécanisme permet d'ajouter une dimension virtuelle venant à choisir
dans quel fichier allait chercher les données. C'est tout au moins un des usages.
Néanmoins, dans ce cas de figure tout au moins, cela impacte la façon d'accéder aux
données. A défaut de recherches plus approfondies et d'expérimentation, le constat
que je peux faire aujourd'hui semble indiquer que l'on ne puisse pas scinder un
tableau sur plusieurs fichiers.

### Compatibilité avec le CDC

Hercule exploite sa capacité à écrire dans plusieurs fichiers afin d'augmenter son
débit. Mais un autre avantage est de, potentiellement (à voir si c'est toujours le cas)
de limiter le chargement de fichiers lorsque les fichiers ont migré vers les unités
de stockage longue durée.

Avec HDF, au vu des latitudes que l'on a pour décrie la base dans plusieurs fichiers,
il semble évident qu'il faudra forcément rapatrier tout le fichier afin d'accéder à
une toute petite partie.

<span style="color: #e65b4c">HDF 1*</span> / <span style="color: #00bb00">3* Hercule</span> :
HDF nécessite, a priori, le rapatriement intégral du fichier représentant la base pour
fonctionner alors qu'Hercule peut, potentiellement, ne rapatrier que les derniers fichiers
pour n'accéder qu'au dernier temps de simulation écrit.

### Lock

En l'état, nous constatons avec la version 0.4.0 que l'écriture d'une base HDF
n'est pas compatible avec son utilisation simultanée en lecture, tout
particulièrement à trouver l'ouverture de la base avec ParaView.

<span style="color: #e65b4c">HDF 0*</span> / <span style="color: #00bb00">4* Hercule</span> :
même si le mode opératoire n'est pas parfait, Hercule autorise, dans une certaine mesure,
de visualiser une base tout en la complétant par ailleurs ; HDF ne l'autorise pas.

<span style="color: #e65b4c">Remarque</span>
Il existe un paramètre `locking` lors de la création / ouverture de la base HDF, qui
gére le comportement de verrouillage des fichiers... sans être plus précis dans la
documentation. C'est probablement quelque chose qu'il faudra regarder et expérimenter
pour comprendre ce que cela couvre.

### Création 'w' et 'a'

Peu avant la version 0.4.0, la création du fichier se faisait via `h5py.File` en mode
`a` puisque ce dernier crée le fichier lorsqu'il n'existe pas ou le complète si il existe.

Sauf que l'on constate un accroissement de la taille du fichier alors que h5dump indique
le même contenu alors qu'on ne fait que relancer un test qui réalise une création et
écriture de données.

Il se trouve que c'est lié au fait que l'on fasse un `close` ce qui entraîne une perte
de l'information de chaînage des blocs libres. Ceci amenant à ne pas réutiliser les
espaces libres.

Pour éviter cela, il faut positionner l'option `fs_persist=True` sauf que cela n'est
autorisé qu'à la création de la base... et donc avec un mode 'w'. Cela permet de
rendre persistent (sauvegarder) le chaînage des blocs libres. Pour activer cette
option, il est nécessaire de choisir une stratégie de stockage, j'ai pris celle
qui est par défaut `fs_strategy='fsm'.`

En différenciant ainsi la phase de création d'une nouvelle base des phases de complétion,
où l'emploi de ces arguments sont interdits, on constate alors que la taille du fichier
ne change plus au fur et à mesure des relances des tests sur un fichier existant.

On peut vérifier les options du `SUPER_BLOCK` à travers la commande :
```shell
h5dump -B -H myfile.vtkhdf
```

La commande 
```shell
h5stat -S myfile.vtkhdf
```
livre d'autres informations de remplissage de la base.

<span style="color: #e65b4c">
Faisons remarquer qu'aucun de ces utilitaires nous indique que l'option `track_order` a
été positionné. Hors, le fait de ne pas le faire rend la base non lisible par ParaView
ce qui montre une incidence importante.
</span>

### Contraintes d'utilisation de l'API HDF

HDF ne supporte pas les index non croissant.

Il ne supporte pas non plus qu'un processus crée une clef et que les
autres ne le font pas.

Il ne supporte pas non plus la relecture d'un champ par un processus
dont les valeurs ont été par un autre.

Il nécessite que tous les processus dimensionnent tous les tableaux même
si le processus n'interviendra pas sur l'affectation de valeurs.

### Comparaison théoriques des performances : HDF5 vs Hercule

Les paramètres Hercule d'évaluation :
- Ts : le nombre de temps ;
- RTs : le nombre moyen de temps décrit dans un fichier ;
- Ps : le nombre de partitions ;
- RPs : le nombre de partitions décrit dans un fichier.

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">1* Hercule</span> :
la description de la sémantique est faite une fois (HDF) vs Ts * Ps fois (Hercule).

<span style="color: #e65b4c">HDF 0*</span> / <span style="color: #00bb00">4* Hercule</span> :
HDF à pleinement confiance dans le file system alors qu'Hercule ***pourrait*** avoir une
certaine tolérance.

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">2* Hercule</span> :
HDF5 (peut) utilise mieux le strapping d'un fichier tout en étant plus dépendant de ce paramètre
au contraire d'Hercule qui s'en détache en ouvrant plus de fichiers. Hercule est très clairement
plus dépendant du volume de données qui est attribué par partition au contraire de HDF.

<span style="color: #e65b4c">HDF 2*</span> / <span style="color: #00bb00">5* Hercule</span> :
HDF5 nécessite de gérer soi-même les offsets ce que Hercule gère par lui-même. Cette contrainte
s'efface en utilisant KDI.

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">1* Hercule</span> :
HDF5 (peut) utilise qu'un indode ou Hercule en utilisera plusieurs (Ps/RPs * Ts/RTs)

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">1* Hercule</span> :
A l'écriture, HDF5 facilite l'écriture d'une vision mono-domaine exploitant mieux le strapping
alors qu'Hercule favorise l'écriture par partition. A la lecture, HDF5 facilite une
redistribution différente que l'écriture au contraire d'Hercule qui impose un chargement en
nombre de paritions (tout particulièrement dans ParaView).

Le bilan est plutôt en faveur de HDF5 avec une dépendance plus forte sur les choix proposés
par le CDC (Centre De Calcul) au niveau du paramètrage fixé ou à dessein du fichier.

Tout cela devra être confrotné à des mesures, certaines ne pouvant être réalisées avec
la version 0.4.0.

## Sémantique VTK

La philosphie de Kitware est clairement d'avoir à écrire un lecteur VTK HDF pour ParaView/VTK
réalisant le moins d'opération, de traitements possibles.

Pour cela, leur choix a été de mimer au plus près la structure mémoire des objets VTK dans ce
format de fichier.

### UnstructuredGrid, un point c'est tout

Dans Hercule, nous avons fait le choix de définir une fois le maillage non structuré
(ou structuré d'ailleurs) correspondant à la simulation totale puis de décrire les
milieux comme étant une sélection de cellules de ce maillage en y attribuant des champs
dit partiels relatifs à chacun des milieux.

Les milieux pouvant se recouvrir, c'est à dire définir la même cellule du maillage initial,
il est proposé de pouvoir définir des interfaces planes entre ces milieux pour ces
cellules particulières dites mixtes. Cela se fait en Hercule à travers une description spécifique.

A charge du lecteur Hercule de traduire ces informations afin de produire le juste maillage
non structuré.

Dans le cas de VTK HDF, il faut définir une hiérarchie de maillages non structurés explicitement
décrits, donc sans cette notion de milieux décrit comme une sélection de cellules d'un maillage
plus grand.

<span style="color: #e65b4c">HDF 4*</span> / <span style="color: #00bb00">5* Hercule</span> :
la description est plus compacte en Hercule qu'en VTK HDF.

Si C² est le nombre de cellules d'un maillage non structuré d'hexaèdres, une rapide estimation
du coût moyen donne 3C² pour décrire les points et (1+8)C² pour décrire les cellules (type +
connectivité d'un hexaèdre) soit au total 12C².
Dans le même esprit, le nombre de cellules mixtes pour M milieux est M*C
(le cas d'un saucissonnage en M milieux d'un maillage carré de côté C, c'est (M-1)C).
La description de la sémantique de VTK HDF sera de 12C²+24CM où celle d'Hercule est de 12C²+C²+CM.
Finalement, suivant ce calcul trés approximatif, le stockage du maillage est plus coûteux
en VTK HDF qu'en Hercule pour un M < C/23 !
Concernant les champs partiels, les coûts sont équivalents pour les deux solutions.
Pour les champs globaux, le coût sera pour VTK HDF de C²+2CM au lieu de C² en Hercule.
Le coût des 6 valeurs pour décrire les interfaces des cellules mixtes coûtent 6C² en VTK HDF
alors qu'il est de 8CM.
A remarquer, que le surcoût relatif entre VTK HDF et Hercule diminue en fonction du nombre
de champs partiels décrits dans la base ; notablement à partir d'une vingtaine.

Notons que la sémantique compacte pour le stockage Hercule nécessite un chargement plus important
et un travail de mise en forme pour charger un milieu, ce qui est désavantageux pour la
visualisation, tout particulièrement interactive, ce qui n'est pas le cas en VTK HDF.

A faire remarquer que l'application de la compression sur les tableaux décrivant les interfaces
des cellules mixtes en VTK HDF pourrait être un sérieux atout afin de réduire le coût du
stockage. En effet, les cellules pures positionnent les valeurs à 0 puisqu'elles n'ont pas
d'interfaces.

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">3* Hercule</span> :
le chargement d'un milieu doit être notablement plus performant en VTK HDF qu'en Hercule.

<span style="color: #e65b4c">HDF 4*</span> / <span style="color: #00bb00">5* Hercule</span> :
le chargement de tous les milieux pourrait être plus performant en Hercule qu'en VTK HDF.

Ces avis sont donnés dans le cas où un serveur charge une partition. Dans le cas où un
serveur aurait plusieurs partitions, la politique d'écriture en VTK HDF peut faire fortement
varier les performances en lecture.

Si Kitware envisage un jour de décrire des tableaux virtuels pour le stockage tels qu'ils les
ont en mémoire, ce serait alors une autre approche offerte à l'optimisation du stockage pour
le format VTK HDF notablement pour la description des interfaces pour les cellules mixtes
(mutualisation, indexation, concaténation).

### Offets

Le choix fait par Kitware de la description des offsets de décalage qu'il soit temporel ou
par partition est discutable dans VTK HDF.

En effet, une solution nécessitant le moins de déplacement et chargement de données
aurait été de décrire les offsets suivant le couple (temps, partition) au lieu de juste (temps).

Dans les deux cas, nous avons à la position `iStep` de `/Steps/PartOffsets` l'offset `origin` du
début de l'enregistrement pour le temps concerné.

Prenons :
- S le nombre de temps de simulation sauvegardé ;
- P le nombre de partitions.

#### Offsets attendus

Suivant ma solution permettant d'accéder plus rapidement aux chargements des valeurs,
à l'offset `origin` + `iPart` dans `/Steps/[Point|Cell]Offsets`,
à défaut d'avoir `/Steps/[Point|Cell]DataOffsets/[FieldName]`, nous avons deux valeurs 
contigües qui correspondent à l'offset de début `begin` et à l'offset de fin `end` (une position
plus loin que la dernière valeur à charger) décrivant les valerus à charger pour `FieldName`.

De fait, cela se fait en :
- un premier déplacement est nécessaire pour charger une valeur `origin` ; puis
- un second déplacement est nécessaire pour charger deux valeurs  `begin` et de `end` ;
- pour enseuite terminer par charger les valeurs entre l'offset `begin` (inclus) et `end` (non inclus).

Le coùt du stockage des offsets (`/Steps/[Point|Cell]Offsets` ou
`/Steps/[Point|Cell]DataOffsets/[FieldName]`) est de SP.
Ce surcoût est restreint si `/Steps/[Point|Cell]Offsets` suffit à déterminer les évolutions des
champs de valeurs.

Cette solution était implémentée en KDI jusqu'au commit #f668bf01d80db8cdf3d6273721c4860c92b8c42e .

#### Offsets VTK

Kitware a opté pour une solution plus compact en stockage mais nécessitant plus de
déplacements et chargements.
Cette solution nécessite de charger l'offset `begin_partition_0` dans `/Steps/[Point|Cell]Offsets`,
à défaut d'avoir `/Steps/[Point|Cell]DataOffsets/[FieldName]`.
Ensuite, il faut charger le nombre de `/NumberOf[Points|Cellules]` de l'offset `origin`
(inclus) à `origin` + iPart + 1 (exclus).
L'offset de début `begin` vaut alors `begin_partition_0` augmenté des valeurs de `/NumberOf[Points|Cellules]`
de  l'offset `origin` (inclus) à `origin` + iPart (exclus).
L'offset de début `end` vaut alors `begin_partition_0` augmenté des valeurs de `/NumberOf[Points|Cellules]`
de  l'offset `origin` (inclus) à `origin` + iPart + 1 (exclus).

De fait, cela se fait en :
- un premier déplacement est nécessaire pour charger une valeur `origin` ; puis
- un second déplacement est nécessaire pour charger une valeur  `begin_partition_0` ;
- un troisième déplacement est nécessaire pour charger `iPart` + 1 valeurs ;
- un calcul est nécessaire pour obtenir les deux valeurs d'offsets `begin` et de `end` ;
- pour enseuite terminer par charger les valeurs entre l'offset `begin` (inclus) et `end` (non inclus).

Le coùt du stockage des offsets (`/Steps/[Point|Cell]Offsets` ou
`/Steps/[Point|Cell]DataOffsets/[FieldName]`) est de S.

C'est la solution retenue pour l'écriture afin que la base soit lisible par le lecteur VTK HDF.

Le gain en stockage est de (S-1)P par milieu (si tous les champs évoluent de la même façon).
Le surcoût est lié au fait qu'il faille faire un déplacement supplémentaire pour lire `iPart` + 1 valeurs.
Ce surcoût dépend du temps de latence relatif au repositionnement (***seek***), de fait du choix
de la taille d'un chunk HDF, et du nombre de valeurs à charger (une page étant généralement de 4ko,
il faudra charger une page tous les 4096/8=512 partitions), sans prendre compte le coût en calcul
des offsets `begin` et `end` qui reste négligeable face au reste.

## Global field

Les champs globaux sont décrits par maillage sous la clef `FieldData`.
Ils doivent être décrits pour chacun des maillages... l'emploi des `SoftLink` de HDF
peut néanmoins remédier à ce problème de duplication apparente, même si au final,
il est très probable que le lecteur VTK HDF relise autant de fois les données
qu'il y a de liens !

<span style="color: #e65b4c">HDF 3*</span> / <span style="color: #00bb00">5* Hercule</span> :
le lecteur VTK HDF par rapport à celui d'Hercule va relire autant de fois les champs
globaux qu'il y a de milieux.

La description d'un champ global est légérement complexe car elle permet de décrire
des champs globaux 1D avec une dimension variable et 2D avec la première dimension variable
et la seconde fixée et surdimensionné afin d'acceuillir le nombre maximal de valeurs
suivant cette dimension au cours de toute la simulation.
Le contrôle des offsets se fait à travers :
- `FieldDataOffsets` qui décrit pour chaque entrée de combien de valeurs décaler la
  lecture dans le tableau associé ; et
- `FieldDataSizes` qui décrit pour chaque entrée le composant de donnés de champ et la 
  taille du tuple décrivant le shape.

Dans Themys, un lecteur spécifique est fourni afin d'extraire les valeurs des champs
globaux au cours du temps. A voir son usage dans une exploitation via Python.

En 0.4.0, nous n'avons pas encore expérimenté la sauvegarde de champs globaux.

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">1* Hercule</span> :
la description des champs glboaux en VTK HDF permette une lecture temporelle nettement
plus rapide qu'en Hercule (une amélioration peut être obtenue avec l'activation de
l'historisation, mécanisme obscure, donnée difficilement accessible et peu performant
d'Hercule ; qui aurait pu être compensé par HDc, bibliothèque abandonné trop rapidement).
C'est tout particulièrment le cas des champs globaux (1D, 2D) avec des dimensions
invariantes au cours du temps).

<span style="color: #00bb00">HDF 5*</span> / <span style="color: #e65b4c">0* Hercule</span> :
Hercule ne supporte que les champs globaux à dimension fixée au cours du temps alors
que VTK HDF supporte les champs globaux 1D à dimension fixée ou variable et les 2D avec la
première dimension fixée ou variable et la seconde dimensionée fixé ou variable jusqu'à
une valeur correspond à la dimension maximale fixée pour toute la simulation.

## Bibliographie

[PythonAndHDF52013] ***Python and HDF5***, Andrew Collette, 2013, O'Reilly.

[ThèseLoïcStrafella2023] ...