// https://docs.python.org/3/extending/embedding.html
//
// https://www.codeproject.com/Articles/11805/Embedding-Python-in-C-C-Part-I
// Types
// https://docs.python.org/3/c-api/arg.html
// https://www.mit.edu/people/amliu/vrut/python/ext/buildValue.html

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <numpy/arrayobject.h>

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "arcaninou/Kdi.h"

using namespace KDI;

int
test_one_mesh_non_milieu(int argc, char *argv[])
{
    bool trace { false };
    int nb_part { 4 };

    std::shared_ptr<KDI::KDIBase> base = createBase("cpp__uns_1pe_4ssd_1uns_10tps__A", nb_part, trace);
    // base->dump();
    base->update("UnstructuredGrid", "/mymesh");
    // base->dump();
    base->update("UnstructuredGrid", "/mymesh2");
    // base->dump();

    base = createBase("cpp__uns_1pe_4ssd_1uns_10tps__A", nb_part, trace);
    // base->dump();
    base->update("UnstructuredGrid", "/mymesh");
    base->update_fields("/mymesh/points/fields", "myFieldPoint");
    base->update_fields("/mymesh/cells/fields", "myFieldCell");
    // base->dump();
    for(int int_step=0; int_step<10; ++int_step)
    {
        float float_step = float(int_step * 1.1);
        std::shared_ptr<KDI::KDIChunk> chunk_partless = base->chunk(float_step);

        for(int int_part=0; int_part<nb_part; ++int_part)
        {
            std::shared_ptr<KDI::KDIChunk> chunk_stepless_partless = base->chunk();
            chunk_stepless_partless->dump();
            chunk_stepless_partless->dump();
            chunk_stepless_partless->dump();

            std::shared_ptr<KDI::KDIChunk> chunk_stepless_partless2 = base->chunk();
            chunk_stepless_partless2->dump();

            chunk_stepless_partless = base->chunk();
            chunk_stepless_partless->dump();

            chunk_stepless_partless = base->chunk();
            {
                std::vector<int> myFieldCell {0, 1};
                PyArrayObject* version = vector_to_nparray(myFieldCell, PyArray_INT);
                chunk_stepless_partless->insert("/glu/version", version);
            }
            {
                //std::string name = "MyEtude";

                //std::vector<char> vect_name {'M', 'y', 'E', 't', 'u', 'd', 'e'};
                //PyArrayObject* pmyFieldCell = vector_to_nparray(vect_name, NPY_CHAR);

                //std::vector<std::string> vect_name {"MyEtude"};
                //PyArrayObject* pmyFieldCell = vector_to_nparray(vect_name, NPY_STRING);

                //PyObject* pmyFieldCell = create_np_array(vect_name, 8);

                //chunk_stepless_partless->insert("/study/name", pmyFieldCell);
            }
            {
                //chunk_partless->insert(/study/date", "2024/06")
            }
            std::shared_ptr<KDI::KDIChunk> chunk = base->chunk(float_step, int_part);

            std::cout << ">>> .-.-.-.-.-.-. WRITE DATAS .-.-.-.-.-.-." << std::endl;
            {
                std::vector<double> cartesianCoordinates { 0. + 2 * int_part, 0. - 0.1 * float_step, 0.,
                             1. + 2 * int_part, 0. - 0.1 * float_step, 0.,
                             1. + 2 * int_part, 1. + 0.1 * float_step, 0.,
                             0. + 2 * int_part, 1. + 0.1 * float_step, 0. };
                PyArrayObject* pcartesianCoordinates = vector_to_nparray(cartesianCoordinates, PyArray_DOUBLE, 3);
                chunk->insert("/mymesh/points/cartesianCoordinates", pcartesianCoordinates);
            }
            {
                std::vector<float> myFieldCell {1.1, 2.2, 3.3, 4.4};
                PyArrayObject* pmyFieldCell = vector_to_nparray(myFieldCell, PyArray_FLOAT);
                chunk->insert("/mymesh/points/fields/myFieldPoint", pmyFieldCell);
            }
            {
                std::vector<int> types {0, 0};
                PyArrayObject* ptypes = vector_to_nparray(types, PyArray_INT);
                chunk->insert("/mymesh/cells/types", ptypes);
            }
            {
                std::vector<int> connectivity {0, 1, 2, 2, 3, 0};
                PyArrayObject* pconnectivity = vector_to_nparray(connectivity, PyArray_INT);
                chunk->insert("/mymesh/cells/connectivity", pconnectivity);
            }
            {
                std::vector<float> myFieldCell {42, 69};
                PyArrayObject* pmyFieldCell = vector_to_nparray(myFieldCell, PyArray_FLOAT);
                chunk->insert("/mymesh/cells/fields/myFieldCell", pmyFieldCell);
            }
            // chunk_stepless_partless.insert('/study/version', np.array([1, 0], dtype='i1'))


            // chunk_stepless_partless.insert('/glu/version', np.array([1, 0], dtype='i1'))

            // chunk_partless.insert('/study/date', '2024/06')
            // base->dump();


            std::cout << ">>> End " << float_step << "," << int_part << std::endl;
        }

        std::cout << ">>> End " << float_step << std::endl;

        chunk_partless->saveVTKHDF("cpp__uns_1pe_4ssd_1uns_10tps__A");

        chunk_partless->saveVTKHDFNto1("cpp__uns_1pe_4ssd_1uns_10tps__A_one");

        base = loadVTKHDF("cpp__uns_1pe_4ssd_1uns_10tps__A", false);

    }

    std::cout << ">>> End Fully (mono)" << std::endl;

    return 0;
}


int
test_one_mesh_multi_milieux(int argc, char *argv[])
{
    bool trace { false };
    int nb_part { 4 };

    std::shared_ptr<KDI::KDIBase> base = createBase("cpp__uns_1pe_4ssd_1uns_10tps__B", nb_part, trace);
    base->update("UnstructuredGrid", "/mymesh");
    base->update_fields("/mymesh/points/fields", "myFieldPoint");
    base->update_fields("/mymesh/cells/fields", "myFieldCell");
    const std::string submeshA = base->update_sub("/mymesh", "/mymilA");

    for(int int_step=0; int_step<10; ++int_step)
    {
        float float_step = float(int_step * 1.1);
        std::shared_ptr<KDI::KDIChunk> chunk_partless = base->chunk(float_step);

        for(int int_part=0; int_part<nb_part; ++int_part)
        {
            std::shared_ptr<KDI::KDIChunk> chunk_stepless_partless = base->chunk();
            // chunk_stepless_partless->dump();
            // chunk_stepless_partless->dump();
            // chunk_stepless_partless->dump();

            std::shared_ptr<KDI::KDIChunk> chunk_stepless_partless2 = base->chunk();
            // chunk_stepless_partless2->dump();

            chunk_stepless_partless = base->chunk();
            // chunk_stepless_partless->dump();

            chunk_stepless_partless = base->chunk();
            {
                std::vector<int> myFieldCell {0, 1};
                PyArrayObject* version = vector_to_nparray(myFieldCell, PyArray_INT);
                chunk_stepless_partless->insert("/glu/version", version);
            }
            {
                //std::string name = "MyEtude";

                //std::vector<char> vect_name {'M', 'y', 'E', 't', 'u', 'd', 'e'};
                //PyArrayObject* pmyFieldCell = vector_to_nparray(vect_name, NPY_CHAR);

                //std::vector<std::string> vect_name {"MyEtude"};
                //PyArrayObject* pmyFieldCell = vector_to_nparray(vect_name, NPY_STRING);

                //PyObject* pmyFieldCell = create_np_array(vect_name, 8);

                //chunk_stepless_partless->insert("/study/name", pmyFieldCell);
            }
            {
                //chunk_partless->insert(/study/date", "2024/06")
            }
            std::shared_ptr<KDI::KDIChunk> chunk = base->chunk(float_step, int_part);

            std::cout << ">>> .-.-.-.-.-.-. WRITE DATAS .-.-.-.-.-.-." << std::endl;
            {
                std::vector<long> globalIndexContinuous {
                     4 * int_part + 0,
                     4 * int_part + 1,
                     4 * int_part + 2,
                     4 * int_part + 3, };
                PyArrayObject* pglobalIndexContinuous = vector_to_nparray(globalIndexContinuous, PyArray_LONG);
                chunk->insert("/mymesh/points/globalIndexContinuous", pglobalIndexContinuous);
            }
            {
                std::vector<double> cartesianCoordinates { 0. + 2 * int_part, 0. - 0.1 * float_step, 0.,
                             1. + 2 * int_part, 0. - 0.1 * float_step, 0.,
                             1. + 2 * int_part, 1. + 0.1 * float_step, 0.,
                             0. + 2 * int_part, 1. + 0.1 * float_step, 0. };
                PyArrayObject* pcartesianCoordinates = vector_to_nparray(cartesianCoordinates, PyArray_DOUBLE, 3);
                chunk->insert("/mymesh/points/cartesianCoordinates", pcartesianCoordinates);
            }
            {
                std::vector<float> myFieldCell {1.1, 2.2, 3.3, 4.4};
                PyArrayObject* pmyFieldCell = vector_to_nparray(myFieldCell, PyArray_FLOAT);
                chunk->insert("/mymesh/points/fields/myFieldPoint", pmyFieldCell);
            }
            {
                std::vector<long> globalIndexContinuous {
                     2 * int_part + 0,
                     2 * int_part + 1, };
                PyArrayObject* pglobalIndexContinuous = vector_to_nparray(globalIndexContinuous, PyArray_LONG);
                chunk->insert("/mymesh/cells/globalIndexContinuous", pglobalIndexContinuous);
            }
            {
                std::vector<int> types {0, 0};
                PyArrayObject* ptypes = vector_to_nparray(types, PyArray_INT);
                chunk->insert("/mymesh/cells/types", ptypes);
            }
            {
                std::vector<int> connectivity {0, 1, 2, 2, 3, 0};
                PyArrayObject* pconnectivity = vector_to_nparray(connectivity, PyArray_INT);
                chunk->insert("/mymesh/cells/connectivity", pconnectivity);
            }
            {
                std::vector<float> myFieldCell {42, 69};
                PyArrayObject* pmyFieldCell = vector_to_nparray(myFieldCell, PyArray_FLOAT);
                chunk->insert("/mymesh/cells/fields/myFieldCell", pmyFieldCell);
            }
            // chunk_stepless_partless.insert('/study/version', np.array([1, 0], dtype='i1'))


            // chunk_stepless_partless.insert('/glu/version', np.array([1, 0], dtype='i1'))

            // chunk_partless.insert('/study/date', '2024/06')

            int numero = int_step % 2;
            {
                std::vector<int> selcells {numero};
                PyArrayObject* pselcells = vector_to_nparray(selcells, PyArray_INT);
                chunk->insert(submeshA+"/indexescells", pselcells);
            }

            // base->dump();

            std::cout << ">>> End " << float_step << "," << int_part << std::endl;
        }

        std::cout << ">>> End " << float_step << std::endl;

        chunk_partless->saveVTKHDF("cpp__uns_1pe_4ssd_1uns_10tps__B");

        // TODO PLANTE chunk_partless->saveVTKHDFCompute("cpp__uns_1pe_4ssd_1uns_10tps__B_multi");

        chunk_partless->saveVTKHDFNto1("cpp__uns_1pe_4ssd_1uns_10tps__B_one");

        // TODO PLANTE chunk_partless->saveVTKHDFComputeNto1("cpp__uns_1pe_4ssd_1uns_10tps__B_multi_one");

        base = loadVTKHDF("cpp__uns_1pe_4ssd_1uns_10tps__B");
    }

    std::cout << ">>> End fully (multi)" << std::endl;

    return 0;
}

int
main(int argc, char *argv[])
{
    Py_Initialize();
    import_array();

    test_one_mesh_non_milieu(argc, argv);

    test_one_mesh_multi_milieux(argc, argv);

    Py_Finalize();

    return 0;
}
