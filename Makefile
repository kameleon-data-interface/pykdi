essai: main.cxx
	rm -f essai *.o

	g++ -g -I/usr/lib/python3/dist-packages/numpy/core/include -c `/usr/bin/python3.11-config --cflags` main.cxx

	g++ -o essai main.o `/usr/bin/python3.11-config --ldflags --embed` -L/usr/lib/python3/dist-packages/numpy/core/lib -lnpymath -lm
	
	export PYTHONPATH=${PWD}/src; export KDI_DICTIONARY_PATH=${PWD}/src/pykdi/dictionary; ./essai

	export PYTHONPATH=${PWD}/src; export KDI_DICTIONARY_PATH=${PWD}/src/pykdi/dictionary; mpirun -n 4 ./essai
