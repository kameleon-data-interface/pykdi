**Auteur** : *Jacques-Bernard Lekien*, jacques-bernard.lekien@cea.fr

**Projet gitlab.com** : *pykdi*, *6 mai 2024*.

Mise à jour avec la version 0.13.0.

# KDI : **K**ameleon **D**ata **I**nterface

La plate-forme ***KDI***, ***K***ameleon ***D***ata ***I***nterface, vise à proposer une
solution pour gérer des flux de données sortant / entrant d'un code de simulation comme
d'outils de sauvegarde, d'analyse et de visualisation, dans un environnement ***HPC***.

Ce projet est avant tout un prototypage afin d'éprouver les concepts déjà été mise en oeuvre
dès 2012 dans ***PaDaWAn***, ***Pa***rallel ***Da***ta ***W***orkflow for ***An***alysis,
[PaDaWAn18](https://sc18.supercomputing.org/proceedings/workshops/workshop_files/ws_isav116s3-file1.pdf),
à travers une réécriture complète, plus moderne, toujours en Python.

Cette fois-ci, l'objectif inclut l'expérimentation de la sauvegarde au format ***VTK*** ***HDF***
(***HDF*** pour ***H***ierarchical ***D***ata ***F***ormat et une sémantique adaptée
***VTK*** pour ***V***isualization ***T***ool***k***it) toujours en cours de définition
et de développement chez Kitware.
Afin de ne pas se limiter au flux de données de résultats mais prendre de
prendre en compte aussi le flux de données entre codes / outils, on se permettera d'étendre cette
sémantique. D'autres formats ou sémantiques pourront être pris en compte par cette
plate-forme.

# Sommaire

- [Description](#description)
- [Sémantique](#semantique)
- [Contrat](#contrat)
- [Variant](#variant)
- [Agrément](#agrement)
- [Chunk](#chunk)
  - [Chunk stepless-partless](#chunksteplesspartless)
  - [Chunk step-partless](#chunksteppartless)
  - [Chunk step-part](#chunksteppart)
- [Instanciation](#instanciation)
  - [Instanciation d'un maillage](#instanciationmaillage)
  - [Instanciation d'un champ](#instanciationchamp)
- [Affectation](#affectation)
- [Enrichissement](#enrichissement)
- [Mutation](#mutation)
- [VTK HDF](#vtkhdf)
  - [Ecriture VTK HDF](#ecriturevtkhdf)
  - [Lecture VTK HDF](#lecturevtkhdf)
  - [Ecriture VTK HDF Nto1](#ecriturevtkhdfnto1)
- [Bibliographie](#bibliographie)

## <a name="#description"></a>Description

La plate-forme ***KDI*** est composée :

- d'une API générique (Python, C++) basée sur un dictionnaire de sémantique enrichissable ;
- d'une représentation interne des données sous la forme d'un dictionnaire Python ; et
- de mécanisme d'enrichississement, de manipulation, de sauvegarde, de communication, etc.

## <a name="#semantique"></a>Sémantique

La ***sémantique*** est de type ***OOP***, ***O***bject-***O***riented ***P***rogramming
paradigm, avec la définition de type simple informatique et de type complexe décrit à
travers un liste d'attributs qui sont eux-mêmes de type simple ou complexe.
La définition d'un type complexe définie une sémantique commune entres codes / outils
leur permettant d'échanger des données en ayant la compréhension de ce qu'elles représentent.

Ainsi, un maillage non structuré pourrait se décrire suivant un objet complexe _UnstructuredGrid_ :
```python
'UnstructuredGrid':
  { '/cells': 'Cells',
    '/fields': 'Fields',
    '/points': 'Points',
  },
```
composé de trois attributs :

- _/cells_ de type compelexe _Cells_ ;
- _/points_ de type complexe _Points_,
- _/fields_ de type complexe _Fields_ afin de permettre de décrire les champs globaux de valeurs de
  l'instance courante d'un maillage non structuré ; contrairement à ***Hercule***, nous n'utilisons pas
  la sémantique tableau via les _[]_ mais à travers la définition d'un type spécifique, ici
  KDIFields. La définition d'une instance de cet objet nécessite l'emploi de la
  méthode `kdi_update_fields`.

Cette description est complétée par la description d'autres objets complexes comme _Points_ :
```python
'Points':
  { '/globalIndexContinuous': 'GlobalIndexContinuous',
    '/cartesianCoordinates': 'CartesianCoordinates',
    '/fields': 'Fields',
  },
```
composé de trois attributs :

- _/globalIndexContinuous_ dont on parlera plus en détail plus tard;
- _/cartesianCoordinates_ qui est en fait un type simple équivalent à _float[][3]_ pour indiquer
  un tableau de réelles (nombres floattants) à 2 dimensions dont la seconde est fixée à 3,
- _/fields_ de type complexe _Fields_ afin de décrire les champs de valeurs associés à l'instance
  courante de points lui même associé à l'instance d'un maillage non structuré ; nous avons déjà
  décrit cet objet complexe plus haut.

On trouve aussi l'objet complexe _Cells_ pour compléter la description de l'objet
complexe _UnstructuredGrid_ :
```python
'Cells':
  { '/globalIndexContinuous': 'GlobalIndexContinuous',
    '/types': 'Types',
    '/connectivity': 'Connectivity',
    '/fields': 'Fields',
  },
```
composé de quatre attributs :

- _/globalIndexContinuous_ dont on parlera plus en détail plus tard;
- _/types_ qui est en fait un type simple équivalent à _int[]_ pour indiquer un tableau d'entiers
  à une dimension,
- _/connectivity_ aussi un type simple équivalent à _int[]_ pour indiquer un tableau d'entiers
  à une dimension,
- _/fields_ de type complexe _Fields_ afin de décrire cette fois-ci les champs de valeurs associés
  à l'instnace courante de cellules.

<span style="color: #e65b4c">
***Remarque*** : Le dictionnaire regroupera la définition des types simples dits informatiques afin
de les nommer  explicitement en fonction de leur sémantique, de leur usage, et non en fonction
de leur définition.
Ainsi, le type _Types_ et _Connectivity_ décrive le même type simple _int[]_.
</span>

<span style="color: #e65b4c">
***Remarque*** : Cette sémantique n'est pas précisément
décrite de cette façon. On construit et renseigne un champ de valeurs dans une instance de
Fields à tragvers l'emploi de la méthode kdi_update_fields.
</span>

L'emploi de la méthode `kdi_update_fields` se fait de la façon suivante :

```python
kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell').insert(value)
```

- Le premier paramètre est le dictionnaire Python qui décrit la simulation courante.
- Le second paramètre est le nom de l'instance de l'attribut _Fields_ que l'on
  souhaite renseigner ; ici c'est l'attribut `/fields` sous l'attribut '/cells'
  d'une instance de _UnstructuredGrid_ nommée `/mymesh`.
- Le troisième paramètre est le nom de l'instance du champ de valeurs. Ce nom ne
  doit pas contenir le caractère '/' qui est utilisé pour décrire une sorte
  d'arborescence entre instances.

Cet appel retourne l'instance de l'objet qu'il ne reste plus qu'à manipuler que
ce soit en écriture, comme ici via la méthode `set` en passant en paramètre la
valeur que l'on associe à ce champ de valeurs, ou que ce soit en lecture.

## <a name="#contrat"></a>Contrat

Suivant la ***sémantique*** ainsi définie, des ***instances nommées*** pourront être
instanciées suivant une logique bien déterminée afin de mettre en place une base spécifique
à un besoin. C'est ce qui définit un ***contrat***.

***Remarque*** : Cette notion de ***contrat*** s'exprime
sous la forme de la méthode `KVTKHDFCheck` qui vérifie si le contenu de la base est
sémantiquement comptabible avec l'écrivain en ***VTK HDF***.

## <a name="#variant"></a>Variant

***KDI*** comporte une sémantique commune particulière à côté de la mise en place d'une
sémantique propre à une communauté. Cette sémantique introduit la notion de ***variant***
qui permet d'indiquer suivant quelle logique une valeur associée à une ***instance nommée***
d'objet  sémantique est déclinée en plusieurs valeurs.
La définition de ces variants reste quant à elle générique.

## <a name="#agrement"></a>Agrément

La généricité peut être un frein à l'utilisation de cette plate-forme, c'est pourquoi
a été introduit la notion d'***agrément*** qui permet d'associer à un nom d'agrément
une liste prédéfinie de ***variants***. propre à un type de simulation.

Nous avons l'agrément ***KDI_AGREEMENT_STEPPART*** qui est propre aux codes
de simulation en éléments finis. Elle décrit les deux variants nécessaires dans ce
cas de figure :

- un variant temporel, ***KVTKHDF_VARIANT_STEP***, décrivant une variation suivant
  une série strictement croissante de valeurs réelles ;
```python
[KVTKHDF_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
```

- un variant lié au partitionnement, ***KVTKHDF_VARIANT_PART***, décrivant un interval
  de valeurs correspondant à l'identifiant entier de chaque partie :
```python
[KVTKHDF_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': nb_parts, }, ], ])
```

<span style="color: #e65b4c">
***Remarque*** : La description du ***KVTKHDF_VARIANT_PART***
est la même pour toute la simulation, empêchant le changement de partitionnement
sur un nombre variant de parties.
</span>

Du point de vue de l'utilisateur, la création d'une base (en mémoire) se fait
simplement à travers l'appel à :
```python
base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 1})
```
Automatiquement, les deux variants décrits plus haut seront définis.
Dans notre exemple, le nombre de partitions étant fixé à 1, nous sommes face à une base
séquentielle (***partless***).

En C++, nous avons opté pour un choix par défaut de cet agrément ce qui donne l'appel suivant :
```cpp
KDIBase* base = createBase(nb_part);
```

Il existe trois modes d'exécutions compatible avec KDI :
- un mode séquentiel pouvant décrire une ou plusieurs partitions ;
- un mode parallèle via MPI. 

A l'issu de cet appel, un ***KDIBase*** est créé.

La description mémoire correspondant à cette instruction est la suivante :
```python
{
  '': KDIComplexType('Base'),
  '/agreement': KDIMemory(base=KDIBase, key_base='/agreement', datas=np.array(['STEPPART']),),
  '/chunks': KDIChunks(base=KDIBase,
                       order=['step', 'part'],
                       variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},
                                 'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}),
  '/fields': KDIFields({}), '': KDIComplexType('Base'),
  '/glu': KDIComplexType('Glu'),
  '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', datas={},),
  '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', datas={},),
  '/study': KDIComplexType('Study'),
  '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', datas={},),
  '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', datas={},),
  '/vtkhdf': KDIComplexType('VTKHDF'),
  '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', datas=np.array([2, 0]),),
  '/vtkhdf/force_common_data_offsets': True,
  '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8),
  '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"),
}
```
C'est le résultat obtenu en réalisant une sauvegarde JSON à travers la commande :
```python
saveJSON(base, 'myKDIBase.json')
```

Dans les faits, cette commande n'est jamais directement appelée car l'appel est inclus à saveVTKHDF.

La création d'une base inclut la création d'une instance du type complexe ***Base***
(il ne peut y en avoir qu'une dans une base). Cela inclut la création d'attributs
de type complexe ***/glu*** (***Glu***), ***/fields*** (***Fields***) et
***/study*** (***Study***) qui restent à renseigner, les autres l'étant automatiquement
lors de l'appel à la méthode de création d'une base.

Le choix de cet agrément défini aussi une valeur par défaut pour les conversions
d'une énumération du type de cellules du code écrivain vers celle de KDI à travers
***/code2kdi***. Elle se fait à travers la définition d'un tableau dimensionné à la
valeur d'énumération (+1) la plus élevé décrite par le code. Si l'on considère que
la valeur 0 venant du code décrit un triangle alors on positionne en index 0 de ce
tableau la valeur de KDI_TRIANGLE ; resp. 1 pour un quadrangle KDI_QUADRANGLE et
resp. 12, ici la valeur la plus élevé que peut fournir le code, pour décrire un
hexahédron KDI_HEXAHEDRON. Les autres valeurs du tableau non associées à un type
de cellules sont positionnés à KDI_PIT.
Il se trouve qu'ici, on considère que le code à la même définition de l'énumération
de types de cellules que KDI (d'où le 0 en 0, le 1 en 1 et le 12 en 12, le reste
étant à 13).

<span style="color: #e65b4c">
***Remarque*** : Seules les types de cellules KDI_TRIANGLE,
KDI_QUADRANGLE et KDI_HEXAHEDRON sont définis. L'énumération peut bien entendu
être étendu dans KDI. A ne pas oublier aussi d'étendre l'association du nombre
de points associés à un type de cellule KDI ainsi que la correspondance
entre une énumération de types de cellules KDI vers celle de VTK, toutes les
valeurs d'énumération VTK ayant déjà été défini.
</span>

De plus, comme cet agrément est volontairement compatible avec l'écriture d'une base
VTK HDF, l'objet de type complexe ***VTKHDF*** est automatiquement instancié sous
la dénomination ***/vtkhdf*** ainsi que son attribut ***/vtkhdf/version*** qui est
automatiquement positionné aux valeurs ***[2, 0]*** correspondant
à la description sémantique en cours.

## <a name="#chunk"></a>Chunk

Comme nous venons de le voir, le positionnement d'un agrément définit une liste de variants.

Afin d'associer une valeur à une donnée, il faut au préalable décrire le ***chunk*** courant,
c'est à dire la partie de la simulation que l'on souhaite renseigner, et ceci en associant
une valeur à chacun des variants. 

En C++, cela se fait en créant un ***KDIChunk*** depuis une instance de ***KDIBase*** via
la méthode ***chunk***.
Par choix, l'API en C++ est dans le cas d'usage de l'agrément KDI_AGREEMENT_STEPPART, c'est
pourquoi cette méthode peut prendre en paramètre de 0 à 2 valeurs.
En fonction de ce que l'on renseigne, en C++ :
- les valeurs pour les deux variants, le mode est dit ***step-part*** :
```cpp
KDIChunk* chunk(float _vstep, int _ipart)
```
- uniquement la valeur du temps de simulation, le mode est dit ***step-partless***, et
```cpp
KDIChunk* chunk(float _vstep)
```
- juste un dictionnaire vide, le mode est dit ***stepless-partless***.
```cpp
KDIChunk* chunk()
```

En Python, le mode est assez similaire à partir du moment que l'on a défini
le même agrément KDI_AGREEMENT_STEPPART lors de la création de la base.
Il ne reste plus alors qu'à renseigner les informations relatives à la description d'un chunk
via non plus une méthode mais une des instructions suivantes.
En fonction de ce que l'on renseigne, en Python:
- les valeurs pour les deux variants, le mode est dit ***step-part*** (voir plus haut) ;
```python
base['/chunks'].set({'step': float_step, 'part': issd})
```
- uniquement la valeur du temps de simulation, le mode est dit ***step-partless***, et
```python
base['/chunks'].set({'step': float_step})
```
- juste un dictionnaire vide, le mode est dit ***stepless-partless***.
```python
base['/chunks'].set({})
```

### <a name="#chunksteplesspartless"></a>Chunk stepless-partless

Le mode est ***stepless-partless***, c'est-à-dire intemporel et indépendant de la partie,
si on ne passe aucune valeur à l'appel de cette méthode.
C'est utile, par exemple, pour décrire :
- la version de la glu (`/glu/version`),
- le nom de l'étude (`/study/name`),
- comme un champ global à la simulation (dans `/fields`).

### <a name="#chunksteppartless"></a>Chunk step-partless

Le mode est ***step-partless***, c'est-à-dire dépendant du temps mais pas de la partie de
la simulation, si on ne passe que la valeur du temps de la simulation à cette méthode.
C'est utile, par exemple, pour décrire :
- la date d'écriture (`/study/date`),
- comme un champ global à la simulation (dans `/fields`).

### <a name="#chunksteppart"></a>Chunk step-part

Le mode est ***step-part***, c'est-à-dire dépendant du temps et de la partie de la
simulation, si on passe la valeur du temps et le numéro de la partie.
Le numéro de la partie doit être compris en 0 et le nombre de partitions réduit de 1
qui a été défini lors de la création de la base.
C'est bien évidemment ce qui est le plus employé.

## <a name="#instanciation"></a>Instanciation

Comme nous l'avons, la création d'une base se traduite par la création de plusieurs
éléments dont une instance de l'objet ***Base***, et par récurrence de ses attributs.

Par la suite, l'utilisateur peut ensuite compléter sa base en fonction de ses propres
objectifs.

Nous pouvons construire une ou plusieurs instances :
- de maillage non structuré, ***UnstructuredGrid*** avec des sous-maillage (milieu, matériau)
  à travers la définition d'une instance de ***SubUnstructuredGrid*** ; et
- de champ de valeurs.

### <a name="#instanciationmaillage"></a>Instanciation d'un maillage

L'instanciation d'un maillage se fait, en C++ :
```cpp
base->update("UnstructuredGrid", "/mymesh");
```
en Python :
```python
kdi_update(base, 'UnstructuredGrid', '/mymesh')
```

<span style="color: #e65b4c">
Le nom doit commencer par un ***/*** (backslash) et
ensuite ne plus en comporter. La partie restante sera la dénomination de ce
maillage.
</span>

Cet objet comprenant des attributs, ils seront instancés en même temps.

Dans le cas de la définition d'un sous-maillage (décrivant un milieu ou matériau),
on procéde de façon similaire sans omettre de décrire sur quel maillage existant
se fait la sélection. Cette description étant ***stepless-partless***
(commune durant toute la simulation), en C++ :
```cpp
const std::string submeshA = base->update_sub("/mymesh", "/mymilA");
```
en Python :
```python
str_my_milA = kdi_update_sub(base, '/mymesh', '/mymilA')
```

La base peut comporter plusieurs maillages non structurés dont sur certain des milieux ont
été définis.

### <a name="#instanciationchamp"></a>Instanciation d'un champ

L'instanciation d'un champ de valeurs nécessite l'emploi d'une méthode particulière, en C++ :
```cpp
base->update_fields("/mymesh/cells/fields", "myFieldCell");
```
en Python :
```python
kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell')
```
A priori, cet appel n'est qu'à faire lors de la création de la base mais néanmoins il
peut être réalisé autant de fois que l'on souhaite sans plus de conséquence. 

## <a name="#affectation"></a>Affectation

Seuls les attributs de type informatique sont affectable par une valeur.
Cela nécessite de préciser le ***chunk*** sur lequel on souhaite agir.

L'affectation d'une valeur se fait en C++ :
```cpp
KDIChunk* chunk = base->chunk(float_step, int_part);

std::vector<float> myFieldCell {1.1, 2.2, 3.3, 2.2};
PyArrayObject* pmyFieldCell = vector_to_nparray(myFieldCell, PyArray_FLOAT);
chunk->set("/mymesh/cells/fields/myFieldCell", pmyFieldCell);
```
en Python

```python
base['/chunks'].set({'step': float_step, 'part': issd})
base['/mymesh/cells/fields/myFieldCell'].insert(np.array([1.1, 2.2, 3.3, 2.2]))
```
ce qui positionne le chunk par défaut pour tout Set ou Get dans la base,
ou encore

```python
base['/mymesh/cells/fields/myFieldCell'].insert(
    np.array([1.1, 2.2, 3.3, 2.2]),
    {'step': float_step, 'part': issd})
```
pour une définition de chunk relative qu'à cette instance.

## <a name="#enrichissement"></a>Enrichissement

L'***enrichissement*** consiste à rajouter un nouveau champ par rapport à d'autres.

Un attribut ***/offsets*** est rajouté aux cellules d'un maillage non structuré
lors de l'écriture en ***VTK HDF***. La valeur est calculée par rapport à l'attribut ***/types***.

Ce calcul est réalisé à la demande lors d'un appel à la méthode ***Get*** de l'objet,
en mode paresseux (***lazy***) à travers cette description :

```python
command_eval_offsets = """
base = Base
code2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    nbPoints = code2nbPoints[typesCode.view(dtype=np.int8)]
else:
    nbPoints = code2nbPoints[typesCode]
Ret = np.empty((len(nbPoints) + 1), dtype=np.int64)
Ret[0] = 0
np.cumsum(nbPoints, out=Ret[1:])
"""


def kdi_update_eval_offsets(base: dict[str, any], abs_path_cells: str):
    if abs_path_cells + '/offsets' in base:
        if command_eval_offsets != base[abs_path_cells + '/offsets'].lines:
            raise KDIException('pre: command already exist but it\'s different!')
        return base
    base[abs_path_cells + '/offsets'] = KDIEvalString(base, abs_path_cells + '/offsets', command_eval_offsets,
                                                      [base['/code2nbPoints'], abs_path_cells + '/types'])
```
Cela se traduit dans le dictionnaire sous la forme suivante :
```python
"/mymesh/cells/offsets":  "KDIEvalBase64(base=KDIBase,
                                         key_base='/mymesh/cells/offsets',
                                         lines='CmJhc[...]]sxOl0pCg==',
                                         params=[array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8),
                                                 '/mymesh/cells/types'])"
```

En Python, le développeur doit appeler l'instruction suivante explicitement dans son code :
```python
kdi_update_eval_offsets(base, '/mymesh/cells')
```

En C++, il en est quelque peu autrement car à la création d'un `UnstructuredGrid` via la méthode
`update`, cette instruction est directement appelée. Cela est dû au fait que le C++ n'appelle
pas directement les méthodes des objets de `base` mais une encapsulation de type d'objet
`KDIAgreementStepPartBase` qui permet de gérer la base en considérant qu'elle est dans le cadre
d'un agrément StepPartBase compatible avec ***VTKHDF***.
Bien sûr, en Python, on pourrait aussi créer directement un `KDIAgreementStepPartBase` au lieu
de passer par la création d'une base.

Comme on a pu le constater à travers le rendu dans le dictionnaire qui correspond à un
affichage de la description, cette façon de faire est sérialisable. Cela veut dire
que ***KDIEvalString*** est compatible à une sauvegarde en JSON.

<span style="color: #e65b4c">
***Remarque*** : Ce n'est pas le cas de KDIEvalMethod qui consiste à passer
une méthode Python.
D'une façon plus générale, ce n'est pas le cas non plus si l'instance d'un
KDIEvalString comporte des paramètres non sérialisables.
Nous verrons plus tard que l'application de la mutation via la fonction
KDIComputeMultiMilieux produit une description de base non sériablisable.
</span>

Le choix du Python est d'ailleurs directement lié aux facilités de ce langage pour
définir une opération d'enrichissement de la base, langage qui ne nécessite pas
de compilation et promets des performances correctes dans une certaine mesure avec
l'utilisation de modules de calculs comme numpy.

## <a name="#mutation"></a>Mutation

Contrairement à l'***enrichissement***, la ***mutation*** remet en cause une partie
de la description des données.

Cela arrive tout particulièrement :
- pour changer de description, comme par exemple, pour passer d'une description
  comportant des ***SubUnstructuredGrid*** vers des ***UnstructuredGrid*** ;
- pour passer d'une description de partitions vers une unique partition ou un
  nombre différent de partitions qu'à l'origine ;
- pour appliquer une projection sur un autre maillage ;
- pour changer de système d'unité ; etc.

C'est ce que nous faisons lorsqu'on veut transformer une description de
type ***SubUnstructuredGrid*** vers une description explicite de
***UnstructuredGrid***.

Pour cela, nous avons défini une méthode ***KDIComputeMultiMilieux*** qui réalise ce
travail à partir d'une base pour en produire une nouvelle. Toute la subtilité,
mais aussi toute la difficulté, c'est de réaliser ce travail en mode paresseux (***lazy***),
c'est à dire lors de la demande à travers la méthode ***Get***, et tout en évitant
de refaire plusieurs fois les mêmes opérations ce qui nécessite la mise en place
d'un mécanisme de cache.

Sans rentrer plus dans les détails de cette méthode, il faut juste comprendre :
- qu'afin de conserver des informations déjà dans l'ancienne base, les clefs du
  dictionnaire de la base concernés seront encapsulés par ***KDIProxy*** permettant
  d'accéder aux valeurs d'une donnée de l'ancienne base depuis la nouvelle ;
- qu'il faut rajouter des nouvelles clefs, généralement des ***KDIEvalString*** qui
  à la demande appliqueront un programme pour calculer la valeur de la nouvelle clef ;
  l'emploi de ***KDIEvent*** permet d'exploiter un des résultats d'un ***KDIEvalString***
  intermédiaire qui en produiraient plusieurs.

<span style="color: #e65b4c">
***Remarque*** : De façon générale, la base ainsi mutée ne peut pas être soumise
à la sauvegarde JSON car elle est de par sa nature enclin à utiliser des
***KDIProxy*** référençant au moins une autre base.
</span>

Afin de détecter une mutation, la mutation doit positionner dans la clef
`/before_write` la description pour appliquer cette mutation.
Cela permet d'éviter de tenter d'écrire un fichier JSON mais aussi de 
savoir quelle mutation est demandé pour réaliser la sortie.
La sauvegarde d'un JSON après une mutation peut être réalisée mais sur la
base avant mutation et en exploitant pour les prochaines lectures cette
description `/before_write`.

## <a name="#vtkhdf"></a>VTK HDF

La sémantique ***VTK*** dans le format de stockage ***HDF*** proposée par Kitware
nécessite de maîtriser ***HDF***, de maîtriser la sémantique décrite par ***VTK***
est tout particulièrement la façon de stocker les données temporelles et partionnées.

Concernant les contraintes relatifs à ce ***VTK HDF***, je vous joins à regarder le VTKHDF.md.

### <a name="#ecriturevtkhdf"></a>Ecriture VTK HDF

La sauvegarde en ***VTK HDF*** nécessite plusieurs conditions dont :
- être dans le bon agrément, en l'occurence KDI_AGREEMENT_STEPPART,
- comporter un maillage ***UnstructuredGrid*** avec ou sans plusieurs sous-maillages.

A savoir que ce n'est pas le KDI_AGREEMENT_STEPPART qui est vérifié mais la
description dans la base qui en découle.

Tout ce que l'écrivain ne comprend pas, il l'ignorera.
Ainsi, la description d'un sous-maillage sera ignoré lors de cette phase d'écriture.
Pour prendre en compte ces sous-maillages, il est indispensable d'appeler la mutation
***KDIComputeMultiMilieux***.

L'écrivain, ***saveVTKHDF***, ne fait pas qu'écrire une base ***VTK HDF***... il modifie
aussi la représentation de la base afin de permettre l'accès aux données qui seraient alors
stockées.

Une sauvegarde automatique en JSON est réalisée sans dans le cas de l'application
de la mutation ***KDIComputeMulitMilieux*** qui rend cela impossible.

En C++, l'écriture se fait à travers la méthode ***saveVTKHDF*** disponible dans ***KDIChunk***
pour une description de ***chunk*** de type ***step-partless*** :
```cpp
KDIChunk* chunk_step_partless = base->chunk(float_step);
chunk_step_partless->saveVTKHDF("myBase");
```
en Python, cela se fait de cette façon :
```python
base['/chunks'].set({KVTKHDF_VARIANT_STEP: float_step})
saveVTKHDF(base, 'myBase.vtkhdf')
```

En C++, il existe aussi la méthode ***saveVTKHDFCompute*** du ***KDIAgreementStepPartChunk***
qui inclut l'application de la mutation ***KDIComputeMultiMilieux*** alors que ce n'est pas
le cas en Python où il faut explicitement l'appeler.
On a le même comportement qu'en C++, si au lieu de créer une base, on a crée un ***KDIAgreementStepPartBase***.

Dans le cas d'une simulation avec des sous-maillages, l'application en C++ du ***saveVTKHDF***
peut être vu comme une base de reprise/protection alors que le ***saveVTKHDFCompute*** comme une
base de dépouillement.

Une base ***VTK HDF*** est écrite à partir des données dont la sémantique correspond à VTK.
On peut vérifier ce qu'il en est avec la méthode ***KDIVTKHDFCheck***.

Ensuite, la description de la base est modifiée afin de permettre l'accès aux données stockées en mode lecture.

### <a name="#lecturevtkhdf"></a>Lecture VTK HDF

La lecture peut se faire :
- soit directement sur le fichier ***VTK HDF***, comme peut le faire
  le lecteur VTK HDF de ParaView ;
- soit en lisant le fichier JSON produit en même temps.

En C++, la lecture se fait à travers la commande :
```cpp
KDIBase base = createBase("myBase");
```
alors qu'en Python :
```python
base = loadJSON('myBase.json')
```

L'avantage de charger le fichier JSON c'est de vous permettre de conserver les données décrites
mais non sauvegardées en VTK HDF.

Bien sûr, faut-il que lors de l'écriture de la base en JSON, cette base puisse être
sérialisable (donc avant application d'une mutation comme ***KDIComputeMulitMilieux***).

### <a name="#ecriturevtkhdfnto1"></a>Ecriture VTK HDF Nto1

La sauvegarde en ***VTK HDF Nto1*** nécessite les mêmes conditions que celle en ***VTK HDF***.

Elle n'est applicable qu'en mode d'exécution parallèle distribuée et permet de
transformer un maillage décrit sous la forme de plusieurs partitions,
une par processus MPI, en un maillage mono-partition / non distribué.

Pour cela, ce service s'aide d'une information fournit par le code les `/globalIndexContinuous`
sur les cellules comme sur les points du maillage principal non structuré. Elle consiste
à indexer toutes les cellules (resp. points) de façon unique à travers la simulation
distribuée et suivant une indexation allant de 0 à N-1 (N étant le nombre de) de façon
continue.

L'intérêt que cette indexation soit fournit par le code permet de définir la façon doit
être décrit le maillage mono-partition en sortie et cela de façon identique au cours des temps de
simulation écrit.

Un rééquilibrage de la distribution des cellules peut être réalisé au cours de la simulation
du moment que cet index soit défini afin de produire toujours le même maillage mono-partition
en sortie.

Du fait des sous-maillages, la sortie n'impose pas que d'un temps sur l'autre, le maillage
mais surtout les sous-maillages produits soient identiques au cours du temps.

## <a name="#bibliographie"></a>Bibliographie

[PaDaWAn18] 
[*PaDaWAn: a Python Infrastructure for Loosely Coupled In Situ Workflows*](https://sc18.supercomputing.org/proceedings/workshops/workshop_files/ws_isav116s3-file1.pdf),
Julien Capul, Sébastien Morais, Jacques-Bernard Lekien, SC18, ISAV 2018, *Honorable Mention*
