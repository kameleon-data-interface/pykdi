# state file generated using paraview version 5.13.2-1031-g4134b75421
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.Set(
    ViewSize=[486, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.Set(
    ViewSize=[484, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.Set(
    ViewSize=[483, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.Set(
    ViewSize=[241, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView5 = CreateView('RenderView')
renderView5.Set(
    ViewSize=[240, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView6 = CreateView('RenderView')
renderView6.Set(
    ViewSize=[240, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView7 = CreateView('RenderView')
renderView7.Set(
    ViewSize=[241, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView8 = CreateView('RenderView')
renderView8.Set(
    ViewSize=[240, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView9 = CreateView('RenderView')
renderView9.Set(
    ViewSize=[239, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.23606797749979,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.SplitHorizontal(0, 0.333333)
layout1.SplitVertical(1, 0.500000)
layout1.AssignView(3, renderView1)
layout1.SplitHorizontal(4, 0.500000)
layout1.AssignView(9, renderView4)
layout1.AssignView(10, renderView7)
layout1.SplitHorizontal(2, 0.500000)
layout1.SplitVertical(5, 0.500000)
layout1.AssignView(11, renderView2)
layout1.SplitHorizontal(12, 0.500000)
layout1.AssignView(25, renderView5)
layout1.AssignView(26, renderView8)
layout1.SplitVertical(6, 0.500000)
layout1.AssignView(13, renderView3)
layout1.SplitHorizontal(14, 0.500000)
layout1.AssignView(29, renderView6)
layout1.AssignView(30, renderView9)
layout1.SetSize(1455, 960)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView9)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Annotate Time'
glob_myGIC = AnnotateTime(registrationName='glob_myGIC')
glob_myGIC.Set(
    Format='glob_myGIC',
)

# create a new 'Annotate Time'
mymesh = AnnotateTime(registrationName='mymesh')
mymesh.Set(
    Format='mymesh',
)

# create a new 'Annotate Time'
mymil1 = AnnotateTime(registrationName='mymil1')
mymil1.Set(
    Format='mymil1',
)

# create a new 'Annotate Time'
basename = AnnotateTime(registrationName='basename')
basename.Set(
    Format='3pe_1uns_2subuns_3ssd_10tps__multi__PARA',
)

# create a new 'Annotate Time'
glob_myFieldPoint = AnnotateTime(registrationName='glob_myFieldPoint')
glob_myFieldPoint.Set(
    Format='glob_myFieldPoint',
)

# create a new 'VTKHDF Reader'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf = VTKHDFReader(registrationName='3pe_1uns_2subuns_3ssd_10tps__multi__PARA.vtkhdf', FileName=['/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/3pe_1uns_2subuns_3ssd_10tps__multi__PARA.vtkhdf'])
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf.Set(
    CellArrayStatus=['glob_OriginalPartId', 'glob_PartId', 'glob_myFieldCell', 'glob_myFieldCell2', 'glob_myGIC', 'glob_mySSD', 'mylocFieldCell', 'OriginalPartId', 'mylocFieldCell3', 'mylocFieldCell4'],
    PointArrayStatus=['glob_OriginalPartId', 'glob_myFieldPoint'],
)

# create a new 'Contour'
contour1525 = Contour(registrationName='contour 1.5 2.5', Input=a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf)
contour1525.Set(
    ContourBy=['POINTS', 'glob_myFieldPoint'],
    Isosurfaces=[1.5, 2.5],
    PointMergeMethod='Uniform Binning',
)

# create a new 'Annotate Time'
glob_PartId = AnnotateTime(registrationName='glob_PartId')
glob_PartId.Set(
    Format='glob_PartId',
)

# create a new 'Annotate Time'
mymil2 = AnnotateTime(registrationName='mymil2')
mymil2.Set(
    Format='mymil2',
)

# create a new 'Annotate Time'
iso1525 = AnnotateTime(registrationName='iso 1.5 2.5')
iso1525.Set(
    Format='iso (1.5 ; 2.5)',
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from mymesh
mymeshDisplay = Show(mymesh, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
mymeshDisplay.Set(
    WindowLocation='Lower Right Corner',
)

# show data from glob_myFieldPoint
glob_myFieldPointDisplay = Show(glob_myFieldPoint, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
glob_myFieldPointDisplay.Set(
    WindowLocation='Lower Left Corner',
)

# show data from basename
basenameDisplay = Show(basename, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
basenameDisplay.Set(
    WindowLocation='Upper Center',
)

# show data from contour1525
contour1525Display = Show(contour1525, renderView1, 'GeometryRepresentation')

# get 2D transfer function for 'glob_myFieldPoint'
glob_myFieldPointTF2D = GetTransferFunction2D('glob_myFieldPoint')

# get color transfer function/color map for 'glob_myFieldPoint'
glob_myFieldPointLUT = GetColorTransferFunction('glob_myFieldPoint')
glob_myFieldPointLUT.Set(
    TransferFunction2D=glob_myFieldPointTF2D,
    RGBPoints=[1.0, 0.0564, 0.0564, 0.47, 1.343184, 0.243, 0.46035, 0.81, 1.5969820000000001, 0.356814, 0.745025, 0.954368, 1.864258, 0.6882, 0.93, 0.91791, 2.0, 0.899496, 0.944646, 0.768657, 2.1764520000000003, 0.957108, 0.833819, 0.508916, 2.4122820000000003, 0.927521, 0.621439, 0.315357, 2.69528, 0.8, 0.352, 0.16, 3.0, 0.59, 0.0767, 0.119475],
    ScalarRangeInitialized=1.0,
)

# trace defaults for the display properties.
contour1525Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymesh'],
    SelectOrientationVectors='None',
    ScaleFactor=0.25,
    SelectScaleArray='glob_myFieldPoint',
    GlyphType='Arrow',
    GlyphTableIndexArray='glob_myFieldPoint',
    GaussianRadius=0.0125,
    SetScaleArray=['POINTS', 'glob_myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour1525Display.ScaleTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour1525Display.OpacityTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# show data from iso1525
iso1525Display = Show(iso1525, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
iso1525Display.Set(
    WindowLocation='Any Location',
    Position=[0.5174712643678162, 0.4370046082949308],
)

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView1, 'UnstructuredGridRepresentation')

# get opacity transfer function/opacity map for 'glob_myFieldPoint'
glob_myFieldPointPWF = GetOpacityTransferFunction('glob_myFieldPoint')
glob_myFieldPointPWF.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
    ScalarRangeInitialized=1,
)

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay.Set(
    Representation='Surface With Edges',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymesh'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myFieldPointPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView2'
# ----------------------------------------------------------------

# show data from mymil1
mymil1Display = Show(mymil1, renderView2, 'TextSourceRepresentation')

# trace defaults for the display properties.
mymil1Display.Set(
    WindowLocation='Lower Right Corner',
)

# show data from contour1525
contour1525Display_1 = Show(contour1525, renderView2, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1525Display_1.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil1'],
    SelectOrientationVectors='None',
    ScaleFactor=0.25,
    SelectScaleArray='glob_myFieldPoint',
    GlyphType='Arrow',
    GlyphTableIndexArray='glob_myFieldPoint',
    GaussianRadius=0.0125,
    SetScaleArray=['POINTS', 'glob_myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour1525Display_1.ScaleTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour1525Display_1.OpacityTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_1 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView2, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_1.Set(
    Representation='Surface With Edges',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil1'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myFieldPointPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_1.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_1.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView3'
# ----------------------------------------------------------------

# show data from mymil2
mymil2Display = Show(mymil2, renderView3, 'TextSourceRepresentation')

# trace defaults for the display properties.
mymil2Display.Set(
    WindowLocation='Lower Right Corner',
)

# show data from contour1525
contour1525Display_2 = Show(contour1525, renderView3, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1525Display_2.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil2'],
    SelectOrientationVectors='None',
    ScaleFactor=0.25,
    SelectScaleArray='glob_myFieldPoint',
    GlyphType='Arrow',
    GlyphTableIndexArray='glob_myFieldPoint',
    GaussianRadius=0.0125,
    SetScaleArray=['POINTS', 'glob_myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour1525Display_2.ScaleTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour1525Display_2.OpacityTransferFunction.Set(
    Points=[1.5, 0.0, 0.5, 0.0, 2.5, 1.0, 0.5, 0.0],
)

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_2 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView3, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_2.Set(
    Representation='Surface With Edges',
    ColorArrayName=['POINTS', 'glob_myFieldPoint'],
    LookupTable=glob_myFieldPointLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil2'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myFieldPointPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_2.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_2.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView4'
# ----------------------------------------------------------------

# show data from glob_myGIC
glob_myGICDisplay = Show(glob_myGIC, renderView4, 'TextSourceRepresentation')

# trace defaults for the display properties.
glob_myGICDisplay.Set(
    WindowLocation='Lower Left Corner',
)

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_3 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView4, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'glob_myGIC'
glob_myGICTF2D = GetTransferFunction2D('glob_myGIC')

# get color transfer function/color map for 'glob_myGIC'
glob_myGICLUT = GetColorTransferFunction('glob_myGIC')
glob_myGICLUT.Set(
    TransferFunction2D=glob_myGICTF2D,
    RGBPoints=[0.0, 0.0564, 0.0564, 0.47, 1.029552, 0.243, 0.46035, 0.81, 1.790946, 0.356814, 0.745025, 0.954368, 2.592774, 0.6882, 0.93, 0.91791, 3.0, 0.899496, 0.944646, 0.768657, 3.529356, 0.957108, 0.833819, 0.508916, 4.236846, 0.927521, 0.621439, 0.315357, 5.085839999999999, 0.8, 0.352, 0.16, 6.0, 0.59, 0.0767, 0.119475],
    ScalarRangeInitialized=1.0,
)

# get opacity transfer function/opacity map for 'glob_myGIC'
glob_myGICPWF = GetOpacityTransferFunction('glob_myGIC')
glob_myGICPWF.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 6.0, 1.0, 0.5, 0.0],
    ScalarRangeInitialized=1,
)

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_3.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_myGIC'],
    LookupTable=glob_myGICLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymesh'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myGICPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_3.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_3.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView5'
# ----------------------------------------------------------------

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_4 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView5, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_4.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_myGIC'],
    LookupTable=glob_myGICLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil1'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myGICPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_4.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_4.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView6'
# ----------------------------------------------------------------

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_5 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView6, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_5.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_myGIC'],
    LookupTable=glob_myGICLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil2'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_myGICPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_5.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_5.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView7'
# ----------------------------------------------------------------

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_6 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView7, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'glob_PartId'
glob_PartIdTF2D = GetTransferFunction2D('glob_PartId')

# get color transfer function/color map for 'glob_PartId'
glob_PartIdLUT = GetColorTransferFunction('glob_PartId')
glob_PartIdLUT.Set(
    TransferFunction2D=glob_PartIdTF2D,
    RGBPoints=[0.0, 0.0564, 0.0564, 0.47, 0.343184, 0.243, 0.46035, 0.81, 0.596982, 0.356814, 0.745025, 0.954368, 0.864258, 0.6882, 0.93, 0.91791, 1.0, 0.899496, 0.944646, 0.768657, 1.176452, 0.957108, 0.833819, 0.508916, 1.412282, 0.927521, 0.621439, 0.315357, 1.69528, 0.8, 0.352, 0.16, 2.0, 0.59, 0.0767, 0.119475],
    ScalarRangeInitialized=1.0,
)

# get opacity transfer function/opacity map for 'glob_PartId'
glob_PartIdPWF = GetOpacityTransferFunction('glob_PartId')
glob_PartIdPWF.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
    ScalarRangeInitialized=1,
)

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_6.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_PartId'],
    LookupTable=glob_PartIdLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymesh'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_PartIdPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_6.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_6.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# show data from glob_PartId
glob_PartIdDisplay = Show(glob_PartId, renderView7, 'TextSourceRepresentation')

# trace defaults for the display properties.
glob_PartIdDisplay.Set(
    WindowLocation='Lower Left Corner',
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView8'
# ----------------------------------------------------------------

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_7 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView8, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_7.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_PartId'],
    LookupTable=glob_PartIdLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil1'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_PartIdPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_7.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_7.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView9'
# ----------------------------------------------------------------

# show data from a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_8 = Show(a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdf, renderView9, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_8.Set(
    Representation='Surface With Edges',
    ColorArrayName=['CELLS', 'glob_PartId'],
    LookupTable=glob_PartIdLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='glob_OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    BlockSelectors=['/Root/mymesh/mymil2'],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'glob_OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'glob_OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=glob_PartIdPWF,
    ScalarOpacityUnitDistance=1.8555485381399173,
    OpacityArrayName=['POINTS', 'glob_OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_8.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_2subuns_3ssd_10tps__multi__PARAvtkhdfDisplay_8.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup color maps and opacity maps used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup animation scene, tracks and keyframes
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get time animation track
timeAnimationCue1 = GetTimeTrack()

# initialize the animation scene

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# initialize the timekeeper

# initialize the animation track

# get animation scene
animationScene1 = GetAnimationScene()

# initialize the animation scene
animationScene1.Set(
    ViewModules=[renderView1, renderView2, renderView3, renderView4, renderView5, renderView6, renderView7, renderView8, renderView9],
    Cues=timeAnimationCue1,
    AnimationTime=0.0,
)

# ----------------------------------------------------------------
# restore active source
SetActiveSource(contour1525)
# ----------------------------------------------------------------


##--------------------------------------------
## You may need to add some code at the end of this python script depending on your usage, eg:
#
## Render all views to see them appears
# RenderAllViews()
#
## Interact with the view, usefull when running from pvpython
# Interact()
#
## Save a screenshot of the active view
# SaveScreenshot("path/to/screenshot.png")
#
## Save a screenshot of a layout (multiple splitted view)
# SaveScreenshot("path/to/screenshot.png", GetLayout())
#
## Save all "Extractors" from the pipeline browser
# SaveExtracts()
#
## Save a animation of the current active view
# SaveAnimation()
#
## Please refer to the documentation of paraview.simple
## https://www.paraview.org/paraview-docs/latest/python/paraview.simple.html
##--------------------------------------------