"""
Compares the values of an array of an HDF5 dataset with a pattern of expected values.

python3 compare_array_hdf5.py
    3pe_1uns_3ssd_10tps__one.vtkhdf
    "/VTKHDF/PointData/OriginalPartId"
    "[2, 2, [0, 2], [0, 1, 2], 1, [0, 2], [0, 1, 2], 1, 0, 0]"
has effect of:
- open HDF5 file
- access the values of the dataset named as a parameter
- cyclically compares each of the values in the dataset with the pattern sequence, here:
  dset[0] == 2 ?
  dset[1] == 2 ?
  dset[2] in [0, 2] ?
  dset[3] in [0, 1, 3] ?
  etc
  dset[10] == 2 ? (again the first value in this pattern)
  etc
"""
import sys
import h5py

h5_file = sys.argv[1]
hdf5_dataset_name = sys.argv[2]
pattern_values = sys.argv[3]
pattern_values = eval(pattern_values)
failed = 0
with h5py.File(h5_file, 'r') as h5_fd:
    values = h5_fd[hdf5_dataset_name]
    pattern_offset = 0
    for pos, val in enumerate(values):
        if pattern_offset == len(pattern_values):
            pattern_offset = 0
        if isinstance(pattern_values[pattern_offset], list):
            if val not in pattern_values[pattern_offset]:
                print('pos#:', pos, 'value:', val, 'expected pattern:', pattern_values[pattern_offset], 'FAILED', 'off#', pattern_offset)
                failed += 1
        elif val != pattern_values[pattern_offset]:
            print('pos#:', pos, 'value:', val, 'expected pattern:', pattern_values[pattern_offset], 'FAILED', 'off#', pattern_offset)
            failed += 1
        pattern_offset += 1
if failed != 0:
    print('FAILED COMPARE')
    exit(1)
