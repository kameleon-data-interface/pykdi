# Intégration de Python en C/C++
Embedding Python in C/C++

L'objet de cet article est de décrire comment intégrer des modules Python
dans des applications C/C++ à l'aide de l'API Python/C.

## Introduction

Cet article s'inspire de « Embedding Python in Multi-Threaded C/C++ Applications »
(Linux Journal)
, j'ai ressenti le besoin d'une couverture plus complète sur le sujet de l'intégration de Python. En écrivant cet article, j’avais deux objectifs :

    Ceci est écrit pour les programmeurs plus expérimentés en C/C++ qu'en Python, le tutoriel adopte une approche pratique et omet toutes les discussions théoriques.
    Essayez de maintenir la compatibilité multiplateforme de Python lors de l'écriture du code d'intégration. 

Maintenant, vous disposez de modules écrits en Python par d’autres et vous souhaitez les utiliser. Vous êtes expérimenté en C/C++, mais relativement nouveau en Python. Vous vous demandez peut-être si vous pouvez trouver un outil pour les convertir en code C, comme la conversion depuis FORTRAN. La réponse est non. Certains outils peuvent vous aider à générer l'exécutable à partir d'un module Python. Le problème est-il résolu ? Non. La conversion du code en exécutables rend généralement les choses encore plus compliquées, car vous devez comprendre comment votre application C/C++ communique avec la « boîte noire » exécutable.

Je vais présenter aux programmeurs C/C++ l'API Python/C, une bibliothèque C qui permet d'intégrer des modules Python dans des applications C/C++. La bibliothèque API fournit un ensemble de routines C pour initialiser l'interpréteur Python, appeler vos modules Python et terminer l'intégration. La bibliothèque est construite avec Python et distribuée avec toutes les versions récentes de Python.

La première partie de cette série d'articles traite des bases de l'intégration Python. La deuxième partie passera à des sujets plus avancés. Ce tutoriel n'enseigne pas systématiquement le langage Python, mais je décrirai brièvement le fonctionnement du code Python lorsqu'il apparaîtra. L'accent sera mis sur la manière d'intégrer les modules Python à vos applications C/C++. Voir l'article : « Intégration de Python en C/C++ : Partie II ».

Pour utiliser le code source, vous devez installer une version récente de Python, Visual C++ (ou un compilateur GCC sous Linux). L'environnement que j'ai utilisé pour tester est : Python 2.4 (Windows et Linux), Visual C++ 6.0 (Windows) ou GCC 3.2 (RedHat 8.0 Linux). Avec Visual C++, sélectionnez la configuration Release à générer. La configuration du débogage nécessite la bibliothèque de débogage Python "python24_d.lib", qui n'est pas fournie avec les distributions normales.

Icône de validation par la communauté


https://www.paulnorvig.com/guides/interfacing-python-with-cc-for-performance.html
https://litux.nl/mirror/pythonprogramming/0596000855_python2-CHP-20-SECT-5.html 


