**Auteur** : *Jacques-Bernard Lekien*, jacques-bernard.lekien@cea.fr

**Projet gitlab.com** : *pykdi*, *6 mai 2024*.

Mise à jour avec la version 0.18.0.

# KDI : **K**ameleon **D**ata **I**nterface

La plate-forme ***KDI***, ***K***ameleon ***D***ata ***I***nterface, vise à proposer une
solution pour gérer des flux de données sortant / entrant d'un code de simulation comme
d'outils de sauvegarde, d'analyse et de visualisation, dans un environnement ***HPC***.

Ce document décrit son utilisation à travers du code Python.

# Sommaire

- [Include](#include)
- [Creation](#creation)
- [Chunk](#chunk)
- [Sémantique](#sémantique)
  - [Base](#base)
  - [Fields](#fields)
  - [UnstructuredGrid](#unstructuredgrid)
  - [SubUnstructuredGrid](#subunstructuredgrid)
- [Exécution](#execution)

## <a name="#include"></a>Include

### Include requis

L'utilisation de ***pykdi*** dans un code C++ nécessite l'include ***Kdi.h***,
les différentes classes et méthodes étant regroupées dans le namespace KDI :

```cpp
#include "Kdi.h"

using namespace KDI;

#include "mpi.h"
```

L'include ***mpi.h*** est nécessaire pour réaliser une exécution parallèle distribuée.
Il est ainsi aisé de récupérer pour le code utilisateur au cours de l'exécution le
rang du processus ainsi que le nombre total de processus participant à ce calcul
à travers les instructions suivantes :

```cpp
  int mpi_size {1};
  int mpi_rank {0};

  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
```

D'autres services permettent de communiquer entre processus.
Ce module est utilisé en interne de ***pykdi***.
Il n'y a pas de transmission de communicateur entre le code utilisateur et ***pykdi***.

Quid de la gestion des chemins d'accès à un répertoire ou un fichier indépendemment
de l'OS sous-jacent ?

## <a name="#creation"></a>Creation

La façon la plus efficient de créer une base conforme aux codes de simulation
discrétisés en temps et en espace est d'exécuter cette instruction :

```cpp
std::shared_ptr<KDIBase> kdi_base = createBase(nbParts, false); // true/false permet d'activer les traces
```

Cette instruction instancie la description d'une ***base*** en mémoire sous la forme
d'un dictionnaire Python.

L'agréement retenu est automatiquement ***StepPart*** (au contraire du code Python qui
offre la possibilité d'opter pour d'autres agréement) est conforme à une discrétisation
temporelle (***step***) et spatiale (***part***).
Pour cet agréement, il est nécessaire d'indiquer à la création le nombre de partitions
qui seront à créer par temps de simulation.
Cette création peut être l'oeuvre d'un unique processus qui crée toutes ces
partitions ou d'autant de processus que de partitions qui ne crée qu'une partition.
Dans cette version, le nombre de partitions est fixé à la création de la base et
il restera inchangé durant toute l'exécution... à moins de créer une nouvelle base.

Une description mono-partition se fait en positionnant ***nb_parts*** à 1.

## <a name="#chunk"></a>Chunk

Pour ce type d'agréement, l'identification d'un ***chunk*** ou partie de la simulation
peut se faire de trois façons différentes :

- pour une identification la plus fine :

```cpp
std::shared_ptr<KDIChunk> kdi_chunk = kdi_base->chunk(float_step, int_part)
```

où ***float_step*** décrit la valeur d'un temps de simulation et ***int_part***
le numéro de la partition dans [0, ***nb_parts***[.

- pour une identification indépendante des partitions :

```cpp
std::shared_ptr<KDIChunk> kdi_chunk_partless = kdi_base->chunk(float_step);
```

où l'on ne décrit que le temps de simulation ou ***partless*** ce qui revient
à considérer que la valeur est la même pour toutes les partitions de ce temps
de simulation.

- pour une identification indépendante des partitions :

```cpp
std::shared_ptr<KDIChunk> kdi_chunk_stepless = kdi_base.chunk()
```

où l'on ne décrit rien ou ***steptless*** ce qui revient à considérer que la
valeur est la même au cours de la simulation et pour toutes les partitions.

L'emploi de **shared ptr** permet de ne pas à devoir faire appel à **delete**.

## <a name="#semantique"></a>Sémantique

A partir d'une instance de chunk, on va pouvoir décrire le contenu sémantique
de la base en associant à une clef du dictionnaire une instance d'objet
et, potentiellement, une valeur propre à ce chunk.

La description sémantique est intemporelle c'est à dire que la sémantique
associé à une clef du dictionnaire restera inchangé au cours de la simulation.

### <a name="#base"></a>Base

Lors de l'instanciation d'une base pour cet agréement ***StepPart***, nous avons
obtenu un dictionnaire Python.
Il comprend entre autre une instance non nommée de l'objet sémantique ***Base***:

```cpp
base->dump(""
> "KDIComplexType('Base')"
```

Ce type sémantique ***Base*** est décrit dans le dictionnaire sémantique
***dictionary/dictionary.json*** :

```python
  "Base": {
    "/agreement": "Agreement",
    "/glu": "Glu",
    "/study": "Study",
    "/chunks": "Chunks",
    "/fields": "Fields"
  }
```

Cette description sémantique peut-être récursive comme ici pour l'attribut
***/agreement*** qui est lui-même de type sémantique complexe ***Agreement***.

L'instance d'un attribut d'une instance de type est obtenu en concaténent au nom
de l'instance (ici pour l'instance de ***Base***, la chaîne vide) de l'objet le
nom de l'attribut :

```cpp
base->dump("/glu/version")
> "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)"
```

Il ne reste plus qu'à associer une valeur à un ***chunk*** donné par l'instruction suivante:

```cpp
std::vector<long> version(2) {2, 0};
PyArrayObject* parray = _numpyDataSet1D<long>(version);
kdi_chunk_stepless->insert("/glu/version", parray);
Py_DECREF(parray);
```

### <a name="#fields"></a>Fields

Un champ de valeurs ou grandeur physique peut être associé au type sémantique ***Fields***.

```cpp
base->dump("/fields")
> "KDIFields({})"
```

Pour cela, il suffit de définir une instance de champ de valeurs associé à une instance de ***Fields***
puis de lui associer une valeur pour un chunk donné :

```cpp
const std::string namefield = kdi_base->update_fields("/fields", "myGlobScalarField");
std::vector<long> field(...) {...};
PyArrayObject* parray = _numpyDataSet1D<long>(field);
kdi_chunk->insert(namefield, parray);
Py_DECREF(parray);
```

La première instruction crée si nécessaire le nouveau champ de valeurs et en retourne
le fieldkey, la clef dans le dictionnaire qui décrit la base en mémoire.
La seconde instruction associe une valeur à ce champ de valeurs pour le chunk donné.

La description de l'instance ***Fields*** est alors modifiée afin d'indiquer le chemin
d'accès aux champs de valeurs qui lui sont associés :

```cpp
base->dump("/fields")
> "KDIFields({'/fields/myGlobScalarField'})"
```

Spécifiquement pour cette instance de ***Fields***, on s'attendrait à ne définir que
des champs avec des valeurs ***stepless***, même si aucune vérification est faite
dans ce sens pour la version de ***pykdi*** actuelle.

Les coordonnées des points sont vues comme un tableau avec deux dimensions
dont la seconde dimension a pour valeur 3. La description d'un tel tableau nécessite
deux étapes. La première pour décrire un tableau contenant toutes valeurs.
La seconde pour décrire le ***shape*** (la forme) de ce tableau.
Voici ce que cela donne en instruction C++ lorsqu'on veut définir les coordonnées
des points pour un maillage non structuré défini dont le nom est ***/mymesh*** :

```cpp
std::vector<double> points(nNodes * 3) {...};
std::vector<int> shape {nNodes, 3};
PyArrayObject* parray = _numpyDataSet2D<double>(points, shape);
kdi_chunk->insert("/mymesh/points/cartesianCoordinates", parray);
Py_DECREF(parray);
```

Bien évidemment, cette approche est vraie aussi pour des champs de valeurs.

### <a name="#unstructuredgrid"></a>UnstructuredGrid

La ceéation d'une instance représentant un maillage non structuré se fait de la façon suivante :

```cpp
base->update("UnstructuredGrid", "/mymesh")

base->dump("/mymesh")
> "KDIComplexType('UnstructuredGrid')"
```

Le nom doit nécessairement comporter '/' comme premier caractère.

Le type sémantique associé à ***UnstructuredGrid*** est un type complexe décrit dans le
dictionnaire de sémantique.

Il ne reste ensuite qu'à décrire :

- ***/mymesh/points/cartesianCoordinates*** pour décrire les coordonnées des points ;

```cpp
std::vector<double> points(nNodes * 3) {...};
std::vector<int> shape {nNodes, 3};
PyArrayObject* parray = _numpyDataSet2D<double>(points, shape);
kdi_chunk->insert("/mymesh/points/cartesianCoordinates", parray);
Py_DECREF(parray);
```

- ***/mymesh/cells/types*** pour décrire le type des cellules ;

```cpp
std::vector<unsigned char> cells_types(...) {...};
PyArrayObject* parray = _numpyDataSet1D<long>(cells_types);
kdi_chunk->insert("/mymesh/cells/types", parray);
Py_DECREF(parray);
```

avec, l'emploi d'une sémantique pour ces valeurs de type exploitant
KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_QUADRANGLE_TYPE_VALUE et
KDI_SIMULATION_HEXAHEDRON_TYPE_VALUE.

En C++, seule la variable d'environnement ***KDI_AGREEMENT_CODE2KDI*** permet
de piloter la codification des valeurs pour décrire le type d'une cellule.

- ***/mymesh/cells/connectivity*** pour décrire la connectivité des cellules à travers
  des index ordonnés de points. Le nombre d'index de points est lié au type de la cellule
  l'ordonnancement est imposé suivant la convention généralement admise que l'on retrouve
  aussi dans VTK.

```cpp
std::vector<unsigned char> cells_connectivity(...) {...};
PyArrayObject* parray = _numpyDataSet1D<long>(cells_connectivity);
kdi_chunk->insert("/mymesh/cells/connectivity", parray);
Py_DECREF(parray);
```

Il est possible ensuite de définir des champs de valeurs au niveau global du maillage
mais aussi au niveau des points et des cellules de la même façon que l'on avait fait
au niveau de la base.

```cpp
const std::string pointfieldkey = kdi_base->update_fields("/mymesh/points/fields", "...")
const std::string cellfieldkey = kdi_base->update_fields("/mymesh/cells/fields", "...")
```

Les services ***update***, ***update_fields*** et ***update_sub*** ne sont disponibles
qu'au niveau de la base car elles influent sur le contenu sémantique de cette dernière
et que cette sémantique est indépendant de tout ***chunk***.

A faire remarquer que seul le support géométrique d'un champ de valeurs sera  

### <a name="#subunstructuredgrid"></a>SubUnstructuredGrid

A ce maillage non structuré peut être associé des sous-maillages qui sont décrits par
une liste d'index de cellules. Cela se fait avec l'instruction suivante :

```cpp
const std::string submeshkey = kdi_base->update_sub("/mymesh", "mymilA")
```

Le type sémantique de sous-maillage est défini en fonction du type de sémantique
du maillage parent.

Ensuite, il ne reste plus qu'à renseigner l'index de cellules qui va décrire une
sélection sur le maillage parent :

```cpp
std::vector<long> cells_indexes(...) {...};
PyArrayObject* parray = _numpyDataSet1D<long>(cells_indexes);
kdi_chunk->insert(submeshkey + "/cells/indexescells", parray);
Py_DECREF(parray);
```

puis les champs de valeurs aux cellules ***submeshkey + '/cells/fields'*** de
façon similaire à ce que l'on a vu auparavant :

```cpp
const std::string cellfieldkey = kdi_base->update_fields(submeshkey + "/cells/fields", "...")
```

## <a name="#execution"></a>Execution

Pour exécuter un programme utilisant ***pykdi***, il est nécessaire de positionner
quelques variables d'environnement :

```bash
export PYTHONPATH=${PWD}/src
export KDI_DICTIONARY_PATH=${PWD}/src/pykdi/dictionary
export KDI_AGREEMENT_CODE2KDI=VTK
```

La première positionne le PYTHONPATH du mode ***pykdi***.
La seconde localise le dictionnaire de sémantique commun ou propre à ce code.
La dernière, optionnelle car aujourd'hui le défaut, indique que la valeur du type des
cellules par défaut est bien conforme à celle de VTK.

### Exemple d'exécution mono-serveur

Cet exemple
[test_example_uns_1pe_4ssd_1uns_10tps__mono_server.py](tests%2Ftest_example_uns_1pe_4ssd_1uns_10tps__mono_server.py)
décrit la création d'une base ayant 4 partitions.

L'exécution se fait à travers les instructions suivantes :
```bash
export PYTHONPATH=$PWD/src
export KDI_DICTIONARY_PATH=$PWD/src/pykdi/dictionary
python3 tests/test_example_uns_1pe_4ssd_1uns_10tps__mono_server.py
```

Une base VTK HDF est produite ainsi que le fichier de description sémantique ***pydi*** de cette base :
```bash
ex_uns_1pe_4ssd_1uns_10tps__mono_server.json
ex_uns_1pe_4ssd_1uns_10tps__mono_server.vtkhdf
```

### Exemple d'exécution multi-serveur

Cet exemple
[test_example_uns_1pe_4ssd_1uns_10tps__multi_server.py](tests%2Ftest_example_uns_1pe_4ssd_1uns_10tps__multi_server.py)
décrit la création d'une base ayant autant de partitions que de processus exécutés.

L'exécution se fait à travers les instructions suivantes :
```bash
export PYTHONPATH=$PWD/src
export KDI_DICTIONARY_PATH=$PWD/src/pykdi/dictionary
/usr/bin/mpiexec -n 4 python3 tests/test_example_uns_1pe_4ssd_1uns_10tps__multi_server.py
```

Une base VTK HDF est produite ainsi que le fichier de description sémantique ***pydi*** de cette base :
```bash
ex_uns_1pe_4ssd_1uns_10tps__mono_server.json
ex_uns_1pe_4ssd_1uns_10tps__mono_server.vtkhdf
```

Cette base est strictement identique à l'exécution mono-serveur si l'on opte pour 4 serveurs.
La seule différence au niveau du fichier ***pykdi*** JSON réside dans le nommage du fichier VTK HDF qui a changé.
