from .KDIDatas import KDIDatas


class KDIList(KDIDatas):
    """
    Not directly object in graph.
    KDILists allows you to list the keys of the attributes of this object in the sense of the semantic dictionary.
    Even if KDIList inherits from KDIDatas which makes it an instantiable object at the graph level,
    we refrain from using it directly but prefer its semantic versions KDIAttributes and KDIFields.

    Fr:
        Testé indirectement par test_kdi_base.py
    """
    __slots__ = 'data'

    def __init__(self, data: any):
        """

        :param data:
        """
        super().__init__()
        if len(data) == 0:
            self.data = set()
            return
        if not isinstance(data, set):
            self.data = set()
            for value in data:
                self.data.add(value)
            return
        self.data = data

    def insert(self, item, chunk=None) -> None:
        assert (isinstance(item, str))
        self.data.add(item)

    def erase(self, chunk=None) -> None:
        pass

    def get(self, chunk=None) -> any:
        return sorted(self.data)

    def compute_str(self, name) -> str:
        msg = name + '({'
        for elementList in sorted(self.data):
            msg += '\'' + elementList + '\','
        msg += '})'
        return msg

    def __repr__(self):
        return self.compute_str('KDIList')
