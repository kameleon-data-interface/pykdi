import numpy as np
import os

from abc import ABC, abstractmethod

from pykdi.tools import kdi_log_python

class KDIAgreementChunk:
    """
    """
    __slots__ = '_kdi_base', '_configuration', '_chunk'

    def __init__(self, kdi_base: any, configuration: dict[str, any] = None):
        self._kdi_base = kdi_base
        self._configuration = configuration
        self._chunk = None

    @property
    def base(self):
        return self._kdi_base.base
    
    @property
    def chunk(self):
        return self._chunk

    @abstractmethod
    def new_chunk(self, configuration: dict[str, any] = None) -> any:
        return self._kdi_base.chunk(configuration)

    def update(self, typename: str, name: str):
        """

        :param typename:
        :param name:
        :return:
        """
        self._kdi_base.update(typename, name)
    def update_fields(self, field_parent_name: str, field_basename: str) -> str:
        """

        :param field_parent_name:
        :param field_basename:
        :return:
        """
        return self._kdi_base.update_fields(field_parent_name, field_basename)

    def update_sub(self, mesh_parent_name: str, submesh_basename: str) -> str:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        return self._kdi_base.update_sub(field_parent_name, field_basename)

    def insert(self, key: str, value: any) -> None:
        """

        :param key:
        :param value:
        :return:
        """
        kdi_log_python.log('KDIAgreementChunk insert() key:', key, 'value:', value)
        if isinstance(value, str):
            self._kdi_base.base[key].insert(np.array([value]), chunk=self._chunk)
            return

        # TODO a cause du C++ qui ne passe pas en double shape
        kdi_log_python.log('self._KDIAgreementChunk insert()', '/'.join(key.split('/')[2:]))
        if '/'.join(key.split('/')[2:]) == 'points/cartesianCoordinates':
            if len(value.shape) == 1:
                kdi_log_python.log('self._KDIAgreementChunk insert() specific case points/cartesianCoordinates cause not in (n,3)')
                shape2 = (value.shape[0]//3, 3,)
                value2 = value.reshape(shape2)
                self._kdi_base.base[key].insert(value2, chunk=self._chunk)
                return

        self._kdi_base.base[key].insert(value, chunk=self._chunk)

    def get(self, key: str) -> any:
        """
        :param key:
        :return:
        """
        kdi_log_python.log('self._KDIAgreementChunk get() key:', key)
        return self._kdi_base.base[key].get(chunk=self._chunk)


    def erase(self, key: str):
        """
        :param key:
        :return:
        """
        kdi_log_python.log('self._KDIAgreementChunk get() erase:', key)
        self._kdi_base.base[key].erase(chunk=self._chunk)

    @abstractmethod
    def garbageCollector(self) -> None:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def saveVTKHDF(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def saveVTKHDFNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def saveVTKHDFCompute(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def saveVTKHDFComputeNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    def dump(self) -> None:
        """
        """
        print(self._chunk)

    def __repr__(self) -> str:
        """

        :return:
        """
        return str(self._kdi_base.base)
