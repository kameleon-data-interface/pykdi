from abc import ABC, abstractmethod

from pykdi.agreement.KDIAgreementChunk import KDIAgreementChunk

class KDIAgreementBase:
    """
    """
    __slots__ = '_filename', '_mode', '_conf', '_in_progress', '_base'

    def __init__(self, filename: str):
        """
        Builder method
        """
        self._filename = filename
        self._mode = 'w'
        self._conf = dict()
        self._in_progress = False
        self._base = None

    def set_conf_int(self, key: str, val: int):
        """

        :param typename:
        :param name:
        :return:
        """
        assert (not self._in_progress)
        self._conf[key] = val

    def set_conf(self, val: dict()):
        """

        :param typename:
        :param name:
        :return:
        """
        assert (not self._in_progress)
        self._conf |= val

    def initialize(self, mode_read: bool = False):
        """

        :param typename:
        :param name:
        :return:
        """
        assert (not self._in_progress)
        if mode_read:
            self._mode = 'rw'
            return
        self._mode = 'w'

    @abstractmethod
    def update(self, typename: str, name: str):
        """

        :param typename:
        :param name:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def update_fields(self, field_parent_name: str, field_basename: str) -> str:
        """

        :param field_parent_name:
        :param field_basename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def update_sub(self, mesh_parent_name: str, submesh_basename: str) -> str:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def chunk(self, configuration: dict[str, any] = None) -> KDIAgreementChunk:
        raise NotImplementedError()

    @abstractmethod
    def dump(self):
        """
        """
        raise NotImplementedError()

    @abstractmethod
    def light_dump(self):
        """
        """
        raise NotImplementedError()

    @abstractmethod
    def __repr__(self) -> str:
        """
        Representation of the description of the current instance.
        In KDIMemory, for example, dump all values.
        """
        raise NotImplementedError()
