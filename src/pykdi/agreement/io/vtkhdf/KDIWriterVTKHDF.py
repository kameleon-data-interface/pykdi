import gc
import h5py
from mpi4py import MPI
import numpy as np
import os
import os.path as osp

from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory

from pykdi.agreement.step_part.kdi_agreement_steppart import KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART

from .KVTKHDFDatas import data_read_vtk_hdf, KVTKHDFDatas, KVTKHDFCloseAll, build_keys
from .KVTKHDFStepPartChunks import KVTKHDFStepPartChunks

from .KVTKType import kdi2vtk
from pykdi.io.json.KJSON import write_json
from .KVTKHDFGarbageCollector import KVTKHDFGarbageCollector
from .KDIWriterVTKHDF_common import get_hdf_group, set_hdf_group_global_fields_dataset, set_vtk_hdf_globaldatas
from .KDIWriterVTKHDF_common import KVTKHDF_STEPS_CHUNK_SIZE, KVTKHDF_CHUNK_SIZE, trace, tic, tac, tictac_summary


def set_datas(fw, key_full_fw, dtype, base, key_base, vStep, iStep, nb_parts, first_offset, is_assembly: bool,
              force_common_data_offsets: bool, chunk_size: int):
    (key_parent_fw, key_fw, is_key_data_parent_offsets_fw, key_parent_offsets_fw, is_common_key_data_offsets_fw,
     key_offsets_fw, key_full_offsets_fw, with_step_shifting) = build_keys(key_full_fw, is_assembly, force_common_data_offsets)
    tic("set_datas")
    tic("set_datas_"+key_base)
    # --- check pre key_base and get len_datas
    variants = {'step': vStep, 'part': MPI.COMM_WORLD.Get_rank()}
    base['/chunks'].set(variants)

    try:
        current_datas = base[key_base].get()
    except:
        raise KDIException('pre: \'' + key_base + '\' (' + key_base + ') must be a key of \'base\' or problem on get()!')
    # len size and len component
    len_component = current_datas.size // current_datas.shape[0]
    assert (not current_datas.size % current_datas.shape[0])
    # store current_datas.size
    datas_size = np.zeros(shape=(nb_parts,), dtype=np.int64)
    data_dtype = current_datas.dtype
    if MPI.COMM_WORLD.Get_size() == 1:
        # Sequential Mode
        # Remark: current_datas is the data of subdomain 0 (Get_rank() is 0)
        datas_size[0] = current_datas.shape[0]
        for iPart in range(1, nb_parts):
            variants['part'] = iPart
            base['/chunks'].set(variants)
            try:
                datas = base[key_base].get()
            except KeyError:
                raise KDIException('pre: \'' + key_base + '\' must be a key of \'base\'!')
            except KDIException:
                raise KDIException('Error on Get for \'' + key_base + '\'!')
            # check coherence len_component
            dsize = datas.shape[0]
            if dsize != 0 and datas.size != 0:
                datas_size[iPart] = dsize
                assert (data_dtype == datas.dtype)
                assert (not datas.size % dsize)
            else:
                assert (datas.size == dsize)
    else:
        # Concurrent Parallel Mode
        # Remark: current_datas is the data of current process/subdomain (Get_rank())
        datas_size[MPI.COMM_WORLD.Get_rank()] = current_datas.shape[0]
        # TODO Il faut verifier la cohernce du type entre processus
        # TODO PARALLEL Il faut faire une somme sur datas_size
        all_reduce_datas_size = np.zeros(shape=(nb_parts,), dtype=np.int64)
        MPI.COMM_WORLD.Allreduce(sendbuf=[datas_size, MPI.LONG], recvbuf=[all_reduce_datas_size, MPI.LONG], op=MPI.SUM)
        datas_size = all_reduce_datas_size
    trace('datas_size #', MPI.COMM_WORLD.Get_rank(), datas_size)

    # --- check and get
    # on converge vers un type hdf
    trace('KDI DTYPE ', key_base)
    if dtype is None:
        trace('KDI DTYPE NEW ', data_dtype, ' -> ', str(data_dtype))
        hdf_dtype = str(data_dtype)
        trace('KDI DTYPE CONVERT hdf_dtype ', hdf_dtype)
    else:
        # TODO Verifier la coherence de type entre dtype et data_dtype
        hdf_dtype = dtype
        trace('KDI DTYPE FIXED hdf_dtype ', hdf_dtype)

    try:
        dset = fw[key_full_fw]
        trace('KDI DTYPE HDF ', dset.dtype)  # c'est un type numpy ;) donc non compatible avec hdf_dtype
        assert (dset.dtype == hdf_dtype)
        if key_offsets_fw:
            try:
                dset_offsets = fw[key_full_offsets_fw]
            except KeyError:
                raise KDIException('pre: if \'key_fw\'(' + key_full_fw + ') exist in \'fw\' then \'key_offset_fw\' (' +
                                   key_full_offsets_fw + ') exist also!')
    except KeyError:
        try:
            _ = fw[key_parent_fw]
        except KeyError:
            raise KDIException('pre: parent \'key_parent_fw\' (' + key_parent_fw + ') not exist in \'fw\'!')
        trace('len_component:', len_component)
        if len_component == 1:
            dset = fw[key_parent_fw].create_dataset(name=key_fw, shape=(1,), dtype=hdf_dtype,
                                                    maxshape=(None,),
                                                    chunks=(chunk_size,))  # chunks=True
        else:
            dset = fw[key_parent_fw].create_dataset(name=key_fw, shape=(1, len_component), dtype=hdf_dtype,
                                                    maxshape=(None, len_component),
                                                    chunks=(chunk_size, len_component))  # chunks=True
        trace('KDIWriterVTKHDF:set_datas ', key_fw, 'dtype:', hdf_dtype, 'chunksize:', chunk_size)
        trace('KDI DTYPE CHECK ...')
        #
        if key_offsets_fw:
            try:
                dset_offsets = fw[key_full_offsets_fw]
                if not is_common_key_data_offsets_fw:
                    raise KDIException(
                        'pre: if \'key_fw\' (' + key_full_fw + ') NOT exist in \'fw\' then \'key_full_offsets_fw\' (' +
                        key_full_offsets_fw + ') NOT exist also!')
            except KeyError:
                try:
                    fw[key_parent_offsets_fw]
                except KeyError:
                    if is_key_data_parent_offsets_fw:
                        fw.create_group(key_parent_offsets_fw, track_order=True)
                    else:
                        raise KDIException(
                            'pre: parent \'key_parent_offsets_fw\' (' + key_parent_offsets_fw + ') not exist in \'fw\'!')

                dset_offsets = fw[key_parent_offsets_fw].create_dataset(name=key_offsets_fw,
                                                                        data=np.array([0], dtype='i8'),
                                                                        shape=(1,),
                                                                        dtype='i8', maxshape=(None,),
                                                                        chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))  # chunks=True
    #
    first_common_set = True
    # Comput offsets
    offsets = None

    if key_offsets_fw:
        # Avec une clef d'offsets sous /VTKHDF/Steps
        if is_common_key_data_offsets_fw:
            # '/VTKHDF/Steps/CellOffsets' pour '/VTKHDF/Types' puis '/VTKHDF/Offsets'
            try:
                # if dset_offsets.size < first_offset + nb_parts + 1:
                #    raise IndexError
                # offsets = dset_offsets[first_offset:first_offset + nb_parts + 1]
                offsets = np.zeros(shape=(nb_parts + 1,), dtype='i8')
                try:
                    offsets[0] = dset_offsets[iStep] + with_step_shifting * first_offset
                except KeyError:
                    raise KDIException('pre: blabla not value in \'iStep\' (' + iStep + ' firest offsets ' + str(
                        first_offset) + ' must be <' + str(dset_offsets.size) +
                                       ') for \'key_offset_fw\' (' + key_full_offsets_fw + ') in fw!')
                for iPart in range(0, nb_parts):
                    offsets[iPart + 1] += offsets[iPart] + datas_size[iPart]
                if iStep+1+1 == dset_offsets.size:
                    if offsets[nb_parts] - with_step_shifting * (first_offset + nb_parts) != dset_offsets[iStep+1]:
                        raise KDIException('Internal error')
                else:
                    dset_offsets.resize([iStep + 2])
                    dset_offsets[iStep + 1:iStep + 2] = offsets[nb_parts] - with_step_shifting * nb_parts
                first_common_set = False
            except IndexError:
                pass
        if first_common_set:
            # '/VTKHDF/Steps/PointOffsets', '/VTKHDF/Steps/PointDataOffsets',
            offsets = np.zeros(shape=(nb_parts + 1,), dtype=np.int64)
            try:
                offsets[0] = dset_offsets[iStep]
            except KeyError:
                raise KDIException('pre: not value in \'iStep\' (' + iStep + ' firest offsets ' + str(
                    first_offset) + ' must be <' + str(dset_offsets.size) +
                                   ') for \'key_offset_fw\' (' + key_full_offsets_fw + ') in fw!')
            for iPart in range(0, nb_parts):
                offsets[iPart + 1] += offsets[iPart] + datas_size[iPart]
            dset_offsets.resize([iStep+2])
            dset_offsets[iStep+1:iStep+2] = offsets[nb_parts] - with_step_shifting * nb_parts
    else:
        # Default
        # pas de clef sous /VTKHDF/Steps pour '/VTKHDF/NumberOfPoints', '/VTKHDF/NumberOfCells',
        offsets = np.zeros(shape=(nb_parts + 1,), dtype='i8')
        for iPart in range(nb_parts + 1):
            offsets[iPart] = first_offset + iPart

    trace('offsets #', MPI.COMM_WORLD.Get_rank(), offsets)

    # resize datas and set datas
    trace('offsets', offsets[nb_parts])
    if len_component == 1:
        fw[key_full_fw].resize((offsets[nb_parts],))
    else:
        fw[key_full_fw].resize((offsets[nb_parts], len_component,))

    # iPart current (Sequential Mode is 0, Parallel Mode is MPI.Get_rank())
    variants['part'] = iPart = MPI.COMM_WORLD.Get_rank()
    base['/chunks'].set(variants)
    shape = current_datas.shape
    if len_component == 1:
        fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]] = current_datas.reshape((offsets[iPart + 1] - offsets[iPart],))[
                                                             :]
    else:
        fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]] = current_datas.reshape(
            (offsets[iPart + 1] - offsets[iPart], len_component))[:]
    trace('write iPart', iPart, offsets[iPart], offsets[iPart + 1], fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]])
    current_datas.reshape(shape)

    if MPI.COMM_WORLD.Get_size() == 1:
        # Sequential Mode: next iPart
        for iPart in range(1, nb_parts):
            variants['part'] = iPart
            base['/chunks'].set(variants)
            try:
                datas = base[key_base].get()
            except:
                raise KDIException('pre: \'key_base\' must be a key of \'base\'!')
            shape = datas.shape
            if len_component == 1:
                fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]] = datas.reshape((offsets[iPart + 1] - offsets[iPart],))[
                                                                     :]
            else:
                fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]] = datas.reshape(
                    (offsets[iPart + 1] - offsets[iPart], len_component))[:]
            trace('write iPart', iPart, offsets[iPart], offsets[iPart + 1], fw[key_full_fw][offsets[iPart]:offsets[iPart + 1]])
            datas.reshape(shape)

    trace('KDI DTYPE set_datas END')
    tac("set_datas_"+key_base)
    tac("set_datas")


def write_vtk_hdf_unstructured_grid(fw, base, vStep, iStep, code2vtk, key_mesh, hdf_group_name, is_assembly,
                                    is_parent_mesh, hdf_parent_group_name):
    tic("saveVTKHDFUNS")
    dtype_numberof_parts = 'u2'
    dtype_numberof_points = 'u4'
    dtype_numberof_cells = 'u4'
    dtype_type_cells = '|u1'
    dtype_offsets_cells = 'i4'
    dtype_connectivity_cells = dtype_offsets_cells  # fixe by ParaView/VTK
    dtype_numberOf_connectivity_cells = 'u4'

    step_index = iStep
    hdf_wfd = fw
    hdf_group = get_hdf_group(hdf_wfd, hdf_group_name)
    if hdf_parent_group_name:
        hdf_parent_group = get_hdf_group(hdf_wfd, hdf_parent_group_name)
    else:
        hdf_parent_group = None

    # Attribut Type
    strType = 'UnstructuredGrid'.encode('utf-8')
    # It's not compatible with VTK:
    #   fw['/VTKHDF'].attrs['Type'] = strType
    # this's yes:
    try:
        value = fw[hdf_group_name].attrs['Type']
        assert (value == strType)
    except:
        fw[hdf_group_name].attrs.create(name='Type', data=strType, shape=None, dtype='|S' + str(len(strType)))

    # Attribut Version
    # It's not compatible with VTK:
    #  fw['/VTKHDF'].attrs['Version'] = base['/glu/version'].get()
    # this's yes:

    try:
        value = fw[hdf_group_name].attrs['Version']
        assert (np.all(value == base['/vtkhdf/version'].get()))
    except:
        fw[hdf_group_name].attrs.create(name='Version', data=base['/vtkhdf/version'].get(), dtype='|u4')

    try:
        fw[hdf_group_name + '/Steps']
    except KeyError:
        fw.create_group(hdf_group_name + '/Steps')

    try:
        nSteps = fw[hdf_group_name + '/Steps'].attrs['NSteps']
        initialNSteps = np.array(nSteps)
        assert ('pre: index \'step\' (' + str(iStep) + ') must be <= actual array steps (' + str(nSteps) + ')' and
                iStep <= nSteps)
        if iStep < nSteps:
            if iStep == 0:
                trace('full truncature with (re)write', str(iStep), '/', str(nSteps))
            else:
                trace('truncature to', str(iStep - 1), ' with rewrite', str(iStep), '/', str(nSteps))
        elif nSteps == iStep:
            trace('add next step')
        #
        # REMARK En contradiction avec la doc., on ne peut pas faire ca
        # TODO CECI CHANGE LE COMPORTEMENT !!?
        # assert(isinstance(nSteps, np.ndarray))
        nSteps = iStep + 1
        # En effet, le type a change
        assert (isinstance(nSteps, int))
        # de fait la valeur de l'attribut n'a pas change
        assert (fw[hdf_group_name + '/Steps'].attrs['NSteps'] == initialNSteps)
        # REMARK La bonne methode
        fw[hdf_group_name + '/Steps'].attrs['NSteps'] = iStep + 1
        assert (fw[hdf_group_name + '/Steps'].attrs['NSteps'] == iStep + 1)
    except:
        nSteps = 0
        initialNSteps = np.array(nSteps)
        assert ('pre: index \'step\' (' + str(iStep) + ') expected 0!')
        fw[hdf_group_name + '/Steps'].attrs.create(name='NSteps', data=np.array(iStep + 1), shape=(1,), dtype='i4')

    # Set the current time step
    # TODO Il faudrait etendre saveVTKHDF afin qu'il sauvegarde les valeurs step/part pour alléger le json
    try:
        dset = fw[hdf_group_name + '/Steps/Values']
        dset.resize((iStep + 1,))
        dset[iStep] = vStep
    except:
        dset = fw[hdf_group_name + '/Steps'].create_dataset(name='Values', data=np.array([vStep, ], dtype='f8'),
                                                       shape=(1,), dtype='f8',
                                                       maxshape=(None,),
                                                       chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))  # chunks=True

    # Set NumberOfParts as coming from sequential execution
    # TODO distributed execution
    nb_parts = base['/chunks'].GetMaxConstantValue('part')
    try:
        dset = fw[hdf_group_name + '/Steps/NumberOfParts']
        dset.resize((iStep + 1,))
        dset[iStep] = nb_parts
    except:
        fw[hdf_group_name + '/Steps'].create_dataset(name='NumberOfParts',
                                                data=np.array([nb_parts], dtype=dtype_numberof_parts),
                                                shape=(1,), dtype='<i8',
                                                maxshape=(None,),
                                                chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))  # chunks=True

    try:
        dset = fw[hdf_group_name + '/Steps/PartOffsets']
        dset.resize([iStep + 2])
    except:
        dset = fw[hdf_group_name + '/Steps'].create_dataset(name='PartOffsets',
                                                       data=np.array([0, 0], dtype='i8'),
                                                       shape=(2,), dtype='<i8',
                                                       maxshape=(None,),
                                                       chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))  # chunks=True
    first_offset = dset[iStep]
    dset[iStep + 1] = first_offset + nb_parts

    trace('saveVTKHDF Check Step')

    try:
        force_common_data_offsets = base['/vtkhdf/force_common_data_offsets']
    except KeyError:
        raise KDIException('\'/vtkhdf/force_common_data_offsets\' not exists in base!')

    params = {'fw': fw, 'base': base, 'vStep': vStep, 'iStep': iStep, 'nb_parts': nb_parts,
              'first_offset': first_offset, 'is_assembly': is_assembly, 'force_common_data_offsets': force_common_data_offsets}

    trace('before set ##points')

    # Compute NumberOfPoints
    commandNumberOf = """
array = Base[Params[0]].get()
numberOf = np.zeros(shape=(1,), dtype=np.int64)
numberOf[0] = array.shape[0]
Ret = numberOf
    """

    base[key_mesh + '/points/##points'] = KDIEvalString(base, key_mesh + '/points/##points', commandNumberOf,
                                                        [key_mesh + '/points/cartesianCoordinates', ])

    trace('write ' + hdf_group_name + '/NumberOfPoints')

    params.update({'key_full_fw': hdf_group_name + '/NumberOfPoints', 'dtype': dtype_numberof_points,
                   'key_base': key_mesh + '/points/##points', 'chunk_size': KVTKHDF_STEPS_CHUNK_SIZE})
    set_datas(**params)
    # APRES : FIN

    del base[key_mesh + '/points/##points']

    trace('write ' + hdf_group_name + '/Points')

    params.update({'key_full_fw': hdf_group_name + '/Points', 'dtype': '<f8',
                   'key_base': key_mesh + '/points/cartesianCoordinates', 'chunk_size': KVTKHDF_CHUNK_SIZE})
    set_datas(**params)

    # Group PointData
    try:
        fw[hdf_group_name + '/PointData']
    except:
        fw[hdf_group_name].create_group('PointData')

    trace('write ' + hdf_group_name + '/PointData')

    # DataSets Field on PointData
    for field_key in base[key_mesh + '/points/fields'].get():
        field_name = field_key[field_key.rfind('/') + 1:]
        trace('write ' + hdf_group_name + '/PointData/' + field_name)
        params.update({'key_full_fw': hdf_group_name + '/PointData/' + field_name,
                       'dtype': None, 'key_base': field_key, 'chunk_size': KVTKHDF_CHUNK_SIZE})
        set_datas(**params)

    try:
        fw['/KDI']
    except:
        fw.create_group('/KDI', track_order=True)

    if is_parent_mesh:
        try:
            fw['/KDI' + key_mesh]
        except:
            fw.create_group('/KDI' + key_mesh + '/PointData', track_order=True)

        try:
            fw['/KDI' + key_mesh + '/PointData']
        except:
            fw.create_group('/KDI' + key_mesh + '/PointData', track_order=True)

        try:
            fw['/KDI' + key_mesh + '/CellData']
        except:
            fw.create_group('/KDI' + key_mesh + '/CellData', track_order=True)

        try:
            trace('write ' + hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/PointData/globalIndexContinuous')
            params.update({
                'key_full_fw': hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/PointData/globalIndexContinuous',
                'dtype': None,
                'key_base': key_mesh + '/points/globalIndexContinuous', 'chunk_size': KVTKHDF_CHUNK_SIZE})
            set_datas(**params)
        except:
            pass

    trace('write ' + hdf_group_name + '/NumberOfCells')

    # Compute NumberOfCells
    assert (key_mesh + '/cells/##cells' not in base)
    base[key_mesh + '/cells/##cells'] = KDIEvalString(base, key_mesh + '/cells/##cells', commandNumberOf, [key_mesh + '/cells/types'])
    params.update({'key_full_fw': hdf_group_name + '/NumberOfCells', 'dtype': dtype_numberof_cells,
                   'key_base': key_mesh + '/cells/##cells', 'chunk_size': KVTKHDF_STEPS_CHUNK_SIZE, })
    set_datas(**params)

    del base[key_mesh + '/cells/##cells']

    trace('write ' + hdf_group_name + '/NumberOfCells check')

    # Compute new describe types
    assert (key_mesh + '/cells/neotypes' not in base)
    # le test [S1 c'est lorsque le code envoie sous la forme d'un unsigned char
    command = """
code2vtk = Params[0]
types = Base[Params[1]].get()
Ret = code2vtk[types]
    """
    base[key_mesh + '/cells/neotypes'] = KDIEvalString(base, key_mesh + '/cells/neotypes', command, [code2vtk, key_mesh + '/cells/types'])
    params.update({'key_full_fw': hdf_group_name + '/Types', 'dtype': dtype_type_cells, 'key_base': key_mesh + '/cells/neotypes', 'chunk_size': KVTKHDF_CHUNK_SIZE })
    set_datas(**params)

    del base[key_mesh + '/cells/neotypes']

    # TODO 'Offsets' doit imperativement etre ecrit apres 'Types'
    params.update({'key_full_fw': hdf_group_name + '/Offsets', 'dtype': dtype_offsets_cells, 'key_base': key_mesh + '/cells/offsets', 'chunk_size': KVTKHDF_CHUNK_SIZE })
    set_datas(**params)

    connectivity = base[key_mesh + '/cells/connectivity'].get()

    assert (key_mesh + '/cells/##connectivity' not in base)
    base[key_mesh + '/cells/##connectivity'] = KDIEvalString(base, key_mesh + '/cells/##connectivity', commandNumberOf,
                                                         [key_mesh + '/cells/connectivity'])
    params.update({'key_full_fw': hdf_group_name + '/NumberOfConnectivityIds', 'dtype': dtype_numberOf_connectivity_cells,
                   'key_base': key_mesh + '/cells/##connectivity', 'chunk_size': KVTKHDF_STEPS_CHUNK_SIZE})
    set_datas(**params)

    del base[key_mesh + '/cells/##connectivity']

    params.update(
        {'key_full_fw': hdf_group_name + '/Connectivity', 'dtype': dtype_connectivity_cells, 'key_base': key_mesh + '/cells/connectivity', 'chunk_size': KVTKHDF_CHUNK_SIZE })
    set_datas(**params)

    # Group CellData
    try:
        fw[hdf_group_name + '/CellData']
    except:
        fw[hdf_group_name].create_group('CellData')

    # DataSets Field on CellData
    for field_key in base[key_mesh + '/cells/fields'].get():
        field_name = field_key[field_key.rfind('/') + 1:]
        params.update({'key_full_fw': hdf_group_name + '/CellData/' + field_name, 'dtype': None, 'key_base': field_key, 'chunk_size': KVTKHDF_CHUNK_SIZE})
        set_datas(**params)

    if is_parent_mesh:
        try:
            trace('write ' + hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/CellData/globalIndexContinuous')
            params.update({
                'key_full_fw': hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/CellData/globalIndexContinuous',
                'dtype': None,
                'key_base': key_mesh + '/cells/globalIndexContinuous', 'chunk_size': KVTKHDF_CHUNK_SIZE})
            set_datas(**params)
        except:
            pass

    set_hdf_group_global_fields_dataset(step_index, base, hdf_group, hdf_parent_group)

    tac("saveVTKHDFUNS")


def get_parent(base: dict[str, any], key_parent_meshes, key_mesh):
    is_parent_mesh = key_mesh in key_parent_meshes
    hdf_parent_group_name = None
    if '/assembly' in base:
        assembly = base['/assembly']
        for key, value in assembly.items():
            # TODO ATTENTION La clef mesh de assembly ne comporte pas de /
            for key_submesh in value:
                if key_mesh == key_submesh:
                    hdf_parent_group_name = '/VTKHDF/' + key
                    break
    else:
        hdf_parent_group_name = '/VTKHDF'
    return is_parent_mesh, hdf_parent_group_name


def write_vtk_hdf(base: dict[str, any], filename: str | os.PathLike, chunk=None,
                  with_clean_data_submeshes=False, with_json=True):
    mpi_rank = MPI.COMM_WORLD.Get_rank()
    mpi_size = MPI.COMM_WORLD.Get_size()

    tic("saveVTKHDF")
    trace('saveVTKHDF (from process %d/%d)' % (mpi_rank, mpi_size))
    trace('KVTKHDF_CHUNK_SIZE:', KVTKHDF_CHUNK_SIZE)
    trace('KVTKHDF_STEPS_CHUNK_SIZE:', KVTKHDF_STEPS_CHUNK_SIZE)

    KVTKHDFCloseAll()

    try:
        incoming_base_current_chunk = base['/chunks/current']
    except:
        incoming_base_current_chunk = None

    if chunk:
        current_chunk = chunk
    else:
        assert ('pre: The \'/chunks/current\' key must be a base' and '/chunks/current' in base)
        current_chunk = base['/chunks/current']

    for key in [KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART]:
        if key not in base['/chunks'].variants:
            raise KDIException('pre: \'' + key + '\' must be a base variant')

    assert ('pre: there must be two variant' and len(base['/chunks'].order) == 2)

    assert ('pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_STEP + '\' must be a first variant order' and
            base['/chunks'].order[0] == KDI_AGREEMENT_STEPPART_VARIANT_STEP)

    assert ('pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_PART + '\' must be a second variant order' and
            base['/chunks'].order[1] == KDI_AGREEMENT_STEPPART_VARIANT_PART)

    authorized_keys = ['#' + KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_STEP, '#' + KDI_AGREEMENT_STEPPART_VARIANT_PART, KDI_AGREEMENT_STEPPART_VARIANT_PART]
    for key in current_chunk.keys():
        if key not in authorized_keys:
            raise KDIException('pre: \'' + key + '\' must be an authorized key ' + str(authorized_keys))

    nb_parts = base['/chunks'].GetMaxConstantValue('part')

    assert ('pre: Writing is only possible in sequential or in parallel with as many processes (' +
            str(mpi_size) + ') as there are subdomains (' + str(nb_parts) + ')' and
            (mpi_size == 1 or mpi_size == nb_parts))

    vStep = current_chunk[KDI_AGREEMENT_STEPPART_VARIANT_STEP]
    iStep = base['/chunks'].GetOffset(KDI_AGREEMENT_STEPPART_VARIANT_STEP, vStep)

    # TODO il faudrait verifier la coherence du dictionnaire
    # TODO verifier qu'on écrit bien le même temps vStep/iStep, avec le même nombre de parts

    variants = {'step': vStep, 'part': mpi_rank}
    base['/chunks'].set(variants)

    code2kdi = base['/code2kdi']
    code2vtk = kdi2vtk[code2kdi]

    # TODO Ici on pourrait retrouver la notion de force_common_data_offsets

    # It's multi-mil?
    key_meshes = set()
    assembly = None
    is_assembly = False
    key_parent_meshes = []
    if '/assembly' in base:
        assembly = base['/assembly']
        trace('Not implemented write VTK HDF with assembly!')
        trace(assembly)
        # TODO ATTENTION La clef mesh de assembly ne comporte pas de /

        # TODO Ne fonctionne qu'avec un maillage avec multi milieux et non plusieurs maillages
        assert (len(assembly) == 1)

        for key, value in assembly.items():
            if isinstance(value, set):
                for key_mesh in value:
                    key_meshes.add(key_mesh)
            # TODO parce que la clef mesh de assembly ne comporte pas de / on doit la rajouter
            key_parent_meshes.append('/' + key)
            trace('key_parent_meshes', key_parent_meshes)
            trace('key_meshes', key_meshes)
        is_assembly = True
    else:
        for key, item in base.items():
            try:
                if item.get_complex_type() == 'UnstructuredGrid':
                    key_meshes.add(key)
                    key_parent_meshes.append(key)
            except:
                continue
        trace('key_parent_meshes', key_parent_meshes)
        trace('key_meshes', key_meshes)

    key_meshes = sorted(key_meshes)
    trace('sorted key_meshes', key_meshes)

    trace('saveVTKHDF Check chunk')

    trace('saveVTKHDF -> h5py.File ' + filename)
    # open HDF file: https://docs.h5py.org/en/3.7.0/high/file.html#reference
    h5py_file_params = {'name': filename}
    if mpi_size > 1:
        # Setting up parallel execution mode
        h5py_file_params.update({'driver': 'mpio', 'comm': MPI.COMM_WORLD})
    if not osp.isfile(filename):
        # File creation stage
        # It is only at creation that we define the strategy whose persist mode
        h5py_file_params.update({'mode': 'w', 'track_order': True, 'fs_strategy': 'fsm', 'fs_persist': True})
    else:
        # File completion stage
        h5py_file_params.update({'mode': 'a'})
    fw = h5py.File(**h5py_file_params)

    trace('saveVTKHDF Check open', filename)

    try:
        fw['/VTKHDF']
    except KeyError:
        fw.create_group('/VTKHDF', track_order=True)

    # Attribut Type
    if is_assembly:
        strType = 'MultiBlockDataSet'.encode('utf-8')
    else:
        strType = 'UnstructuredGrid'.encode('utf-8')
    # It's not compatible with VTK:
    #   fw['/VTKHDF'].attrs['Type'] = strType
    # this's yes:
    try:
        value = fw['/VTKHDF'].attrs['Type']
        assert (value == strType)
    except:
        fw['/VTKHDF'].attrs.create(name='Type', data=strType, shape=None, dtype='|S' + str(len(strType)))

    # Attribut Version
    # It's not compatible with VTK:
    #  fw['/VTKHDF'].attrs['Version'] = base['/glu/version'].Get()
    # this's yes:

    try:
        value = fw['/VTKHDF'].attrs['Version']
        assert (np.all(value == base['/vtkhdf/version'].get()))
    except:
        fw['/VTKHDF'].attrs.create(name='Version', data=base['/vtkhdf/version'].get(), dtype='i4')

    trace('saveVTKHDF Check Version')

    for key_mesh in key_meshes:
        if is_assembly:
            hdf_group_name = '/VTKHDF' + key_mesh
        else:
            hdf_group_name = '/VTKHDF'

        is_parent_mesh, hdf_parent_group_name = get_parent(base, key_parent_meshes, key_mesh)

        write_vtk_hdf_unstructured_grid(fw, base, vStep, iStep, code2vtk, key_mesh, hdf_group_name, is_assembly,
                                        is_parent_mesh, hdf_parent_group_name)

    conf = {'filename': filename, 'mode': 'r'}

    hdf_steps_key = None

    # mutate key_mesh after write
    for key_mesh in key_parent_meshes:
        if is_assembly:
            hdf_group_name = '/VTKHDF' + key_mesh
        else:
            hdf_group_name = '/VTKHDF'

        base_mesh = base
        try:
            while '/before_write' in base.keys():
                base_mesh = base_mesh['/before_write'][0]
        except:
            pass

        if hdf_steps_key is None:
            hdf_steps_key = hdf_group_name + '/Steps'

        # mute VTKNumpy to VTKHDFValues
        if not isinstance(base_mesh[key_mesh + '/points/cartesianCoordinates'], KVTKHDFDatas):
            trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', key_mesh + '/points/cartesianCoordinates')
            data_read_vtk_hdf(base_mesh, key_mesh + '/points/cartesianCoordinates', conf, hdf_group_name + '/Points')

        for field_key in base_mesh[key_mesh + '/points/fields'].get():
            if not isinstance(base_mesh[field_key], KVTKHDFDatas):
                name_field = field_key[field_key.rfind('/') + 1:]
                trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', field_key)
                data_read_vtk_hdf(base_mesh, field_key, conf, hdf_group_name + '/PointData/' + name_field)

        if key_mesh + '/points/globalIndexContinuous' not in base_mesh or not isinstance(base_mesh[key_mesh + '/points/globalIndexContinuous'], KVTKHDFDatas):
            trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', key_mesh + '/points/globalIndexContinuous')
            data_read_vtk_hdf(base_mesh, key_mesh + '/points/globalIndexContinuous', conf, hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/PointData/globalIndexContinuous')

        # values read of types
        reste = hdf_group_name[7:]
        if '/KVTKHDF/types' not in base.keys():
            # TODO types devrait etre decline par nom de mesh
            if '/KVTKHDF/types' not in base_mesh or not isinstance(base_mesh['/KVTKHDF' + reste + '/types'], KVTKHDFDatas):
                trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', key_mesh + '/KVTKHDF/types')
                data_read_vtk_hdf(base_mesh, '/KVTKHDF' + reste + '/types', conf, hdf_group_name + '/Types')

            # TODO il vaudrait mieux au lieu de np.int8 le type pour types cell code
            vtk2code = np.zeros(code2vtk.max() + 1, dtype=np.int8)
            for key, value in enumerate(code2vtk):
                vtk2code[value] = key

            # lazy compute real values of types
            # lazy since read
            # TODO use vectorize
            # TODO il vaudrait mieux en place de types.dtype mettre le type pour types cell code
            command = """
vtk2code = Params[0]
types = Base[Params[1]].get()
Ret = vtk2code[types]
"""
            trace('KDIWriterVTKHDF remplacement KDIEvalString ', key_mesh + '/cells/types')
            base_mesh[key_mesh + '/cells/types'] = KDIEvalString(base_mesh, key_mesh + '/cells/types',
                                                                 command, [vtk2code, '/KVTKHDF' + reste + '/types'])

        if not isinstance(base_mesh[key_mesh + '/cells/connectivity'], KVTKHDFDatas):
            trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', key_mesh + '/cells/connectivity')
            data_read_vtk_hdf(base_mesh, key_mesh + '/cells/connectivity', conf, hdf_group_name + '/Connectivity')

        for field_key in base_mesh[key_mesh + '/cells/fields'].get():
            if not isinstance(base_mesh[field_key], KVTKHDFDatas):
                field_name = field_key[field_key.rfind('/') + 1:]
                trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', field_key)
                data_read_vtk_hdf(base_mesh, field_key, conf, hdf_group_name + '/CellData/' + field_name)

        if key_mesh + '/cells/globalIndexContinuous' not in base_mesh or not isinstance(base_mesh[key_mesh + '/cells/globalIndexContinuous'], KVTKHDFDatas):
            trace('KDIWriterVTKHDF remplacement KVTKHDFDatas ', key_mesh + '/cells/globalIndexContinuous')
            data_read_vtk_hdf(base_mesh, key_mesh + '/cells/globalIndexContinuous', conf,
                              hdf_group_name.replace('/VTKHDF', '/KDI') + key_mesh + '/CellData/globalIndexContinuous')

        is_parent_mesh, hdf_parent_group_name = get_parent(base_mesh, key_parent_meshes, key_mesh)
        trace('KDIWriterVTKHDF is_parent_mesh:', is_parent_mesh, hdf_parent_group_name)
        if is_parent_mesh:
            set_vtk_hdf_globaldatas(base_mesh, conf, hdf_parent_group_name)

    if is_assembly:
        hdf_group_name = '/VTKHDF/Assembly'
        try:
            fw[hdf_group_name]
        except:
            fw.create_group(hdf_group_name, track_order=True)
        trace('- CREATE GROUP: ', hdf_group_name)

        for keyblock, key_meshes in assembly.items():
            try:
                fw[hdf_group_name + '/' + keyblock]
            except:
                fw.create_group(hdf_group_name + '/' + keyblock, track_order=True)
            trace('  - CREATE GROUP: ', hdf_group_name + keyblock)
            for key_mesh in key_meshes:
                try:
                    fw[hdf_group_name + '/' + keyblock + key_mesh]
                except:
                    try:
                        fw[hdf_group_name + '/' + keyblock + key_mesh] = h5py.SoftLink('/VTKHDF' + key_mesh)
                    except:
                        raise Exception('Error on softlink on ' + '/VTKHDF' + key_mesh)
                trace('    - CREATE LINK: ', hdf_group_name + '/' + keyblock + key_mesh, 'on', '/VTKHDF' + key_mesh)

    # TODO La description des multi-materiaux sous la forme d'un submresh reste stocke dans le json, donc en mémoire
    # TODO Il faudrait etendre saveVTKHDF afin qu'il sauvegarde les valeurs step/part pour alléger le json

    fw.flush()
    trace('Checked write VTKHDF:', filename)

    base_mesh['/chunks'] = KVTKHDFStepPartChunks(base_mesh, conf['filename'], hdf_steps_key)

    step_partless = {KDI_AGREEMENT_STEPPART_VARIANT_STEP: vStep}
    KVTKHDFGarbageCollector(base, step_partless, with_clean_data_submeshes)

    tac("saveVTKHDF")

    if with_json:
        if '/before_write' not in base.keys():
            if write_json(base, filename):
                trace("saveJSON")
            else:
                trace("NO saveJSON")

    tictac_summary()

    if incoming_base_current_chunk:
        base['/chunks/current'] = incoming_base_current_chunk

    KVTKHDFCloseAll()

    gc.collect()
