import h5py
import numpy as np
import os

from pykdi.KDIDatas import KDIDatas
from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory

fpVTKHDF = {'r': {}, 'w': {}}


# TODO A refactorer GetFp, Close, CloseAll, buildkeys, GetDatas, KVTKHDFDatas

def KVTKHDFGetFp(conf: dict[str, any]):
    global fpVTKHDF
    try:
        return fpVTKHDF[conf['mode']][conf['filename']]
    except:
        fpVTKHDF[conf['mode']][conf['filename']] = h5py.File(conf['filename'], conf['mode'])
        return fpVTKHDF[conf['mode']][conf['filename']]


def KVTKHDFClose(conf: dict[str, any]):
    global fpVTKHDF
    try:
        fpVTKHDF[conf['mode']][conf['filename']].close()
    except:
        pass


def KVTKHDFCloseAll():
    global fpVTKHDF
    for mode, filenames in fpVTKHDF.items():
        for filename, fp in filenames.items():
            fp.close()
    fpVTKHDF = {'r': {}, 'w': {}}


def build_keys(key_full_fw: str, isAssembly: bool, force_common_data_offsets: bool):
    """
    Builds keys from the complete identification key of the VTK HDF file.

    From the complete identification key of the VTK HDF file, we return:
    - key_parent_fw: the parent key of the VTK HDF file;
    - key_fw: the current name key of the VTK HDF file;
    - is_key_data_parent_offsets_fw: if the parent offsets key (temporal, parts) is 'CellData' or 'PointData';
    - key_parent_offsets_fw: the parent offsets key (temporal, parts);
    - key_offsets_fw: the current name offsets key (temporal, parts);
    - key_full_offsets_fw: the complete identification offsets key (temporal, parts).

    If key_fw is 'Types', the offset keys are set to 'CellOffsets'.
    If key_fw is 'Points', the offset keys are set to 'PointOffsets'.
    If key_fw is 'Connectivity', the offset keys are set to 'ConnectivityIdOffsets'.

    If parent key contain 'CellData' or 'PointData', the parent key is prefixed with 'Offsets' and can be created
    in set_datas method.

    :param key_full_fw:
    :return: several keys
    """
    split_key_fw = key_full_fw.split('/')
    #
    key_parent_fw = '/'.join(split_key_fw[0:-1])
    key_fw = split_key_fw[-1]
    #
    with_step_shifting = False
    is_common_key_data_offsets_fw = False
    is_key_data_parent_offsets_fw = False
    #
    if key_fw in ['Types']:
        split_key_fw[-1] = 'Cell'
        is_common_key_data_offsets_fw = True
    elif key_fw in ['Offsets']:
        split_key_fw[-1] = 'Cell'
        with_step_shifting = True
        is_common_key_data_offsets_fw = True
    elif key_fw in ['NumberOfCells', 'NumberOfPoints', 'Offsets', 'NumberOfConnectivityIds']:
        return key_parent_fw, key_fw, None, None, None, None, None, None
    elif key_fw in ['Points']:
        split_key_fw[-1] = 'Point'
    elif key_fw in ['Connectivity']:
        split_key_fw[-1] = 'ConnectivityId'
    #
    if split_key_fw[-2] in ['CellData', 'PointData']:
        if force_common_data_offsets:
            # if force the use of common data offsets for data cell or point instead of one per field
            del split_key_fw[-1]
            if split_key_fw[1] == 'KDI':
                split_key_fw[1] = 'VTKHDF'
                del split_key_fw[2]
            split_key_fw[-1] = split_key_fw[-1][:-4]
            is_common_key_data_offsets_fw = True
        else:
            split_key_fw[-2] = split_key_fw[-2] + 'Offsets'
            is_key_data_parent_offsets_fw = True
    # for generic solution included assembly
    assert (split_key_fw[-1] in ['Cell', 'Point', 'ConnectivityId'])
    split_key_fw.insert(len(split_key_fw) - 1, 'Steps')

    split_key_fw[-1] = split_key_fw[-1] + 'Offsets'
    key_parent_offsets_fw = '/'.join(split_key_fw[0:-1])
    key_offsets_fw = split_key_fw[-1]
    key_full_offsets_fw = '/'.join(split_key_fw)
    return (key_parent_fw, key_fw, is_key_data_parent_offsets_fw, key_parent_offsets_fw,
            is_common_key_data_offsets_fw, key_offsets_fw, key_full_offsets_fw, with_step_shifting)


def GetDatas(fw: any, key_full_fw: str, iStep: int, iPart: float, isAssembly: bool, force_common_data_offsets: bool):
    (key_parent_fw, key_fw, _, key_parent_offsets_fw, _, key_offsets_fw, key_full_offsets_fw,
     with_step_shifting) = build_keys(key_full_fw, isAssembly, force_common_data_offsets)
    # --- check
    try:
        dset = fw[key_full_fw]
    except KeyError:
        raise KDIException('pre: \'' + key_full_fw + '\' not exist in \'fw\'!')

    try:
        dset_offsets_steps = fw[key_full_offsets_fw]
    except KeyError:
        raise KDIException('pre: \'' + key_full_offsets_fw + '\' not exist in \'fw\'!')

    # for generic solution included assembly
    assert (key_full_offsets_fw.split('/')[-1] in ['CellOffsets', 'CellDataOffsets', 'PointOffsets', 'PointDataOffsets', 'ConnectivityIdOffsets'])
    split_key_full_offsets_fw = key_full_offsets_fw.split('/')
    if split_key_full_offsets_fw[-1] in ['CellOffsets', 'CellDataOffsets']:
        split_key_full_offsets_fw.pop(-1)
        split_key_full_offsets_fw[-1] = 'NumberOfCells'
    elif split_key_full_offsets_fw[-1] in ['PointOffsets', 'PointDataOffsets']:
        split_key_full_offsets_fw.pop(-1)
        split_key_full_offsets_fw[-1] = 'NumberOfPoints'
    else:
        assert (split_key_full_offsets_fw[-1] == 'ConnectivityIdOffsets')
        split_key_full_offsets_fw.pop(-1)
        split_key_full_offsets_fw[-1] = 'NumberOfConnectivityIds'
    key_tmp_fw = '/'.join(split_key_full_offsets_fw)
    try:
        dset_offsets = fw[key_tmp_fw]
    except KeyError:
        raise KDIException('pre: \'' + key_tmp_fw + '\' not exist in \'fw\'!')
    intra = '/' + '/'.join(split_key_full_offsets_fw[2:-1])

    # --- first and last offset
    try:
        origin_offset = fw['/VTKHDF' + intra + '/Steps/PartOffsets'][iStep]
        number_of_parts = fw['/VTKHDF' + intra + '/Steps/NumberOfParts'][iStep]
        offsets_step = dset_offsets_steps[iStep]
        offsets_parts = dset_offsets[origin_offset:origin_offset+number_of_parts]
        first_offset = offsets_step
        last_offset = offsets_step + offsets_parts[0]
        for i in range(1, iPart+1):
            first_offset = last_offset
            last_offset = last_offset + offsets_parts[iPart]
    except KeyError:
        # TODO CHanger le message d'erreur
        raise KDIException('pre: not values in \'' + key_full_offsets_fw + '\'([' + iStep +
                           ',' + str(iStep + 1) + '] must be <' + str(dset_offsets.size) + ') in fw!')
    # --- get values
    try:
        datas = np.copy(fw[key_full_fw][first_offset:last_offset])
    except KeyError:
        raise KDIException('pre: not values in \'' + key_full_fw + '\'([' + first_offset +
                           ',' + str(last_offset) + '] must be <' + str(dset_offsets.size) + ') in fw!')
    if len(datas.shape) == 1:
        return datas.reshape((last_offset - first_offset))
    return datas.reshape((last_offset - first_offset, datas.shape[1]))


#TODO renommer KVTKHDFDatas en KVTKHDFReadDatas
class KVTKHDFDatas(KDIDatas):
    __slots__ = 'base', 'key_base', 'conf', 'hdf_fullname', 'kmemo'

    def __init__(self, base: dict[str, any], key_base: str, conf: dict[str, any], hdf_fullname: str):
        super().__init__()
        self.base = base
        self.key_base = key_base
        self.conf = conf
        self.hdf_fullname = hdf_fullname
        self.kmemo = KDIMemory(base=base, key_base=self.key_base)

    def insert(self, data, chunk=None) -> None:
        self.kmemo.insert(data, chunk)

    def erase(self, chunk=None):
        try:
            return self.kmemo.erase(chunk)
        except:
            pass

    def get(self, chunk=None) -> any:
        try:
            return self.kmemo.get(chunk)
        except:
            current_chunk = self.base['/chunks'].string_chunk(chunk, False)
            vStep = current_chunk['step']
            iStep = self.base['/chunks'].GetOffset('step', vStep)
            iPart = current_chunk['part']
            isAssembly = False
            if '/assembly' in self.base:
                isAssembly = True
                print('WARNING You don\'t have load VTK HDF with assembly!')
            return GetDatas(KVTKHDFGetFp(self.conf), self.hdf_fullname, iStep, iPart, isAssembly,
                            self.base['/vtkhdf/force_common_data_offsets'])

    def light_dump(self) -> str:
        msg = 'KVTKHDFDatas(base=KDIBase, key_base=\''
        msg += self.key_base
        msg += '\', conf='
        msg += str(self.conf)
        msg += ', hdf_fullname=\''
        msg += self.hdf_fullname
        msg += '\', datas='
        msg += self.kmemo.light_dump()
        msg += ')'
        return msg

    def __repr__(self) -> str:
        return 'KVTKHDFDatas(KDIBase, \'' + self.key_base + '\', ' + str(self.conf) + ', \'' + self.hdf_fullname + '\')'

    def __sizeof__(self) -> int:
        return self.kmemo.__sizeof__()


def data_read_vtk_hdf(base: dict[str, any], key_base: str, conf: dict[str, any], hdf_fullname: str):
    base[key_base] = KVTKHDFDatas(base, key_base, conf, hdf_fullname)
