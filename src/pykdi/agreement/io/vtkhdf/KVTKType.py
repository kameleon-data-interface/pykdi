import numpy as np

from pykdi.KDIType import KDI_TRIANGLE, KDI_QUADRANGLE, KDI_POLYGON, KDI_HEXAHEDRON, KDI_PIT, KDI_SIZE, kdi2nbPoints

KVTK_EMPTY_CELL: int = 0
KVTK_VERTEX: int = 1
KVTK_POLY_VERTEX: int = 2
KVTK_LINE: int = 3
KVTK_POLY_LINE: int = 4
KVTK_TRIANGLE: int = 5
KVTK_TRIANGLE_STRIP: int = 6
KVTK_POLYGON: int = 7
KVTK_PIXEL: int = 8
KVTK_QUAD: int = 9
KVTK_TETRA: int = 10
KVTK_VOXEL: int = 11
KVTK_HEXAHEDRON: int = 12
KVTK_WEDGE: int = 13
KVTK_PYRAMID: int = 14
KVTK_QUADRATIC_EDGE: int = 21
KVTK_QUADRATIC_TRIANGLE: int = 22
KVTK_QUADRATIC_QUAD: int = 23
KVTK_QUADRATIC_TETRA: int = 24
KVTK_QUADRATIC_HEXAHEDRON: int = 25
KVTK_CONVEX_POINT_SET: int = 41
KVTK_PARAMETRIC_CURVE: int = 51
KVTK_PARAMETRIC_TRI_SURFACE: int = 53
KVTK_PARAMETRIC_QUAD_SURFACE: int = 54
KVTK_PARAMETRIC_TETRA_REGION: int = 55
KVTK_PARAMETRIC_HEX_REGION: int = 56

KVTK_PIT: int = 56 + 1  # +1 de la valeur d'enumeration la plus elevee
KVTK_SIZE: int = KVTK_PIT + 1

dict_kdi2vtk = {
    KDI_TRIANGLE: KVTK_TRIANGLE,
    KDI_QUADRANGLE: KVTK_QUAD,
    KDI_POLYGON: KVTK_POLYGON,
    KDI_HEXAHEDRON: KVTK_HEXAHEDRON}

kdi2vtk = np.full(shape=KDI_SIZE, fill_value=KVTK_PIT, dtype=np.int8)
for k, v in dict_kdi2vtk.items():
    assert (k < len(kdi2vtk))
    kdi2vtk[k] = v

dict_vtk2kdi = {
    KVTK_TRIANGLE: KDI_TRIANGLE,
    KVTK_QUAD: KDI_QUADRANGLE,
    KVTK_POLYGON: KDI_POLYGON,
    KVTK_HEXAHEDRON: KDI_HEXAHEDRON}

vtk2kdi = np.full(shape=KVTK_SIZE, fill_value=KDI_PIT, dtype=np.int8)
for k, v in dict_vtk2kdi.items():
    assert (k < len(vtk2kdi))
    vtk2kdi[k] = v

vtk2nbPoints = kdi2nbPoints[vtk2kdi]
