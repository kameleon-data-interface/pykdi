import hashlib
from mpi4py import MPI
import numpy as np

from pykdi.tools.kdi_get_env import kdi_get_env
from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART
from pykdi.agreement.io.vtkhdf.KVTKType import KDI_PIT, dict_kdi2vtk

kdi_vtk_hdf_check_trace = kdi_get_env('KDI_VTK_HDF_CHECK_TRACE', False)


def KDIVTKHDFCheck(base: dict[str, any]):
    ignored = set()

    if '/agreement' not in base or base['/agreement'].get({})[0] != KDI_AGREEMENT_STEPPART:
        print('KDIVTKHDFCheck Invalid base \'/agreement\' not in base or is not KDI_AGREEMENT_STEPPART!')
        return False
    if '/code2kdi' not in base:
        print('KDIVTKHDFCheck Invalid base \'/code2kdi\' not in base!')
        return False

    if MPI.COMM_WORLD.Get_size() > 1:
        # This part validates if the description is standardized, the same, on all processes.
        # This is done by checking that we have the same keys.
        str_keys = str(sorted(base.keys()))
        hash = hashlib.md5()
        hash.update(str_keys.encode('utf-8'))
        in_data = np.array([v for v in hash.digest()], dtype=np.int16)
        out_data = np.empty((in_data.size,), dtype=np.int16)
        MPI.COMM_WORLD.Allreduce(sendbuf=[in_data, MPI.INT], recvbuf=[out_data, MPI.INT], op=MPI.MAX)
        assert (np.all(in_data == out_data))
        out_data = np.empty((in_data.size,), dtype=np.int16)
        MPI.COMM_WORLD.Allreduce(sendbuf=[in_data, MPI.INT], recvbuf=[out_data, MPI.INT], op=MPI.MIN)
        assert (np.all(in_data == out_data))
        # In this way, we force the code to make a uniform declaration on the processes
        # But we could also validate the consistency between the processes, complete
        # the description if necessary, this should then be done when we produce the JSON file

    check = True
    for keycode, keykdi in enumerate(base['/code2kdi']):
        if keykdi == KDI_PIT:
            continue
        if keykdi not in dict_kdi2vtk.keys():
            print('KDIVTKHDFCheck Invalid keykdi (' + str(keykdi), ') in \'/code2kdi\' for keycode:\'' + str(keycode) + '\'')
            check = False
    if not check:
        return False

    ignored.add('')
    ignored.add('/agreement')
    ignored.add('/chunks')
    ignored.add('/chunks/current')
    ignored.add('/vtkhdf')
    ignored.add('/vtkhdf/version')
    ignored.add('/vtkhdf/force_common_data_offsets')
    ignored.add('/code2kdi')
    ignored.add('/code2nbPoints')

    for key, item in base.items():
        if key == '/glu' or key[:5] == '/glu/':
            ignored.add(key)
        if key == '/study' or key[:7] == '/study/':
            ignored.add(key)
        if key == '/fields' or key[:7] == '/fields/':
            ignored.add(key)

        if key == '/caching' or key[:9] == '/caching/':
            ignored.add(key)

        if key == '/KVTKHDF' or key[:9] == '/KVTKHDF/':
            ignored.add(key)

        try:
            if item.get_complex_type() == 'UnstructuredGrid':
                check = True
                for attr in ['/cells', '/cells/types',
                             '/cells/offsets', '/cells/connectivity',
                             '/points', '/points/globalIndexContinuous', '/points/cartesianCoordinates',
                             '/fields', '/cells/fields', '/points/fields', ]:
                    if key + attr not in base:
                        print('KDIVTKHDFCheck BAD', key + attr)
                        check = False

                how_much = 0
                for attr in ['/cells/globalIndexContinuous', '/points/globalIndexContinuous']:
                    if key + attr in base:
                        how_much += 1
                if how_much not in [0, 2]:
                    print('KDIVTKHDFCheck BAD All globalIndexContinuous (/cells and /points) or nothing!')
                    check = False

                if check:
                    ignored.add(key)
                    for attr in ['/cells', '/cells/globalIndexContinuous', '/cells/types',
                                 '/cells/offsets', '/cells/connectivity', '/points', '/points/globalIndexContinuous',
                                 '/points/cartesianCoordinates', '/fields', '/cells/fields', '/points/fields', ]:
                        ignored.add(key + attr)
                    if kdi_vtk_hdf_check_trace:
                        print('+', key)
        except:
            pass

    ignored.add('/assembly')
    ignored.add('/before_write')

    check = True
    for key in base.keys():
        if key not in ignored:
            if key.find('/fields/') != -1:
                continue
            if kdi_vtk_hdf_check_trace:
                print('?', key)
            check = False

    return check
