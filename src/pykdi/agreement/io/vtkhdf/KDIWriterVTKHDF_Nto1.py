import h5py
from mpi4py import MPI
import numpy as np
import os
import os.path as osp

from pykdi.agreement.step_part.kdi_agreement_steppart import KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART

from pykdi.KDIException import KDIException

from .KVTKHDFDatas import KVTKHDFCloseAll, build_keys
from .KVTKHDFGarbageCollector import KVTKHDFGarbageCollector
from .KVTKType import kdi2vtk, vtk2kdi, vtk2nbPoints
from .KDIWriterVTKHDF_common import (get_in_base, get_hdf_group, set_hdf_type_attr, set_hdf_version_attr,
                                     set_hdf_steps_group_nsteps_attr, set_hdf_steps_group_values,
                                     set_hdf_steps_group_number_of_parts, set_hdf_steps_group_parts_offsets,
                                     set_hdf_group_nb_of_dataset, set_hdf_group_points_dataset,
                                     set_hdf_group_types_dataset, set_hdf_group_fields_dataset,
                                     set_hdf_group_global_fields_dataset)
from .KDIWriterVTKHDF_common import KVTKHDF_STEPS_CHUNK_SIZE, KVTKHDF_CHUNK_SIZE, trace, kdi_vtk_hdf_trace, tic, tac, tictac_summary


class KDIExceptionVTKHDF_Nto1(KDIException):
    """
    Specific Exception
    """
    pass


def write_vtk_hdf_n_to_1_unstructured_grid(fw, base, step_value, step_index, code2vtk, key_mesh, group_hdf, hdf_parent_group_name):
    tic("save_vtk_hdf_unstructured_grid__" + key_mesh)

    hdf_wfd = fw
    hdf_group_name = group_hdf

    hdf_group = get_hdf_group(hdf_wfd, hdf_group_name)
    if hdf_parent_group_name:
        hdf_parent_group = get_hdf_group(hdf_wfd, hdf_parent_group_name)
    else:
        hdf_parent_group = None

    set_hdf_type_attr(hdf_group, 'UnstructuredGrid')
    set_hdf_version_attr(hdf_group, base['/vtkhdf/version'].get())

    hdf_steps_group = get_hdf_group(hdf_wfd, hdf_group_name + '/Steps')

    set_hdf_steps_group_nsteps_attr(hdf_steps_group, step_index)

    set_hdf_steps_group_values(hdf_steps_group, step_index, step_value)

    set_hdf_steps_group_number_of_parts(hdf_steps_group, step_index)

    set_hdf_steps_group_parts_offsets(hdf_steps_group, step_index)

    tic("set_hdf_group_nb_of_dataset Point")
    (first_offset_points, after_last_offset_points, global_nb_points, sorted_indices_points,
     points_global_index_continuous, offsets_loc2glo_points) = (
        set_hdf_group_nb_of_dataset(step_index,
                                    base, key_mesh + '/points',
                                    hdf_group, 'NumberOfPoints',
                                    hdf_steps_group, 'PointOffsets'))
    tac("set_hdf_group_nb_of_dataset Point")

    tic("set_hdf_group_nb_of_dataset Cell")
    (first_offset_cells, after_last_offset_cells, global_nb_cells, sorted_indices_cells,
     cells_global_index_continuous, offsets_loc2glo_cells) = (
        set_hdf_group_nb_of_dataset(step_index,
                                    base, key_mesh + '/cells',
                                    hdf_group, 'NumberOfCells',
                                    hdf_steps_group, 'CellOffsets'))
    tac("set_hdf_group_nb_of_dataset Cell")

    tic("set_hdf_group_points_dataset Points coordinates")
    set_hdf_group_points_dataset(after_last_offset_points, sorted_indices_points, offsets_loc2glo_points,
                                 base, key_mesh, hdf_group)
    tac("set_hdf_group_points_dataset Points coordinates")

    hdf_points_data_group = get_hdf_group(hdf_group, 'PointData')

    tic("set_hdf_group_fields_dataset PointData")
    set_hdf_group_fields_dataset(after_last_offset_points, sorted_indices_points, offsets_loc2glo_points,
                                 base, key_mesh + '/points/fields', hdf_points_data_group)
    tac("set_hdf_group_fields_dataset PointData")

    tic("compute Connectivity Offsets")
    local_cells_connectivity_nb_points = set_hdf_group_types_dataset(
        after_last_offset_cells, sorted_indices_cells, offsets_loc2glo_cells, base, key_mesh, hdf_group)
    # Offsets is the value of the local offset (at a time) of each cell in the connectivity table
    try:
        dset = hdf_group['Offsets']
    except KeyError:
        dset = hdf_group.create_dataset(
            name='Offsets', shape=(1, ), dtype=np.int64, maxshape=(None, ), chunks=(KVTKHDF_CHUNK_SIZE, ))
    dset.resize((after_last_offset_cells + step_index + 1,))

    # NumberOfConnectivityIds is the size of the cells connectivity table
    try:
        dset_nb = hdf_group['NumberOfConnectivityIds']
    except KeyError:
        dset_nb = hdf_group.create_dataset(
            name='NumberOfConnectivityIds', data=np.array([0], dtype=np.int64), shape=(1, ), dtype=np.int64,
            maxshape=(None, ), chunks=(KVTKHDF_STEPS_CHUNK_SIZE, ))
    dset_nb.resize((step_index + 1,))

    # Steps/ConnectivityIdOffsets is the temporal offset
    try:
        dset_nb_off = hdf_steps_group['ConnectivityIdOffsets']
    except KeyError:
        dset_nb_off = hdf_steps_group.create_dataset(
            name='ConnectivityIdOffsets', data=np.array([0], dtype=np.int64), shape=(1, ), dtype=np.int64,
            maxshape=(None, ), chunks=(KVTKHDF_STEPS_CHUNK_SIZE, ))
    dset_nb_off.resize((step_index + 2,))

    global_cells_connectivity_offsets = np.zeros((global_nb_cells + 1), dtype=np.int64)
    
    # Mode parallel on local description (non implemented)
    # - get local Types
    # - compute local cells connectivity nb points
    # - but not simply compute np.cumsum cause order it's not simply concatenate subdomain
    #
    # Mode sequential on global description (implemented)
    # - get Types
    # - compute cells connectivity nb points
    # - compute offsets by np.cumsum
    # - set Offsets
    #
    # TODO IMPLEMENTED
    # Mode mixte
    # - distribute evenly Types (Nto1)
    #      global_cells_nb // mpi_size with
    #           if mpi_rank < global_cells_nb % mpi_size: + mpi_rank
    #           else: + global_cells_nb % mpi_size
    # - compute on this part cells connectivity nb points (parallel compute)
    # - iteratively from process 0 to P :
    #   -- (the start offset for process 0 is 0) np.cumsum compute on this part
    #   -- set this part offsets
    #   -- send last offset next processus for the start offset
    # and more
    # - local global cells connectivity offsets compute by
    #   loc_glo_cc_off = dset[globalIndexContinuous + first_offset_temporal]
    # after compute local cells connectivity nb points :
    # and
    #    for iCell in range():
    #       dset[dset_nb_off[step_index] + loc_glo_cc_off[iCell] : dset_nb_off[step_index] + loc_glo_cc_off[iCell] + local_cells_connectivity_nb_points[iCell]] =\
    #          connectivity[local_offsets_connectivity[iCell]:local_offsets_connectivity[iCell+1]]

    if MPI.COMM_WORLD.Get_rank() == 1:
        # Work just process 0
        # Get ALL cells types
        dset = hdf_group['Types']
        assert (after_last_offset_cells - first_offset_cells == global_nb_cells)
        global_cells_types = dset[first_offset_cells:after_last_offset_cells]  # type VTK cells
        # Compute cells nb points global
        global_cells_connectivity_nb_points = vtk2nbPoints[global_cells_types]
        assert (len(global_cells_connectivity_nb_points) == global_nb_cells)
        # Compute cells offsets connectivity global
        np.cumsum(global_cells_connectivity_nb_points, out=global_cells_connectivity_offsets[1:])
        # Set global cells connectivity offsets on one step in hdf
        dset = hdf_group['Offsets']
        dset[first_offset_cells + step_index:after_last_offset_cells + step_index+1] = global_cells_connectivity_offsets[:]
        # Set global cells connectivity offsets size on one step in hdf
        dset_nb.resize((step_index + 1,))
        dset_nb[step_index] = global_cells_connectivity_offsets[-1]
        # Set global cells connectivity offsets over steps in hdf
        dset_nb_off.resize((step_index + 2,))
        dset_nb_off[step_index + 1] = dset_nb_off[step_index] + global_cells_connectivity_offsets[-1]
    # This is so that all processes have the global_cells_connectivity_offsets
    all_reduce_global_cells_connectivity_offsets = np.empty((global_nb_cells + 1), dtype=np.int64)
    MPI.COMM_WORLD.Allreduce(sendbuf=[global_cells_connectivity_offsets, MPI.LONG],
                             recvbuf=[all_reduce_global_cells_connectivity_offsets, MPI.LONG],
                             op=MPI.SUM)
    global_cells_connectivity_offsets = all_reduce_global_cells_connectivity_offsets
    # Get local connectivity as local index points
    connectivity = get_in_base(base, key_mesh + '/cells/connectivity')
    # Compute local connectivity as global index points
    connectivity = points_global_index_continuous[connectivity]
    tac("compute Connectivity Offsets")

    tic("set_hdf_group_nb_of_dataset Connectivity")
    tic("set_hdf_group_nb_of_dataset Connectivity STEP 1")
    local_offsets_connectivity = np.empty((len(local_cells_connectivity_nb_points) + 1), dtype=np.int64)
    local_offsets_connectivity[0] = 0
    np.cumsum(local_cells_connectivity_nb_points, out=local_offsets_connectivity[1:])
    tac("set_hdf_group_nb_of_dataset Connectivity STEP 1")
    try:
        dset = fw[group_hdf + '/Connectivity']
    except KeyError:
        dset = fw[group_hdf].create_dataset(name='Connectivity', shape=(1, ), dtype=np.int64,
                                            maxshape=(None, ), chunks=(KVTKHDF_CHUNK_SIZE, ))
    tic("set_hdf_group_nb_of_dataset Connectivity STEP 2")
    dset.resize((dset_nb_off[step_index + 1],))
    trace("set_hdf_group_nb_of_dataset Connectivity STEP 2")
    if True:
        tic("set_hdf_group_nb_of_dataset Connectivity STEP 3 new")
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new len:", len(connectivity))
        global_indexes = np.empty((len(connectivity),), dtype=np.int64)
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new A")
        shift_global_indexes = dset_nb_off[step_index]
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new B shift_global_indexes:", shift_global_indexes)
        for inCell, outCell in enumerate(cells_global_index_continuous):
            # assert (local_cells_connectivity_nb_points[inCell] == local_cells_connectivity_nb_points[inCell])
            trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new C #", inCell)
            in_off = shift_global_indexes + global_cells_connectivity_offsets[outCell]
            trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new C #", inCell, "in_off:", in_off)
            for inPt in range(local_cells_connectivity_nb_points[inCell]):
                global_indexes[local_offsets_connectivity[inCell] + inPt] = in_off + inPt
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new E")
        #
        # Remark:
        #    Cause TypeError: Indexing elements must be in increasing order
        #
        #   dset[global_indexes] = connectivity
        #
        # Computes sorted_indexes as sorts the indices by calling the argsort method which
        # returns the indices that would sort the array global_index_continuous.
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new F ", global_indexes)
        tic("set_hdf_group_nb_of_dataset Connectivity STEP 3 new sorted")
        sorted_indexes = np.argsort(global_indexes)
        tac("set_hdf_group_nb_of_dataset Connectivity STEP 3 new sorted")
        trace("set_hdf_group_nb_of_dataset Connectivity STEP 3 new G ", sorted_indexes)
        tic("set_hdf_group_nb_of_dataset Connectivity STEP 3 new writed")
        dset[global_indexes[sorted_indexes]] = connectivity[sorted_indexes]
        tac("set_hdf_group_nb_of_dataset Connectivity STEP 3 new writed")
        tac("set_hdf_group_nb_of_dataset Connectivity STEP 3 new")
    else:
        tic("set_hdf_group_nb_of_dataset Connectivity STEP 3")
        for inCell, outCell in enumerate(cells_global_index_continuous):
            assert(local_cells_connectivity_nb_points[inCell] == local_cells_connectivity_nb_points[inCell])
            dset[dset_nb_off[step_index]+global_cells_connectivity_offsets[outCell]:dset_nb_off[step_index]+global_cells_connectivity_offsets[outCell] + local_cells_connectivity_nb_points[inCell]] =\
                connectivity[local_offsets_connectivity[inCell]:local_offsets_connectivity[inCell+1]]
        tic("set_hdf_group_nb_of_dataset Connectivity STEP 3")
    tac("set_hdf_group_nb_of_dataset Connectivity")

    hdf_cells_data_group = get_hdf_group(hdf_group, 'CellData')

    tic("set_hdf_group_fields_dataset CellData")
    set_hdf_group_fields_dataset(after_last_offset_cells, sorted_indices_cells, offsets_loc2glo_cells,
                                 base, key_mesh + '/cells/fields', hdf_cells_data_group)
    tac("set_hdf_group_fields_dataset CellData")

    tic("set_hdf_group_global_fields_dataset")
    set_hdf_group_global_fields_dataset(step_index, base, hdf_group, hdf_parent_group)
    tac("set_hdf_group_global_fields_dataset")

    tac("save_vtk_hdf_unstructured_grid__" + key_mesh)


def write_vtk_hdf_n_to_1(base: dict[str, any], filename: str | os.PathLike, chunk=None, force_all_clean: bool = False):
    mpi_rank = MPI.COMM_WORLD.Get_rank()
    mpi_size = MPI.COMM_WORLD.Get_size()

    tic("saveVTKHDF_Nto1")
    if kdi_vtk_hdf_trace:
        trace('saveVTKHDF_Nto1 (from process %d/%d)' % (mpi_rank, mpi_size))

    KVTKHDFCloseAll()
    trace('saveVTKHDF_Nto1 close all')

    try:
        incoming_base_current_chunk = base['/chunks/current']
    except:
        incoming_base_current_chunk = None
    trace('saveVTKHDF_Nto1 incoming_base_current_chunk:', incoming_base_current_chunk)

    if chunk:
        current_chunk = chunk
    else:
        assert ('pre: The \'/chunks/current\' key must be a base' and '/chunks/current' in base)
        current_chunk = base['/chunks/current']
    trace('saveVTKHDF_Nto1 chunk:', chunk)

    for key in [KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART]:
        if key not in base['/chunks'].variants:
            trace('saveVTKHDF_Nto1 pre: \'' + key + '\' must be a base variant')
            raise KDIException('pre: \'' + key + '\' must be a base variant')

    assert ('pre: there must be two variant' and len(base['/chunks'].order) == 2)
    trace('checked pre: there must be two variant')

    assert ('pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_STEP + '\' must be a first variant order' and
            base['/chunks'].order[0] == KDI_AGREEMENT_STEPPART_VARIANT_STEP)
    trace('checked pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_STEP + '\' must be a first variant order')

    assert ('pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_PART + '\' must be a second variant order' and
            base['/chunks'].order[1] == KDI_AGREEMENT_STEPPART_VARIANT_PART)
    trace('checked pre: \'' + KDI_AGREEMENT_STEPPART_VARIANT_PART + '\' must be a second variant order')

    authorized_keys = ['#' + KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_STEP, '#' + KDI_AGREEMENT_STEPPART_VARIANT_PART, KDI_AGREEMENT_STEPPART_VARIANT_PART]
    for key in current_chunk.keys():
        if key not in authorized_keys:
            trace('pre: \'' + key + '\' must be an authorized key ' + str(authorized_keys))
            raise KDIException('pre: \'' + key + '\' must be an authorized key ' + str(authorized_keys))

    nb_parts = base['/chunks'].GetMaxConstantValue('part')
    trace('nb_parts:', nb_parts)

    assert ('pre: Writing is only possible in sequential or in parallel with as many processes (' +
            str(mpi_size) + ') as there are subdomains (' + str(nb_parts) + ')' and
            (mpi_size == 1 or mpi_size == nb_parts))
    trace('checked pre: Writing is only possible in sequential or in parallel with as many processes (' +
          str(mpi_size) + ') as there are subdomains (' + str(nb_parts) + ')')

    step_value = current_chunk[KDI_AGREEMENT_STEPPART_VARIANT_STEP]
    trace('step_value:', step_value)
    step_index = base['/chunks'].GetOffset(KDI_AGREEMENT_STEPPART_VARIANT_STEP, step_value)
    trace('step_index:', step_index)

    # TODO il faudrait verifier la coherence du dictionnaire
    # TODO verifier qu'on écrit bien le même temps step_value/step_index, avec le même nombre de parts

    variants = {'step': step_value, 'part': mpi_rank}
    base['/chunks'].set(variants)

    trace('variants:', variants)

    assert ('pre: \'/code2kdi\' must be in base)' and '/code2kdi' in base)

    code2kdi = base['/code2kdi']
    code2vtk = kdi2vtk[code2kdi]

    # This version does not differentiate the description of points/cells offsets
    # vs description data points/cells offsets
    # TODO Ici il pourrait y avoir intérêt de le faire si la topologie ne change pas, les coordonnées ne change pas...
    try:
        force_common_data_offsets = base['/vtkhdf/force_common_data_offsets']
    except KeyError:
        raise KDIException('\'/vtkhdf/force_common_data_offsets\' not exists in base!')

    # It's multi-mil?
    key_meshes = set()
    assembly = None
    is_assembly = False
    key_parent_meshes = []
    if '/assembly' in base:
        assembly = base['/assembly']
        trace('Not implemented write VTK HDF with assembly!')
        trace(assembly)
        for key, value in assembly.items():
            if isinstance(value, set):
                for key_mesh in value:
                    key_meshes.add(key_mesh)
        # TODO parce que la clef mesh de assembly ne comporte pas de / on doit la rajouter
        key_parent_meshes.append('/' + key)
        trace('key_parent_meshes', key_parent_meshes)
        trace('key_meshes', key_meshes)
        is_assembly = True
    else:
        for key, item in base.items():
            try:
                if item.get_complex_type() == 'UnstructuredGrid':
                    if len(key_meshes) > 0:
                        raise Exception('For multi-mesh or multi-mil, you can call KDIComputeMultiMilieux!')
                    key_meshes.add(key)
                    key_parent_meshes.append(key)
            except:
                continue
        trace('key_parent_meshes', key_parent_meshes)
        trace('key_meshes', key_meshes)

    key_meshes = sorted(key_meshes)
    trace('sorted key_meshes', key_meshes)

    if kdi_vtk_hdf_trace:
        trace('saveVTKHDF_Nto1 Check chunk')

    trace('saveVTKHDF_Nto1 -> h5py.File')
    # open HDF file: https://docs.h5py.org/en/3.7.0/high/file.html#reference
    h5py_file_params = {'name': filename}
    if mpi_size > 1:
        # Setting up parallel execution mode
        h5py_file_params.update({'driver': 'mpio', 'comm': MPI.COMM_WORLD})
    if not osp.isfile(filename):
        # File creation stage
        # It is only at creation that we define the strategy whose persist mode
        h5py_file_params.update({'mode': 'w', 'track_order': True, 'fs_strategy': 'fsm', 'fs_persist': True})
    else:
        # File completion stage
        h5py_file_params.update({'mode': 'a'})
    fw = h5py.File(**h5py_file_params)

    if kdi_vtk_hdf_trace:
        trace('saveVTKHDF Check open', filename)

    vtkhdf_hdf_group = get_hdf_group(fw, '/VTKHDF')

    # Attribut Type
    if is_assembly:
        set_hdf_type_attr(vtkhdf_hdf_group, 'MultiBlockDataSet')
    else:
        set_hdf_type_attr(vtkhdf_hdf_group, 'UnstructuredGrid')
    set_hdf_version_attr(vtkhdf_hdf_group, base['/vtkhdf/version'].get())

    trace('saveVTKHDF_Nto1 Check Version')

    for key_mesh in key_meshes:
        if is_assembly:
            group_hdf = '/VTKHDF' + key_mesh
        else:
            group_hdf = '/VTKHDF'

        parent_group_hdf = None
        if '/assembly' in base:
            assembly = base['/assembly']
            for key, value in assembly.items():
                # TODO ATTENTION La clef mesh de assembly ne comporte pas de /
                for key_submesh in value:
                    if key_mesh == key_submesh:
                        parent_group_hdf = '/VTKHDF/' + key
                        break

        write_vtk_hdf_n_to_1_unstructured_grid(fw, base, step_value, step_index, code2vtk, key_mesh, group_hdf, parent_group_hdf)

        # TODO A traiter /fields

        conf = {'filename': filename, 'mode': 'r'}

    if is_assembly:
        hdf_group = get_hdf_group(vtkhdf_hdf_group, 'Assembly')
        group_hdf = '/VTKHDF/Assembly'
        try:
            fw[group_hdf]
        except:
            fw.create_group(group_hdf, track_order=True)
        trace('- CREATE GROUP: ', group_hdf)

        for keyblock, key_meshes in assembly.items():
            try:
                fw[group_hdf + '/' + keyblock]
            except:
                fw.create_group(group_hdf + '/' + keyblock, track_order=True)
            trace('  - CREATE GROUP: ', group_hdf + keyblock)
            for key_mesh in key_meshes:
                try:
                    fw[group_hdf + '/' + keyblock + key_mesh]
                except:
                    try:
                        fw[group_hdf + '/' + keyblock + key_mesh] = h5py.SoftLink('/VTKHDF' + key_mesh)
                    except:
                        raise Exception('Error on softlink on ' + '/VTKHDF' + key_mesh)
                trace('    - CREATE LINK: ', group_hdf + '/' + keyblock + key_mesh, 'on', '/VTKHDF' + key_mesh)

    fw.close()
    trace('Checked write VTKHDF:', filename)

    tac("saveVTKHDF_Nto1")
    tictac_summary()

    if force_all_clean:
        step_partless = {'step': step_value}
        KVTKHDFGarbageCollector(base, step_partless, True, force=force_all_clean)

    if incoming_base_current_chunk:
        base['/chunks/current'] = incoming_base_current_chunk
