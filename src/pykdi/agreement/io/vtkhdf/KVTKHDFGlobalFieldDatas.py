import h5py
import numpy as np
import os

from pykdi.KDIDatas import KDIDatas
from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory

from .KVTKHDFDatas import fpVTKHDF, KVTKHDFGetFp


def GetGlobalFieldDatas(fw: any, key_full_fw: str, iStep: int):
    split_key_fw = key_full_fw.split('/')
    key_full_offsets_fw = '/'.join(split_key_fw[0:-2]) + '/Steps/FieldDataOffsets/' + split_key_fw[-1]
    # --- check
    try:
        dset = fw[key_full_fw]
    except KeyError:
        raise KDIException('pre: \'' + key_full_fw + '\' not exist in \'fw\'!')

    try:
        dset_offsets_steps = fw[key_full_offsets_fw]
    except KeyError:
        raise KDIException('pre: \'' + key_full_offsets_fw + '\' not exist in \'fw\'!')

    # --- first and last offset
    try:
        origin_offset = fw['/VTKHDF/Steps/PartOffsets'][iStep]
        number_of_parts = fw['/VTKHDF/Steps/NumberOfParts'][iStep]
        offsets = dset_offsets_steps[iStep:iStep + 2]
        first_offset = offsets[0]
        last_offset = offsets[1]
    except KeyError:
        # TODO CHanger le message d'erreur
        raise KDIException('pre: not values in \'' + key_full_offsets_fw + '\'([' + iStep +
                           ',' + str(iStep + 1) + '] must be <' + str(dset_offsets_steps.size) + ') in fw!')
    # --- get values
    try:
        datas = np.copy(fw[key_full_fw][first_offset:last_offset])
    except KeyError:
        raise KDIException('pre: not values in \'' + key_full_fw + '\'([' + first_offset +
                           ',' + str(last_offset) + '] must be <' + str(dset_offsets_steps.size) + ') in fw!')
    if len(datas.shape) == 1:
        return datas.reshape((last_offset - first_offset))
    return datas.reshape((datas.shape[1], ))


class KVTKHDFGlobalFieldDatas(KDIDatas):
    __slots__ = 'base', 'conf', 'fullname', 'kmemo'

    def __init__(self, base: dict[str, any], conf: dict[str, any], fullname: str | os.PathLike):
        super().__init__()
        self.base = base
        self.key_base = ''  #TODO A passer en parametre
        self.conf = conf
        self.fullname = fullname
        self.kmemo = KDIMemory(base=base, key_base=self.key_base)

    def insert(self, data, chunk=None) -> None:
        self.kmemo.insert(data, chunk)

    def erase(self, chunk=None) -> None:
        try:
            return self.kmemo.erase(chunk)
        except:
            pass

    def get(self, chunk=None) -> any:
        try:
            return self.kmemo.get(chunk)
        except:
            current_chunk = self.base['/chunks'].string_chunk(chunk, False)
            vStep = current_chunk['step']
            iStep = self.base['/chunks'].GetOffset('step', vStep)
            return GetGlobalFieldDatas(KVTKHDFGetFp(self.conf), self.fullname, iStep)

    def light_dump(self) ->str:
        msg = 'KVTKHDFDatas(base=KDIBase, conf='
        msg += str(self.conf)
        msg += ', key_base=\''
        msg += self.key_base
        msg += '\', datas='
        msg += self.kmemo.light_dump()
        msg += ')'
        return msg

    def __repr__(self) -> str:
        return 'KVTKHDFGlobalFieldDatas(KDIBase, ' + str(self.conf) + ', \'' + self.fullname + '\')'

    def __sizeof__(self) -> int:
        return self.kmemo.__sizeof__()
