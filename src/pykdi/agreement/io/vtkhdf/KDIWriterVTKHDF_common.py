import h5py
from mpi4py import MPI
import numpy as np
import time

from pykdi.tools import kdi_get_env, kdi_get_env_int
from pykdi.KDIException import KDIException
from pykdi.KDIType import kdi2nbPoints

from .KVTKType import kdi2vtk
from .KVTKHDFGlobalFieldDatas import KVTKHDFGlobalFieldDatas

KVTKHDF_CHUNK_SIZE = 131072  # 128k
KVTKHDF_CHUNK_SIZE = 32768  # 32k
KVTKHDF_CHUNK_SIZE = 262144  # 256k
KVTKHDF_CHUNK_SIZE = 1048576  # 1M

KVTKHDF_CHUNK_SIZE = kdi_get_env_int('KDI_VTK_HDF_CHUNK_SIZE', KVTKHDF_CHUNK_SIZE)
KVTKHDF_STEPS_CHUNK_SIZE = kdi_get_env_int('KDI_VTK_HDF_STEPS_CHUNK_SIZE', 1024)

kdi_vtk_hdf_trace = kdi_get_env('KDI_VTK_HDF_TRACE', False)
kdi_vtk_hdf_trace_tictac = kdi_get_env('KDI_VTK_HDF_TRACE_TICTAC', kdi_vtk_hdf_trace)
kdi_vtk_hdf_trace_tictac_summary = kdi_get_env('KDI_VTK_HDF_TRACE_TICTAC_SUMMARY', False)

tic_tac = dict()


def trace_process():
    return "#" + str(MPI.COMM_WORLD.Get_rank()) + '/' + str(MPI.COMM_WORLD.Get_size()) + ' '


def trace(*args):
    if kdi_vtk_hdf_trace:
        print(trace_process(), *args)


def tic(msg):
    if not kdi_vtk_hdf_trace_tictac and not kdi_vtk_hdf_trace_tictac_summary:
        return
    global tic_tac
    if msg in tic_tac:
        tic_tac[msg][1] = time.time()
    else:
        tic_tac[msg] = [0, time.time(), 0]
    if kdi_vtk_hdf_trace_tictac:
        print(trace_process(), "KDI ELAPSED PYTHON TIME TIC " + msg + " " + str(tic_tac[msg]))


def tac(msg):
    if not kdi_vtk_hdf_trace_tictac and not kdi_vtk_hdf_trace_tictac_summary:
        return
    global tic_tac
    add = time.time() - tic_tac[msg][1]
    tic_tac[msg] = [tic_tac[msg][0] + 1, add, tic_tac[msg][2] + add]
    if kdi_vtk_hdf_trace_tictac:
        print(trace_process(), "KDI ELAPSED PYTHON TIME TAC " + msg + " " + str(tic_tac[msg]))


def tictac_summary():
    if not kdi_vtk_hdf_trace_tictac_summary:
        return
    for key, value in tic_tac.items():
        print(trace_process(), "KDI ELAPSED PYTHON TIME TAC " + key + " " + str(value))


def get_in_base(base: any, base_key_name: str) -> any:
    try:
        return base[base_key_name].get()
    except:
        raise KDIException(
            'pre: \'' + base_key_name + '\' must be a key of \'base\'!')


def get_hdf_group(hdf_wfd: any, hdf_group_name: str) -> any:
    """
    Get or create an hdf group from a name
    :param hdf_wfd: hdf writer file descriptor
    :param hdf_group_name: hdf group name to recover
    :return: hdf group
    """
    try:
        return hdf_wfd[hdf_group_name]
    except KeyError:
        return hdf_wfd.create_group(hdf_group_name, track_order=True)


def set_hdf_type_attr(hdf_group: any, str_value: str) -> None:
    """
    Get or create an hdf group from a hdf 'Type' attribute
    :param hdf_group: hdf group
    :param str_value: string value associated to hdf attribute name
    :return: None
    """
    # Change encoding
    strType = str_value.encode('utf-8')
    try:
        hdf_current_value = hdf_group.attrs['Type']
        # Get and compare the hdf current value if it already exists
        assert (hdf_current_value == strType)
    except:
        # Create and set the current value if it does not exist
        hdf_group.attrs.create(name='Type', data=strType, shape=None, dtype='|S' + str(len(strType)))


def set_hdf_version_attr(hdf_group: any, value: any) -> None:
    """
    Get or create an hdf group from a hdf 'Version' attribute
    :param hdf_group: hdf group
    :param value: np.int32 value associated to hdf attribute name as np.array(, dtype=np.int64 or np.int32)
    :return: None
    """
    try:
        hdf_actual_value = hdf_group.attrs['Version']
        # Get and compare the actual value if it already exists
        assert (np.all(hdf_actual_value == value))
    except:
        # Create and set the current value if it does not exist (in HDF np.int32)
        hdf_group.attrs.create(name='Version', data=value, shape=None, dtype=np.int32)


def set_hdf_steps_group_nsteps_attr(hdf_group: any, step_index: int) -> None:
    """
    Get or create an hdf group from a hdf 'Version' attribute
    :param hdf_group: hdf group
    :param step_index: simulation step index
    :return: None
    """
    try:
        # Get the actual value if it already exists
        hdf_actual_value = np.array(hdf_group.attrs['NSteps'])
        assert ('pre: \'step index\' (' + str(step_index) + ') must be positif and less than or equal to ' +
                'hdf actual value (' + str(hdf_actual_value) + ')' and step_index <= hdf_actual_value)
        # What action
        if step_index < hdf_actual_value:
            if step_index == 0:
                trace('add by full truncature and rewrite step', str(step_index), '/', str(hdf_actual_value))
            else:
                trace('add by simple truncature to', str(step_index - 1), 'and rewrite step',
                      str(step_index), '/', str(hdf_actual_value))
        elif hdf_actual_value == step_index:
            trace('add new write step', str(step_index), '/', str(hdf_actual_value))
        # Set the current value if it alrea dy exists
        hdf_group.attrs['NSteps'] = step_index + 1
    except:
        # Set the zero value if it does not exist (in HDF np.int32)
        hdf_group.attrs.create(name='NSteps', data=np.array(step_index + 1), shape=(1,), dtype=np.int32)


def set_hdf_steps_group_dataset(hdf_steps_group: any, step_index: int, hdf_name_dataset: str,
                                value_dataset: any, value_dtype: any) -> any:
    try:
        dset = hdf_steps_group[hdf_name_dataset]
        dset.resize((step_index + 1,))
        dset[step_index] = value_dataset
    except:
        hdf_steps_group.create_dataset(name=hdf_name_dataset,
                                       data=np.array([value_dataset, ], dtype=value_dtype),
                                       shape=(1,), dtype=value_dtype, maxshape=(None,),
                                       chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))


def set_hdf_steps_group_offset_dataset(hdf_steps_group: any, step_index: int, hdf_name_dataset: str,
                                       value_dataset: any, value_dtype: any) -> any:
    try:
        dset = hdf_steps_group[hdf_name_dataset]
        dset.resize((step_index + 2,))
        dset[step_index + 1] = dset[step_index] + value_dataset
    except:
        hdf_steps_group.create_dataset(name=hdf_name_dataset,
                                       data=np.array([0, value_dataset, ], dtype=value_dtype),
                                       shape=(2,), dtype=value_dtype, maxshape=(None,),
                                       chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))


def set_hdf_steps_group_values(hdf_steps_group, step_index, step_value):
    set_hdf_steps_group_dataset(hdf_steps_group, step_index, 'Values', step_value, np.float64)


def set_hdf_steps_group_number_of_parts(hdf_steps_group, step_index):
    set_hdf_steps_group_dataset(hdf_steps_group, step_index, 'NumberOfParts', 1, np.int64)


def set_hdf_steps_group_parts_offsets(hdf_steps_group, step_index):
    set_hdf_steps_group_offset_dataset(hdf_steps_group, step_index, 'PartOffsets', 1, np.int64)


def set_hdf_group_nb_of_dataset(step_index,
                                base, base_key_name,
                                hdf_group, hdf_name_nb_of_dataset,
                                hdf_steps_group, hdf_name_offsets_dataset):
    """
    :param step_index: simulation step index
    :param base_key_name: 'points' or 'cells'
    :param hdf_group: hdf group
    :param hdf_name_nb_of_dataset: 'NumberOfPoints' or 'NumberOfCells'
    :param hdf_steps_group: hdf steps group
    :param hdf_name_offsets_dataset: 'PointOffsets' or 'CellOffsets'
    """
    global_index_continuous = get_in_base(base, base_key_name + '/globalIndexContinuous')

    # Locally computes the maximum value of the continuous global index
    max_value = global_index_continuous.max()
    # Global computes the maximum value by Allreduce(MPI.MAX)
    in_max_value = np.full(shape=(1,), fill_value=max_value, dtype=np.int64)
    out_max_value = np.zeros(shape=(1,), dtype=np.int64)
    MPI.COMM_WORLD.Allreduce(sendbuf=[in_max_value, MPI.LONG], recvbuf=[out_max_value, MPI.LONG], op=MPI.MAX)
    # Global computes the number of values
    nb_of = out_max_value[0] + 1

    #----------- Necessary if it is a sub-mesh

    ## Built an array with 0 as default and 1 for positions associated with global_index_continuous
    ## which is not continuous in the case of a sub-mesh
    in_max_value = np.full(shape=(nb_of,), fill_value=0, dtype=np.int64)
    in_max_value[global_index_continuous] = 1
    out_max_value = np.zeros(shape=(nb_of,), dtype=np.int64)
    MPI.COMM_WORLD.Allreduce(sendbuf=[in_max_value, MPI.LONG], recvbuf=[out_max_value, MPI.LONG], op=MPI.MAX)
    ## Build an increasing index first by doing an np.cumsum then removing 1 in order to
    ## have an index that goes from 0 to , for valid values (elsewhere it will also be 0)
    np.cumsum(out_max_value, out=in_max_value)
    in_max_value -= 1
    ## Mutates global index continuous on which we applied a selection
    ## to a real global index continuous for this sub-mesh
    global_index_continuous = in_max_value[global_index_continuous]

    # Locally computes the maximum value of the continuous global index
    max_value = global_index_continuous.max()
    # Global computes the maximum value by Allreduce(MPI.MAX)
    in_max_value = np.full(shape=(1,), fill_value=max_value, dtype=np.int64)
    out_max_value = np.zeros(shape=(1,), dtype=np.int64)
    MPI.COMM_WORLD.Allreduce(sendbuf=[in_max_value, MPI.LONG], recvbuf=[out_max_value, MPI.LONG], op=MPI.MAX)
    # Global computes the number of values
    nb_of = out_max_value[0] + 1
    # warning nb_of corresponds to the selection and therefore not necessarily to the nb_of higher

    # Set the number of values
    try:
        dset = hdf_group[hdf_name_nb_of_dataset]
        dset.resize((step_index + 1,))
        dset[step_index] = nb_of
    except KeyError:
        hdf_group.create_dataset(name=hdf_name_nb_of_dataset,
                                 data=np.array([nb_of, ], dtype=np.int64), shape=(1,),
                                 dtype=np.int64, maxshape=(None,), chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))
    # Set offsets on the number of values
    try:
        dset = hdf_steps_group[hdf_name_offsets_dataset]
        dset.resize((step_index + 2,))
        dset[step_index + 1] = dset[step_index] + nb_of
    except KeyError:
        dset = hdf_steps_group.create_dataset(name=hdf_name_offsets_dataset,
                                              data=np.array([0, nb_of], dtype=np.int64), shape=(2,),
                                              dtype=np.int64, maxshape=(None,), chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))
    #
    global_first_offsets = dset[step_index]
    global_after_last_offsets = dset[step_index + 1]
    global_number_of = nb_of
    #
    # Remark:
    #    Cause TypeError: Indexing elements must be in increasing order
    # Computes sorted_indexes as sorts the indices by calling the argsort method which
    # returns the indices that would sort the array global_index_continuous.
    sorted_indexes = np.argsort(global_index_continuous)
    # Compute global_index_continuous_sorted (increasing values)
    global_index_continuous_sorted = global_index_continuous[sorted_indexes]
    # Computes offsets for points/cells to store values from local array into global array
    offsets_points_loc2glo = global_index_continuous_sorted + global_first_offsets

    # Return
    return (global_first_offsets, global_after_last_offsets, global_number_of,
            sorted_indexes, global_index_continuous, offsets_points_loc2glo)


def set_hdf_group_points_dataset(after_last_offset, sorted_indices, offsets_loc2glo, base, key_mesh, hdf_group):
    cartesian_coordinates = get_in_base(base, key_mesh + '/points/cartesianCoordinates')
    assert (cartesian_coordinates.shape[1] == 3)
    # Compute cartesian_coordinates_sorted (by global_index_continuous_sorted increasing values)
    cartesian_coordinates_sorted = cartesian_coordinates[sorted_indices]
    # Get or create dataset
    try:
        dset = hdf_group['Points']
    except KeyError:
        dset = hdf_group.create_dataset(name='Points', shape=(1, 3), dtype=np.float64,
                                        maxshape=(None, 3), chunks=(KVTKHDF_CHUNK_SIZE, 3))
    # Set the cartesian coordinates values
    dset.resize((after_last_offset, 3))
    dset[offsets_loc2glo[:]] = cartesian_coordinates_sorted[:]


def set_hdf_group_types_dataset(after_last_offset, sorted_indices, offsets_loc2glo, base, key_mesh, hdf_group):
    # Get code types cells
    types = get_in_base(base, key_mesh + '/cells/types')
    # Compute index code types cells to vtk types cells
    code2kdi = base['/code2kdi']
    code2vtk = kdi2vtk[code2kdi]
    # Compute neo_types in vtk types cells
    if types.dtype == '|S1':
        neo_types = code2vtk[types.view(dtype=np.int8)]
    else:
        neo_types = code2vtk[types]
    # Compute neo_types_cells_sorted (by global_index_continuous_sorted increasing values)
    neo_types_sorted = neo_types[sorted_indices]
    # Get or create dataset
    try:
        dset = hdf_group['Types']
    except KeyError:
        dset = hdf_group.create_dataset(name='Types', shape=(1, ), dtype=np.uint8,
                                        maxshape=(None, ), chunks=(KVTKHDF_CHUNK_SIZE, ))
    # Set the cartesian coordinates values
    dset.resize((after_last_offset, ))
    dset[offsets_loc2glo[:]] = neo_types_sorted[:]
    return kdi2nbPoints[code2kdi[types]]


def set_hdf_group_fields_dataset(after_last_offset: any, sorted_indices: any, offsets_loc2glo: any,
                                base: any, key_mesh: str, hdf_points_data_group: any) -> None:
    first = True
    for field_key in base[key_mesh].get():
        field_name = field_key[field_key.rfind('/') + 1:]
        try:
            current_datas = base[field_key].get()
        except:
            raise KDIException(
                'pre: \'' + field_key + '\' (' + field_key + ') must be a key of \'base\' or problem on get()!')
        len_component = current_datas.size // current_datas.shape[0]
        try:
            dset = hdf_points_data_group[field_name]
        except KeyError:
            if len_component == 1:
                dset = hdf_points_data_group.create_dataset(
                    name=field_name, shape=(1,), dtype=current_datas.dtype, maxshape=(None,),
                    chunks=(KVTKHDF_CHUNK_SIZE,))
            else:
                dset = hdf_points_data_group.create_dataset(
                    name=field_name, shape=(1, len_component), dtype=current_datas.dtype,
                    maxshape=(None, len_component), chunks=(KVTKHDF_CHUNK_SIZE, len_component))
        current_datas_sorted = current_datas[sorted_indices]
        if len_component == 1:
            dset.resize((after_last_offset,))
            dset[offsets_loc2glo] = current_datas_sorted.reshape((current_datas.shape[0],))
        else:
            dset.resize((after_last_offset, len_component))
            dset[offsets_loc2glo] = current_datas_sorted.reshape((current_datas.shape[0], len_component))

        if first:
            # TODO Ce serait mieux de le faire sous la forme d'une étape d'enrichissement de base
            first = False
            rank_name = 'OriginalPartId'
            split_key = field_key.split('/')[:-1]
            split_key.append(rank_name)
            rank_key = '/'.join(split_key)
            try:
                dset = hdf_points_data_group[rank_name]
            except KeyError:
                dset = hdf_points_data_group.create_dataset(
                    name=rank_name, shape=(1,), dtype=current_datas.dtype, maxshape=(None,),
                    chunks=(KVTKHDF_CHUNK_SIZE,))
            dset.resize((after_last_offset,))
            dset[offsets_loc2glo] = MPI.COMM_WORLD.Get_rank()


def set_hdf_group_parent_mesh_global_fields_dataset(
        step_index: int, base: any, hdf_group: any, field_key: str) -> None:
    hdf_fields_field_data_group = get_hdf_group(hdf_group, 'FieldData')
    hdf_fields_steps_group = get_hdf_group(hdf_group, 'Steps')
    hdf_fields_steps_field_date_offsets_group = get_hdf_group(hdf_fields_steps_group, 'FieldDataOffsets')
    # This group should be entered if you have a multiple dimension or a dimension that varies over time
    #   hdf_fields_steps_field_data_sizes_group = get_hdf_group(hdf_fields_steps_group, 'FieldDataSizes')

    field_name = field_key[field_key.rfind('/') + 1:]
    try:
        current_datas = base[field_key].get()
    except:
        raise KDIException(
            'pre: \'' + field_key + '\' (' + field_key + ') must be a key of \'base\' or problem on get()!')
    assert (len(current_datas.shape) == 1)
    len_component = current_datas.size

    try:
        # offsets group dataset
        dset = hdf_fields_steps_field_date_offsets_group[field_name]
        dset.resize((step_index + 2,))
        first_offset = dset[step_index]
        after_last_offset = first_offset + 1
        dset[step_index + 1] = after_last_offset
        # values group dataset
        try:
            dset = hdf_fields_field_data_group[field_name]
            assert (len(dset.shape) == 1 or dset.shape[1] == len_component)
        except KeyError:
            raise KDIException('pre: if \'field_key\'(' + field_key + ') exist in \'fw\' then exist also in \'fw[\'Steps\'][\'FieldDataOffsets\']\'!')
        # pre size

    except KeyError:
        # pre offsets group dataset
        try:
            dset = hdf_fields_field_data_group[field_name]
            raise KDIException('pre: if \'field_key\'(' + field_key + ') NOT exist in \'fw\' then NOT exist also in \'fw[\'Steps\'][\'FieldDataOffsets\']\'!')
        except KeyError:
            pass
        # values group dataset
        if len_component == 1:
            dset = hdf_fields_field_data_group.create_dataset(
                name=field_name, shape=(1,), dtype=current_datas.dtype, maxshape=(None,),
                chunks=(KVTKHDF_CHUNK_SIZE,))
        else:
            dset = hdf_fields_field_data_group.create_dataset(
                name=field_name, shape=(1, len_component), dtype=current_datas.dtype,
                maxshape=(None, len_component), chunks=(KVTKHDF_CHUNK_SIZE, len_component))
        # offsets group dataset
        hdf_fields_steps_field_date_offsets_group.create_dataset(name=field_name,
                                       data=np.array([0, 1, ], dtype=np.int64),
                                       shape=(2,), dtype=np.int64, maxshape=(None,),
                                       chunks=(KVTKHDF_STEPS_CHUNK_SIZE,))
        first_offset = 0
        after_last_offset = 1

    try:
        dset = hdf_fields_field_data_group[field_name]
    except KeyError:
        if len_component == 1:
            dset = hdf_fields_field_data_group.create_dataset(
                name=field_name, shape=(1,), dtype=current_datas.dtype, maxshape=(None,),
                chunks=(KVTKHDF_CHUNK_SIZE,))
        else:
            dset = hdf_fields_field_data_group.create_dataset(
                name=field_name, shape=(1, len_component), dtype=current_datas.dtype,
                maxshape=(None, len_component), chunks=(KVTKHDF_CHUNK_SIZE, len_component))

    if len_component == 1:
        dset.resize((after_last_offset,))
        dset[first_offset] = current_datas
    else:
        dset.resize((after_last_offset, len_component))
        dset[first_offset] = current_datas


def set_hdf_group_submesh_global_fields_dataset(
        hdf_group: any, field_key: str, hdf_parent_group: any) -> None:
    hdf_fields_field_data_group = get_hdf_group(hdf_group, 'FieldData')
    hdf_fields_steps_group = get_hdf_group(hdf_group, 'Steps')
    hdf_fields_steps_field_date_offsets_group = get_hdf_group(hdf_fields_steps_group, 'FieldDataOffsets')
    # This group should be entered if you have a multiple dimension or a dimension that varies over time
    #   hdf_fields_steps_field_data_sizes_group = get_hdf_group(hdf_fields_steps_group, 'FieldDataSizes')

    field_name = field_key[field_key.rfind('/') + 1:]
    try:
        # offsets group dataset
        dset = hdf_fields_steps_field_date_offsets_group[field_name]
        # values group dataset
        try:
            dset = hdf_fields_field_data_group[field_name]
        except KeyError:
            raise KDIException('pre: if \'field_key\'(' + field_key + ') exist in \'fw\' then exist also in \'fw[\'Steps\'][\'FieldDataOffsets\']\'!')
    except KeyError:
        try:
            hdf_fields_steps_field_date_offsets_group[field_name] = h5py.SoftLink(hdf_parent_group.name + '/Steps/FieldDataOffsets/' + field_name)
        except:
            raise Exception('Error on softlink on ' + '/VTKHDF' + field_key)
        try:
            hdf_fields_field_data_group[field_name] = h5py.SoftLink(hdf_parent_group.name + '/FieldData/' + field_name)
        except:
            raise Exception('Error on softlink on ' + '/VTKHDF' + field_key)


def set_hdf_group_global_fields_dataset(step_index: int, base: any, hdf_group: any, hdf_parent_group_name: any) -> None:
    for field_key in base['/fields'].get():
        if hdf_parent_group_name:
            set_hdf_group_parent_mesh_global_fields_dataset(step_index, base, hdf_parent_group_name, field_key)
            set_hdf_group_submesh_global_fields_dataset(hdf_group, field_key, hdf_parent_group_name)
        else:
            set_hdf_group_parent_mesh_global_fields_dataset(step_index, base, hdf_group, field_key)


def set_vtk_hdf_globaldatas(base: any, conf: any, hdf_parent_group_name: any) -> None:
    for field_key in base['/fields'].get():
        if hdf_parent_group_name:
            if not isinstance(base[field_key], KVTKHDFGlobalFieldDatas):
                trace('set_vtk_hdf_globaldatas remplacement KVTKHDFGlobalFieldDatas ', field_key)
                base[field_key] = KVTKHDFGlobalFieldDatas(base,
                                                          conf,
                                                          hdf_parent_group_name + '/FieldData/' + field_key.split('/')[2])
