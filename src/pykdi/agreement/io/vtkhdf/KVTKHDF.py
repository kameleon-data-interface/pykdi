import h5py
import os


class KVTKHDFGroup:
    """

    """
    __slots__ = 'm_conf', 'm_group'

    READ = 'r'
    WRITE = 'w'

    def __set_group(self, _group: any, _name: str):
        try:
            self.m_group = _group[_name]
        except KeyError as e:
            raise KeyError(e.args[0] + ' on \'' + _group.name + '\'')

    def __create_group(self, _group: any, _name: str):
        self.m_group = _group.create_group(_name, track_order=True)

    def __init__(self, _conf: dict[str, str], _group: any, _name: str, _mode: str = READ):
        """
        Fr: ...
        """
        assert ('pre: mode \'' + _mode + '\' must be \'r\' (READ) or \'w\' (WRITE)' and
                _mode in [KVTKHDF.READ, KVTKHDF.WRITE])

        self.m_conf = _conf

        mode_read = False
        if self.m_conf['mode'] == KVTKHDF.READ:
            assert ('pre: mode \'' + _mode + '\' must be \'r\' (READ) ...' and _mode == KVTKHDF.READ)
            self.__set_group(_group, _name)
            return
        if _mode == KVTKHDF.READ:
            self.__set_group(_group, _name)
            return
        assert ('pre: HDF file mode \'' + self.conf['mode'] +
                '\' must be \'w\' (WRITE) when you want used mode \'w\' (WRITE)' and self.m_conf['mode'] == 'WRITE')
        self.__create_group(_group, _name)

    def group(self, _name: str, _mode: str = READ):
        return KVTKHDFGroup(self.m_conf, self.m_group, _name, _mode)

    @property
    def name(self):
        return self.m_group.name

    @property
    def keys(self):
        return self.m_group.keys


class KVTKHDF:
    """

    """
    __slots__ = 'm_conf'

    READ = 'r'
    WRITE = 'w'

    def __init__(self, _filename: str | os.PathLike, _mode: str = READ):
        """
        Fr: Ouvre le fichier HDF dans le mode demandé
        """
        assert ('pre: mode \'' + _mode + '\' must be \'r\' (READ) or \'w\' (WRITE)' and _mode in [KVTKHDF.READ, KVTKHDF.WRITE])
        self.m_conf = {'filename': _filename, 'mode': _mode, 'hdf_fd': h5py.File(_filename, _mode)}

    def __del__(self):
        """
        Fr: Ferme le fichier HDF
        """
        self.m_conf['hdf_fd'].close()

    def group(self, _name: str, _mode: str = READ):
        return KVTKHDFGroup(self.m_conf, self.m_conf['hdf_fd'], _name, _mode)
