import numpy as np

from pykdi.KDIException import KDIException
from pykdi.KDIChunks import KDIChunks, KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT

from .KVTKHDFDatas import KVTKHDFGetFp


class KVTKHDFStepPartChunks(KDIChunks):
    __slots__ = 'filename', 'fullname'

    def __init_steps_parts(self, filename, fullname):
        group = KVTKHDFGetFp({'filename': filename, 'mode': 'r'})

        try:
            dset = group[fullname]
        except KeyError:
            raise KDIException('pre: \'' + fullname + '\' not exist in \'' + filename + '\'!')

        nb_steps = int(dset.attrs['NSteps'])

        parts = dset['NumberOfParts'][:nb_steps]
        nb_parts = parts[0]
        for part in parts:
            if part != nb_parts:
                raise KDIException('pre: inconsistency of the number of partitions over the S!')

        return dset['Values'][:nb_steps], nb_parts

    def __init__(self, base: dict[str, any], filename: str, fullname: str):
        """
        Initializes an instance of KDIChunks.
        The user will not call this method directly which will be called:
        - by kdi_update when creating the database than with the parameter `base`; or
        - by loadJSON when deserialize an instance of this object necessary to pass all these parameters.
        :param base: the current baseomplexType
        :param order: only via loadJSON, an ordered list of variant names
        :param variants: only via loadJSON, a dictionary describing the specificities of the variant
        """
        self.filename = filename
        self.fullname = fullname

        steps, nb_parts = self.__init_steps_parts(filename, fullname)

        variants = {'step': {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', 'data': steps},
                    'part': {'type': KDI_CONSTANT_VARIANT, 'data': nb_parts}, }

        super().__init__(base, ['step', 'part'], variants)

    def __repr__(self):
        return 'KVTKHDFStepPartChunks(base=KDIBase, filename=\'' + self.filename + '\', fullname=\'' + self.fullname + '\')'
