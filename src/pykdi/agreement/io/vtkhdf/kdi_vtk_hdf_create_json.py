import numpy as np
import os


from pykdi.KDIChunks import KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT

from pykdi.tools.kdi_log import KDILog

from pykdi.evaluation_string.kdi_update_eval_offsets import kdi_update_eval_offsets

from .KVTKType import kdi2vtk
from pykdi.io.json.KJSON import write_json

from pykdi.kdi_update import kdi_update
from pykdi.kdi_update_fields import kdi_update_fields
from pykdi.kdi_update_sub import kdi_update_sub
from pykdi.evaluation_string.KDIEvalString import KDIEvalString

from pykdi.agreement.step_part.KDIAgreementBaseStepPart import KDIAgreementBaseStepPart
from pykdi.agreement.KDIAgreementBaseConf import KDI_AGREEMENT_CONF_NB_PARTS
from pykdi.agreement.io.vtkhdf.KVTKHDFStepPartChunks import KVTKHDFStepPartChunks

from .KVTKHDF import KVTKHDF, KVTKHDFGroup
from .KVTKHDFDatas import data_read_vtk_hdf


kdi_log_create_json = KDILog('CREATE_JSON', False)


def kdi_vtk_hdf_create_uns_json(filename: str, base: dict[str, any], base_mesh_key: str, hdf_group: KVTKHDFGroup):
    """
    filename:
    base:
    base_mesh_key: jusqu'à la racine incluant le nom du maillage UNS
    hdf_group: le groupe racine HDF du maillage UNS
    """
    kdi_log = kdi_log_create_json.indent(4)

    hdf_key = hdf_group.name
    kdi_log.log('UNS', hdf_key)

    kdi_log = kdi_log_create_json.indent(6)

    # TODO Attributs
    kdi_log.log('No treated attributes')
    # /glu/version ?
    # /glu/name ?
    # /study/date ?
    # /study/name ?
    # /vtkhdf/Version ? loadable
    #
    # code type = vtk type
    # but... TODO proposer un service qui remplace le code2vtk si Types memory est vide et /VTK/Types
    #     "/fields/myGlobScalarField": "KVTKHDFGlobalFieldDatas(KDIBase, {'filename': '3pe_1uns_3ssd_10tps__hard.vtkhdf', 'mode': 'r'}, '/KVTKHDF/FieldData/myGlobScalarField')",
    #     "/fields/myGlobVectorField": "KVTKHDFGlobalFieldDatas(KDIBase, {'filename': '3pe_1uns_3ssd_10tps__hard.vtkhdf', 'mode': 'r'}, '/KVTKHDF/FieldData/myGlobVectorField')",

    kdi_update(base, 'UnstructuredGrid', base_mesh_key)
    kdi_update_eval_offsets(base, base_mesh_key + '/cells')

    #TODO remplacer conf par une instance de KVTKHDF
    #TODO linérarisation avec une instance de ce KVTKHDF par base à lire
    conf = {'filename': filename, 'mode': 'r'}

    if '/chunks' not in base:
        base['/chunks'] = KVTKHDFStepPartChunks(base, filename, hdf_group.name + '/Steps')

    _ = hdf_group.group('Types')
    kdi_log.log('✓', hdf_group.name + '/Types')

    data_read_vtk_hdf(base, '/KVTKHDF' + hdf_key[7:] + '/types', conf, hdf_key + '/Types')
    command = """
vtk2code = Params[0]
types = Base[Params[1]].get()
Ret = vtk2code[types]
"""
    code2kdi = base['/code2kdi']
    code2vtk = kdi2vtk[code2kdi]
    # TODO il vaudrait mieux au lieu de np.int8 le type pour types cell code
    vtk2code = np.zeros(code2vtk.max() + 1, dtype=np.int8)
    for key, value in enumerate(code2vtk):
        vtk2code[value] = key
    base[base_mesh_key + '/cells/types'] = KDIEvalString(base, base_mesh_key + '/cells/types',
                                                         command, [vtk2code, '/KVTKHDF' + hdf_key[7:] + '/types'])

    _ = hdf_group.group('Connectivity')
    kdi_log.log('✓', hdf_group.name + '/Connectivity')

    data_read_vtk_hdf(base, base_mesh_key + '/cells/connectivity', conf, hdf_key + '/Connectivity')

    try:
        _ = hdf_group.group('/KDI' + base_mesh_key + '/CellData/globalIndexContinuous')
        kdi_log.log('✓', hdf_group.name + '/KDI' + base_mesh_key + '/CellData/globalIndexContinuous')

        data_read_vtk_hdf(base, base_mesh_key + '/cells/globalIndexContinuous', conf, hdf_key.replace('/VTKHDF', '/KDI') + base_mesh_key + '/CellData/globalIndexContinuous')
    except KeyError:
        del base[base_mesh_key + '/cells/globalIndexContinuous']
        kdi_log.log('X', 'WARNING', hdf_group.name + '/KDI' + base_mesh_key + '/CellData/globalIndexContinuous NOT EXIST')

    hdf_vtkhdf_celldata = hdf_group.group('CellData')
    for field_key in hdf_vtkhdf_celldata.keys():
        if field_key[:len('glob_')] == 'glob_':
            master_field_key = field_key[len('glob_'):]
        else:
            master_field_key = field_key

        kdi_update_fields(base, base_mesh_key + '/cells/fields', master_field_key)
        kdi_log.log('✓', hdf_group.name +  '/CellData/' + master_field_key)

        data_read_vtk_hdf(base, base_mesh_key + '/cells/fields/' + master_field_key, conf, hdf_key + '/CellData/' + field_key)

    _ = hdf_group.group('Points')
    kdi_log.log('✓', hdf_group.name + '/Points')
    data_read_vtk_hdf(base, base_mesh_key + '/points/cartesianCoordinates', conf, hdf_key + '/Points')

    hdf_vtkhdf_pointdata = hdf_group.group('PointData')
    for field_key in hdf_vtkhdf_pointdata.keys():
        if field_key[:len('glob_')] == 'glob_':
            master_field_key = field_key[len('glob_'):]
        else:
            master_field_key = field_key

        kdi_update_fields(base, base_mesh_key + '/points/fields', master_field_key)
        kdi_log.log('✓', hdf_group.name + '/PointData/' + master_field_key)

        data_read_vtk_hdf(base, base_mesh_key + '/points/fields/' + master_field_key, conf, hdf_key + '/PointData/' + field_key)

    try:
        _ = hdf_group.group('/KDI' + base_mesh_key + '/PointData/globalIndexContinuous')
        kdi_log.log('✓', hdf_group.name + '/KDI' + base_mesh_key + '/PointData/globalIndexContinuous')

        data_read_vtk_hdf(base, base_mesh_key + '/points/globalIndexContinuous', conf, hdf_key.replace('/VTKHDF', '/KDI') + base_mesh_key + '/PointData/globalIndexContinuous')
    except KeyError:
        del base[base_mesh_key + '/points/globalIndexContinuous']
        kdi_log.log('X', 'WARNING', hdf_group.name + '/KDI' + base_mesh_key + '/PointData/globalIndexContinuous NOT EXIST')


def kdi_vtk_hdf_create_sub_uns_json(filename: str, base: dict[str, any], base_mesh_key: str, hdf_group: KVTKHDFGroup):
    """
    filename:
    base:
    base_mesh_key: jusqu'à la racine incluant le nom du maillage UNS
    hdf_group: le groupe racine HDF du maillage UNS
    """
    kdi_log = kdi_log_create_json.indent(4)

    kdi_log.state = True

    hdf_key = hdf_group.name
    kdi_log.log('SUBUNS', hdf_key)

    kdi_log = kdi_log_create_json.indent(6)

    #TODO remplacer conf par une instance de KVTKHDF
    #TODO linérarisation avec une instance de ce KVTKHDF par base à lire
    conf = {'filename': filename, 'mode': 'r'}

    data_read_vtk_hdf(base, base_mesh_key + '/indexescells', conf, hdf_key + '/CellData/glob_myGIC')

    hdf_vtkhdf_celldata = hdf_group.group('CellData')
    for field_key in hdf_vtkhdf_celldata.keys():
        if field_key[:len('glob_')] == 'glob_':
            continue

        kdi_update_fields(base, base_mesh_key + '/cells/fields', field_key)
        kdi_log.log('✓', hdf_group.name +  '/CellData/' + field_key)

        data_read_vtk_hdf(base, base_mesh_key + '/cells/fields/' + field_key, conf, hdf_key + '/CellData/' + field_key)

    return


def kdi_vtk_hdf_create_json(filename: str | os.PathLike, prefix_before_extension: str = '_CREATE'):
    """
    Fr: ...
    """
    kdi_log_create_json.log()
    kdi_log_create_json.log('Create JSON', filename)

    kdi_log = kdi_log_create_json.indent(2)

    hdf = KVTKHDF(filename, 'r')

    kdi_log.log('opening validated')

    hdf_vtkhdf = hdf.group('VTKHDF')

    os.environ['KDI_AGREEMENT_CODE2KDI'] = 'VTK'

    kdi = KDIAgreementBaseStepPart('filepath')
    kdi.set_conf_int(KDI_AGREEMENT_CONF_NB_PARTS, 3)
    kdi.initialize()

    ###################### TODO BEGIN
    # a-temporel and a-partition declinaison
    # TODO Il ne faut pas passer par kdi._base, c'est mal !
    kdi._base['/chunks'].set({})
    kdi._base['/glu/version'].insert(np.array([2, 0]))
    kdi._base['/study/name'].insert(np.array(['MyEtude']))

    # # temporel and a-partition declinaison
    # # TODO base['/chunks'].set({'step': float_step})
    # kdi._base['/study/date'].insert('2024/05')
    #
    # kdi_update_fields(kdi._base, '/fields', 'myGlobScalarField')
    # kdi._base['/fields/myGlobScalarField'].insert(np.array([0]))
    # kdi_update_fields(kdi._base, '/fields', 'myGlobVectorField')
    # kdi._base['/fields/myGlobVectorField'].insert(np.array([2, 1, 0]))
    del kdi._base['/chunks']
    del kdi._base['/chunks/current']
    ###################### TODO END

    # TODO lire temps
    # TODO lire nb partitions (constant)

    try:
        hdf_vtkhdf_assembly = hdf_vtkhdf.group('Assembly')

        kdi_log.log('included Assembly')
        print('', hdf_vtkhdf_assembly.name)

        assert(len(hdf_vtkhdf_assembly.keys()) == 1)

        for master_mesh_key in hdf_vtkhdf_assembly.keys():
            hdf_vtkhdf_assembly_master = hdf_vtkhdf_assembly.group(master_mesh_key)
            print('master:', hdf_vtkhdf_assembly_master.name)

            # On traite d'abord le maillage maitre parmi les sous-maillages
            for sub_mesh_key in hdf_vtkhdf_assembly_master.keys():
                print('  sub mesh:', sub_mesh_key)
                if sub_mesh_key == master_mesh_key:
                    print('    link mesh name:', hdf_vtkhdf_assembly_master.group(sub_mesh_key).name)
                    kdi_vtk_hdf_create_uns_json(filename, kdi._base,  '/' + sub_mesh_key, hdf_vtkhdf_assembly_master.group(sub_mesh_key))
                    break

            for sub_mesh_key in hdf_vtkhdf_assembly_master.keys():
                print('  sub mesh:', sub_mesh_key)
                if sub_mesh_key != master_mesh_key:
                    str_my_mil = kdi_update_sub(kdi._base, '/mymesh', '/' + sub_mesh_key)
                    print('    ', str_my_mil)
                    print('    link mesh name:', hdf_vtkhdf_assembly_master.group(sub_mesh_key).name)
                    kdi_vtk_hdf_create_sub_uns_json(filename, kdi._base, str_my_mil, hdf_vtkhdf_assembly_master.group(sub_mesh_key))
    except KeyError:
        # TODO Je ne peux pas le trouver dans la base
        mesh_key = 'mymesh'
        kdi_log.log('included one mesh \'/' + mesh_key + '\'')
        #    lire version et en fonction appeler truc
        kdi_vtk_hdf_create_uns_json(filename, kdi._base,  '/' + mesh_key, hdf_vtkhdf)

    json_dir_name = os.path.dirname(filename)
    if json_dir_name:
        json_dir_name += '/'
    assert (os.path.basename(filename).split('.')[1] in ['json', 'vtkhdf'])
    json_filename = json_dir_name + os.path.basename(filename).split('.')[0] + prefix_before_extension + '.json'

    write_json(kdi._base, json_filename)
