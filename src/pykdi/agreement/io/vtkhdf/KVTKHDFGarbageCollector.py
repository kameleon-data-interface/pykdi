from mpi4py import MPI

from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory
from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from pykdi.evaluation_string.KDIEvalBase64 import KDIEvalBase64
from pykdi.agreement.step_part.kdi_agreement_steppart import KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART

from pykdi.tools.kdi_log import KDILog

from .KVTKHDFDatas import KVTKHDFDatas
from .KVTKHDFGlobalFieldDatas import KVTKHDFGlobalFieldDatas


def just_a_erase(kdi_log_garbage_collector, base, key, step_partless, current_part=None):
    if current_part is not None:
        chunk = step_partless.copy()
        chunk[KDI_AGREEMENT_STEPPART_VARIANT_PART] = current_part
    else:
        chunk = step_partless

    # Important: chunk must be to passed in parameter
    try:
        base[key].erase(chunk)
    except KDIException as e:
        pass
    except Exception as e:
        pass
        # TODO cerains sont acceptes et justifies pas d'autres il faudrait verifier le variants
        # kdi_log_garbage_collector.fatal_exception('key: \'' + key + '\' erase()', e)
        # raise KDIException('KVTKHDFGarbageCollector key: \'' + key + '\' erase()', e)


def all_erase(kdi_log_garbage_collector, base, key, step_partless, nb_parts):
    kdi_log_garbage_collector.log('all_erase step_partless:', step_partless)
    just_a_erase(kdi_log_garbage_collector, base, key, step_partless)

    kdi_log_garbage_collector.log('all_erase step_part rank:', MPI.COMM_WORLD.Get_rank())
    just_a_erase(kdi_log_garbage_collector, base, key, step_partless, MPI.COMM_WORLD.Get_rank())

    kdi_log_garbage_collector.log('all_erase step_part MPI size:', MPI.COMM_WORLD.Get_size(), '==1')
    if MPI.COMM_WORLD.Get_size() == 1:
        kdi_log_garbage_collector.log('all_erase step_part nb_parts:', nb_parts)
        for iPart in range(1, nb_parts):
            kdi_log_garbage_collector.log('all_erase step_part     iPart:', iPart)
            just_a_erase(kdi_log_garbage_collector, base, key, step_partless, iPart)

    return base[key].__sizeof__()


def KVTKHDFGarbageCollector(base: dict[str, any], step_partless: any, with_clean_data_submeshes: bool,
                            force: bool = False):
    """
    This method allows you to garbage your memory:
    - garbage the memory for all chunks of 'submeshes" datas;
      cas VTK HDF without KDIComputeMilieux
    - garbage the memory for the current chunk of KVTKHDFDatas et KVTKHDFGlobalFieldDatas;
    """
    kdi_log_garbage_collector = KDILog('VTK_HTF_GARBAGE_COLLECTOR', False)

    try:
        step_partless[KDI_AGREEMENT_STEPPART_VARIANT_STEP]
    except KeyError:
        raise KDIException('pre: step_partless must contain the \'' + KDI_AGREEMENT_STEPPART_VARIANT_STEP + '\' dictionary key')

    try:
        step_partless[KDI_AGREEMENT_STEPPART_VARIANT_PART]
        raise KDIException('pre: step_partless must not contain the \'' + KDI_AGREEMENT_STEPPART_VARIANT_PART + '\' dictionary key')
    except KeyError:
        pass

    kdi_log_garbage_collector.log('begin')

    try:
        nb_parts = base['/chunks'].GetMaxConstantValue('part')
        if '/chunks/current' in base:
            before_chunks_current = base['/chunks/current']
        else:
            before_chunks_current = None
            base['/chunks'].set(step_partless)

        # before and after cumulative sizeof
        before, after = 0, 0

        keys = base.keys()
        for key in keys:
            val = base[key]

            try:
                old_sizeof = val.__sizeof__()
                before += old_sizeof
                kdi_log_garbage_collector.log('key: \'' + key + '\' type: \'' + str(type(val)) + '\'',
                                              'old sizeof:', old_sizeof, 'before:', before)
            except Exception as e:
                kdi_log_garbage_collector.fatal_exception('key: \'' + key + '\' getsizeof()', e)
                raise KDIException('KVTKHDFGarbageCollector key: \'' + key + '\' getsizeof()', e)

            # This is not completely satisfactory, however it allows us not to clutter up the
            # JSON while waiting for this information to be stored in KDI HDF if it becomes necessary.
            if with_clean_data_submeshes:
                if '/'.join(key.split('/')[2:3]) == 'submeshes':
                    if isinstance(val, KDIMemory):
                        try:
                            base[key].reset()
                        except Exception as e:
                            kdi_log_garbage_collector.fatal_exception('key (submeshes): \'' + key + '\' reset()', e)
                            raise KDIException('KVTKHDFGarbageCollector key (submeshes): \'' + key + '\' reset()', e)
                        new_sizeof = base[key].__sizeof__()
                        after += new_sizeof
                        kdi_log_garbage_collector.log('    (submeshes) new sizeof:', new_sizeof, 'after:', after)
                        continue

            if isinstance(val, KVTKHDFDatas) or isinstance(val, KVTKHDFGlobalFieldDatas):
                new_sizeof = all_erase(kdi_log_garbage_collector, base, key, step_partless, nb_parts)
                after += new_sizeof
                kdi_log_garbage_collector.log('    (KVTKHDFDatas,KVTKHDFGlobalFieldDatas) new sizeof:', new_sizeof, 'after:', after)
                continue

            if isinstance(val, KDIEvalString) or isinstance(val, KDIEvalBase64):
                new_sizeof = all_erase(kdi_log_garbage_collector, base, key, step_partless, nb_parts)
                after += new_sizeof
                kdi_log_garbage_collector.log('    (KDIEvalString,KDIEvalBase64) new sizeof:', new_sizeof, 'after:', after)
                continue

            if isinstance(val, KDIMemory):
                # On ne devrait pas nettoyer
                ignored = ['/agreement', '/glu/version', '/glu/name', '/study/date', '/study/name',
                           '/vtkhdf/version']
                if key in ignored:
                    continue

                if force:
                    new_sizeof = all_erase(kdi_log_garbage_collector, base, key, step_partless, nb_parts)
                    after += new_sizeof
                    kdi_log_garbage_collector.log('    (KDIMemory) new sizeof:', new_sizeof, 'after:', after)
                else:
                    if not with_clean_data_submeshes and '/'.join(key.split('/')[2:3]) == 'submeshes':
                        continue

                    print('GarbageCollector WHAT THE FUCK key:', key)

                continue

            after += old_sizeof
            kdi_log_garbage_collector.log('    (IGNORED)  after:', after)

        kdi_log_garbage_collector.log('before:', before, ' after:', after, ' (gain ', (1.-after/before)*100, '%)')

        if before_chunks_current:
            base['/chunks/current'] = before_chunks_current
        else:
            del base['/chunks/current']

        kdi_log_garbage_collector.log('end')

    except Exception as e:
        kdi_log_garbage_collector.fatal_exception('before completion', e)
        raise KDIException('KVTKHDFGarbageCollector before completion', e)
