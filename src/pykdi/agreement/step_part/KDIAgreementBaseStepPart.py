from pykdi.tools.kdi_get_env import kdi_get_env_str

from pykdi.agreement.io.vtkhdf import KVTKHDFDatas

from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART
from pykdi.tools import kdi_log_python
from pykdi.KDIException import KDIException

from pykdi.evaluation_string.kdi_update_eval_offsets import kdi_update_eval_offsets

from pykdi.KDIDatas import KDIDatas
from pykdi.kdi_base import kdi_base
from .KDIAgreementChunkStepPart import KDIAgreementChunkStepPart
from pykdi.kdi_update import kdi_update
from pykdi.kdi_update_fields import kdi_update_fields
from pykdi.kdi_update_sub import kdi_update_sub
from pykdi.io.json.KJSON import read_json
from pykdi.KDIMemory import KDIMemory
from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from pykdi.evaluation_string.KDIEvalBase64 import KDIEvalBase64
from pykdi.agreement.io.vtkhdf.KVTKHDFGlobalFieldDatas import KVTKHDFGlobalFieldDatas
from pykdi.agreement.io.vtkhdf.KVTKType import  KVTK_TRIANGLE, KVTK_QUAD, KVTK_HEXAHEDRON
from pykdi.KDIType import KDI_TRIANGLE, KDI_QUADRANGLE, KDI_HEXAHEDRON

from pykdi.agreement.KDIAgreementBase import KDIAgreementBase
from pykdi.agreement.KDIAgreementBaseConf import KDI_AGREEMENT_CONF_NB_PARTS, KDI_AGREEMENT_CONF_CODE_2_KDI
from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART


KDI_AGREEMENT_CODE2KDI = kdi_get_env_str('KDI_AGREEMENT_CODE2KDI', '')

class KDIAgreementBaseStepPart(KDIAgreementBase):

    def __init__(self, filename: str):
        """
        :param filename_or_nb_parts:
        :param nb_parts:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::__init__()')
        kdi_log_python.log('   filename', filename)
        super().__init__(filename)
        # Don't do this, this disables the garbage collector.
        # gc.disable()

    def initialize(self, mode_read: bool = False):
        """
        :param filename: filename without extension
        :param nb_parts:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::initialize()')
        kdi_log_python.log('   mode_read', mode_read)
        super().initialize(mode_read)
        try:
            nb_parts = self._conf[KDI_AGREEMENT_CONF_NB_PARTS]
        except:
            nb_parts = 1
        try:
            code2kdi = self._conf[KDI_AGREEMENT_CONF_CODE_2_KDI]
        except:
            code2kdi = None

        if self._mode in ['rw', 'r',]:
            kdi_log_python.log('KDIAgreementStepPartBase::initialize() by load')
            self._base = read_json(self._filename + '.json')
        else:
            if code2kdi is None:
                if KDI_AGREEMENT_CODE2KDI == 'VTK':
                    code2kdi = {KVTK_TRIANGLE: KDI_TRIANGLE,
                                KVTK_QUAD: KDI_QUADRANGLE,
                                KVTK_HEXAHEDRON: KDI_HEXAHEDRON}
                    configuration = {'agreement': KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS: nb_parts, KDI_AGREEMENT_CONF_CODE_2_KDI: code2kdi}
                else:
                    configuration = {'agreement': KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS: nb_parts}
            else:
                configuration = {'agreement': KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS: nb_parts, KDI_AGREEMENT_CONF_CODE_2_KDI: code2kdi}
            kdi_log_python.log('KDIAgreementStepPartBase::initialize() by create (int)')
            self._base = kdi_base(configuration)

    @property
    def base(self):
        return self._base

    def chunk(self, float_step: float=None, int_part: int=None) -> KDIAgreementChunkStepPart:
        """

        :param float_step:
        :param int_part:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::chunk()')
        if int_part is None:
            configuration = float_step
            if configuration == dict() or configuration is None:
                return KDIAgreementChunkStepPart(self)
            try:
                if len(configuration) == 1 and KDI_AGREEMENT_STEPPART_VARIANT_STEP in configuration:
                    return KDIAgreementChunkStepPart(self, configuration)
                if len(configuration) == 2 and KDI_AGREEMENT_STEPPART_VARIANT_STEP in configuration and KDI_AGREEMENT_STEPPART_VARIANT_PART in configuration:
                    return KDIAgreementChunkStepPart(self, configuration)
                raise KDIException('chunk first argument is configuration dict but non conforme!')
            except:
                pass
        return KDIAgreementChunkStepPart(self, {KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step,
                                                KDI_AGREEMENT_STEPPART_VARIANT_PART: int_part,})

    def update(self, typename: str, name: str):
        """

        :param typename:
        :param name:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::update()')
        kdi_update(self._base, typename, name)
        if typename == 'UnstructuredGrid':
            kdi_update_eval_offsets(self._base, name + '/cells')

    def update_fields(self, field_parent_name: str, field_basename: str) -> str:
        """

        :param field_parent_name:
        :param field_basename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::update_fields()')
        kdi_update_fields(self._base, field_parent_name, field_basename)
        return field_parent_name + '/' + field_basename

    def update_sub(self, mesh_parent_name: str, submesh_basename: str) -> str:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::kdi_update_sub()')
        return kdi_update_sub(self._base, mesh_parent_name, submesh_basename)

    def get_keys(self) -> [str]:
        return [key for key in self._base.keys()]

    def dump(self):
        """
        """
        kdi_log_python.log('KDIAgreementStepPartBase::dump()')
        print(self._base)

    def light_dump(self):
        """
        """
        kdi_log_python.log('KDIAgreementStepPartBase::light_dump() begin')
        ret = '\n{\n'
        for key, val in self._base.items():
            ret += '   \'' + key + '\': '
            if isinstance(val, KDIDatas):
                ret += val.light_dump() + ',\n'
            else:
                ret += str(val) + ',\n'
        ret += '}\n\n'
        print(ret)

    def __repr__(self):
        """

        :return:
        """
        return str(self._base)