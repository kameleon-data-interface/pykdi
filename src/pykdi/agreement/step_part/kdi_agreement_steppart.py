import numpy as np

from pykdi.tools.kdi_get_env import kdi_get_env
from pykdi.tools.kdi_log import KDILog, kdi_log_python

from pykdi.KDIChunks import KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT
from pykdi.KDIException import KDIException
from pykdi.KDIType import KDI_TRIANGLE, KDI_QUADRANGLE, KDI_HEXAHEDRON, KDI_PIT, KDI_SIZE, kdi2nbPoints
from pykdi.agreement.KDIAgreementType import (KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_QUADRANGLE_TYPE_VALUE,
                                              KDI_SIMULATION_HEXAHEDRON_TYPE_VALUE)
from pykdi.kdi_update import kdi_update

from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART


def kdi_agreement_step_part(base: dict[str, any],
                            configuration: dict[str, str | int] = None) -> bool:
    """
    :return:
    """
    kdi_log_python.log('kdi_agreement_steppart configuration:', configuration)
    if (configuration is None or
            'agreement' not in configuration or
            configuration['agreement'] != KDI_AGREEMENT_STEPPART):
        return False

    try:
        nb_parts = int(configuration['nb_parts'])
    except (KeyError, ValueError):
        raise KDIException(
            'pre: configuration must be have the key \'nb_parts\' in configuration \'agreement\'=\'' +
            KDI_AGREEMENT_STEPPART + '\', type value int or string describe int')

    base['/chunks'].SetVariants([[KDI_AGREEMENT_STEPPART_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
                                 [KDI_AGREEMENT_STEPPART_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': nb_parts, }, ], ])

    base['/agreement'].insert(np.array([KDI_AGREEMENT_STEPPART]), chunk={})

    kdi_update(base, 'VTKHDF', '/vtkhdf')
    base['/vtkhdf/version'].insert(np.array([2, 0]), chunk={})
    # Force the use of common data offsets for data cell or point instead of one per field
    base['/vtkhdf/force_common_data_offsets'] = True

    if 'code2kdi' in configuration:
        dict_code2kdi = configuration['code2kdi']
        # TODO Il faudrait verifier que les valeurs correspondent bien à un type de cellules KDI
    else:
        dict_code2kdi = {KDI_SIMULATION_TRIANGLE_TYPE_VALUE: KDI_TRIANGLE,
                         KDI_SIMULATION_QUADRANGLE_TYPE_VALUE: KDI_QUADRANGLE,
                         KDI_SIMULATION_HEXAHEDRON_TYPE_VALUE: KDI_HEXAHEDRON}

    array_code2kdi = np.full(shape=(max(dict_code2kdi.keys())+1), fill_value=KDI_PIT, dtype=np.int8)
    # TODO Verifier que np.int8 max <= KDI_PIT
    for k, v in dict_code2kdi.items():
        assert (k < len(array_code2kdi))
        assert (v < KDI_SIZE)
        array_code2kdi[k] = v

    base['/code2kdi'] = array_code2kdi
    base['/code2nbPoints'] = kdi2nbPoints[array_code2kdi]

    return True
