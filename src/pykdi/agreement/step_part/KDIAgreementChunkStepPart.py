from mpi4py import MPI
import numpy as np
import os

from pykdi.agreement.step_part.kdi_agreement_steppart import kdi_log_python, KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART
from pykdi.agreement.io.vtkhdf.KDIWriterVTKHDF import write_vtk_hdf
from pykdi.agreement.io.vtkhdf.KDIWriterVTKHDF_Nto1 import write_vtk_hdf_n_to_1

from pykdi.KDIException import KDIException
from pykdi.mutation.KDIComputeMultiMilieux import KDIComputeMultiMilieux
from pykdi.agreement.io.vtkhdf.KVTKHDFGarbageCollector import KVTKHDFGarbageCollector

from pykdi.agreement.KDIAgreementChunk import KDIAgreementChunk


class KDIAgreementChunkStepPart(KDIAgreementChunk):
    """
    Fr: KDIAgreementStepPartChunk hérite dans l'esprit de KDIDatas, on doit retrouver les mêmes services avec une
        signature différente puisque l'on passe ici la clef.
    """
    __slots__ = 'step_part'

    def __init__(self, kdi_base: any, configuration: dict[str, any] = None):
        super().__init__(kdi_base, configuration)
        kdi_log_python.log('KDIAgreementStepPartChunk::__init__() IN')

        try:
            float_step = self._configuration[KDI_AGREEMENT_STEPPART_VARIANT_STEP]
            assert (isinstance(float_step, float) or isinstance(float_step, int))
        except:
            float_step = None
            
        try:
            int_part = self._configuration[KDI_AGREEMENT_STEPPART_VARIANT_PART]
            assert (isinstance(int_part, int))
        except:
            int_part = None

        if int_part is None:
            if float_step is None:
                step_part = {}
            else:
                step_part = {KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step}
        else:
            if float_step is None:
                raise KDIException('pre: float_step must be not None if int_part is not None!')
            step_part = {KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step, KDI_AGREEMENT_STEPPART_VARIANT_PART: int_part}

        self._kdi_base = kdi_base
        new_step_part = self._kdi_base.base['/chunks'].build_chunk(step_part, True)

        for key in [KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART]:
            if key in new_step_part and '#' + key not in new_step_part:
                raise KDIException('pre: if \'' + key + '\' exist in chunk then \'#' + key + '\' must be exist')

        self._chunk = new_step_part

        kdi_log_python.log('KDIAgreementStepPartChunk __init__() OUT', self._chunk)

    def new_chunk(self, float_step: float=None, int_part: int=None) -> any:
        """

        :param float_step:
        :param int_part:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartBase::chunk()')
        if int_part is None:
            configuration = float_step
            if configuration == dict() or configuration is None:
                return self._kdi_base.chunk()
            try:
                if len(configuration) == 1 and KDI_AGREEMENT_STEPPART_VARIANT_STEP in configuration:
                    return self._kdi_base.chunk(configuration)
                if len(configuration) == 2 and KDI_AGREEMENT_STEPPART_VARIANT_STEP in configuration and KDI_AGREEMENT_STEPPART_VARIANT_PART in configuration:
                    return self._kdi_base.chunk(configuration)
                raise KDIException('chunk first argument is configuration dict but non conforme!')
            except:
                pass
        return self._kdi_base.chunk({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step,
                                     KDI_AGREEMENT_STEPPART_VARIANT_PART: int_part,})

    def garbageCollector(self) -> None:
        """

        :param filename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartChunk garbageCollector()')
        kdi_log_python.log(' self._chunk:', self._chunk)
        try:
            KVTKHDFGarbageCollector(self._kdi_base.base, self._chunk, with_clean_data_submeshes=True, force=True)
        except:
            kdi_log_python.fatal_exception('KDIAgreementStepPartChunk garbageCollector()')

    def saveVTKHDF(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDF() filename:', filename)
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDF() real filename:', filename+'.vtkhdf')
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDF() self._chunk:', self._chunk)
        try:
            write_vtk_hdf(self._kdi_base.base, filename + '.vtkhdf', chunk=self._chunk, with_clean_data_submeshes=True)
        except:
            kdi_log_python.fatal_exception('KDIAgreementStepPartChunk saveVTKHDF()')

    def saveVTKHDFNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFNto1()')
        if MPI.COMM_WORLD.Get_size() == 1:
            print('WARNING PYTHON >>> KDIAgreementStepPartChunk No saveVTKHDFNto1() cause sequential mode')
            return

        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFNto1() filename:', filename)
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFNto1() real filename:', filename+'_one.vtkhdf')
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFNto1() self._chunk:', self._chunk)

        try:
            write_vtk_hdf_n_to_1(self._kdi_base.base, filename + '_one.vtkhdf', chunk=self._chunk, force_all_clean=True)
        except Exception as e:
            kdi_log_python.fatal_exception('KDIAgreementStepPartChunk saveVTKHDFNto1()', e)

    def saveVTKHDFCompute(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFCompute()')
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFCompute() filename:', filename)
        kdi_log_python.log('filename:', filename+'.vtkhdf')
        kdi_log_python.log(' self._chunk:', self._chunk)

        new_base = KDIComputeMultiMilieux(self._kdi_base.base)

        try:
            write_vtk_hdf(new_base, filename + '.vtkhdf', chunk=self._chunk)
        except:
            kdi_log_python.fatal_exception('KDIAgreementStepPartChunk saveVTKHDFCompute()')

    def saveVTKHDFComputeNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFNto1()')
        if MPI.COMM_WORLD.Get_size() == 1:
            print('WARNING PYTHON >>> KDIAgreementStepPartChunk No saveVTKHDFNto1() cause sequential mode')
            return

        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFComputeNto1()')
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFComputeNto1() filename:', filename)
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFComputeNto1() real filename:', filename+'_one.vtkhdf')
        kdi_log_python.log('KDIAgreementStepPartChunk saveVTKHDFComputeNto1() self._chunk:', self._chunk)

        new_base = KDIComputeMultiMilieux(self._kdi_base.base)

        try:
            write_vtk_hdf_n_to_1(new_base, filename + '_one.vtkhdf', chunk=self._chunk, force_all_clean=True)
        except:
            kdi_log_python.fatal_exception('KDIAgreementStepPartChunk saveVTKHDFComputeNto1()')
