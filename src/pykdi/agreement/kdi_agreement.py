# pykdi.agreement
from pykdi.agreement.step_part.kdi_agreement_steppart import kdi_agreement_step_part
from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART
# pykdi.tools
from pykdi.tools import kdi_log_python
# pykdi
from pykdi.kdi_update import kdi_update
from pykdi.KDIException import KDIException
from pykdi.KDITyping import KDIDataBaseTyping

def kdi_agreement(configuration: dict[str, any] = None) -> KDIDataBaseTyping:
    """
    """
    kdi_log_python.log('kdi_agreement configuration:', configuration)
    base = kdi_update(None, 'Base', '')

    if configuration and 'agreement' in configuration:
        if kdi_agreement_step_part(base, configuration):
            return base
        raise KDIException('pre: this agreement \'' + configuration['agreement'] +
                           '\' does not match any of the known agreements. [\'' + KDI_AGREEMENT_STEPPART + '\',]!')

    return base
