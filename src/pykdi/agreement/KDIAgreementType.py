KDI_AGREEMENT_STEPPART = 'STEPPART'

KDI_AGREEMENT_STEPPART_VARIANT_STEP = 'step'
KDI_AGREEMENT_STEPPART_VARIANT_PART = 'part'

# Fr: Ce seront les sémantiques retenues par rapport aux valeurs de type des cellules
#     à défaut que le code fournisse sa propre sémantique (code2kdi)
KDI_SIMULATION_TRIANGLE_TYPE_VALUE: int = 0
KDI_SIMULATION_QUADRANGLE_TYPE_VALUE: int = 1
KDI_SIMULATION_HEXAHEDRON_TYPE_VALUE: int = 12
