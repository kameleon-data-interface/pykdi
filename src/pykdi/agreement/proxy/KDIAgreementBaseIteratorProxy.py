from contextlib import contextmanager
import time

from pykdi.session.tools.kdi_log_session import kdi_log_session

class KDIAgreementBaseIteratorProxy:
    """
    """
    __slots__ = '_kdi_base', '_it', '_delay', '_first_variant'

    def __init__(self, base: any, delay: float = 0.5):
        kdi_log_session.log('KDIAgreementBaseIteratorProxy::__init__()')
        kdi_log_session.log('   delay:', delay)
        self._kdi_base = base
        self._it = self._kdi_base._proxy_semantic.iterator(self._kdi_base._id_server_semantic)
        self._delay = delay
        self._first_variant = self._kdi_base._proxy_semantic.first_variant(self._kdi_base._id_server_semantic)

    @contextmanager
    def next(self) -> any:
        """
        Cette méthode ne doit faire qu'un seul yield.
        """
        kdi_log_session.log('KDIAgreementBaseIteratorProxy::next()')
        while True:
            next = self._kdi_base._proxy_semantic.next_iterator(self._kdi_base._id_server_semantic, self._it)
            if next == 'TERMINATED':
                kdi_log_session.log('KDIAgreementBaseIteratorProxy::next() TERMINATED')
                yield 'TERMINATED'
                break
            if next == 'LOOP':
                kdi_log_session.log('KDIAgreementBaseIteratorProxy::next() TERMINATED')
                time.sleep(self._delay)
                continue
            kdi_log_session.log('KDIAgreementBaseIteratorProxy::next()', next)
            with self._kdi_base.open({self._first_variant: next}) as pkchunk:
                print('iterator BEFORE next BASE OPEN')
                yield pkchunk
                print('iterator AFTER next BASE OPEN')
            break
