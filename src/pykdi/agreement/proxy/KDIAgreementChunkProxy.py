import json
import os
import xmlrpc.client

from pykdi.agreement.KDIAgreementChunk import KDIAgreementChunk
from pykdi.session.tools.encorder_decoder import PythonObjectEncoder, PythonObjectDecoder
from pykdi.session.tools.kdi_log_session import kdi_log_session
from pykdi.session.tools.get_uri import get_uri

class KDIAgreementChunkProxy(KDIAgreementChunk):
    """
    """
    def __init__(self, kdi_base: any, configuration: dict[str, any]):
        super().__init__(kdi_base, configuration)
        kdi_log_session.log('KDIAgreementChunkProxy constructeur', self._kdi_base._id_server_semantic, self._kdi_base._mode, self._configuration)

    def first_chunk(self):
        return self._kdi_base._proxy_semantic.first_chunk(self._kdi_base._id_server_semantic, self._kdi_base._mode, self._configuration)

    def close(self):
        kdi_log_session.log('KDIAgreementChunkProxy close(\'' + self._kdi_base._filename + '\')')
        assert (self._kdi_base._proxy_semantic)
        self._kdi_base._proxy_semantic.close(self._kdi_base._id_server_semantic, self._kdi_base._mode, self._configuration)
        self._kdi_base._proxy_storage.close(self._kdi_base._id_server_storage, self._kdi_base._mode, self._configuration)

    def __del__(self):
        kdi_log_session.log('KDIAgreementChunkProxy destructeur', self._kdi_base._id_server_semantic, self._kdi_base._mode, self._configuration)

    def update(self, typename: str, name: str):
        """

        :param typename:
        :param name:
        :return:
        """
        self._kdi_base.update(typename, name)
    def update_fields(self, field_parent_name: str, field_basename: str) -> str:
        """

        :param field_parent_name:
        :param field_basename:
        :return:
        """
        return self._kdi_base.update_fields(field_parent_name, field_basename)

    def update_sub(self, mesh_parent_name: str, submesh_basename: str) -> str:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        return self._kdi_base.update_sub(field_parent_name, field_basename)

    def get_keys(self) -> [str]:
        return self._kdi_base.get_keys()

    def insert(self, key: str, value: any) -> None:
        """

        :param key:
        :param value:
        :return:
        """
        kdi_log_session.log('self._KDIAgreementChunk get() key:', key)
        assert (self._kdi_base._mode == 'w')
        print('_KDIAgreementChunk insert configuration', self._configuration, 'rank', self._kdi_base._mpi_rank)
        numpyData = {
            "array": value
        }
        encodedNumpyData = json.dumps(numpyData, cls=PythonObjectEncoder)
        self._kdi_base._proxy_storage.insert(self._kdi_base._id_server_storage, self._configuration, key, encodedNumpyData)

    def get(self, key: str, chunk: dict[str, any]) -> any:
        """
        :param key:
        :return:
        """
        kdi_log_session.log('self._KDIAgreementChunk get() key:', key, 'chunk', chunk)
        print('_KDIAgreementChunk get configuration', self._configuration)
        encodedNumpyData = self._kdi_base._proxy_storage.get(self._kdi_base._id_server_storage, chunk, key)
        decodedArrays = json.loads(encodedNumpyData, cls=PythonObjectDecoder)
        return decodedArrays['array']

    def erase(self, key: str):
        """
        :param key:
        :return:
        """
        kdi_log_session.log('self._KDIAgreementChunk get() erase:', key)
        self._kdi_base.base[key].erase(chunk=self._chunk)

    def garbageCollector(self) -> None:
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        raise NotImplementedError()

    def saveVTKHDF(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    def saveVTKHDFNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    def saveVTKHDFCompute(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    def saveVTKHDFComputeNto1(self, filename: str | os.PathLike) -> None:
        """

        :param filename:
        :return:
        """
        raise NotImplementedError()

    def dump(self) -> None:
        kdi_log_session.log('KDIAgreementChunkProxy dump')
        print('Euh')

    def __repr__(self) -> str:
        """

        :return:
        """
        kdi_log_session.log('self._KDIAgreementChunk __repr__')
