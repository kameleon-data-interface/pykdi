from contextlib import contextmanager
from mpi4py import MPI
import os
import time
import xmlrpc.client

from pykdi.agreement.KDIAgreementBase import KDIAgreementBase
from pykdi.agreement.proxy.KDIAgreementChunkProxy import KDIAgreementChunkProxy
from pykdi.agreement.proxy.KDIAgreementBaseIteratorProxy import KDIAgreementBaseIteratorProxy
from pykdi.session.tools.kdi_log_session import kdi_log_session
from pykdi.session.tools.get_uri import get_uri

class KDIAgreementBaseProxy(KDIAgreementBase):
    """
    """
    __slots__ = ('_mpi_rank', '_mpi_size', '_id_server_semantic', '_proxy_semantic', '_id_server_storage',
                 '_proxy_storage', '_mode', '_open')
    def __init__(self, filename: str):
        """
        Builder method
        """
        kdi_log_session.log('KDIAgreementBaseProxy::__init__()')
        kdi_log_session.log('   filename: \'', filename, '\'')
        super().__init__(filename)
        kdi_log_session.log('KDIAgreementBaseProxy KDI_SESSION', os.environ['KDI_SESSION'])
        kdi_log_session.log('KDIAgreementBaseProxy KDI_TASK_NAME', os.environ['KDI_TASK_NAME'])
        self._mpi_rank = MPI.COMM_WORLD.Get_rank()
        self._mpi_size = MPI.COMM_WORLD.Get_size()
        self._id_server_semantic = None
        self._proxy_semantic = None
        self._id_server_storage = None
        self._proxy_storage = None
        self._mode = None
        self._open = False

    def initialize(self, mode_read: bool = False, mode: str = ''):
        """
        :param typename:
        :param mode_read: true/false initialisation par lecture d'un fichier
        :param mode:
        :return:
        """
        kdi_log_session.log('KDIAgreementStepPartBase::initialize(\'' + self._filename + '\'')
        kdi_log_session.log('   mode_read \'', mode_read, '\'')
        kdi_log_session.log('   mode \'', mode, '\'')
        super().initialize(mode_read)

        assert (mode in ['r', 'w'])
        self._mode = mode

        kdi_session_conf = eval(os.environ['KDI_SESSION'])

        self._conf['nb_writers'] = kdi_session_conf[self._filename]['nb_writers']
        self._conf['nb_readers'] = kdi_session_conf[self._filename]['nb_readers']

        address, port = kdi_session_conf[self._filename]['semantic']
        kdi_log_session.log('semantic server:')
        kdi_log_session.log('   address \'' + address + '\'')
        kdi_log_session.log('   port', port)
        uri = get_uri(address, port)
        kdi_log_session.log('   uri \'' + uri + '\'')
        self._proxy_semantic = xmlrpc.client.ServerProxy(uri)
        self._id_server_semantic = self._proxy_semantic.initialize(self._filename, self._conf, self._mode)

        # TODO Suivant la politique d'utilisation des serveurs de stockage
        # On pourrait avoir un stockage par temps par serveur ou une distribution des parts... etc...
        type_storage, (address, port) = kdi_session_conf[self._filename]['storages'][self._mpi_rank % len(kdi_session_conf[self._filename]['storages'])]
        type_storage, (address, port) = kdi_session_conf[self._filename]['storages'][0]
        kdi_log_session.log('storage server:')
        kdi_log_session.log('   address \'' + address + '\'')
        kdi_log_session.log('   port', port)
        uri = get_uri(address, port)
        kdi_log_session.log('   uri \'' + uri + '\'')
        self._proxy_storage = xmlrpc.client.ServerProxy(uri)
        self._id_server_storage = self._proxy_storage.initialize(self._filename, self._conf, self._mode)

        kdi_log_session.log('KDIAgreementStepPartBase::initialize(\'' + self._filename + '\',', self._id_server_semantic, ')')

    def finalize(self):
        assert (self._proxy_semantic)
        self._proxy_semantic.finalize(self._id_server_semantic, self._mode)

    def iterator(self, delay: float = 0.5) -> KDIAgreementBaseIteratorProxy:
        assert (self._mode == 'r')
        return KDIAgreementBaseIteratorProxy(self, delay)

    @contextmanager
    def open(self, configuration: dict[str, any]) -> KDIAgreementChunkProxy:
        """
        Cette méthode ne doit faire qu'un seul yield.
        """
        kdi_log_session.log('KDIAgreementStepPartBase::open(\'' + self._filename + '\'')
        assert (self._proxy_semantic)
        if self._proxy_semantic.open(self._id_server_semantic, self._mode, configuration):
            assert (self._proxy_storage.open(self._id_server_storage, self._mode, configuration))
            kdi_chunk = KDIAgreementChunkProxy(self, configuration)
            try:
                print('base proxy open BEFORE yield OPEN for close')
                yield kdi_chunk
                print('base proxy open AFTER yield OPEN for close')
            finally:
                kdi_chunk.close()

    def update(self, typename: str, name: str): #TODO depends_variant ?
        """

        :param typename:
        :param name:
        :return:
        """
        assert (self._mode == 'w')
        assert (self._proxy_semantic and self._proxy_storage)
        self._proxy_semantic.update(self._id_server_semantic, typename, name)
        self._proxy_storage.update(self._id_server_storage, typename, name)

    def update_fields(self, field_parent_name: str, field_basename: str) -> str: #TODO depends_variant ?
        """

        :param field_parent_name:
        :param field_basename:
        :return:
        """
        assert (self._mode == 'w')
        assert (self._proxy_semantic and self._proxy_storage)
        field = self._proxy_storage.update_fields(self._id_server_semantic, field_parent_name, field_basename)
        assert (field == self._proxy_semantic.update_fields(self._id_server_semantic, field_parent_name, field_basename))
        return field

    def update_sub(self, mesh_parent_name: str, submesh_basename: str) -> str: #TODO depends_variant ?
        """

        :param mesh_parent_name:
        :param submesh_basename:
        :return:
        """
        assert (self._mode == 'w')
        assert (self._proxy_semantic and self._proxy_storage)
        mesh = self._proxy_semantic.update_sub(self._id_server_semantic, mesh_parent_name, submesh_basename)
        assert( mesh == self._proxy_semantic.update_sub(self._id_server_semantic, mesh_parent_name, submesh_basename))
        return mesh

    def get_keys(self) -> [str]:
        assert (self._proxy_semantic)
        return self._proxy_semantic.get_keys(self._id_server_semantic)

    def chunk(self, configuration: dict[str, any] = None) -> KDIAgreementChunkProxy:
        raise NotImplementedError()

    def dump(self):
        """
        """
        assert (self._proxy_semantic)
        print(self._proxy_semantic.dump(self._id_server_semantic))

    def light_dump(self):
        """
        """
        assert (self._proxy_storage)
        print(self._proxy_storage.light_dump(self._id_server_semantic))

    def __repr__(self) -> str:
        """
        Representation of the description of the current instance.
        In KDIMemory, for example, dump all values.
        """
        return 'KDIAgreementBaseProxy'
