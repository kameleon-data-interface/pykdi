import numpy as np

class Plan:

    __slots__ = '_position', '_normal', '_d'

    def __init__(self, position, normal):
        self._position = position
        normal.astype('float64')
        self._normal = normal / np.linalg.norm(normal)
        tmp = self._position * self._normal
        self._d = - tmp.sum()

    @property
    def normal(self):
        return self._normal

    @property
    def a(self):
        return self._normal[0]

    @property
    def b(self):
        return self._normal[1]

    @property
    def c(self):
        return self._normal[2]

    @property
    def d(self):
        return self._d

    def __repr__(self):
        return 'Plan(' + str(self._position) + ',' + str(self._normal) + ')'


if __name__ == '__main__':
    plan = Plan(np.array([0., 0., 0.]), np.array([0., 0., 1.]))
    print('----- PLAN -----')
    print(plan)