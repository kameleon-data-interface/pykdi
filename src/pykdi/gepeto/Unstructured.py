from Cell import *
from CellTypes import *
from Plan import *

class Unstructured:

    __slots__ = '_vertices_coordinates', '_cells_type', '_cells_connectivity'

    def __init__(self, vertices_coordinates, cells_type, cells_connectivity):
        self._vertices_coordinates = vertices_coordinates
        self._cells_type = cells_type
        self._cells_connectivity = cells_connectivity

    def cells_iterator(self, vertices_pounds = None):
        cell_connectivity_begin = 0
        for cell_type in self._cells_type:
            cell_connectivity_end = cell_connectivity_begin + cell_type_to_number_of_vertices[cell_type]
            if vertices_pounds is None:
                yield Cell(cell_type, self._vertices_coordinates[self._cells_connectivity[cell_connectivity_begin:cell_connectivity_end]])
            else:
                connectivity = self._cells_connectivity[cell_connectivity_begin:cell_connectivity_end]
                yield Cell(cell_type, self._vertices_coordinates[connectivity]), vertices_pounds[connectivity]
            cell_connectivity_begin = cell_connectivity_end

    def plan_slice(self, plan):
        # compute distance from a vertice to the plane
        tmp = self._vertices_coordinates * plan.normal
        vertices_pounds = tmp.sum(axis=1) + plan.d
        print('self._vertices_coordinates', self._vertices_coordinates)
        print('PLAN plan._position', plan._position)
        print('        plan.normal', plan.normal)
        print('tmp.sum(axis=1)', tmp.sum(axis=1))
        print('plan.d', plan.d)
        print('>>> vertices_pounds', vertices_pounds)
        #
        n_vertice = 0
        vertices_coordinates_dict = dict()
        vertices_coordinates = []
        cells_type = []
        cells_connectivity = []
        for cell, cell_vertices_pounds in mesh.cells_iterator(vertices_pounds):
            new_cells = cell.slice(cell_vertices_pounds)
            for new_cell in new_cells:
                for vertice in new_cell.vertices_iterator():
                    key = hash((vertice[0],vertice[1],vertice[2]))
                    try:
                        cells_connectivity.append(vertices_coordinates_dict[key])
                    except:
                        vertices_coordinates_dict[key] = n_vertice
                        vertices_coordinates.append(vertice)
                        cells_connectivity.append(n_vertice)
                        n_vertice += 1
                cells_type.append(new_cell.cell_type)
        return Unstructured(np.array(vertices_coordinates), np.array(cells_type), np.array(cells_connectivity))

    def __repr__(self):
        return 'Unstructured(' + str(self._vertices_coordinates) + ',' + str(self._cells_type) + ',' + str(self._cells_connectivity) + ')'



if __name__ == '__main__':
    coordinates = []
    for ix in range(3):
        for iy in range(2):
            coordinates.append([float(ix), float(iy), 0.])
    vertices_coordinates = np.array(coordinates)

    cells_types = np.array([TRIANGLE, QUADRANGLE, TRIANGLE])

    cells_connectivity = np.array(
        [0, 1, 2,
         2, 3, 5, 4,
         1, 3, 2]
    )

    mesh = Unstructured(vertices_coordinates, cells_types, cells_connectivity)
    print('----- MESH -----')
    for cell in mesh.cells_iterator():
        print(cell)

    plan = Plan(np.array([0.5, 0.25, 0.]), np.array([1., 0., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    print('NEW MESH')
    print('  vertices_coordinates', new_mesh._vertices_coordinates)
    print('            cells_type', new_mesh._cells_type)
    print('    cells_connectivity', new_mesh._cells_connectivity)
    assert np.all(new_mesh._vertices_coordinates == np.array([[0.5, 0., 0.], [0.5, 0.5, 0.], [0.5, 1., 0.]]))
    assert np.all(new_mesh._cells_type == np.array([3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 1, 2]))

    plan = Plan(np.array([0.5, 0.25, 0.]), np.array([-1., 0., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    assert np.all(new_mesh._vertices_coordinates == np.array([[0.5, 0., 0.], [0.5, 0.5, 0.], [0.5, 1., 0.]]))
    assert np.all(new_mesh._cells_type == np.array([3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 1, 2]))

    plan = Plan(np.array([0.5, 0.25, 0.]), np.array([0., 1., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    assert np.all(new_mesh._vertices_coordinates == np.array([[0., 0.25, 0.], [0.75, 0.25, 0.], [1., 0.25, 0.], [2., 0.25, 0.]]))
    assert np.all(new_mesh._cells_type == np.array([3, 3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 2, 3, 1, 2]))

    plan = Plan(np.array([0.5, 0.25, 0.]), np.array([0., -1., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    assert np.all(new_mesh._vertices_coordinates == np.array([[0., 0.25, 0.], [0.75, 0.25, 0.], [1., 0.25, 0.], [2., 0.25, 0.]]))
    assert np.all(new_mesh._cells_type == np.array([3, 3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 2, 3, 1, 2]))

    plan = Plan(np.array([1., 1., 0.]), np.array([1., -1., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    print('NEW MESH')
    print('  vertices_coordinates', new_mesh._vertices_coordinates)
    print('            cells_type', new_mesh._cells_type)
    print('    cells_connectivity', new_mesh._cells_connectivity)
    assert np.all((new_mesh._vertices_coordinates - np.array([[0., 0., 0.], [0.70710678, 0.70710678, 0.], [1., 1., 0.]])) < 1e-7)
    assert np.all(new_mesh._cells_type == np.array([3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 1, 2]))

    plan = Plan(np.array([-0.25, 0.75, 0.]), np.array([-0.25, -1., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    print('NEW MESH')
    print('  vertices_coordinates', new_mesh._vertices_coordinates)
    print('            cells_type', new_mesh._cells_type)
    print('    cells_connectivity', new_mesh._cells_connectivity)
    assert np.all((new_mesh._vertices_coordinates - np.array([[0., 0.66697297, 0.], [0.30316953, 0.42443734, 0], [0.9701425, 0.42443734, 0.], [1.940285, 0.18190172, 0.]])) < 1e-7)
    assert np.all(new_mesh._cells_type == np.array([3, 3, 3]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 2, 3, 1, 2]))

    # HEXAEDRE

    coordinates = []
    for ix in range(3):
        for iz in range(2):
            for iy in range(2):
                coordinates.append([float(ix), float(iy), float(iz)])
    vertices_coordinates = np.array(coordinates)

    cells_types = np.array([HEXAHEDRON, HEXAHEDRON])

    cells_connectivity = np.array(
        [0, 1, 5, 4, 2, 3, 7, 6,
         4, 5, 9, 8, 6, 7, 11, 10]
    )

    mesh = Unstructured(vertices_coordinates, cells_types, cells_connectivity)
    print('----- MESH -----')
    for cell in mesh.cells_iterator():
        print(cell)

    plan = Plan(np.array([0.25, 0.25, 0.25]), np.array([1., 0., 0.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    print('NEW MESH')
    print('  vertices_coordinates', new_mesh._vertices_coordinates)
    print('            cells_type', new_mesh._cells_type)
    print('    cells_connectivity', new_mesh._cells_connectivity)
    assert np.all((new_mesh._vertices_coordinates - np.array([[0.25, 1., 0.], [0.25, 1., 1.], [0.25, 0., 0.], [0.25, 0., 1.]])) < 1e-7)
    assert np.all(new_mesh._cells_type == np.array([5, 5]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 2, 2, 1, 3]))

    plan = Plan(np.array([0.25, 0.25, 0.25]), np.array([1., 1., 1.]))
    print('----- PLAN_SLICE -----')
    new_mesh = mesh.plan_slice(plan)
    print('----- CELLS NEW MESH -----')
    for cell in new_mesh.cells_iterator():
        print(cell)
    print('NEW MESH')
    print('  vertices_coordinates', new_mesh._vertices_coordinates)
    print('            cells_type', new_mesh._cells_type)
    print('    cells_connectivity', new_mesh._cells_connectivity)
    assert np.all((new_mesh._vertices_coordinates - np.array([[0., 0.75, 0.], [0., 0., 0.75], [0.75, 0., 0.]])) < 1e-7)
    assert np.all(new_mesh._cells_type == np.array([5]))
    assert np.all(new_mesh._cells_connectivity == np.array([0, 1, 2]))
