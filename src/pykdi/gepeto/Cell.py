import numpy as np

from CellTypes import *
from MarchingCubesCases import *

class Cell:

    __slots__ = '_type', '_vertices'

    def __init__(self, type, vertices):
        self._type = type
        self._vertices = vertices

    @property
    def cell_type(self):
        return self._type

    def slice(self, cell_vertices_pounds):
        value = 0

        new_vertices = []
        if cell_is_polygon(self._type):
            if np.all(cell_vertices_pounds < value - 1e-10) or np.all(cell_vertices_pounds > value + 1e10):
                return []
            prev_vertice = self._vertices[-1]
            prev_pounds = cell_vertices_pounds[-1]
            for vertice, pounds in zip(self._vertices, cell_vertices_pounds):
                if pounds == value:
                    new_vertices.append(vertice)
                elif prev_pounds == value:
                    pass
                elif pounds < value:
                    if value < prev_pounds:
                        new_vertice = vertice * prev_pounds - prev_vertice * pounds
                        new_vertices.append(new_vertice)
                elif prev_pounds < value:
                    new_vertice = prev_vertice * pounds - vertice * prev_pounds
                    new_vertices.append(new_vertice)
                prev_vertice = vertice
                prev_pounds = pounds
            if len(new_vertices) < 2:
                return []
            assert (len(new_vertices) == 2)
            return [Cell(LINE, new_vertices)]

        new_cells = []
        i_case = 0

        if self._type == TETRAHEDRON:
            for pounds, mask in zip(cell_vertices_pounds, MARCHING_CUBES_CASE_MASK[0:4]):
                if pounds >= 0:
                    i_case |= mask

            cases = MARCHING_CUBES_CASES[i_case]
            edges = MARCHING_CUBES_EDGES_TETRAHEDRON

        elif self._type == HEXAHEDRON:
            for pounds, mask in zip(cell_vertices_pounds, MARCHING_CUBES_CASE_MASK[0:9]):
                if pounds >= 0:
                    i_case |= mask

            cases = MARCHING_CUBES_CASES[i_case]
            edges = MARCHING_CUBES_EDGES_HEXAHEDRON

        else:
            print('NO COMPUTE SLICE')
            return [self]

        for mctrivert in cases.reshape((5, 3)):
            if mctrivert[0] == -1:
                break
            # chaque face quadrangle est decoupee en triangle
            for mcvert in mctrivert:
                vert = edges[mcvert]
                #  calculate a preferred interpolation direction
                v1 = vert[0]
                v2 = vert[1]
                delta_pounds = cell_vertices_pounds[v2] - cell_vertices_pounds[v1]
                if delta_pounds > 0:
                    pass
                else:
                    v1, v2 = v2, v1
                    delta_pounds = -delta_pounds

                # linear interpolation
                if delta_pounds == 0.:
                    t = 0.
                else:
                    t = value - cell_vertices_pounds[v1] / delta_pounds

                x1 = self._vertices[v1]
                x2 = self._vertices[v2]
                x = x1 + t * (x2 - x1)
                new_vertices.append(x)

            if len(new_vertices) == 2:
                new_cells.append(Cell(LINE, new_vertices))
            elif len(new_vertices) == 3:
                new_cells.append(Cell(TRIANGLE, new_vertices))
            elif len(new_vertices) == 4:
                new_cells.append(Cell(QUADRANGLE, new_vertices))
            else:
                exit(1)
            new_vertices = []
        return new_cells

    def vertices_iterator(self):
        for vertice in self._vertices:
            yield vertice

    def __repr__(self):
        return 'Cell(' + str(self._type) + ',' + str(self._vertices) + ')'


if __name__ == '__main__':
    cell = Cell(TRIANGLE, np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.]]))
    print('----- CELL -----')
    print(cell)
    print('----- VERTICE -----')
    for vertice in cell.vertices_iterator():
        print(vertice)
