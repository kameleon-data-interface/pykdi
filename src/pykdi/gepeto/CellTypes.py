import numpy as np

LINE: int = 3
TRIANGLE: int = 5
QUADRANGLE: int = 9

TETRAHEDRON: int = 10
HEXAHEDRON: int = 12


def cell_is_polygon(cell_type):
    return cell_type in [LINE, TRIANGLE, QUADRANGLE]


cell_type_to_number_of_vertices = np.ndarray(16, dtype=np.int8)

cell_type_to_number_of_vertices[LINE] = 2
cell_type_to_number_of_vertices[TRIANGLE] = 3
cell_type_to_number_of_vertices[QUADRANGLE] = 4

cell_type_to_number_of_vertices[TETRAHEDRON] = 4
cell_type_to_number_of_vertices[HEXAHEDRON] = 8
