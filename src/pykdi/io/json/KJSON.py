import json
from json import JSONEncoder

import numpy as np
from mpi4py import MPI
from numpy import array, int8  # essential when calling Eval
import os
from typing import Any

from pykdi.KDIComplexType import KDIComplexType
from pykdi.KDIChunks import KDIChunks, KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT
from pykdi.mutation.KDIComputeMultiMilieux import KDIComputeMultiMilieux
from pykdi.evaluation_string.KDIEvalBase64 import KDIEvalBase64
from pykdi.evaluation_string.KDIEvalScript import KDIEvalScript
from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from pykdi.KDIException import KDIException
from pykdi.KDIExpression import KDIExpression
from pykdi.KDIFields import KDIFields
from pykdi.KDIMemory import KDIMemory
from pykdi.KDISubMeshes import KDISubMeshes

from pykdi.agreement.io.vtkhdf.KVTKHDFDatas import KVTKHDFDatas
from pykdi.agreement.io.vtkhdf.KVTKHDFStepPartChunks import KVTKHDFStepPartChunks
from pykdi.agreement.io.vtkhdf.KVTKHDFGlobalFieldDatas import KVTKHDFGlobalFieldDatas


class KDIExceptionNoWriteFileJson(KDIException):
    """
    Fr: Gestion spécifique des exceptions
    """
    pass


class NumpyArrayEncoder(JSONEncoder):

    def default(self, obj: Any) -> Any:
        if isinstance(obj, np.ndarray):
            # returns something like: array([], dtype=)
            return obj.__repr__()
        if isinstance(obj, KDIComplexType):
            return obj.serialize()
        if isinstance(obj, KDIChunks):
            return obj.serialize()
        if isinstance(obj, KDIFields):
            return obj.serialize()
        if isinstance(obj, KDISubMeshes):
            return obj.serialize()
        if isinstance(obj, KDIMemory):
            return obj.serialize()
        if isinstance(obj, KDIEvalBase64):
            return obj.serialize()
        if isinstance(obj, KDIEvalString):
            return obj.serialize()
        if isinstance(obj, KDIEvalScript):
            return obj.serialize()
        if isinstance(obj, KDIExpression):
            return obj.serialize()
        if isinstance(obj, KVTKHDFStepPartChunks):
            return obj.serialize()
        if isinstance(obj, KVTKHDFDatas):
            return obj.serialize()
        if isinstance(obj, KVTKHDFGlobalFieldDatas):
            return obj.serialize()
        return JSONEncoder.default(self, obj)


def write_json(base: dict[str, any], filename: str | os.PathLike):
    """
    Ecrit dans un fichier JSON la description d'une base KDI.

    Attention, certaines manipulations de base peuvent ne pas permettre
    la sérialisation. C'est tout particulièrement le cas si vous
    utilisez KDIEvalMethod ou KDIComputeMultiMilieux.

    :param base: le dictionnaire qui décrit la base KDI à sauvegarder en JSON.
    :param filename: le nom du fichier à écrire, l'extension .json sera rajouté.
    """
    mpi_rank = MPI.COMM_WORLD.Get_rank()

    json_dir_name = os.path.dirname(filename)
    if json_dir_name:
        json_dir_name += '/'
    assert (os.path.basename(filename).split('.')[1] in ['json', 'vtkhdf'])
    json_filename = json_dir_name + os.path.basename(filename).split('.')[0] + '.json'

    if mpi_rank != 0:
        MPI.COMM_WORLD.Barrier()
        return False

    after_load_json = []
    json_base = base
    while '/before_write' in json_base:
        json_base = base['/before_write'][0]
        after_load_json.insert(0, base['/before_write'][1])
    assert ('/after_load_json' not in base)

    try:
        current = json_base['/chunks/current']
        del json_base['/chunks/current']
    except:
        current = None

    if len(after_load_json) > 0:
        json_base['/after_load_json'] = str(after_load_json)

    try:
        with open(json_filename, 'w', encoding='utf-8') as fd:
            json.dump(json_base, fd, cls=NumpyArrayEncoder, ensure_ascii=False, indent=2)
    except Exception as e:
        raise KDIExceptionNoWriteFileJson('No write JSON:', json_filename, 'Cause', e)

    try:
        del json_base['/after_load_json']
    except:
        pass

    if current:
        json_base['/chunks/current'] = current

    MPI.COMM_WORLD.Barrier()
    return True


def read_json(filename: str | os.PathLike):
    """
    Lit depuis un fichier JSON la description d'une base KDI.
    :param filename: le nom du fichier à lire, à l'utilisateur de passer ce nom avec l'extension souhaitée.
    :return: retourne la base ainsi chargée.
    """
    mpi_rank = MPI.COMM_WORLD.Get_rank()

    json_dir_name = os.path.dirname(filename)
    if json_dir_name:
        json_dir_name += '/'
    assert(os.path.basename(filename).split('.')[1] in ['json', 'vtkhdf'])
    json_filename = json_dir_name + os.path.basename(filename).split('.')[0] + '.json'

    KDIBase = dict()
    with open(json_filename, 'r') as fd:
        loadbase = json.load(fd)

    if '/after_load_json' in loadbase:
        treatments = eval(loadbase['/after_load_json'])
        del loadbase['/after_load_json']
    else:
        treatments = ()

    KDIBase.update(loadbase)
    code2kdi = None
    force_common_data_offsets = None
    for key, val in KDIBase.items():
        if key == '/vtkhdf/force_common_data_offsets':
            force_common_data_offsets = val
            continue
        try:
            KDIBase[key] = eval(val)
        except TypeError:
            print('TypeError:', val)
            raise KDIException()
        except SyntaxError:
            print('SyntaxError:', val)
            raise KDIException()
    if code2kdi is not None:
        KDIBase['/code2kdi'] = code2kdi
    if force_common_data_offsets is not None:
        KDIBase['/vtkhdf/force_common_data_offsets'] = force_common_data_offsets

    KDIBase['/chunks'].set({'step': KDIBase['/chunks'].GetValue('step', -1), 'part': KDIBase['/chunks'].GetValue('part', -1)})

    for treatment in treatments:
        KDIBase = eval(treatment)

    return KDIBase
