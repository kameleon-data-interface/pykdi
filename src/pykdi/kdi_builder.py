import json
import os

from pykdi.tools.kdi_get_env import kdi_get_env_str

from pykdi.agreement.proxy.KDIAgreementBaseProxy import KDIAgreementBaseProxy
from pykdi.agreement.step_part.KDIAgreementBaseStepPart import KDIAgreementBaseStepPart

def kdi_builder(filepath: str):
    try:
        print ('KDI_SESSION', os.environ['KDI_SESSION'])
        print ('KDI_TASK_NAME', os.environ['KDI_TASK_NAME'])
        print('kdi_build KDIAgreementBaseProxy')
        kdi_base = KDIAgreementBaseProxy(filepath)
    except:
        print('kdi_build default')
        kdi_base = KDIAgreementBaseStepPart(filepath)
    return kdi_base
