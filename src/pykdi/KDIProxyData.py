from .KDIDatas import KDIDatas


class KDIProxyData(KDIDatas):
    """
    Cette classe permet d'associer comme valeur du dictionnaire en sortie, une clef du dictionnaire en entree
    avec possibilité de changer la valeur d'un variant du chunk courant du dictionnaire en sortie qui sera passé
    au dictionnaire en entrée lors de l'accès à la valeur pour un chunk donné.
    """
    __slots__ = 'base_out', 'base_out_chunk_todo', 'base_in_data', 'base_in_key', 'chg_variants'

    def __init__(self, base_out: dict[str, any], base_in: dict[str, any], base_in_key: str, chg_variants: dict[str, any]= {}):
        """
        :param base_out: (in) le dictionnaire en sortie afin d'atteindre la valeur du chunk courant
        :param base_in: (in) le dictionnaire en entree afin d'atteindre la liste des variants et la valeur
        pour la clef en entree
        :param base_in_key: (in) la clef dans le dictionnaire en entree ; on considère à tord qu'elle sera toujours
        la même que celle dnas le dicitonnaire de sortie... ce qui est actuellement le cas dans l'usage actuel.
        :param chg_variants: (in) un dictionnaire pour lequel on associe une valeur à une clef de variants
            Les variants étant définis dans un ordre, les clefs que l'on retrouve ici sont forcément celles que l'on
            retrouve d'abord en parcourant en sens inverse cette liste des variants ordonnées au niveau du
            dictionnaire en sortie.
            Ce champ peut ne pas être renseigné, c'est le cas pour KDIComputeMultiMilieux.
            Ce champ peut être renseigné, c'est le cas pour KDI1toN afin de transformer un chunk {'step':?, 'part':?}
            en l'équivalent mono-domaine {'step':?, 'part':0} avant d'appliquer une sélection sur ces valeurs.
        """
        super().__init__()
        self.base_out = base_out
        self.base_in = base_in
        self.base_in_key = base_in_key
        self.base_in['/chunks'].check_variants(chg_variants)
        if '/chunks' in self.base_out:
            self.base_out['/chunks'].check_variants(chg_variants)
            self.base_out_chunk_todo = True
        else:
            self.base_out_chunk_todo = False
        self.chg_variants = chg_variants

    def insert(self, data, chunk=None) -> None:
        raise NotImplementedError()

    def erase(self, chunk: dict[str, any]=None) -> None:
        raise NotImplementedError()

    def get(self, chunk=None) -> any:
        if self.base_out_chunk_todo:
            self.base_out['/chunks'].check_variants(self.chg_variants)
            self.base_out_chunk_todo = True
        chunk = self.base_out['/chunks'].reduce_chunk(self.chg_variants, chunk)
        return self.base_in[self.base_in_key].get(chunk)

    def get_complex_type(self) -> str:
        return self.base_in[self.base_in_key].get_complex_type()

    def __repr__(self) -> str:
        return self.base_in[self.base_in_key].__repr__()
