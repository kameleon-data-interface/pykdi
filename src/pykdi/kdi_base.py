# pykdi.agreement
from pykdi.agreement.step_part.kdi_agreement_steppart import kdi_agreement_step_part
from pykdi.agreement.KDIAgreementType import KDI_AGREEMENT_STEPPART
# pykdi.tools
from pykdi.tools import kdi_log_python
# pykdi
from .kdi_update import kdi_update
from .KDIException import KDIException
from .KDIDatas import KDIDatas
from .KDITyping import KDIDataBaseTyping

def kdi_base(configuration: dict[str, any] = None) -> KDIDataBaseTyping:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Cette méthode crée une nouvelle base en mémoire à partir de rien.
        Cela inclut automatiquement la création d'une instance de type Base nommée ''
        ainsi que de ses attributs.
        Si la configuration comprend le mot clef 'agreement' alors une configuration pré-définie
        sera utilisée déclarant de fait les variants qui sont associés à cet agrément.
        :configuration: (in) La configuration est un dictionnaire qui peut comprendre le mot clef
            'agreement'. En fonction de la valeur associée à 'agreement', on a potentiellement
            d'autres clefs de défini.
            Ainsi, pour la valeur d'agrément KDI_AGREEMENT_STEPPART associée à la clef 'agreement', les clefs
            'nb_parts' (obligatoire) et 'code2kdi' (optionnelle) sont à définir.
        :return: base

        Testé par test_kdi_base.py
    """
    kdi_log_python.log('kdi_base configuration:', configuration)
    base = kdi_update(None, 'Base', '')

    if configuration is not None and 'agreement' in configuration:
        if kdi_agreement_step_part(base, configuration):
            return base
        # Fr: A l'avenir
        #   C'est ici que l'on pourra insérer d'autre nouveau agrément.
        #   Il ne faudra pas oublier de rajouter dans le message d'exception qui suit le nom
        #   possible pour ces nouveaux agréments.
        raise KDIException('pre: this agreement \'' + configuration['agreement'] +
                           '\' does not match any of the known agreements. [\'' + KDI_AGREEMENT_STEPPART + '\',]!')

    return base
