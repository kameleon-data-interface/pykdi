from .KDIList import KDIList


class KDISubMeshes(KDIList):
    """
    Object in graph.
    KDISubMesh allows you to list the keys of the fields under this object.
    This object inherits from KDIList as KDIAttributes.
    KDIList inherits from KDIDatas which makes it an instantiable object at the graph level.

    REMARK/TODO
    All of these objects must be the same size.
    An optional check could be implemented to validate this in accordance with the
    size of the support which is, a priori, the object corresponding to the parent key.
    This check should be done as this object is populated and from the moment the parent object has been created.
    Currently, there are no such constraints.
    """

    def __repr__(self):
        """
        The description via the string thus returned is equivalent to that of a serialization.
        That is to say that applying an eval on it is possible... to a certain extent.
        :return: a string representing the instance of this object
        """
        return self.compute_str('KDISubMeshes')
