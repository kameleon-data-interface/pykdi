class CompositeManager(object):
    """
    An object to store the managers of underlying storages
    """
    def __init__(self, managers):
        self._managers = managers

    @property
    def info(self):
        """
        Return the list of underlying factory infos
        """
        return [(name, storage.info) for (name, storage) in self._managers.items()]

    def close(self):
        """
        Close all storage managers
        """
        exit_code = set()
        for storage in self._managers.values():
            exit_code.add(storage.close())
        return max(exit_code)
