#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
 This module provides a ResourceManager class that can be used to spawn processes on specific hosts
 based on user-defined allocation strategy.

 Contract for spawn target function:
 - be non-blocking
 - return an object that has 'info' attribute and 'close' method
 - 'close' method returns an exit code

 The spawn() method of the ResourceManager returns a proxy of the target implementing the above contract.
"""
import sys
import itertools
from functools import partial
import multiprocessing
import os
import shlex
import subprocess

import pykdi.session.net.ssh_process
from pykdi.session.net.singleton import Singleton

# Various allocation strategies
def local_allocation():
    """
    Always return localhost
    """
    if 'SLURM_NODELIST' in os.environ:
        hostname = os.environ['SLURM_NODELIST'].split()[0]
    else:
        hostname = 'localhost'
    print('allocate on node \'%s\'', hostname)
    return itertools.repeat(hostname)


def round_robin_allocation_slurm():
    """
    Generate the nodelist from slurm and do a round robin on it
    """
    nodelist_pid = subprocess.Popen(shlex.split("ccc_nodelist -e {0}".format(os.environ['SLURM_NODELIST'])),
                                    stdout=subprocess.PIPE)
    nodelist_pid.wait()
    nodelist = nodelist_pid.stdout.read().strip('\n').split(' ')
    return itertools.cycle(nodelist)


class TargetProxy(object):
    """
    Proxy of the spawned object.
    """
    def __init__(self, process, info_queue, stop_queue):
        self._process = process
        self._info_queue = info_queue
        self._stop_queue = stop_queue
        self._info = None

    @property
    def info(self):
        """
        Retrieve 'info' attribute from the spawned object
        """
        if not self._info:
            self._info = self._info_queue.get()
        return self._info

    def close(self):
        """
        Send stop signal to spawned object and retrieve its exit code
        """
        self._stop_queue.put(0)
        self._process.join()
        return self._process.exitcode


class ResourcesManager(object):
    """
    Manage hosts on which new processes are spawned. Default allocation strategy is on localhost.
    """
    __metaclass__ = Singleton

    def __init__(self, host_strategy=local_allocation()):
        self._host_strategy = host_strategy

    def set_host_strategy(self, strategy):
        """
        Set strategy to determine on which host a process is spawned.

        :param strategy: generator returning mapping
        """
        self._host_strategy = strategy

    def spawn(self, target, args=()):
        """
        Spawn target according to host strategy.

        :param target: function called in new process
        :param args: target function arguments
        :return: proxy to spawned process
        """
        def _spawn_target(target_func, arguments, info_q, stop_q):
            """
            Function launched in a new process
            """
            obj = target_func(*arguments)
            info_q.put(obj.info)
            stop_q.get()  # next item in queue signals to stop process
            ret = obj.close()
            exit_code = ret or 0
            sys.exit(exit_code)

        host_allocated = next(self._host_strategy)
        new_process, new_queue = gen_process_factory(host_allocated)
        info_queue, stop_queue = new_queue(), new_queue()
        process = new_process(target=_spawn_target, args=[target, args, info_queue, stop_queue])
        process.start()
        return TargetProxy(process, info_queue, stop_queue)


def gen_process_factory(host):
    """
    Return Process and Queue classes to spawn a new process on host (localhost if None)
    """
    if not host or host == 'localhost':
        queue_cls = multiprocessing.Queue
        process_cls = multiprocessing.Process
    else:
        queue_cls = ssh_process.Queue
        process_cls = partial(ssh_process.SSHProcess, host=host)
    return process_cls, queue_cls
