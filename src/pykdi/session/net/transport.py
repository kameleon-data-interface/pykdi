# -*- coding: iso-8859-1 -*-
"""
Client-Server module based on ZeroMQ sockets and Tornado asynchronous networking mechanisms
"""
import threading
import multiprocessing
import logging
from contextlib import contextmanager
from functools import wraps
import pickle

import zmq

import tornado.gen

from pykdi.session.net.singleton import Singleton

__logger__ = None

class ConnectionCache(object):
    """
    Cache to recycle connection objects
    """
    __metaclass__ = Singleton
    __dict_clients__ = dict()

    @classmethod
    def get_connection(cls, key, factory):
        """
        Get connection object represented by a key, create connection if key not found.

        :param key: key representing a connection
        :param factory: factory to instantiate a new connection object
        :return: connection object
        """
        # Connection must be local to a thread
        # add the thread indent to the conn_key
        key = (key, threading.get_ident())
        try:
            return cls.__dict_clients__[key]
        except KeyError:
            cls.__dict_clients__[key] = factory()
        return cls.__dict_clients__[key]

    @classmethod
    def delete_connection(cls, key):
        """
        Delete connection object represented by a key

        :param key: key representing a connection
        """
        del cls.__dict_clients__[(key, thread.get_ident())]

    @classmethod
    def clean_cache(cls):
        """
        Clean connections cache
        """
        for conn in cls.__dict_clients__.values():
            conn.close()
        cls.__dict_clients__ = dict()


class PickleMessage(object):
    """
    Simple Message class using cPickle as serializer.
    """
    def __init__(self, args):
        self._args = args

    def dump(self):
        """
        Serialize message
        """
        return [cPickle.dumps(self._args)]

    @classmethod
    def load(cls, msg):
        """
        De-serialize message
        """
        return cPickle.loads(msg[0])


class Client(object):
    """
    Request-Reply protocol client
    """
    def __init__(self, uri, message_class=PickleMessage, logger=__logger__):
        self._uri = uri
        self._Message = message_class
        self._logger = logger
        self._zmq_sock = ConnectionCache().get_connection(uri, lambda: self._make_connection(self._uri))

    @staticmethod
    def _make_connection(uri):
        socket = zmq.Context().socket(zmq.DEALER)
        socket.connect(uri)
        return socket

    def request(self, args):
        """
        Send request to server

        :param args: request arguments
        :return: server reply object
        """
        msg = self._Message(args)
        self._zmq_sock.send_multipart([''] + msg.dump())
        reply_msg = self._zmq_sock.recv_multipart()[1:]
        return self._Message.load(reply_msg)


class Server(object):
    """
    Single-threaded coroutine-based TCP Server implementing a Request-Reply protocol
    """
    def __init__(self, bind_address, request_handler, message_class=PickleMessage, logger=__logger__):
        self._bind_address = bind_address
        self._request_handler = request_handler
        self._message_class = message_class
        self._logger = logger
        self._uri_queue = multiprocessing.Queue()
        self._uri = None
        self._server_process = None

    @staticmethod
    def _bootstrap(bind_address, request_handler, Message, uri_queue):
        """
        Bootstrap server. Runs forever until receiving the stop command form parent process.

        :param bind_address: TCP address (host, port) for binding (if port=0, bind to random port)
        :param request_handler: asynchronous handler of incoming request message (must be a Tornado coroutine)
        :param Message: Message class to use to serialize/deserialize incoming request messages
        :param uri_queue: multiprocessing queue to communicate back to parent process the socket URIs
        """

        #from tornado.ioloop import IOLoop
        import zmq
        from zmq.eventloop import ioloop
        from zmq.eventloop.future import Context, Poller

        def tcp_bind(socket, tcp_address):
            """
            Bind socket to TCP address (host, port). If port=0, bind to random port.
            Returns socket URI (protocol://address:port)
            """
            ipaddr, port = tcp_address
            if port == 0:
                uri = "tcp://" + ipaddr
                port = socket.bind_to_random_port(uri)
                uri += ":" + str(port)
            else:
                uri = "tcp://" + ipaddr + ":" + str(port)
                socket.bind(uri)
            return uri

        loop = ioloop.IOLoop()
        loop.make_current()
        #loop.install()
        #loop = ioloop.IOLoop.instance()



        zmq_ctx = Context()

        # create and bind main business socket (receive request and send back reply)
        request_socket = zmq_ctx.socket(zmq.ROUTER)
        request_uri = tcp_bind(request_socket, bind_address)

        # create and bind socket for commanding the server (using localhost, but could use IPC as well)
        command_socket = zmq_ctx.socket(zmq.ROUTER)
        command_uri = tcp_bind(command_socket, tcp_address=("127.0.0.1", 0))

        # send URIs to mummy
        uri_queue.put({"request": request_uri, "command": command_uri})

        poller = Poller()
        poller.register(request_socket, zmq.POLLIN)
        poller.register(command_socket, zmq.POLLIN)

        def new_command(msg):
            """
            Handler for incoming command messages
            """
            body = msg[2:]
            assert len(body) == 1
            if body[0] == "IOLOOPSTOP":
                loop.add_callback(loop.stop)

        @tornado.gen.coroutine
        def new_request(msg):
            """
            Handler for incoming request message
            """
            zmq_header, msg_body = msg[:2], msg[2:]
            request = Message.load(msg_body)
            result = yield request_handler(request)
            reply_msg_body = Message(result)
            reply = zmq_header + reply_msg_body.dump()
            yield request_socket.send_multipart(reply)

        @tornado.gen.coroutine
        def polling_loop():
            """
            Poll sockets for new messages and schedule in the event loop appropriate handlers
            """
            while True:
                events = yield poller.poll()
                events = dict(events)
                if request_socket in events:
                    msg = yield request_socket.recv_multipart()
                    loop.add_callback(new_request, msg)
                if command_socket in events:
                    msg = yield command_socket.recv_multipart()
                    loop.add_callback(new_command, msg)

        loop.add_callback(polling_loop)
        loop.start()

    def start(self):
        """
        Start server in a background process
        """
        self._server_process = multiprocessing.Process(target=Server._bootstrap,
                                                       args=[self._bind_address, self._request_handler,
                                                             self._message_class, self._uri_queue])
        self._server_process.start()
        __logger__.debug("Server started!")
        return self

    def shutdown(self):
        """
        Stop server
        """
        ctx = zmq.Context()
        s = ctx.socket(zmq.DEALER)
        s.connect(self._uri["command"])
        s.send_multipart(["", "IOLOOPSTOP"])
        self._server_process.join()

    @property
    def address(self):
        """
        Return server (host, port) address
        """
        if not self._uri:
            self._uri = self._uri_queue.get()
        return self._uri["request"]


def make_coroutine(fun):
    """
    Wraps a synchronous function into a tornado coroutine
    """
    @tornado.gen.coroutine
    @wraps(fun)
    def decorator(*args, **kwargs):
        raise tornado.gen.Return(fun(*args, **kwargs))
    return decorator


@contextmanager
def start_server(address, request_handler):
    """
    Helper function to create a running server
    """
    server = Server(address, request_handler).start()
    yield server
    server.shutdown()


if __name__ == '__main__':
    pass
