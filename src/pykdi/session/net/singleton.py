"""
Singleton design pattern
"""


class Singleton(type):
    """
    Meta class to implement a Singleton class
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @classmethod
    def clean(mcs):
        """Clean all Singleton instances"""
        mcs._instances = {}

