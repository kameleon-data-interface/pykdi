"""
Spawn a new python interpreter on a remote machine (unsing SSH shell)
Execute a method on this interpreter

Same interface as multiprocessing.Process
"""

# pylint:disable:reimported
# Due to distributed context, modules could be reimported

from multiprocessing.connection import Listener, Client
from multiprocessing import current_process
from multiprocessing.managers import SyncManager
from multiprocessing.process import AuthenticationString
import pickle
import os.path
import socket
import subprocess
import sys
import errno
import logging


__logger__ = logging.getLogger(__name__)

__queue_manager_port_range__ = range(13000, 14000)


class SSHProcessException(Exception):
    """
    Exception dedicated to SSH Process
    """
    pass


#pylint: disable=too-many-instance-attributes
class SSHProcess(object):
    """
    SSH process. Like a multiprocessing process but running on a remote host
    """
    def __init__(self, target=None, args=None, host=None):
        """
        Create a remote process

        :param target: method to launch
        :param args: arguments to the method
        :param host: address of remote host
        """
        import padawan.utilities.cea as pdw_cea
        self._target = target
        self._args = args
        self._host = host
        self._listener = None
        self._process = None
        self._client = None
        self._ip_address = None
        self.exitcode = None
        self.init()

    def _create_listener(self, ip_address):
        """
        Create a listener (see multiprocessing) and select a free port

        :param ip_address: current host ip_address
        :return: port of the Listener
        """
        for port in xrange(3000, 4000):
            try:
                self._listener = Listener((ip_address, port))
                return port
            except socket.error:
                # Port already used!
                pass
        raise SSHProcessException("No ports free on range [3000,4000[")

    def init(self):
        """
        Initialize connection
        """
        import padawan.utilities.cea as pdw_cea
        # Get my address
        ip_address = pdw_cea.get_ipaddress()
        # Create a remote python process (same python interpreter and send him my address)
        listener_port = self._create_listener(ip_address)
        list_args = pdw_cea.ssh(self._host) + [sys.executable, os.path.abspath(__file__), str(ip_address), str(listener_port)]
        __logger__.debug("args to Popen are %s", list_args)
        self._process = subprocess.Popen(list_args)
        # Set up the communication channel
        self._client = self._listener.accept()
        # Send to process the current PYTHONPATH
        self._client.send(sys.path)
        # process authkey (to be able to connect to managers)
        authkey = bytes(current_process().authkey)
        self._client.send(authkey)
        # Receive client ip_address
        self._ip_address = self._client.recv()

    def start(self):
        """
        Launch process
        """
        from cloudpickle import cloudpickle
        # Send method to execute + args
        __logger__.debug("Start ssh process, target=%s, args=%s", self._target, self._args)
        message = {"target": self._target,
                   "args": self._args}
        data = cloudpickle.dumps(message)
        self._client.send(data)

    def join(self):
        """
        Join process
        """
        self._client.close()
        self._listener.close()
        self.exitcode = self._process.wait()

    @property
    def ip_address(self):
        """
        Return the actual ip_address of the host that execute the SSH process
        """
        return self._ip_address
# pylint: enable=too-many-instance-attributes


class Queue(object):
    """
    Queue object to communicate with a SSH process
    """
    __manager__ = None

    def __init__(self):
        import padawan.utilities.cea as pdw_cea
        ip_address = pdw_cea.get_ipaddress()
        if not Queue.__manager__:
            for queue_manager_port in __queue_manager_port_range__:
                try:
                    Queue.__manager__ = SyncManager(address=(ip_address, queue_manager_port),
                                                    authkey=None)
                    Queue.__manager__.start()
                    break
                except EOFError:
                    # If cannot start manager => port already used!
                    pass
        self._queue = Queue.__manager__.Queue()

    def get(self):
        """
        Get object (pop it). Blocking method

        :return: object in queue
        """
        while True:
            try:
                return self._queue.get()
            except IOError as e:
                # Fix a bug in multiprocessing module (no retry after Interrupted System Call).
                # Fixed in newer Python version (bugs.python.org/issue17097)
                if e.errno == errno.EINTR:
                    continue
                else:
                    raise

    def put(self, item):
        """
        Put object in queue. Blocking if queue is full

        :param item: object to add
        """
        return self._queue.put(item)


def bootstrap():
    """
    Bootstrap sequence of the remote process
    """
    from pykdi.session.tools.get_ipaddress import get_ipaddress
    # connect to the spawner
    ip_address, port = sys.argv[1], int(sys.argv[2])
    client = Client((ip_address, port))
    # update sys.path with the client one (assume the path are the same!)
    sys_path = client.recv()
    sys.path = sys_path
    # Get the authkey
    current_process().authkey = AuthenticationString(client.recv())
    # Send my real ip
    my_ip_address = get_ipaddress()
    client.send(my_ip_address)
    # Get the target and arguments
    data = client.recv()
    message = pickle.loads(data)
    #run method
    message["target"](*message["args"])
    #exit
    # Return the exit code
    sys.exit(0)


if __name__ == "__main__":
    #pylint:disable=reimported,invalid-name
    import os.path
    current_file = os.path.abspath(__file__)
    path = current_file.split('/')
    sys.path.insert(0, '/'.join(path[:-3]))
    bootstrap()
