#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
Remote Procedure Call module.
"""
from functools import partial
import traceback as tb
# import cPickle

import numpy as np
import tornado.gen

from pykdi.session.net.transport import Server, Client

__logger__ = None


class RPCException(Exception):
    pass


class RPCHandler(object):
    """
    RPCRequestHandler registers the functions to be exposed through RPC and manages incoming requests.
    """
    def __init__(self):
        self._functions = {}

    def register_function(self, func):
        """
        Register function to be exposed through RPC
        """
        self._functions[func.__name__] = func

    @tornado.gen.coroutine
    def handle(self, args):
        """
        Handler incoming RPC request, apply function to provided arguments and return results

        :param args: incoming RPC request as bytes string
        :return: result in RPC encoded bytes string
        """
        func_name, args = args[0], args[1:]
        try:
            try:
                func = self._functions[func_name]
            except KeyError:
                raise KeyError("Function '%s' not found" % func_name)
            result = yield func(*args)
        except Exception as exc:
            raise tornado.gen.Return(RPCException(exc, tb.format_exc()))
        else:
            raise tornado.gen.Return(result)


class RPCProxy(object):
    """
    Proxy for calling functions exposed through RPC (synchronous)
    """
    def __init__(self, rpc_handler):
        self._rpc_handler = rpc_handler

    def __getattr__(self, func_name):
        def do_rpc(*args):
            """
            Generic decorator to remote functions
            """
            result = self._rpc_handler.handle((func_name,) + args)
            if isinstance(result, RPCException):
                exc, tb = result.args
                msg = "%s [Raised in RPC handler] \n %s" % (exc.message, tb)
                raise type(exc)(msg)
            return result
        return do_rpc


class RequestClient2RPCProxyAdapter(object):
    """
    Adapter from a Padawan Request-Reply Client to RPCProxy
    """
    def __init__(self, client):
        self._client = client

    def handle(self, args):
        """
        Transport RPC messages to/from server
        """
        return self._client.request(args)


def gen_rpc_server(bind_address, funcs, logger=__logger__):
    """
    Start a RPC server

    :param bind_address: server binding address
    :param funcs: function to register for RPC
    :param logger:
    :return: a server object with a shutdown() method
    """
    rpc_handler = RPCHandler()
    for func in funcs:
        rpc_handler.register_function(func)
    rpc_server = Server(bind_address, rpc_handler.handle, RPCMessage, logger)
    rpc_server.start()
    return rpc_server


def gen_rpc_client(address, logger=__logger__):
    """
    Create RPC client connected to remote server

    :param address: server address
    :param logger:
    :return: RPCProxy object
    """
    client = Client(address, RPCMessage, logger)
    handler = RequestClient2RPCProxyAdapter(client)
    return RPCProxy(rpc_handler=handler)


def gen_rpc_services(state, services):
    """
    Return rpc services with injected state
    """
    rpc_services = []
    for service in services:
        rpc_service = partial(service, state)
        rpc_service.__name__ = service.__name__
        rpc_services.append(rpc_service)
    return rpc_services


# RPC Message class and helper functions

class PadawanRPCMessageError(Exception):
    """
    Custom error for RPCMessage class
    """
    pass


def pickle_dumps(obj):
    """
    Serialize Python object
    """
    return cPickle.dumps(obj, protocol=cPickle.HIGHEST_PROTOCOL)


# Header values used in message serialization
ARRAY = "A"
PYOBJ = "P"
STRING = "S"
TUPLE = "T"


class RPCMessage(object):
    """
    RPC-specialized Message class handling its own serialization.

    It handles tuples (RPC arguments or returned values) by serializing individual tuple
    object and using appropriate serialization strategy depending on the object type.
    The serialization returns a list of strings.
    """
    def __init__(self, args):
        self._args = args

    def dump(self):
        """
        Serialize message
        """
        header = ''
        body = []
        if isinstance(self._args, tuple):
            header = TUPLE
            for obj in self._args:
                body += self._dump_obj_to_msg(obj)
        else:
            body = self._dump_obj_to_msg(self._args)
        msg = [header] + body
        return msg

    @classmethod
    def load(cls, msg):
        """
        De-serialize message
        """
        header = msg.pop(0)
        args = cls._load_objects_from_msg(msg)
        if header == TUPLE:
            return tuple(args)
        else:
            return args[0]

    @staticmethod
    def _dump_obj_to_msg(obj):
        """
        Serialize object using appropriate serialization strategy
        """
        if isinstance(obj, np.ndarray):
            return [ARRAY] + dump_nparray(obj)
        elif isinstance(obj, str):
            return [STRING, obj]
        else:
            return [PYOBJ, pickle_dumps(obj)]

    @staticmethod
    def _load_objects_from_msg(msg):
        """
        Parse a message and return a list of loaded python objects
        """
        objects = []
        while msg:
            header = msg.pop(0)
            if header == ARRAY:
                next_obj = load_nparray([msg.pop(0) for _ in range(3)])
            elif header == STRING:
                next_obj = msg.pop(0)
            elif header == PYOBJ:
                next_obj = cPickle.loads(msg.pop(0))
            else:
                raise PadawanRPCMessageError("unknown header: %s" % header)
            objects.append(next_obj)
        return objects


# The two following functions are used for serialization of a numpy array.
# Strategy for encoding the buffer can be changed (e.g. base64 encoding for JSON serialization)


def dump_nparray(array, buffer_encoding=lambda buf: str(buf)):
    """
    Format a numpy array into a list of strings

    :param array: numpy array
    :param buffer_encoding: function to encode array buffer (defaults to bytes string)
    :return: tuple of strings
    """
    buff = np.getbuffer(np.ascontiguousarray(array))
    return [pickle_dumps(array.shape), array.__array_interface__["typestr"], buffer_encoding(buff)]


def load_nparray(msg, buffer_decoding=lambda buf: buf):
    """
    Return numpy array for list of strings formatted with dump_nparray function

    :param msg: message
    :param buffer_decoding: function to decode buffer string into a numpy array buffer
    :return: numpy array
    """
    pickled_shape, dtypestr, buf = msg
    shape = cPickle.loads(pickled_shape)
    return np.frombuffer(buffer_decoding(buf), dtype=dtypestr).reshape(shape)
