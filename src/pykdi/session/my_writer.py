import os
import time

from pykdi import (kdi_builder, KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS,
                   KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART)

print('my_writer BEGIN')

try:
    mpi_rank = int(os.environ['MPI_COMM_WORLD_RANK'])
except:
    try:
        mpi_rank = int(os.environ['OMPI_COMM_WORLD_RANK'])
    except:
        mpi_rank = 0

try:
    mpi_size = int(os.environ['MPI_COMM_WORLD_SIZE'])
except:
    try:
        mpi_size = int(os.environ['OMPI_COMM_WORLD_SIZE'])
    except:
        mpi_size = 1

print('my_writer BEGIN #' + str(mpi_rank) + '/' + str(mpi_size))

pkbase = kdi_builder('my_base')
pkbase.set_conf({'agreement': KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS: 2})
pkbase.initialize(mode='w')

nb_steps = 4 + 0  # mpi_rank

for istep in range(nb_steps):
    with pkbase.open({KDI_AGREEMENT_STEPPART_VARIANT_STEP: istep, KDI_AGREEMENT_STEPPART_VARIANT_PART: mpi_rank,}) as pkchunk:
        str_field = pkbase.update_fields('/fields', 'myGlobScalarField_A')
        print('@@@ W new fields', str_field)
        str_field = pkchunk.update_fields('/fields', 'myGlobScalarField_C')
        print('@@@ W new fields', str_field)

        pkchunk.update('UnstructuredGrid', '/mymesh')
        print('@@@ W mesh', '/mymesh')
        str_field = pkbase.update_sub('/mymesh', '/mymil1')
        print('@@@ W submesh', str_field)
        str_field = pkchunk.update_fields('/mymesh/cells/fields', 'myCellGlobScalarField')
        print('@@@ W new cells fields', str_field)

        import numpy as np
        pkchunk.insert(str_field, np.array([istep + 1 + (mpi_rank + 1) / 10.]))

        # time.sleep(1. + mpi_rank / 10.)

pkbase.finalize()

print('my_writer END')
