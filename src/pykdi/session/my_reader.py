import os
import time

from pykdi import (kdi_builder, KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS,
                   KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART)

print('my_reader BEGIN')

try:
    mpi_rank = int(os.environ['MPI_COMM_WORLD_RANK'])
except:
    try:
        mpi_rank = int(os.environ['OMPI_COMM_WORLD_RANK'])
    except:
        mpi_rank = 0

try:
    mpi_size = int(os.environ['MPI_COMM_WORLD_SIZE'])
except:
    try:
        mpi_size = int(os.environ['OMPI_COMM_WORLD_SIZE'])
    except:
        mpi_size = 1

print('my_reader BEGIN #' + str(mpi_rank) + '/' + str(mpi_size))

time.sleep(1)  # TODO Apparemment, si les serveurs ne se sont pas inscrits avant ca pose quelques problèmes

pkbase = kdi_builder('my_base')
pkbase.set_conf({'agreement': KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_CONF_NB_PARTS: 2})
pkbase.initialize(mode='r')

cpt = 0

pkbaseit = pkbase.iterator()  # default delay=0.5
while True:
    with pkbaseit.next() as pkchunk:
        if pkchunk == 'TERMINATED':
            break
        print('@@@ READER IN PROGRESS B')
        print('@@@ READER IN PROGRESS B', pkchunk.first_chunk())
        print(pkchunk.get_keys())

        str_field = '/mymesh/cells/fields/myCellGlobScalarField'
        istep = int(pkchunk.first_chunk())
        for irank in range(2):
            chunk = {KDI_AGREEMENT_STEPPART_VARIANT_STEP: istep,
                     KDI_AGREEMENT_STEPPART_VARIANT_PART: irank, }
            val = pkchunk.get(str_field, chunk)
            assert (val[0] == istep + 1 + (irank + 1) / 10.)
            cpt += 1

pkbase.finalize()

print('my_reader END', cpt)
