Le lancement d'une session se fait à travers les instructions suivantes : 
```python
with Session(session_configuration_filepath) as session:
    session.run()
```
On crée une session à partir d'un fichier de configuration **session_configuration_filepath**
qui est un fichier toml, json ou yaml.

Une fois lue en mémoire, la description tient dans un dictionnaire.

Pour la clef **server**, nous avons les sous-clefs :
- **host** de type 'str' qui définit l'hôete qui va acceuillir les serveurs ;
- **bases** de type '(str,)' décrit une liste de chaîne de caractères nommant de
  façon unique chaque base que KDI devra prendre en charge.

Ensuite, on a une clef par identifiant unique de base (issue de la liste server.bases),
avec les sous-clefs :
- **path**, la path absolue pour accéder à la base ;
- **name**, le nom de la base (par défaut, si il n'est pas donné, on prendra la
  valeur de l'identifiant de base qui sert de clef) ;
- **mode**, le mode d'accès de la base 'r','rw', 'w' ;
- **nb_readers**, le nombre de processus qui accédera à la base.

Ensuite, on trouve la clef **applications** avec la sous-clef **processes** qui
liste des chaînes de caractères nommant de façon unique chaque code qui
accédera à une des bases.

Pour finir, on a une clef par identifiant unique d'applications (issue de la liste
applications.processess) avec les sous-clefs :
- **executable**, le pathfile de l'exécutable ;
- **args_code**, les arguments de lancement de l'application ;
- **srun_args**, les arguments de lancement de l'application via SLURM ;
- **listing_files_path**, le chemin d'écriture des listings.

Voici un exemple de fichier de configuration de session KDI :
```python
configuration = {
    'server': {
        'host': 'local',
        'bases': ['baseA', 'baseB'],
    },
    'baseA' : {
        'path': '~/ici',
        'name': 'baseA',
        'mode': 'r',
        'nb_readers': 12,
    },
    'baseB': {
        'path': '~/la',
        'name': 'baseBB',
        'mode': 'w',
        'nb_readers': 4,
    },
    'applications': {
        'processes': ['codeA', 'codeB'],
    },
    'codeA': {
        'executable' : 'python3',
        'args': 'scriptA',
        'listing_files_path': '',
    },
    'codeB': {
        'executable' : '',
        'args': '',
        'listing_files_path': '',
    },
}
```