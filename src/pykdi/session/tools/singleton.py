# https://stackoverflow.com/questions/6760685/what-is-the-best-way-of-implementing-singleton-in-python

class Singleton(type):
    """
    Meta class to implement a Singleton class
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @classmethod
    def clean(mcs):
        """Clean all Singleton instances"""
        mcs._instances = {}
