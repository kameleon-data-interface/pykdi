def tuplify(obj):
    """
    Convert a list into tuple, support nesting.
    ex: assert _tuplify([[1, 2], 3]) == ((1, 2), 3)
    """
    if isinstance(obj, tuple) or isinstance(obj, list):
        obj = tuple([tuplify(k) for k in obj])
    return obj
