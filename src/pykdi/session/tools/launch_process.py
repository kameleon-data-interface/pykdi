import os
import shlex
import subprocess
from zmq.eventloop import ioloop

from pykdi.session.tools.get_os import get_os
from pykdi.session.tools.get_slurm_env import get_slurm_env
from pykdi.session.tools.kdi_log_session import kdi_log_session
from pykdi.session.tools.singleton import Singleton

class HandlerOutputCode():
    __slots__ = '_type', '_name'

    def __init__(self, name, type_output):
        self._type = type_output
        self._name = name

    def __call__(self, fd, event):
        """
        Read fd and write into file
        """
        if event & ioloop.ZMQIOLoop.READ:
            self.on_read(fd)
        if event & ioloop.ZMQIOLoop.ERROR:
            self.on_error(fd)

    def on_read(self, fd):
        """
        Read DATA
        """
        line = fd.readline()
        kdi_log_session.log('[' + self._name + '.' + self._type + ']', line.rstrip('\n'))
        print('[' + self._name + '.' + self._type + ']', line.rstrip('\n'))

    def on_error(self, fd):
        """
        Close file desc
        """
        if fd.closed:
            ioloop.IOLoop.current().remove_handler(fd)

def gen_callbacklivingcode(name, pid):
    """
    Generate a callback that checks if code is living

    :param name: code name
    :param pid: pid object (to poll)
    """
    def inner():
        return_code = pid.poll()
        if return_code is not None:
            kdi_log_session.log('[' + self._name + '] This code is DEAD!')
            SetOfProcesses().remove_code(name, pid.returncode)
    return inner

class SetOfProcesses(metaclass=Singleton):
    """
    A singleton that stores all living processes
    """
    __slots__ = '_apps', '_pids', '_desc'
    def __init__(self):
        self._apps = dict()
        self._pids = dict()
        self._desc = dict()

    def add_code(self, name, poller, pid, description):
        """
        Add a new code called name
        """
        self._apps[name] = poller
        self._pids[name] = pid
        self._desc[name] = dict(description)
        self._desc[name]["pid"] = pid.pid
        poller.start()

    def killall(self):
        """
        Kill all living processes
        """
        for pid in self._pids.values():
            pid.kill()

    def remove_code(self, name, returncode):
        self._apps[name].stop()
        del self._apps[name]
        del self._pids[name]

        self._desc[name]["returncode"] = returncode
        if returncode:
            kdi_log_session.log('[' + name + '] Ends with an error (code =', returncode, '). Emergency Kill: shutdown other processes.')
            ioloop.IOLoop.current().add_callback(self.killall)
        if 'SLURM_JOBID' in os.environ:
            # Test if slurmn does not have detected an error
            ioloop.IOLoop.current().spawn_callback(test_slurm_detect_fail)
        if not self._apps:
            kdi_log_session.log('[' + name + '] Stop the loop.')
            ioloop.IOLoop.current().add_callback(ioloop.IOLoop.current().stop)

    @property
    def summary(self):
        """
        Return the summary of process state (read only)
        """
        return dict(self._desc)

    def init(self):
        """
        Init set of processes
        """
        self._apps = dict()
        self._pids = dict()
        self._desc = dict()

def print_codes_status():
    """
    Print code status
    :return:
    """
    print('====================')
    for name, descr in SetOfProcesses().summary.items():
        print ('code' + name)
        print ('--------------------')
        for key, val in descr.items():
            print (key, ':\t', val)
        print ('====================')
    if 'SLURM_JOBID' in os.environ:
        acct_cmdline = 'sacct -j {job_id}'.format(job_id=os.environ['SLURM_JOBID'])
        print ('Slurm summary')
        print(subprocess.check_output(shlex.split(acct_cmdline)).rstrip('\n'))

def launch_process(cmdline, name, srun_args=None, env=None, listing_files_path=None):
    """
    Execute the command line in the padawan session a asynchronous maner.

    The process is launched using SLURM launcher on the padawan session resources

    :param cmdline: command line to launch
    :param name: name for this task
    :param srun_args: optional option to pass to slurm
    :param env: dictionnary of env variables
    :param listing_files_path: path in which stdout and stderr are written into files
    :return: subprocession Popen object on the slurm launcher
    """
    if get_os().startswith('Atos') or srun_args:
        print("DANGER DANGER DANGER DANGER You enter a line of fire! (launch_process srun not validate)")
        # USE SLURM or mpi
        srun_args_list = srun_args if srun_args else shlex.split("-n1 -N1")
        # slurm managment
        # We use srun instead of ccc_mprun to have more control on arguments (task mapping for instance)
        # 'ccc_mprun  -K' is equivalent to salloc. keep ccc_mprun...
        use_srun = "-K" not in srun_args_list
        if use_srun:
            srun_bin = "srun"
            srun_args_list += ["--cpu_bind=verbose,none", "--slurmd-debug=0"]
        else:
            srun_bin = cea.mprun_path()
        command = [srun_bin] + srun_args_list + shlex.split(cmdline)
    else:
        # Launch directly
        command = shlex.split(cmdline)

    kdi_log_session.log('CODE \'' + name + '\': LAUNCH COMMAND \'' + ' '.join(command) + '\'')

    env_process = env if env else get_slurm_env()

    # register task name
    env_process['KDI_TASK_NAME'] = name

    use_default_io = False
    if listing_files_path and listing_files_path == 'STANDARD':
        use_default_io = True
    elif listing_files_path:
        print("DANGER DANGER DANGER DANGER You enter a line of fire! (launch_process listing_files_path not validate)")
        try:
            os.makedirs(listing_files_path)
        except OSError:
            pass  # already exits
        stdout_filename = os.path.join(listing_files_path, name + '.out')
        stderr_filename = os.path.join(listing_files_path, name + '.err')
        fd_stdout = open(stdout_filename, mode='w')
        fd_stderr = open(stderr_filename, mode='w')
        kdi_log_session.log('Log stdout of application ', name, ' to file ', stdout_filename)
        kdi_log_session.log('Log stderr of application ', name, ' to file ', stderr_filename)
        atexit.register(fd_stdout.close)
        atexit.register(fd_stderr.close)
    else:
        fd_stdout = subprocess.PIPE  # ??? c'est pourquoi faire
        fd_stderr = subprocess.PIPE  # ??? c'est pourquoi faire

    try:
        if use_default_io:
            pid = subprocess.Popen(command, env=env_process)
        else:
            pid = subprocess.Popen(command, env=env_process, stdout=fd_stdout, stderr=fd_stderr)
    except OSError:
        kdi_log_session.fatal_exception('Cannot start code ', name)
        raise

    loop = ioloop.IOLoop.current()

    if not listing_files_path:
        # don't write stdout and stderr to file, send to logger instead
        loop.add_handler(pid.stdout, HandlerOutputCode(name, 'out'), ioloop.ZMQIOLoop.READ)
        loop.add_handler(pid.stderr, HandlerOutputCode(name, 'err'), ioloop.ZMQIOLoop.READ)

    poller = ioloop.PeriodicCallback(gen_callbacklivingcode(name, pid), 100)
    SetOfProcesses().add_code(name, poller, pid, {"cmdline": cmdline,
                                                  "srun_args": srun_args,
                                                  "final_cmdline": command})
    kdi_log_session.log('CODE \'' + name + '\': #PID ' + str(pid.pid))
    print_codes_status()
    return pid
