import os

def get_slurm_env() -> dict[str, str]:
    return os.environ.copy()
