import itertools
import json
import os
import pickle
import shlex
import subprocess
import time
import tomllib
import yaml

from pykdi.tools.kdi_log import KDILog
from pykdi.session.net.composite import CompositeManager
from pykdi.session.net.resources import ResourcesManager
from pykdi.session.semantic.semantic_bootstrap_server import semantic_bootstrap_server
from pykdi.session.storage.storage_bootstrap_server import storage_bootstrap_server
from pykdi.session.tools.encorder_decoder import PythonObjectEncoder, PythonObjectDecoder
from pykdi.session.tools.launch_process import launch_process
from pykdi.session.tools.kdi_log_session import kdi_log_session
from pykdi.session.tools.get_slurm_env import get_slurm_env

class KDISessionException(Exception):
    """
    Fr: Gestion spécifique des exceptions
    """
    pass

def expand_path(base_path: str) -> str:
    """
    Expand a path string that may contain shell commands (environment variables, bash functions...)
    """
    return subprocess.check_output('echo ' + base_path, shell=True, encoding='UTF-8').split()[0]

class Session:

    __slots__ = 'host', '_host_strategy', 'bases', 'storages', 'applications', 'clients', 'storages_manager', 'semantic_manager'

    def __init__(self, session_configuration_filepath: str):
        kdi_log_session.log('>>>>> Create session (', session_configuration_filepath, ')')

        # reader session configuration filepath
        conf = None
        try:
            # toml
            with open(session_configuration_filepath, 'rb') as fp:
                conf = tomllib.load(fp)
                kdi_log_session.log('session configuration loaded (tml or toml)')
        except:
            pass

        if conf is None:
            try:
                # yaml
                with open(session_configuration_filepath, 'r') as fp:
                    conf = yaml.safe_load(fp)
                    kdi_log_session.log('session configuration loaded (yaml)')
            except:
                pass

        if conf is None:
            try:
                # json
                with open(session_configuration_filepath, 'r') as fp:
                    conf = json.load(fp)
                    kdi_log_session.log('session configuration loaded (yaml)')
            except:
                pass

        # configuration server host
        try:
            self.host = conf['server']['host']
        except KeyError:
            self.host = None

        # TODO chunk_queue_max_size = int(conf['server']['chunk_queue_max_size'])

        # configuration server bases
        self.bases = dict()
        for base_id in conf['server']['bases']:
            try:
                path = os.path.normpath(expand_path(conf[base_id]['path']))
                name = conf[base_id]['name']
                mode = conf[base_id]['mode']
                if mode not in ['r', 'read']:
                    try:
                        os.makedirs(path)
                    except OSError:
                        pass
                nb_writers = int(conf[base_id]['nb_writers'])
                nb_readers = int(conf[base_id]['nb_readers'])
            except KeyError as e:
                print ('KeyError: Missing a subkey for', base_id, ' (path, name, mode, read, nb_readers)')
                raise e
            self.bases[base_id] = {'mode': mode, 'path': path, 'name': name, 'nb_writers': nb_writers, 'nb_readers': nb_readers}

        # configuration storages
        try:
            storage_type = conf['storage']['type']
        except KeyError:
            storage_type = 'async_tcp'
        try:
            storage_number = int(conf['storage']['number'])
        except KeyError:
            storage_number = 1
        self.storages = storage_number * [storage_type]

        if 'SLURM_NODELIST' in os.environ:
            # One by node
            ResourcesManager().set_host_strategy(round_robin_allocation_slurm())
        else:
            ResourcesManager().set_host_strategy(itertools.repeat(self.host))

        # configuration applications
        self.applications = {}
        for app in conf['applications']['processes']:
            executable = conf[app]['executable']
            try:
                args_code = conf[app]['args']
            except KeyError:
                args_code = ""
            try:
                srun_args = shlex.split(conf[app]['srun_args'])
            except KeyError:
                srun_args = None
            try:
                listing_files_path = conf[app]['listing_files_path']
            except KeyError:
                listing_files_path = None

            self.applications[app] = {'executable': executable, 'args_code': args_code, 'srun_args': srun_args, 'listing_files_path': listing_files_path}

        # configuration clients / pids
        self.storages_manager = None
        self.semantic_manager = None
        self.clients = dict()

        kdi_log_session.log('Create session <<<<<')

    def register(self, pid):
        if pid in self.clients:
            raise KDISessionException('Client' + str(pid) +'already registered in session')
        #storage_factory, semantic_info, session_id, logconfig = self._session_info
        #self._clients[pid] = SemanticClient(semantic_info, storage_factory, logconfig)
        #return ServerPointToPointCommunicator(self._clients[pid], session_id)
        pass

    def unregister(self, pid):
        self.clients[pid].close()
        del self.clients[pid]

    def run(self):
        """
        """
        kdi_log_session.log('Run session')

        # Start data storage
        storages_managers = dict()
        for storage in self.storages:
            storages_managers[storage] = ResourcesManager().spawn(target=storage_bootstrap_server)
        self.storages_manager = CompositeManager(storages_managers)

        # Start semantic server
        self.semantic_manager = ResourcesManager().spawn(target=semantic_bootstrap_server)

        session_info = dict()
        for key_base, val_base in self.bases.items():
            session_info[key_base] = {'storages': self.storages_manager.info, 'semantic': self.semantic_manager.info,
                                      'nb_writers': val_base['nb_writers'], 'nb_readers': val_base['nb_readers']}

        env = get_slurm_env()
        # save session info in env var and in user home directory
        env['KDI_SESSION'] = PythonObjectEncoder().encode(session_info)
        print(env['KDI_SESSION'])
        print(PythonObjectDecoder().decode(env['KDI_SESSION']))
        try:
            os.makedirs(os.path.expanduser("~/.kdi"))
        except OSError:
            # folder already exists
            pass
        with open(os.path.expanduser("~/.kdi/session.info"), "w") as fd:
            fd.write(env['KDI_SESSION'])
        # tell Python not to write .pyc file when importing modules due to non-atomic write access
        # to .pyc files (https://bugs.python.org/issue13146 corrected in next Python version)
        env["PYTHONDONTWRITEBYTECODE"] = '1'

        pids = set()
        for name, application in self.applications.items():
            command = "{exe} {args}".format(exe=application['executable'], args=application['args_code'])
            pids.add(launch_process(command, name, application['srun_args'], env, application['listing_files_path']))
        return pids

    def __enter__(self):
        """
        This method handles the setup logic and is called when entering a new with context.
        Its return value is bound to the with target variable.
        """
        kdi_log_session.log('Enter session')
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        """
        This method handles the teardown logic and is called when the flow of execution
        leaves the with context. If an exception occurs, then exc_type, exc_value, and
        exc_tb hold the exception type, value, and traceback information, respectively.
        """
        kdi_log_session.log('>>>>> Exit session')
        exit_code = set()
        exit_code.add(0)
        for client in self.clients.values():
            exit_code.add(client.close())
        if self.storages_manager:
            exit_code.add(self.storages_manager.close())
        if self.semantic_manager:
            exit_code.add(self.semantic_manager.close())
        kdi_log_session.log('Exit session <<<<<')
        return max(exit_code)


def run_session(session_configuration_filepath: str, launch: bool = True):
    istep = 1
    print('[', istep, '] Ouverture de la Session'); istep += 1
    exit_code = 1
    with Session(session_configuration_filepath) as session:
        if launch:
            print('[', istep, '] Exécution de la Session'); istep += 1
            try:
                pids = session.run()
                kdi_log_session.log('WAIT ALLPID')
                exit_code = max({pid.wait() for pid in pids})
                kdi_log_session.log('WAIT ALLPID CHECKED')
            except:
                exit_code = 1
        else:
            exit_code = 0
    return exit_code


def test_session():
    # Description d'un fichier de configuration de session KDI
    configuration = {
        'server': {
            'host': 'local',
            'bases': ['my_base',],
        },
        'my_base': {
            'path': '~/.kdi',
            'name': 'my_base',
            'mode': 'rw',
            'nb_writers': 2,
            'nb_readers': 1,
        },
        'applications': {
            'processes': ['my_writer', 'my_reader'],
        },
        'my_writer': {
            'executable': 'mpirun -n 2 python3',
            'args': '/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/src/pykdi/session/my_writer.py',
            'listing_files_path': 'STANDARD',  # STANDARD, non défini (subprocess.PIPE) ou filename
        },
        'my_reader': {
            'executable': 'python3',
            'args': '/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/src/pykdi/session/my_reader.py',
            'listing_files_path': 'STANDARD',
        },
    }

    # Creation du répertoire pour stocker un tel fichier de configuration de session
    session_configuration_dirpath = '~/.kdi'
    try:
        os.makedirs(expand_path(session_configuration_dirpath))
    except OSError:
        pass

    # Ecriture de la version JSON du fichier de configuration de session
    session_configuration_filepath = expand_path(session_configuration_dirpath + '/session.json')

    with open(session_configuration_filepath, 'w') as fp:
        json.dump(configuration, fp)

    return run_session(session_configuration_filepath, True)


def test_session_configuration():
    # Description d'un fichier de configuration de session KDI
    configuration = {
        'server': {
            'host': 'local',
            'bases': ['baseA', 'baseB'],
        },
        'baseA' : {
            'path': '~/ici',
            'name': 'baseA',
            'mode': 'r',
            'nb_writers': 12,
            'nb_readers': 2,
        },
        'baseB': {
            'path': '~/la',
            'name': 'baseBB',
            'mode': 'w',
            'nb_writers': 3,
            'nb_readers': 8,
        },
        'applications': {
            'processes': ['codeA', 'codeB'],
        },
        'codeA': {
            'executable' : 'python3',
            'args': 'scriptA',
            'listing_files_path': '',
        },
        'codeB': {
            'executable' : '',
            'args': '',
            'listing_files_path': '',
        },
    }

    # Creation du répertoire pour stocker un tel fichier de configuration de session
    session_configuration_dirpath = '~/.kdi'
    try:
        os.makedirs(expand_path(session_configuration_dirpath))
    except OSError:
        pass

    ##
    ## TOML
    ##
    # Python ne comporte pas les méthodes d'écriture en TOML
    # Voici les méthodes nécessaires pour une telle écriture
    def _toml_dumps_value(value):
        if isinstance(value, bool):
            return "true" if value else "false"
        elif isinstance(value, (int, float)):
            return str(value)
        elif isinstance(value, str):
            return f'"{value}"'
        elif isinstance(value, list):
            return f"[{', '.join(_toml_dumps_value(v) for v in value)}]"
        else:
            raise TypeError(f"{type(value).__name__} {value!r} is not supported")

    def toml_dumps(toml_dict, table=""):
        toml = []
        for key, value in toml_dict.items():
            if isinstance(value, dict):
                table_key = f"{table}.{key}" if table else key
                toml.append(f"\n[{table_key}]\n{toml_dumps(value, table_key)}")
            else:
                toml.append(f"{key} = {_toml_dumps_value(value)}")
        return "\n".join(toml)

    # Ecriture de la version TOML du fichier de configuration de session
    session_configuration_filepath = expand_path(session_configuration_dirpath + '/session.toml')
    with open(session_configuration_filepath, 'w') as fp:
        fp.write(toml_dumps(configuration))

    run_session(session_configuration_filepath, False)

    ##
    ## YAML
    ##
    # Ecriture de la version YAML du fichier de configuration de session
    session_configuration_filepath = expand_path(session_configuration_dirpath + '/session.yaml')

    with open(session_configuration_filepath, 'w') as fp:
        yaml.dump(configuration, fp)

    run_session(session_configuration_filepath, False)

    ##
    ## JSON
    ##
    # Ecriture de la version JSON du fichier de configuration de session
    session_configuration_filepath = expand_path(session_configuration_dirpath + '/session.json')

    with open(session_configuration_filepath, 'w') as fp:
        json.dump(configuration, fp)

    return run_session(session_configuration_filepath, False)


if __name__ == '__main__':
    # test_session_configuration()
    test_session()
