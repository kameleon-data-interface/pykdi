"""
Tests on server object
"""
import base64
import contextlib
import io
import json
import numpy as np
import os
import pickle
import socket
import threading
import time

from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
import xmlrpc.client

from pykdi.session.tools.encorder_decoder import PythonObjectEncoder, PythonObjectDecoder
from pykdi.session.tools.tuplify import tuplify

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

class MyServer:

    _slots__ = '_port', '_ip_address', '_server'

    def __init__(self, port):
        print('MYSERVER', port)
        self._port = port
        self._ip_address = 'localhost'
        self._server = SimpleXMLRPCServer((self._ip_address, self._port), requestHandler=RequestHandler)
        self._server.register_introspection_functions()

        # Register pow() function; this will use the value of
        # pow.__name__ as the name, which is just 'pow'.
        self._server.register_function(pow)

        # Register a function under a different name
        @self._server.register_function(name='add')
        def adder_function(x, y):
            return x + y

        @self._server.register_function
        def ada(x, y):
            return x + y

        @self._server.register_function
        def sub(x, y):
            return x - y

        # Register an instance; all the methods of the instance are
        # published as XML-RPC methods (in this case, just 'mul').
        class MyFuncs:

            def __init__(self):
                self._cpt = 1
                self._reference = dict()

            def set_item(self, id_client, dict_key, data_key):
                print('SERVER MyFuncs set_item before:', self._reference)
                print('SERVER MyFuncs id_client:', id_client)
                try:
                    return self._reference[dict_key][data_key]
                except KeyError:
                    try:
                        self._reference[dict_key][data_key] = self._cpt
                        print('SERVER MyFuncs set_item after:', self._reference)
                    except KeyError:
                        try:
                            self._reference[dict_key] = dict()
                            self._reference[dict_key][data_key] = self._cpt
                            print('SERVER MyFuncs set_item after:', self._reference)
                        except KeyError:
                            self._reference = dict()
                            self._reference[dict_key] = dict()
                            self._reference[dict_key][data_key] = self._cpt
                        print('SERVER MyFuncs set_item after:', self._reference)
                print('SERVER MyFuncs set_item', self._cpt)
                self._cpt += 1
                return self._reference[dict_key][data_key]

            def get_keys(self, id_client):
                print('SERVER MyFuncs get_keys', self._reference)
                print('SERVER MyFuncs get_keys id_client:', id_client)
                return self._reference

            def get_np(self, id_client):
                print('SERVER MyFuncs get_np id_client:', id_client)
                np_data = np.random.rand(20,10)
                numpyData = {
                    "array": np_data
                }
                encodedNumpyData = json.dumps(numpyData, cls=PythonObjectEncoder)
                print('SERVER MyFuncs get_np:', encodedNumpyData)
                return encodedNumpyData

            #def _dispatch(self, method, params):
            #    return getattr(self, method)(*tuplify(params))


        self._server.register_instance(MyFuncs())

        self._server_thread = threading.Thread(target=self._server.serve_forever)
        self._server_thread.daemon = True
        print('MYSERVER start thread', self._server_thread.ident, self._server_thread.native_id)
        print('ici')
        self._server_thread.start()
        print('ici 2')

    def __del__(self):
        print('MYSERVER destructeur', os.getpid())

    @property
    def info(self):
        """Return tuple address/port used for clients factory"""
        return self._ip_address, self._port

    def close(self):
        """Close server"""
        print('MYSERVER close', os.getpid())
        self._server.shutdown()
        #self._server_thread.join()

class LaunchServer:
    __slots__ = '_server'
    def __init__(self, port):
        print('LAUNCH SERVER')
        for test_ind in range(200):
            try:
                print('OPEN SERVER', port + test_ind)
                self._server = MyServer(port + test_ind)
                print('OPEN SERVER checked', self._server)
                break
            except socket.error:
                pass

    @property
    def info(self):
        print('LAUNCH SERVER info')
        return self._server.info

    def close(self):
        print('LAUNCH SERVER close')
        self._server.close()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

def client_1(address, path_base):
    print('CLIENT 1', address)
    uri = 'http://' + address[0] + ':' + str(address[1])
    print('CLIENT 1', uri)
    with xmlrpc.client.ServerProxy(uri) as proxy:
        print('CLIENT 1 Cchecked')
        print(proxy.system.listMethods())
        print(proxy.add(1, 1))
        print('CLIENT 1 Cchecked 2')
        print(proxy.ada(1, 1))
        print(proxy.sub(5, 3))
        print(proxy.set_item('1', 'my_base', 'key_a'))
        print(proxy.set_item('1', 'my_base', 'key_a'))
        print(proxy.set_item('1', 'my_base', 'key_b'))
        print(proxy.set_item('1', 'my_base', 'key_b'))
    print('CLIENT 1 close')

def client_2(address, path_base):
    print('CLIENT 2', address)
    uri = 'http://' + address[0] + ':' + str(address[1])
    print('CLIENT 2', uri)
    with xmlrpc.client.ServerProxy(uri) as proxy:
        print('CLIENT 2 Cchecked')
        print(proxy.system.listMethods())
        print(proxy.add(1, 1))
        print('CLIENT 2 Cchecked 2')
        print(proxy.ada(1, 1))
        print(proxy.sub(5, 3))
        print(proxy.set_item('2', 'my_base', 'key_a'))
        print(proxy.set_item('2', 'my_base', 'key_a'))
        print(proxy.set_item('2', 'my_base', 'key_b'))
        print(proxy.set_item('2', 'my_base', 'key_b'))
        print('CLIENT 2 get_keys Hard:', proxy.get_keys('2'))
        print('CLIENT 2 get_np Hard:')
        decodedArrays = json.loads(proxy.get_np('2'), cls=PythonObjectDecoder)
        finalNumpyArray = decodedArrays["array"]
        print(finalNumpyArray)
    print('CLIENT 2 close')


if __name__ == "__main__":
    with LaunchServer(12000) as srv:
        print('INIT SERVER CHECK')
        print('SERVER ADDRESS', srv.info)
        path_base = 'my_base'
        client_thr_1 = threading.Thread(target=client_1, args=[srv.info, path_base])
        client_thr_2 = threading.Thread(target=client_2, args=[srv.info, path_base])
        client_thr_1.start()
        client_thr_2.start()
        client_thr_1.join()
        client_thr_2.join()
        print('SERVER before close')
    print('SERVER before destructeur')
