import collections
import os
import socket
import threading
from xmlrpc.server import SimpleXMLRPCServer

from pykdi.session.tools.get_ipaddress import get_ipaddress
from pykdi.session.tools.tuplify import tuplify

from pykdi.kdi_builder import kdi_builder

class NetStorageException(Exception):
    """
    Exception for network storage implementations
    """
    pass

class DictCollection:
    """
    Dictionary of dictionaries.
    """
    __slots__ = '_collection', '_bases'
    def __init__(self):
        print('>>>>> DictCollection Semantic constructeur', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        self._collection = dict()
        self._bases = []
        print('DictCollection Semantic constructeur <<<<<')

    def __del__(self):
        print('DictCollection Semantic destructeur', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)

    def initialize(self, filename: str, conf: dict[str, any], mode: str):
        print('DictCollection Semantic initialize', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        try:
            id =self._collection[filename]
            print('DictCollection Semantic initialize ALREADY', filename, id)
            # TODO Il faut verifier que la configuration est bien strictement la meme
            desc = self._bases[id].copy()
            del desc['filename']
            del desc['#']
            del desc['base']
            del desc['chunks']
            del desc['iterator']
            del desc['close_readers']
            del desc['close_writers']
            assert (desc == conf)
            print('DictCollection Semantic initialize ALREADY CHECKED', filename, id)
            return id
        except:
            desc = conf.copy()
            desc['filename'] = filename
            id = len(self._collection)
            desc['#'] = id
            desc['chunks'] = {}
            print(desc)
            desc['base'] = kdi_builder(filename)
            desc['base'].set_conf(conf)
            desc['base'].initialize()
            desc['iterator'] = []
            self._bases.append(desc)
            self._collection[filename] = id
            print('DictCollection Semantic initialize NEW', filename, id)
            self._bases[id]['close_readers'] = self._bases[id]['nb_readers']  # le nb attendu
            self._bases[id]['close_writers'] = self._bases[id]['nb_writers']  # le nb attendu
            print('close_writers', id, self._bases[id]['close_writers'])
            return id

    def finalize(self, id:int, mode:str):
        if mode == 'r':
            self._bases[id]['close_readers'] = self._bases[id]['close_readers'] - 1  # on diminue jusqu'à atteindre 0
        elif mode == 'w':
            self._bases[id]['close_writers'] = self._bases[id]['close_writers'] - 1  # on diminue jusqu'à atteindre 0
            print('close_writers', id, self._bases[id]['close_writers'])
        else:
            raise Exception('mode', mode, 'incorrect')

    def first_variant(self, id: int) -> str:
        return self._bases[id]['base']._base['/chunks'].order[0]

    def iterator(self, id: int) -> float:
        print('DictCollection Semantic iterator', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        it = len(self._bases[id]['iterator'])
        self._bases[id]['iterator'].append({'current': None, 'wait': False,})
        return it

    def next_iterator(self, id: int, it: int) -> float:
        print('DictCollection Semantic next_iterator', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        print('DictCollection Semantic next_iterator', self._bases[id]['iterator'][it])
        if self._bases[id]['iterator'][it]['wait']:
            print('DictCollection Semantic next_iterator en attente', self._bases[id]['iterator'][it])
            if self._bases[id]['chunks'][self._bases[id]['iterator'][it]['current']]['mode'] == 'r':
                print('DictCollection Semantic next_iterator débloqué pour read')
                return  self._bases[id]['iterator'][it]['current']
            print('DictCollection Semantic next_iterator toujours bloqué')
            return 'LOOP'
        # si pas en attente
        sorted_keys = sorted(self._bases[id]['chunks'].keys())
        print('DictCollection Semantic next_iterator sorted_keys', sorted_keys)
        if self._bases[id]['iterator'][it]['current'] is None:
            print('DictCollection Semantic next_iterator first', sorted_keys[0])
            self._bases[id]['iterator'][it]['current'] = sorted_keys[0]
            if self._bases[id]['chunks'][self._bases[id]['iterator'][it]['current']]['mode'] == 'r':
                print('DictCollection Semantic next_iterator débloqué pour read')
                self._bases[id]['iterator'][it]['wait'] = False
                return self._bases[id]['iterator'][it]['current']
            print('DictCollection Semantic next_iterator toujours bloqué')
            self._bases[id]['iterator'][it]['wait'] = True
            return 'LOOP'
        for key in sorted_keys:
            if key > self._bases[id]['iterator'][it]['current']:
                # si il existe un temps plus élevé, on prend
                self._bases[id]['iterator'][it]['current'] = key
                # est-ce qu'on est en mode 'r'
                if self._bases[id]['chunks'][key]['mode'] == 'r':
                    # oui, on retourne la valeur
                    return key
                # non, on retourne None
                self._bases[id]['iterator'][it]['wait'] = True
                return 'LOOP'
        # si pas de temps disponible ('wait' à False)
        if self._bases[id]['close_writers'] == 0:
            return 'TERMINATED'
        return None

    def first_chunk(self, id: int, mode: str, chunk: dict[str, any]) -> str:
        print('DictCollection Semantic __repr__', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        first_chunk = chunk[self._bases[id]['base']._base['/chunks'].order[0]]
        return str(first_chunk)

    def open(self, id: int, mode: str, chunk: dict[str, any]) -> bool:
        print('DictCollection Semantic open', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        print('@@@@ FC open')
        first_chunk = chunk[self._bases[id]['base']._base['/chunks'].order[0]]
        print('@@@@ FC open ', first_chunk)
        print('@@@@ FC open ', mode)
        if mode == 'w':
            print('@@@@ A')
            nb_writers = self._bases[id]['nb_writers']
            print('@@@@ B')
            try:
                assert (self._bases[id]['chunks'][first_chunk]['mode'] == 'w')
                assert (self._bases[id]['chunks'][first_chunk]['open'] < nb_writers)
                self._bases[id]['chunks'][first_chunk]['open'] += 1
            except:
                print('@@@@ C')
                self._bases[id]['chunks'][first_chunk] = {'mode': 'w', 'open': 1, 'close': 0, 'kdi_chunk': self._bases[id]['base'].chunk(chunk)}
                print('@@@@ D')
            print('@@@@ FC open TRUE')
            return True
        elif mode == 'r':
            if self._bases[id]['chunks'][first_chunk]['mode'] != 'r':
                print('@@@@ FC open FALSE')
                return False
            nb_readers = self._bases[id]['nb_readers']
            if self._bases[id]['chunks'][first_chunk]['open'] >= nb_readers:
                print('@@@@ FC open FATAL ERROR FALSE')
                return False
            self._bases[id]['chunks'][first_chunk]['open'] += 1
            print('@@@@ FC open TRUE ', first_chunk)
            return True
        else:
            raise NetStorageException('Incompatible mode open file (\'r\' or \'w\')')

    def close(self, id: int, mode: str, chunk: dict[str, any]):
        print('DictCollection Semantic close', os.getpid(), threading.current_thread().ident, threading.current_thread().native_id)
        print('@@@@ FC semantic close', mode)
        first_chunk = chunk[self._bases[id]['base']._base['/chunks'].order[0]]
        print('@@@@ FC semantic close ', first_chunk)
        if mode == 'w':
            nb_writers = self._bases[id]['nb_writers']
            print('@@@@ FC semantic close nb_writers', nb_writers)
            print('@@@@ FC semantic close mode', self._bases[id]['chunks'][first_chunk]['mode'])
            assert (self._bases[id]['chunks'][first_chunk]['mode'] == 'w')
            assert (self._bases[id]['chunks'][first_chunk]['close'] < self._bases[id]['chunks'][first_chunk]['open'])
            self._bases[id]['chunks'][first_chunk]['close'] += 1
            if self._bases[id]['chunks'][first_chunk]['close'] == nb_writers:
                self._bases[id]['chunks'][first_chunk] = {'mode': 'r', 'open': 0, 'close': 0, 'kdi_chunk': self._bases[id]['chunks'][first_chunk]['kdi_chunk']}
        elif mode == 'r':
            nb_readers = self._bases[id]['nb_readers']
            assert (self._bases[id]['chunks'][first_chunk]['mode'] == 'r')
            assert (self._bases[id]['chunks'][first_chunk]['close'] < self._bases[id]['chunks'][first_chunk]['open'])
            self._bases[id]['chunks'][first_chunk]['close'] += 1
            if self._bases[id]['chunks'][first_chunk]['close'] == nb_readers:
                pass  # TODO hook_finally_read
        else:
            raise NetStorageException('Incompatible mode open file (\'r\' or \'w\')')
        print('@@@@ FC semantic close terminated')

    def get_keys(self, id: int) -> [str]:
        return self._bases[id]['base'].get_keys()

    def update(self, id: int, typename: str, name: str):
        print('### update')
        self._bases[id]['base'].update(typename, name)

    def update_fields(self, id: int, field_parent_name: str, field_basename: str) -> str:
        print('### update_fields')
        print('### id', id)
        print('### field_parent_name', field_parent_name)
        print('### field_basename', field_basename)
        print('### BEFORE', self._bases[id]['base'].base['/fields'])
        strez = self._bases[id]['base'].update_fields(field_parent_name, field_basename)
        print('### AFTER', self._bases[id]['base'].base['/fields'])
        print('### return', strez)
        return self._bases[id]['base'].update_fields(field_parent_name, field_basename)

    def update_sub(self, id: int, mesh_parent_name: str, submesh_basename: str) -> str:
        print('### update_sub')
        return self._bases[id]['base'].update_sub(mesh_parent_name, submesh_basename)


class XMLRPCDictCollection(DictCollection):
    """
    Adapt DictCollection to be used by SimpleXMLRPCServer.
    """
    def _dispatch(self, method, params):
        """
        Used by SimpleXMLRPCServer to dispatch RPC calls to corresponding instance methods.
        _tuplify() is a fix because XML serializer turns tuples into lists...
        """
        print('XMLRPCDictCollection Semantic dispatch', method, params)
        return getattr(self, method)(*tuplify(params))


class SemanticServer:
    """
    DictCollection server using XML-RPC protocol.
    Exposes an instance of an XML-RPC adapted DictCollection to store data.
    """
    def __init__(self, port):
        print('>>>>> SemanticServer constructeur (port', port, ')', os.getpid())
        self._ip_address = get_ipaddress()
        self._port = port
        # A dict of dict
        self._store = XMLRPCDictCollection()
        self._server = SimpleXMLRPCServer((self._ip_address, self._port), allow_none=True, logRequests=False)
        self._server.register_instance(self._store)
        self._server_thread = threading.Thread(target=self._server.serve_forever)
        print('SemanticServer start thread', self._server_thread.ident, self._server_thread.native_id)
        self._server_thread.setDaemon(True)
        self._server_thread.start()
        print('SemanticServer start thread', self._server_thread.ident, self._server_thread.native_id)
        print('SemanticServer constructeur <<<<<')

    def __del__(self):
        print('SemanticServer destructeur', os.getpid())

    @property
    def info(self):
        """Return tuple address/port used for clients factory"""
        return self._ip_address, self._port

    def close(self):
        """Close server"""
        print('>>>>> SemanticServer close')
        print('SemanticServer shutdown thread', self._server_thread.ident, self._server_thread.native_id, self._server_thread.is_alive())
        self._server.shutdown()
        self._server_thread.join()
        print('SemanticServer thread', self._server_thread.ident, self._server_thread.native_id, self._server_thread.is_alive())
        print('SemanticServer close <<<<<')

def semantic_bootstrap_server(port=12000):
    for test_ind in range(200):
        try:
            return SemanticServer(port + test_ind)
        except socket.error:
            pass
    raise NetSemanticException("Cannot create server.")
