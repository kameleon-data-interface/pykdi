import numpy as np

from .KDIException import KDIException
from .KDIDatas import KDIDatas
from .KDITyping import KDIDataBaseTyping

KDI_GROWNING_VARIANT = 'KDI_GROWNING_VARIANT'
KDI_CONSTANT_VARIANT = 'KDI_CONSTANT_VARIANT'

KDI_DATA_CONSTANT_BY_SIMULATION = {}
KDI_DATA_ALL_PARTS_SAME_VALUE_CONSTANT = -42


def check_variant_description(variant_name: str, variant_description: dict[str,], authorized_keys: {str}):
    """
    Check variant description once the type of variant has been identified.
    :param variant_name: the variant name
    :param variant_description: the variant description
    :param authorized_keys: the authorized keys
    :return:
    """
    variant_description_keys = set(variant_description.keys())
    if variant_description_keys != authorized_keys:
        for key in authorized_keys - variant_description_keys:
            raise KDIException('pre: variant `' + variant_name + '` must be have a key `' + key + '`')
        for key in variant_description_keys - authorized_keys:
            raise KDIException('pre: variant `' + variant_name + '` not must be have a key `' + key + '`')


def initialize_variant_description_by_variant_name(function):
    """
    This decorator method initialize variant_description from variant_name.
    Raises an exception if the variant does not exist.
    :param function: the function decorated
    :return: new function
    """

    def internal_checked_and_get_variant(*args, **kwargs):
        try:
            kwargs['variant_description'] = args[0].variants[args[1]]
        except KeyError:
            raise KDIException('pre: variant `' + args[1] + '` is not a variant of the base (' + str(args[0].order) + ')')
        return function(*args, **kwargs)

    return internal_checked_and_get_variant


class KDIChunks(KDIDatas):
    __slots__ = 'base', 'mode', 'order', 'variants'

    def __init__(self, base: KDIDataBaseTyping, order: list[str] = None, variants: dict[str, any] = None):
        """
        Initializes an instance of KDIChunks.
        The user will not call this method directly which will be called:
        - by kdi_update when creating the database than with the parameter `base`; or
        - by loadJSON when deserialize an instance of this object necessary to pass all these parameters.
        :param base: the current base
        :param order: only via loadJSON, an ordered list of variant names
        :param variants: only via loadJSON, a dictionary describing the specificities of the variant
        """
        super().__init__()
        self.base = base
        self.mode = 'w'
        if order:
            self.order = order
        else:
            self.order = []
        if variants:
            self.variants = variants
        else:
            self.variants = dict()

    def check_variants(self, chg_variants: dict[str, any]) -> None :
        nb = len(chg_variants)
        for variant in reversed(self.order):
            if variant in chg_variants:
                nb = nb - 1
                continue
            break
        if nb != 0:
            raise KDIException('KDIChunks:check_del_variants del_variants (' + str(chg_variants) + ') incompatible avec self.order (' + str(self.order) + ')')


    def reduce_chunk(self, chg_variants: list[str], chunk: dict[str, any] = None) -> dict[str, any]:
        if chunk is None:
            new_chunk = self.base['/chunks/current'].copy()
        else:
            new_chunk = chunk.copy()
        for variant, value in chg_variants.items():
            new_chunk[variant] = value
        return new_chunk

    def insert(self, data, chunk=None) -> None:  # Set have been view as Append (KDIList)
        raise NotImplementedError()

    def erase(self, chunk=None) -> None:
        raise NotImplementedError()

    def get(self, chunk=None) -> any:
        raise NotImplementedError()

    def reset(self, chunks):
        self.order = chunks.order.copy()
        self.variants = chunks.variants.copy()

    def SetVariants(self, variants):
        """
        This method SetVariants can only be called once to declare the variants of the base.
        In any case, we can come back to it later.

        Declaration of variants begins by defining an ordered array of pairs, with each pair describing a variant.

        The declaration of a variant is done by this pair describing its name then a dictionary
        to characterize this variant.

        This dictionary includes the 'type' key which must be entered. Today, it can take the following values:
        - KDI_CONSTANT_VARIANT
            If the value of the variant is framed by 0 and a maximum integer which does not
            change during the simulation; this is the case of a variant which would describe
            the partition/subdomain number of a simulation.
        - KDI_GROWNING_VARIANT
            If the range of possible values of this variant increases as the simulation progresses;
            this is the case of a temporal variant.

        Other more specific keys must then be entered in each of these cases.

        For KDI_CONSTANT_VARIANT, the 'max' key is essential in order to define the entire upper
        excluded limit of the range of this key (inaccessible value), the entire lower included limit being 0.
        When we position a chunk with the Set method, the value assigned to the key will be integer and in [0, max[.
        If you want to use the writing service of a VTK HDF database, you will need to define the 'part' key.
        In addition, the writing should only be carried out when having declined the data on all the
        possible values of this variant.

        For KDI_GROWNING_VARIANT, the 'dtype' key is essential to determine the key type.
        When we position a chunk with the Set method, the behavior varies depending on the assigned
        value and the state of the values already saved.
        If the new value is greater than all those already recorded, it is added at the end.
        If the value is lower than the latter, a truncation in the list is then carried out
        to cover the values lower than this new one by including it at the end of the list.
        If you want to use the writing service of a VTK HDF database, you will need to define the 'step' key.

        Here is an example of a declaration when creating a new database:
            base['/chunks'].SetVariants([
                    ['step', {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
                    ['part', {'type': KDI_CONSTANT_VARIANT, 'max': 5, }, ],
                ])
        which translates the ordered declaration of two variants: 'step' and 'part'.
        The 'step' variant, describing the temporal evolution of the simulation, is of increasing type
        and is described by a floating value.
        The 'part' variant, describing the distribution of the simulation, is of constant type and is
        fixed to 5 parts, partitions, subdomain.

        :param variants: orderly declaration of variants of this base.
        """
        if self.order:
            # Just one declaration of variants per base
            raise KDIException('pre: not empty variants. Just a call to create the base!')

        if self.mode != 'w':
            raise Exception('pre: not mode write')

        for variant_name, variant_description in variants:
            if 'type' not in variant_description:
                raise KDIException(
                    'pre: variant `' + variant_name +
                    '` must be have a key `type` set in (KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT)')

            self.order.append(variant_name)

            if variant_description['type'] == KDI_GROWNING_VARIANT:
                check_variant_description(variant_name, variant_description, {'type', 'dtype'})
                self.variants[variant_name] = {
                    'data': np.array([], dtype=variant_description['dtype']),
                }
                self.variants[variant_name].update(variant_description)
                continue

            if variant_description['type'] == KDI_CONSTANT_VARIANT:
                check_variant_description(variant_name, variant_description, {'type', 'max'})
                self.variants[variant_name] = {
                    'data': variant_description['max'],
                }
                self.variants[variant_name].update(variant_description)
                continue

            raise KDIException(
                'pre: variant `' + variant_name +
                '` the key `type` unknow value not in (KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT')

    def build_chunk(self, values: any, create: bool):
        """
        :param values: None, dict ou tableau / tuple dans l'ordre des variants
        :return:
        """
        assert (self.mode == 'w')

        if values is None or not isinstance(values, dict):
            my_values = dict()
            for variant, value in zip(self.order, values):
                my_values[variant] = value
        else:
            my_values = values.copy()

        nbVariants = 0
        for variant in self.order:
            try:
                value = my_values[variant]
            except KeyError:
                # Par exemple, cela veut dire que l'on est dans un chunk partiellement défini mettant ainsi en
                # avant une valeur globale.
                # Dans le cas d'une base pour VTK HDF, on a deux possibilités :
                # - stepless et partless: une valeur globale invariante pour toute la simulation
                # - partless: une valeur globale par temps de simulation
                # TODO Aujourd'hui, c'est aspect ne sont pas traité au niveau du VTK HDF mais le sont au niveau de Memory.
                return my_values
            nbVariants += 1

            try:
                my_variant = self.variants[variant]
            except:
                raise KDIException("Guru Meditation: Internal Error")

            if my_variant['type'] == KDI_GROWNING_VARIANT:
                # TODO Faire la conversion en fonction du type pour ce variant
                my_values[variant] = float(my_values[variant])
                my_values['#' + variant] = str(my_values[variant])
                # TODO optimiser la recherche avec un cache de la position precedente
                save = 0
                for idx, val in np.ndenumerate(my_variant['data']):
                    if val == value:
                        save = -1
                        break
                    if val > value:
                        save = idx[0]
                        break
                    save = idx[0] + 1
                if save >= 0:
                    if not create:
                        raise KDIException('pre: variant (' + variant + ') is KDI_GROWNING_VARIANT but value (' + str(
                            value) + ') not exist in mode get()')
                    assert (len(my_variant['data'].shape) == 1)
                    array = np.zeros(shape=(save + 1), dtype=my_variant['data'].dtype)
                    if save:
                        array[:save] = my_variant['data'][:save]
                    array[save] = value
                    my_variant['data'] = array

            elif my_variant['type'] == KDI_CONSTANT_VARIANT:
                # TODO Faire la conversion en fonction du type pour ce variant
                nv = int(my_values[variant])
                assert (float(my_values[variant]) == float(nv))
                my_values[variant] = nv
                my_values['#' + variant] = str(my_values[variant])

                if value < 0 or my_variant['data'] <= value:
                    raise KDIException('pre: variant (' + variant + ') is KDI_CONSTANT_VARIANT but value (' + str(
                        value) + ') not in [0, ' + str(my_variant['data']) + '[')

            else:
                KDIException('Guru Meditation: Internal Error')

        return my_values

    def string_chunk(self, chunk: any, create: bool) -> str:
        """

        :param chunk:
        :return:
        """
        if chunk is None:
            return self.base['/chunks/current']
        return self.base['/chunks'].build_chunk(chunk, create)

    def set(self, values=None) -> None:
        """

        :param values:
        :return:
        """
        self.base['/chunks/current'] = self.build_chunk(values, True)

    @initialize_variant_description_by_variant_name
    def GetAll(self, variant_name, variant_description=None):
        if variant_description['type'] == KDI_GROWNING_VARIANT:
            return variant_description['data']

        if variant_description['type'] == KDI_CONSTANT_VARIANT:
            return variant_description['data']

        raise KDIException('pre:type variant is not KDI_GROWNING_VARIANT or KDI_CONSTANT_VARIANT!')

    @initialize_variant_description_by_variant_name
    def GetMaxConstantValue(self, variant_name, variant_description=None):
        if variant_description['type'] != KDI_CONSTANT_VARIANT:
            raise KDIException('pre:type variant is not KDI_CONSTANT_VARIANT!')

        return variant_description['data']

    @initialize_variant_description_by_variant_name
    def GetOffset(self, variant_name, value, variant_description=None):
        if variant_description['type'] == KDI_GROWNING_VARIANT:
            for idx, val in np.ndenumerate(variant_description['data']):
                if val == value:
                    return idx[0]

        if variant_description['type'] == KDI_CONSTANT_VARIANT:
            if value < 0 or variant_description['data'] <= value:
                raise KDIException(
                    'Incorrect value ' + variant_name + ' (expended 0<=' + str(value) + '<' + str(variant_description['data']) + ')!')
            return value

        raise KDIException('pre:type variant is not KDI_GROWNING_VARIANT or KDI_CONSTANT_VARIANT!')

    @initialize_variant_description_by_variant_name
    def GetValue(self, variant_name, idx, variant_description=None):
        if variant_description['type'] == KDI_GROWNING_VARIANT:
            try:
                return variant_description['data'][idx]
            except IndexError:
                raise KDIException('pre: no value available for \'' + variant_name + '\' at index ' + str(idx))

        if variant_description['type'] == KDI_CONSTANT_VARIANT:
            value = variant_description['data'] + idx
            if value < 0:
                return 0
            return value

        raise KDIException('pre: type variant is not KDI_GROWNING_VARIANT or KDI_CONSTANT_VARIANT!')

    def GetOffsets(self, variants):
        offsets = dict()
        if len(variants) != len(self.order):
            raise KDIException('missing variant in parameter!')
        for variant in self.order:
            try:
                value = variants[variant]
            except NameError:
                raise KDIException('variant ' + variant + ' not exist!')
            offsets[variant] = self.GetOffset(variant, value)
        return offsets

    def __repr__(self):
        ret = 'KDIChunks(base=KDIBase, order=' + str(self.order) + ', variants={'
        for variant in self.order:
            value = self.variants[variant]
            ret += '\'' + variant + '\':{'
            if value['type'] == KDI_GROWNING_VARIANT:
                ret += '\'type\':' + value['type'] + ', \'dtype\':\'' + value['dtype'] + '\', \'data\':'
            elif value['type'] == KDI_CONSTANT_VARIANT:
                ret += '\'type\':' + value['type'] + ', \'data\':'
            else:
                raise KDIException('Guru Meditation: Internal Error')
            try:
                data = value['data']
                # TODO in place: ret ++ data.__repr__()
                ret += 'np.array(' + str(data.tolist()) + ')'
            except:
                ret += str(value['data'])
            ret += '},'
        ret += '})'
        return ret
