import numpy.typing as npt

from .KDIDatas import KDIDatas

KDIDataBaseTyping = dict[str, KDIDatas | bool | str | npt.ArrayLike]
KDIDataBaseAttributeRestrictionTyping = None | list[str] | tuple[str]
