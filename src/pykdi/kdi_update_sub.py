# pykdi
from .kdi_update import kdi_update
from .KDIDatas import KDIDatas
from .KDIException import KDIException
from .KDISubMeshes import KDISubMeshes
from .KDITyping import KDIDataBaseTyping

def kdi_update_sub(base: KDIDataBaseTyping,
                   mesh_parent_name: str,
                   sub_mesh_basename: str) -> str:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet de compléter la database existante en créant un sous-maillage sur un maillage existant.
        Le type du maillage doit être un type complexe compris dans la liste ['UnstructuredGrid'].
        En fonction de ce type, le type du sous-maillage sera déterminé.
        Ainsi pour un type de maillage 'UnstructuredGrid', le type du sous-maillage sera 'SubUnstructuredGrid'.

        :param base: (in/out) la base existante qui sera complétée
        :param mesh_parent_name: (in) le nom du maillage parent, il doit être déjà défini dans la base
        :param sub_mesh_basename: (in) le nom de base du sous-maillage
        :return: la clef dans la base pour ce sous-maillage

        Testé par test_kdi_base.py
    """
    try:
        base[mesh_parent_name]
    except KeyError:
        raise KDIException('pre: mesh_parent_name \'' + mesh_parent_name + '\' must be exist!')

    try:
        type_mesh = base[mesh_parent_name].get_complex_type()
    except (KeyError, NotImplementedError):
        raise KDIException('pre: mesh_parent_name \'' + mesh_parent_name + '\' must be an ComplexType!')

    if type_mesh not in ['UnstructuredGrid']:
        raise KDIException(
            'pre: mesh_parent_name \'' + mesh_parent_name + '\' must be an UnstructuredGrid (not ' +
            base[mesh_parent_name].get_complex_type() + ')!')

    meshes_fullname = mesh_parent_name + '/submeshes'

    try:
        if isinstance(base[meshes_fullname], KDISubMeshes):
            raise KDIException(
                'pre: meshes_fullname \'' + meshes_fullname + '\' must be an KDISubMeshes (not ' + base[
                    mesh_parent_name].get_complex_type() + ')!')
    except:
        kdi_update(base, 'SubMeshes', meshes_fullname)

    sub_mesh_fullname = meshes_fullname + sub_mesh_basename

    if sub_mesh_fullname in base:
        return sub_mesh_fullname

    # Fr: Le type d'un sous-maillage d'un maillage UnstructuredGrid est SubUnstructuredGrid,
    #     ce dernier décrit juste les champs globaux, locaux aux cellules et la sélection des
    #     cellules pour construire ce sous-maillage à partir du maillage parent.
    type_sub_mesh = {'UnstructuredGrid': 'SubUnstructuredGrid'}

    kdi_update(base, type_sub_mesh[type_mesh], sub_mesh_fullname)

    base[meshes_fullname].insert(sub_mesh_fullname)

    return sub_mesh_fullname
