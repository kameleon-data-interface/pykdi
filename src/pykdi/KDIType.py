import numpy as np

KDI_TRIANGLE: int = 0
KDI_QUADRANGLE: int = 1
KDI_POLYGON: int = 4
KDI_HEXAHEDRON: int = 12

KDI_PIT: int = 12 + 1  # +1 de la valeur d'enumeration la plus elevee
KDI_SIZE: int = KDI_PIT + 1

dict_kdi2nbPoints = {KDI_TRIANGLE: 3, KDI_QUADRANGLE: 4, KDI_POLYGON: -1, KDI_HEXAHEDRON: 8}

kdi2nbPoints = np.full(shape=KDI_SIZE, fill_value=0, dtype=np.int8)
for k, v in dict_kdi2nbPoints.items():
    assert (k < len(kdi2nbPoints))
    kdi2nbPoints[k] = v
