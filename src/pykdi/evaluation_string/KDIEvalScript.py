from pykdi.KDIDatas import KDIDatas
from pykdi.KDIException import KDIException
from pykdi.KDITyping import KDIDataBaseTyping

class KDIEvalScript(KDIDatas):
    __slots__ = 'base', 'key_base', 'script_file', 'params', 'serializable'

    def __init__(self, base: KDIDataBaseTyping, key_base: str, script_file: str, params: list[any], serializable: bool=None):
        #TODO Serialiable depen d beaucoup de l'expression des parametres, autant faire se peu ne pas passer d'objet mais des chaines de caracteres
        #TODO Lorsqu'on veut serialiser... il faudrait néanmoins verifier le fonctionnement avant puis apres d'une exécution fonctionne.
        super().__init__()
        self.base = base
        self.key_base = key_base
        assert (isinstance(script_file, str))
        self.script_file = script_file
        self.params = params
        self.serializable = serializable

    def insert(self, data, chunk=None) -> None:
        raise NotImplemented

    def erase(self, chunk=None) -> None:
        pass

    def get(self, chunk=None) -> any:
        variables = {'Base': self.base, 'Params': self.params, 'Ret': []}
        lines = []
        with open(self.script_file, 'r') as fd:
            lines = fd.read()

        variables = {'Base': self.base, 'Params': self.params, 'Ret': []}
        # TODO A modifier a la facon de KDIEvalString
        exec(lines, variables, variables)
        return variables['Ret']

    def light_dump(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return 'KDIEvalScript(base=KDIBase, script_file=\'' + self.script_file + '\', params=' + str(self.params) + ')'

    def __sizeof__(self) -> int:
        return self.script_file.__sizeof__()

    def serialize(self) -> str:
        if self.serializable:
            return super().serialize()
        raise KDIException('Serialize forbidden!')
