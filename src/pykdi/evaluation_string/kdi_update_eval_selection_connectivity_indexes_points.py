# pykdi.evaluation_string
from .KDIEvalString import KDIEvalString
# pykdi
from pykdi.KDIException import KDIException
from pykdi.KDIEvent import KDIEvent
from pykdi.KDITyping import KDIDataBaseTyping

command_eval_selection_connectivty_indexes_points = """
#idx = np.random.randint(0,10000)
#print('KDI EVAL STRING #', idx, 'command_eval_selection_connectivty_indexes_points')
out_base = Base
#print('KDI EVAL STRING #', idx, 'out_base', out_base)
#assert ('/chunks/current' in out_base)
chunk = out_base['/chunks/current']
#print('KDI EVAL STRING #', idx, 'chunk', chunk)

# indexes
indexes_base = Params[0]
indexes_base_chunk = None
if '/chunks/current' in indexes_base:
    indexes_base_chunk = indexes_base['/chunks/current']
indexes_base['/chunks/current'] = chunk
indexes_cells_key = Params[1]
#assert (indexes_cells_key in indexes_base)
indexes_cells_values = indexes_base[indexes_cells_key].get()
#print('KDI EVAL STRING #', idx, 'indexes_cells_values', indexes_cells_values)
if indexes_base_chunk is None:
    del indexes_base['/chunks/current']
else:
    indexes_base['/chunks/current'] = indexes_base_chunk

# in values
in_base = Params[2]
in_base_chunk = None
if '/chunks/current' in indexes_base:
    in_base_chunk = in_base['/chunks/current']
in_base['/chunks/current'] = chunk
in_types_key = Params[3]
assert (in_types_key in in_base)
in_types_values = in_base[in_types_key].get()
#print('KDI EVAL STRING #', idx, 'in_types_values', in_types_values)
in_connectivity_key = Params[4]
assert (in_connectivity_key in in_base)
in_connectivity_values = in_base[in_connectivity_key].get()
#print('KDI EVAL STRING #', idx, 'in_connectivity_values', in_connectivity_values)
if in_base_chunk is None:
    del in_base['/chunks/current']
else:
    in_base['/chunks/current'] = in_base_chunk

# code2nbPoints
code2nbPoints = Params[5]
#print('KDI EVAL STRING #', idx, 'code2nbPoints', code2nbPoints)
#print('KDI EVAL STRING #', idx, 'step 2')
# out key
out_connectivity_key = Params[6]
out_indexes_points_key = Params[7]
in_nbpoints = code2nbPoints[in_types_values]
in_offsets = np.empty((len(in_nbpoints) + 1), dtype=np.int64)
in_offsets[0] = 0
np.cumsum(in_nbpoints, out=in_offsets[1:])
# compute out_nbpoints, out_nbconnectivity
out_nbpoints = in_nbpoints[indexes_cells_values]
#print('KDI EVAL STRING #', idx, 'out_nbpoints', out_nbpoints)
out_nbconnectivity = np.sum(out_nbpoints)
# compute out_connectivity_values with old indexes on points
out_connectivity_values_with_old = np.empty((out_nbconnectivity,), dtype=np.int64)
begin = np.int64(0)
for out_index_cell, in_index_cell in enumerate(indexes_cells_values):
    nbpoints = out_nbpoints[out_index_cell]
    end = begin + nbpoints
    begin2 = in_offsets[in_index_cell]
    end2 = begin2 + nbpoints
    out_connectivity_values_with_old[begin:end] = in_connectivity_values[begin2:end2]
    begin = end
#assert (begin == out_nbconnectivity)
# actually, out_connectivity_with_in but with old indexes points
# compute out_indexes_points_values
out_indexes_points_values = np.array(list(set(out_connectivity_values_with_old)))
# compute old index to (new) index points
try:
    oldtonewindexpoints = np.empty(out_indexes_points_values.max() + 1, dtype=np.int64)
    for out_pt, in_pt in enumerate(out_indexes_points_values):
        oldtonewindexpoints[in_pt] = out_pt
    # compute out_connectivity_values with (new) indexes on points
    out_connectivity_values = oldtonewindexpoints[out_connectivity_values_with_old]
    # return results
    #print('KDI EVAL STRING #', idx, 'RETURN')
    Ret = {out_connectivity_key: out_connectivity_values, out_indexes_points_key: out_indexes_points_values, }
except ValueError:  # .max() raise exception zero-size array to reduction operation maximum
    Ret = {out_connectivity_key: np.empty(0, dtype=np.int64), out_indexes_points_key: np.empty(0, dtype=np.int64), }
#print('KDI EVAL STRING #', idx, 'RETURN', Ret)
"""

"""
Fr: Le surcoût de cette description en Python/Numpy par rapport à un équivalent
    en langage compilé est au moins d'un facteur x??? chaque étape est un parcours :
    - 1) obtention pour chaque cellule du nombre de points (in_nbpoints)
    - 2) la méthode cumsum (in_offsets) (non multi-threadable)
    - 3) sélection des nombres de points suivant l'indexation (out_nbpoints)
    - 4) la somme (out_nbconnectivity)
    - 5) l'extraction de la connectivité (out_connectivity_values_with_old)
    - 6) construire un set sur les indices des points (OUT out_indexes_points_values)
    - 7) construire un index de old indice points vers new indice points (oldtonewindexpoints)
    - 8) construire la nouvelle table de connectivité (OUT out_connectivity_values)
    
    Le pseudo-code d'un langage compilé pourrait faire tout cela en une unique boucle
    sur les cellules sélectionnées (non multi-threadable).
    ...
"""

def kdi_update_eval_selection_connectivity_indexes_points(outbase: KDIDataBaseTyping,
                                                          outconnectivykey: str,
                                                          outindexespointskey: str,
                                                          indexesbase: KDIDataBaseTyping,
                                                          indexescellskey: str,
                                                          inbase: KDIDataBaseTyping,
                                                          intypeskey: str,
                                                          inconnectivykey: str):
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet d'associer aux clefs outconnectivykey et outindexespointskey de la
        base outbase des résultats différenciés d'une expression réalisant une selection,
        un filtrage des valeurs des clefs intypeskey et inconnectivykey de la base inbase
        suivant des indices fournis par la clef indexescellskey de la base indexesbase
        (on parle d'évaluation lazy, c'est à dire à la demande / réaliser que lorsque
        c'est nécessaire).

        :param outbase: (in/out) une base existante sur laquelle on va créer les clefs
          outconnectivykey, résultat utilisé dans la description d'un maillage non
          structuré, et outindexespointskey, résultat intermédiaire intervenant dans
          cette description c'est pourquoi cette clef doit avoir un nom commençant par
          /caching)
        :param outconnectivykey: (in/out) la clef absolue dans la database outbase qui
          permet d'accéder au résultat décrivant la connectivité entre les cellules et
          les points à travers cette évaluation de type sélection
        :param outindexespointskey: (in/out) la clef absolue dans la database outbase qui
          permet d'accéder au résultat décrivant une sélection sur les points, obtenus
          en appliquant la sélection sur la connectivité des cellules, cette clef doit
          commencer par /caching, car ce résultat est un résultat intermédiaire qui n'a
          pas de sens dans la mise en place d'une description d'un maillage non structuré
        :param indexesbase: (in) une base existante sur laquelle on trouvera indexescellskey
        :param indexescellskey: (in) la clef décrivant les indices de sélection des cellules
        :param inbase: (in) une base existante sur laquelle on trouvera intypeskey et inconnectivykey
        :param intypeskey: (in) la clef décrivant le type de chaque cellule sur laquelle on va
          appliquée la sélection
        :param inconnectivykey: (in) la clef décrivant la connectivité cellule/points de
          chaque cellule sur laquelle on va appliquée la sélection
        :return: None

        Example:
          kdi_update_eval_selection_connectivty_indexes_points(
            new_base, out_key_sub + '/cells/connectivity', '/caching' + out_key_sub + '/indexespoints',
            base, in_key_sub + '/indexescells',
            base, key + '/cells/types', key + '/cells/connectivity')
    """
    outbase['/caching' + outconnectivykey + outindexespointskey] =(
        KDIEvalString(outbase, '/caching' + outconnectivykey + outindexespointskey,
                      command_eval_selection_connectivty_indexes_points,
                      [indexesbase, indexescellskey, inbase, intypeskey, inconnectivykey, inbase['/code2nbPoints'],
                       outconnectivykey, outindexespointskey]))
    outbase[outconnectivykey] = KDIEvent(outbase, '/caching' + outconnectivykey + outindexespointskey, outconnectivykey)
    outbase[outindexespointskey] = KDIEvent(outbase, '/caching' + outconnectivykey + outindexespointskey, outindexespointskey)
