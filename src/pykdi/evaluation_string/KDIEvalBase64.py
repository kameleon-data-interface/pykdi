import base64

from pykdi.KDIMemory import KDIMemory
from pykdi.KDIException import KDIException
from pykdi.KDITyping import KDIDataBaseTyping

from .KDIEvalString import KDIEvalString


class KDIEvalBase64(KDIEvalString):
    """
    Fr: Cette classe fait partie de l'API publique.
        C'est la version serialisée de KDIEvalString que l'on obtient lorsqu'on
        appelle la méthode __repr__.
    """
    def __init__(self, base: KDIDataBaseTyping, key_base: str, lines: str, params: list[any], kmemo=None):
        super().__init__(base, key_base, lines, params, kmemo)
        assert (isinstance(lines, str))
        self.key_base = key_base
        self.lines = base64.b64decode(lines).decode()
        self.params = params
        if kmemo:
            if not isinstance(kmemo, KDIMemory):
                print('#### ')
                print('#### KDIEvalString FATAL ERROR kmemo is not instance KDIMemory')
                print('#### ')
                raise KDIException('KDIEvalString FATAL ERROR kmemo is not instance KDIMemory')
            self.kmemo = kmemo
        else:
            self.kmemo = KDIMemory(base=base, key_base=key_base)

    def get(self, chunk=None) -> any:
        try:
            return self.kmemo.get(chunk)
        except:
            return KDIEvalString.get(self, chunk)

    def light_dump(self) -> str:
        msg = 'KDIEvalBase64(... , datas='
        msg += self.kmemo.light_dump()
        msg += ')'
        return msg
