# pykdi.evaluation_string
from .KDIEvalString import KDIEvalString
# pykdi
from pykdi.KDIException import KDIException
from pykdi.KDITyping import KDIDataBaseTyping

command_eval_offsets = """
base = Base
code2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    nbPoints = code2nbPoints[typesCode.view(dtype=np.int8)]
else:
    nbPoints = code2nbPoints[typesCode]
Ret = np.empty((len(nbPoints) + 1), dtype=np.int64)
Ret[0] = 0
np.cumsum(nbPoints, out=Ret[1:])
"""

"""
Fr: Le surcoût de cette description en Python/Numpy par rapport à un équivalent
    en langage compilé est au plus d'un facteur x2 :
    - un premier parcours est réalisé afin de produire le tableau nbPoints
      donnant le nombre de points de chaque cellule à partir du type de la cellule
      (en fonction de l'implémentation Numpy, cette indexation pourrait être multi-threadé
      en réduisant notablement l'impact)
    - un second parcours est fait par la méthode cumsum (non multi-threadable).

    Le pseudo-code d'un langage compilé pourrait donner ceci (non multi-threadable):
      offsets = array(types.size()+1, long)
      it_offsets = offsets.begin() 
      *it_offsets = 0
      ++it_offsets
      cumsum = 0
      for (it_types = types.begin() ;
           it_types != types.end() ;
           ++it_offsets, ++it_types)
        cumsum += code2nbPoints[*it_types]
        *it_offsets = cumsum
"""

def kdi_update_eval_offsets(base: KDIDataBaseTyping,
                            abs_path_cells: str) -> None:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet d'associer à la clef de la base abs_path_cells+'/offsets' l'expression
        d'un calcul qui sera effectivement réalisé lorsqu'on demandera les valeurs associés
        à cette clef (on parle d'évaluation lazy, c'est à dire à la demande / réalisé que
        lorsque c'est nécessaire).
        L'expression permet ici de calculer, pour toutes les cellules, la valeur de l'offset
        permettant d'accéder aux informations relatifs à la connectivité d'une cellule donnée.
        Pour cela, le calcul nécessite :
        - la présence de '/code2nbPoints' dans la base qui permet d'associer à une valeur de
          type de cellule, un nombre de points ;
        - une clef abs_path_cells qui décrit un ensemble de cellules, une instance de 'Cells':
              "Cells": {
                "/globalIndexContinuous": "GlobalIndexContinuous",
                "/types": "Types",
                "/connectivity": "Connectivity",
                "/fields": "Fields"
              }
          on trouve en effet, le type des cellules via l'attributs 'types".

        Ce type d'objet 'Cells' est utilisé dans la description d'un maillage non structuré
        (type UnstructuredGrid) au niveau de l'attribut 'cells'.

        :param base: (in/out) la base existante qui sera complétée
        :param abs_path_cells: (in) la clef absolue dans la database base permettant d'accéder
          à une instance de 'Cells'.
        :return: None
    """
    if abs_path_cells + '/offsets' in base:
        # Checks that the order is the same, in fact in a database we must guarantee the
        # invariance of the description.
        # If this were to happen, we would no longer be in the context of an addition but
        # of a mutation of the base (as is the case for producing the meshes corresponding
        # to 'SubMesh' sub-meshes)
        if command_eval_offsets != base[abs_path_cells + '/offsets'].lines:
            raise KDIException('pre: command already exist but it\'s different!')
        return
    # Set KDIEvalString
    base[abs_path_cells + '/offsets'] = KDIEvalString(base, abs_path_cells + '/offsets', command_eval_offsets,
                                                      [base['/code2nbPoints'], abs_path_cells + '/types'])
