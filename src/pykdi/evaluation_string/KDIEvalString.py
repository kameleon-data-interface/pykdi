import base64
import numpy as np

from pykdi.KDIDatas import KDIDatas
from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory
from pykdi.KDITyping import KDIDataBaseTyping


class KDIEvalString(KDIDatas):
    """
    Fr: Cette classe fait partie de l'API publique.
        La version serialisée de cette classe KDIEvalString c'est la classe KDIEvalBase64
        que l'on obtient sérialisée lorsqu'on appelle la méthode __repr__.

        Cette classe supporte le stockage de données chunkées ce qui permet d'accéder à
        des données récemment décrites comme pour la clef '/mymesh/cells/types' mais aussi
        de récupérer le types de cellules déjà stockées en appliquant un traitement (une
        indexation via vtk2code) sur les valeurs lues accessibles par '/KVTKHDF/types'.

        TODO Il faut remplacer la clef '/KVTKHDF/types' par '/KVTKHDF/mymesh/types'
        TODO Il reste à utiliser serializable pour autoriser ou non la serialisation de cette instance
        TODO Sauf erreur, le kmemo n'est pas utilisé... il pourrait l'être de le cadre de la mise en place d'un cache
        TODO Même contrainte que KDIEvalScript, et vice et versa
    """
    __slots__ = 'base', 'key_base', 'lines', 'params', 'kmemo', 'serializable'

    def __init__(self, base: KDIDataBaseTyping, key_base: str, lines: str, params: list[any], kmemo=None, serializable: bool=True):
        #TODO Serialiable depen d beaucoup de l'expression des parametres, autant faire se peu ne pas passer d'objet mais des chaines de caracteres
        #TODO Lorsqu'on veut serialiser... il faudrait néanmoins verifier le fonctionnement avant puis apres d'une exécution fonctionne.
        super().__init__()
        self.base = base
        self.key_base = key_base
        assert (isinstance(lines, str))
        self.lines = lines
        self.params = params
        if kmemo:
            if not isinstance(kmemo, KDIMemory):
                print('#### ')
                print('#### KDIEvalString FATAL ERROR kmemo is not instance KDIMemory')
                print('#### ')
                raise KDIException('KDIEvalString FATAL ERROR kmemo is not instance KDIMemory')
            self.kmemo = kmemo
        else:
            self.kmemo = KDIMemory(base=base, key_base=key_base)
        self.serializable = serializable

    def insert(self, data, chunk=None) -> None:
        self.kmemo.insert(data, chunk)

    def erase(self, chunk=None) -> None:
        self.kmemo.erase(chunk)

    def get(self, chunk=None) -> any:
        try:
            return self.kmemo.get(chunk)
        except:
            old_chunk = None
            if chunk:
                if '/chunks/current' in self.base:
                    old_chunk = self.base['/chunks/current']
                self.base['/chunks/current'] = chunk
            variables = {'Base': self.base, 'Params': self.params, 'Ret': []}
            try:
                variables.update(dict(locals(), **globals()))
                exec(self.lines, variables, variables)
            except Exception as e:
                if old_chunk:
                    self.base['/chunks/current'] = old_chunk
                raise KDIException('Error in execution \'' + self.key_base + '\' KDIEvalString!')
            if old_chunk:
                self.base['/chunks/current'] = old_chunk
            return variables['Ret']

    def light_dump(self) -> str:
        msg = 'KDIEvalString(... , datas='
        msg += self.kmemo.light_dump()
        msg += ')'
        return msg

    def __repr__(self) -> str:
        tobytes = self.lines.encode('utf-8')
        tobase64 = base64.b64encode(tobytes)
        tostr = tobase64.decode()
        return 'KDIEvalBase64(base=KDIBase, key_base=\'' + self.key_base + '\', lines=\'' + tostr + '\', params=' + str(self.params) + ')'

    def __sizeof__(self) -> int:
        return self.kmemo.__sizeof__()

    def serialize(self) -> str:
        if self.serializable:
            return super().serialize()
        raise KDIException('Serialize forbidden!')
