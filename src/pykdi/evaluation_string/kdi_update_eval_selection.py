# pykdi.evaluation_string
from .KDIEvalString import KDIEvalString
# pykdi
from pykdi.KDIException import KDIException
from pykdi.KDITyping import KDIDataBaseTyping

command_eval_selection = """
base = Base
chunk = base['/chunks/current']
# indexes
indexesbase = Params[0]
indexesbasechunk = None
if '/chunks/current' in indexesbase:
    indexesbasechunk = indexesbase['/chunks/current']
indexesbase['/chunks/current'] = chunk
indexeskey = Params[1]
indexes = indexesbase[indexeskey].get()
if indexesbasechunk is None:
    del indexesbase['/chunks/current']
else:
    indexesbase['/chunks/current'] = indexesbasechunk
# in values
inbase = Params[2]
inbasechunk = None
if '/chunks/current' in inbase:
    inbasechunk = inbase['/chunks/current']
inbase['/chunks/current'] = chunk
inkey = Params[3]
invalues = inbase[inkey].get()
if inbasechunk is None:
    del inbase['/chunks/current']
else:
    inbase['/chunks/current'] = inbasechunk
# compute
Ret = invalues[indexes]
"""

"""
Fr: Il n'y a pas de surcoût significatif de cette description en Python/Numpy par rapport à
    un équivalent en langage compilé.
"""

def kdi_update_eval_selection(outbase: KDIDataBaseTyping,
                              outkey: str,
                              indexesbase: KDIDataBaseTyping,
                              indexeskey: str,
                              inbase: KDIDataBaseTyping,
                              inkey: str) -> None:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet d'associer à la clef outkey de la base outbase une expression réalisant
        une selection, un filtrage des valeurs de la clef inkey de la base inbase suivant
        des indices fournis par la clef indexskey de la base indexesbase (on parle d'évaluation
        lazy, c'est à dire à la demande / réalisé que lorsque c'est nécessaire).

        :param outbase: (in/out) une base existante sur laquelle on va créer la clef outkey
        :param outkey: (in/out) la clef absolue dans la database outbase qui permet d'accéder
          au résultat de cette évaluation de type sélection
        :param indexesbase: (in) une base existante sur laquelle est défini la clef indexeskey
        :param indexeskey: (in) la clef absolue dans la database indexesbase qui correspond
          à la description de l'indexation, la sélection
        :param inbase: (in) une base existante sur laquelle est défini la clef inkey
        :param inkey: (in) la clef absolue dans la database inbase sur laquelle on va réalisera
          la sélection sur l'index défini par la clef indexeskey dans la database indexesbase
        :return: None
    """
    outbase[outkey] = KDIEvalString(outbase, outkey, command_eval_selection, [indexesbase, indexeskey, inbase, inkey])
