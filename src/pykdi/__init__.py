__version_info__ = ('0', '5', '0')
__version__ = ".".join(__version_info__)

from pykdi.dictionary import Dictionary

from pykdi.tools import (kdi_get_env_log_reset, reset_kdi_log_get_env, kdi_get_env, kdi_get_env_int,
                         kdi_get_env_str, KDILog, KDILogElapse)

from pykdi.agreement.io.vtkhdf.KVTKHDF import KVTKHDF, KVTKHDFGroup
from pykdi.agreement.io.vtkhdf.kdi_vtk_hdf_create_json import kdi_vtk_hdf_create_json
from pykdi.agreement.io.vtkhdf.KVTKHDFDatas import KVTKHDFGetFp, KVTKHDFClose, KVTKHDFDatas

from pykdi.evaluation_string.kdi_update_eval_offsets  import kdi_update_eval_offsets
from pykdi.evaluation_string.kdi_update_eval_selection import kdi_update_eval_selection
from pykdi.evaluation_string.kdi_update_eval_selection_connectivity_indexes_points import kdi_update_eval_selection_connectivity_indexes_points
from pykdi.evaluation_string.kdi_update_eval_selection_1_to_n import kdi_update_eval_selection_1_to_n
from pykdi.evaluation_string.kdi_update_eval_selection_connectivity_indexes_points_1_to_n import kdi_update_eval_selection_connectivity_indexes_points_1_to_n

from .kdi_builder import kdi_builder
from .kdi_base import kdi_base
from .kdi_update import kdi_update
from .kdi_update_fields import kdi_update_fields
from .kdi_update_sub import kdi_update_sub

from pykdi.agreement.KDIAgreementType import (KDI_AGREEMENT_STEPPART,
                                              KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART,
                                              KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_QUADRANGLE_TYPE_VALUE,
                                              KDI_SIMULATION_HEXAHEDRON_TYPE_VALUE)

from pykdi.agreement.step_part.KDIAgreementBaseStepPart import KDIAgreementBaseStepPart
from pykdi.agreement.KDIAgreementBaseConf import KDI_AGREEMENT_CONF_NB_PARTS, KDI_AGREEMENT_CONF_CODE_2_KDI
from pykdi.agreement.step_part.KDIAgreementChunkStepPart import KDIAgreementChunkStepPart

from .KDIComplexType import KDIComplexType
from .KDIChunks import KDIChunks
from .KDIChunks import KDI_DATA_CONSTANT_BY_SIMULATION, KDI_DATA_ALL_PARTS_SAME_VALUE_CONSTANT
from .KDIChunks import KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT
from .KDIDatas import KDIDatas
from pykdi.evaluation_string.KDIEvalBase64 import KDIEvalBase64
from pykdi.evaluation_string.KDIEvalScript import KDIEvalScript
from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from .KDIException import KDIException
from .KDIExpression import KDIExpression
from .KDIFields import KDIFields
from .KDIList import KDIList
from .KDIMemory import KDIMemory
from .KDISubMeshes import KDISubMeshes
from .KDIType import KDI_TRIANGLE, KDI_QUADRANGLE, KDI_POLYGON, KDI_PIT, KDI_SIZE, kdi2nbPoints

from pykdi.agreement.io.vtkhdf.KVTKType import KVTK_TRIANGLE, KVTK_QUAD

from pykdi.agreement.io.vtkhdf.KDIVTKHDFCheck import KDIVTKHDFCheck
from pykdi.agreement.io.vtkhdf.KDIWriterVTKHDF import write_vtk_hdf
from pykdi.io.json.KJSON import write_json, read_json, KDIExceptionNoWriteFileJson
from pykdi.agreement.io.vtkhdf.KDIWriterVTKHDF_Nto1 import write_vtk_hdf_n_to_1

from .KDIEvent import KDIEvent
from .KDIProxyData import KDIProxyData

from pykdi.agreement.io.vtkhdf.KVTKHDFGarbageCollector import KVTKHDFGarbageCollector

from pykdi.mutation.KDIComputeMultiMilieux import KDIComputeMultiMilieux
from pykdi.mutation.KDI1toN import KDI1toN

# doc
__doc__ = """
Please refer to the documentation provided in the README.md,
which can be found at URL: https://gitlab.com/...
"""