# pykdi
from .KDIDatas import KDIDatas
from .KDIException import KDIException
from .KDIMemory import KDIMemory
from .KDITyping import KDIDataBaseTyping

def kdi_update_fields(base: KDIDataBaseTyping,
                      field_parent_name: str,
                      field_basename: str) -> any:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet de compléter la database existante en créant un champ de valeurs sur une instance
        existante (elle peut représenter des cellules, des points ou tout autre élément géométrique).

        :param base: (in/out) la base existante qui sera complétée
        :param field_parent_name: (in) le nom de l'instance du parent, il doit être déjà défini dans la base
        :param field_basename: (in) le nom de base du champ de valeurs
        :return: l'instance de l'objet décrivant les valeurs associés à ce champ

        Testé par test_kdi_base.py
    """
    try:
        base[field_parent_name]
    except KeyError:
        raise KDIException('pre: field_parent_name `' + field_parent_name + '` must be exist!')

    field_fullname = field_parent_name + '/' + field_basename

    try:
        return base[field_fullname]
    except KeyError:
        pass

    base[field_fullname] = KDIMemory(base=base, key_base=field_fullname)

    base[field_parent_name].insert(field_fullname)

    return base[field_fullname]
