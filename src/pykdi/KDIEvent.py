import base64

from .KDIDatas import KDIDatas
from .KDIException import KDIException
from .KDIMemory import KDIMemory


class KDIEvent(KDIDatas):
    __slots__ = 'base', 'caching', 'key'

    def __init__(self, base: dict[str, any], caching: str, key: str):
        super().__init__()
        self.base = base
        self.caching = caching
        self.key = key

    def insert(self, data, chunk=None) -> None:
        raise NotImplementedError()

    def erase(self, chunk=None) -> None:
        pass

    def get(self, chunk=None) -> any:
        try:
            caching = self.base[self.caching]
        except KeyError:
            raise KDIException('pre: name caching \'' + self.caching + '\' is not key in base')

        try:
            value = caching.get(chunk)
        except AttributeError:
            raise KDIException('pre: name caching \'' + self.caching + '\' is key in base but non conform')

        try:
            return value[self.key]
        except KeyError:
            raise KDIException('pre: name key \'' + self.key + '\' is not in caching dict ' + str(
                self.base[self.caching].get(chunk).keys()))
        except (TypeError, IndexError):
            raise KDIException('pre: name caching \'' + self.caching + '\' is not dict')

    def __repr__(self) -> str:
        return 'KDIEvent(..., caching=' + self.caching + ', key=' + self.key + '\''

    def serialize(self) -> str:
        """
        Fr: Dans les faits, la serialisation pourrait être acceptée dans le cas où la base
            passé en paramètre est la même que celle qui reference la clef du cache.
        """
        raise NotImplementedError()
