import json
import os


class Dictionary:
    """La classe Dictionary.
    Cette classe gére la sémantique des objets qui est utilisé dans KDI.
    L'initialisation se fait en lisant un fichier JSON `dictionary.json` actuellement localisé
    par la variable d'environnement KDI_DICTIONARY_PATH.
    Cette sémantique peut être commune ou propre à une communauté limitant alors ce qu'un code
    peut décrire. Ainsi, le code écrivain VTK HDF n'est pas en mesure de réaliser les écritures
    associée à la sémantique SubUnstructured, il faudra au préalable convertir cette information
    à travers la méthode KDIComputeMultiMilieux.

    Concrétement, cette sémantique est décrite dans un dictionnaire/map dont la clef décrit le
    nom d'un type complexe et dont la valeur décrit un dictionnaire/map dont le clef est le nom
    d'un attribut et la valeur sont type, complexe ou non. Par exemple :
    ```python
      "Base": {
        "/agreement": "Agreement",
        "/glu": "Glu",
        "/study": "Study",
        "/chunks": "Chunks",
        "/fields": "Fields"
      }
    ```
    Le type complexe d'un attribut peut être aussi être restreint afin de ne pas inclure tous
    les attributs, ce qui donne par exemple :
    ```python
      "SubUnstructuredGrid": {
        "/cells": "Cells{/fields,/globalIndexContinue}",
    ```
    où l'attribut `/cells` du type complexe `SubUnstructuredGrid` est de type `Cells` en se
    limitant aux attributs nommés `/fields` et `/globalIndexContinue`.

    En outre, il existe deux clefs particulières.

    La clef "@SpecificType" permet de décrire les deux types particuliers que sont "Chunks",
    "Fields" et "SubMeshes".

    La clef "@SimpleType" permet de décrire tous les types simples dont la valeur est :
    - un préfixe "@" ;
    - un nom de type simple informatique : string, int et float ;
    - potentiellement une description dimensionnelle [] (non défini) ou [3] (fixé pour toute
      la simulation à 3).

    Par exemple : "CartesianCoordinates": "@float[][3]".
    """
    __slots__ = 'dictionary'

    def __init__(self):
        """Dictionary class construction method."""
        try:
            dictionary_name = os.environ['KDI_DICTIONARY_PATH']
        except KeyError:
            print('### ERROR ### KDI_DICTIONARY_PATH non define!')
            raise Exception('### ERROR ### KDI_DICTIONARY_PATH non define!')
        dictionary_file = 'dictionary.json'
        abs_dictionary_file = dictionary_name + '/' + dictionary_file
        if not os.path.isfile(abs_dictionary_file):
            raise Exception('### ERROR ### Non file \'' + dictionary_file + '\' exist in KDI_DICTIONARY_PATH (\'' + dictionary_name + '\')!')
        with open(abs_dictionary_file, 'r') as fp:
            self.dictionary = json.load(fp)

    def get(self, typename) -> dict[str, str]:
        """Complex type attributes accessor method.
        return: attributes list
        """
        try:
            return self.dictionary[typename]
        except KeyError:
            return self.dictionary["@SimpleType"][typename]

    @staticmethod
    def complex_type_parse(dtype) -> (str, list[str]):
        """Complex type parser included an optional selection of attributes.
        :param dtype: data type
        :return: the basic type and the sublist of attributes
        """
        assert (dtype[0] != '@')
        pos = dtype.find('{')
        if pos != -1 and dtype[-1] == '}':
            sel_key_complex_type = dtype[:pos]
        else:
            return dtype, None
        attribute_restriction = []
        for attr in dtype[pos + 1:-1].split(','):
            attribute_restriction.append(attr)
        return sel_key_complex_type, attribute_restriction

    @staticmethod
    def simple_type_parse(dtype) -> (str, [int]):
        """Simple type parser included dimension by multiple [] fixed or note.
        :param dtype: data type
        :param shape_dtype: shape data type
        :return: the basic type and the shape data type
        """
        assert (dtype[0] == '@')
        end = -1
        pos = dtype.find('[')
        if pos != -1:
            basic_dtype = dtype[1:pos]
            if basic_dtype not in ["int", "float", "string"]:
                raise Exception('### ERROR ### Incoherent simple type \'', dtype, '\' for ', basic_dtype,
                                'which must be prefixed by \'@int\', \'@float\' or \'@string\'!')
        else:
            basic_dtype = dtype[1:]
        shape_dtype = []
        while pos != -1:
            end = end+1+pos
            pos = dtype[end+1:].find(']')
            if pos == -1:
                raise Exception('### ERROR ### Incoherent simple type \'', dtype, '\' with \'[\' but without \']\'!')
            if pos == 0:
                shape_dtype.append(None)
            else:
                shape_dtype.append(int(dtype[end+1:end+1+pos]))
            end = end+1+pos
            pos = dtype[end+1:].find('[')
            for i in range(pos):
                assert(dtype[end+1+i] in [' ', '\t'])
        return basic_dtype, shape_dtype

    def check(self):
        """Check dictionary consistency."""
        for key_complex_type, attributes in self.dictionary.items():
            if key_complex_type == "@SimpleType":
                # type derived from string, int or float
                # with []
                for key, dtype in attributes.items():
                    assert (dtype[0] == '@')
                    pos = dtype.find('[')
                    simple_dtype = dtype
                    if pos != -1:
                        simple_dtype = dtype[:pos]
                    if simple_dtype not in ["@int", "@float", "@string"]:
                        raise Exception('### ERROR ### Incoherent dictionary \'@SimpleType\' for', simple_dtype,
                                        'not derived from \'@int\', \'@float\' or \'@string\'!')
                continue

            if key_complex_type == "@SpecificType":
                # type specific from KDI
                for attr in attributes:
                    if attr not in ["Chunks", "Fields", "SubMeshes"]:
                        raise Exception('### ERROR ### Incoherent dictionary \'@SpecificType\' for', attr)
                continue

            for key_attr, type_attr in attributes.items():
                assert (key_attr[0] == '/')

                if type_attr in self.dictionary.keys():
                    continue
                if type_attr in self.dictionary["@SimpleType"]:
                    continue
                if type_attr in self.dictionary["@SpecificType"]:
                    continue

                pos = type_attr.find('{')
                if pos != -1 and type_attr[-1] == '}':
                    sel_key_complex_type = type_attr[:pos]
                    try:
                        all_attributes = self.dictionary[sel_key_complex_type]
                    except:
                        raise Exception('### ERROR ### Incoherent dictionary for', key_attr, type_attr, 'derived type',
                                        sel_key_complex_type)
                    for attr in type_attr[pos + 1:-1].split(','):
                        if attr in all_attributes:
                            continue
                        raise Exception('### ERROR ### Incoherent dictionary for', key_attr, type_attr, 'derived type',
                                        sel_key_complex_type, ' not exist attribut', attr)
                    continue

                raise Exception('### ERROR ### Incoherent dictionary for', key_attr, type_attr)
