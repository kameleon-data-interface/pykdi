from abc import ABC, abstractmethod


class KDIDatas(ABC):
    """
    fr: La classe d'interface des différents objets qui seront instanciés
        dans le dictionnaire décrivant une base de données.
    us: The interface class of the different objects which will be
        instantiated in the dictionary describing a database.
    """

    def __init__(self):
        """
        Builder method
        """
        pass

    @abstractmethod
    def insert(self, data, chunk=None) -> None:  # Set have been view as Append (KDIList)
        """
        Insert a data value for a description of a chunk passed explicitly
        as a parameter or not (next '/chunks/current').
        Warning 'insert' has the behavior of an 'append' for the KDIList object.
        """
        raise NotImplementedError()

    @abstractmethod
    def erase(self, chunk=None) -> None:
        """
        Erase the data value for a description of a chunk passed explicitly
        as a parameter or not (next '/chunks/current').
        """
        raise NotImplementedError()

    @abstractmethod
    def get(self, chunk=None) -> any:
        """
        Get the data value for a description of a chunk passed explicitly
        as a parameter or not (next '/chunks/current').
        """
        raise NotImplementedError()

    def get_complex_type(self) -> str:
        """
        ... pnly KDIProxyData ...
        """
        raise NotImplementedError()

    def light_dump(self) -> str:
        """
        Light dump of the description of the current instance.
        In KDIMemory, for example, dump the shape of arrays and not all the values
        as it is done in serialized method used for serialization for writing JSON file.
        """
        return self.__repr__()

    @abstractmethod
    def __repr__(self) -> str:
        """
        Representation of the description of the current instance.
        In KDIMemory, for example, dump all values.
        """
        raise NotImplementedError()

    def serialize(self) -> str:
        """
        Serialize the description of the current instance in such a way that the
        evaluation of this description in the form of a character string allows
        the object to be instantiated.

        WARNING Before the evaluation, the only constraint is to have defined
                the KDIBase variable.
        """
        return self.__repr__()
