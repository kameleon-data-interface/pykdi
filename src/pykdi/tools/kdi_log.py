from mpi4py import MPI
import os


class KDILog:
    """
    This class supports different logs.

    Creating an instance requires a name that will serve as a prefix for displays
    as well as for execution control via an boolean environment variable. A default
    value is also passed as a parameter.

    The name, ie 'A_B', is a string of characters described by a succession of
    letters and numbers that may contain the character '_'.
    We will then deduce:
    - the name of the environment variable being searched will be pre-fixed with 'KDI_'
      and post-fixed with '_LOG', ie 'KDI_A_B_LOG'; using the LOG environment
      variable allows you to log the initialization value of this log.
    - logs will be prefixed at least by the name, ie 'A_B', followed by as many
      space characters as is determined by the (positive) indentation value.

    Logs are prefixed with the process rank index over the total number of processes,
    this name and additional text under certain conditions (among other things 'TODO',
    'WARNING', 'FATAL EXCEPTION'...).

    """
    __slots__ = 'm_name', 'm_state', 'm_indent'

    @staticmethod
    def kdi_log_get_env(name: str, default: bool) -> bool:
        if val := os.getenv(name):
            if val == '0':
                return False
            return True
        return default

    @staticmethod
    def kdi_log_process():
        return "#" + str(MPI.COMM_WORLD.Get_rank()) + '/' + str(MPI.COMM_WORLD.Get_size())

    def __init__(self, name: str, default: bool, indent: int = 0):
        """
        Builder instance KDILog.
        """
        assert ('pre: indent must be positive or zero' and indent >= 0)
        self.m_name = name
        self.m_indent = [indent]
        if val := os.getenv('KDI_' + name + '_LOG'):
            if val == '0':
                self.m_state = False
            else:
                self.m_state = True
        else:
            self.m_state = default
        #
        if self.kdi_log_get_env('KDI_LOG', False):
            print(self.kdi_log_process(), 'name:', 'KDI_' + name + '_LOG', self.m_state)

    @property
    def state(self):
        """
        Return the value of log state.
        """
        return self.m_state

    @state.setter
    def state(self, state):
        """
        Change the value of log state.
        """
        self.m_state = state

    def push_indent(self, indent_add: int):
        """
        Push a positive incremental value for indentation.
        """
        assert ('pre: indent_add must be positive or zero' and indent_add >= 0)
        self.m_indent.append(self.m_indent[-1] + indent_add)

    def pop_indent(self):
        """
        Pop the last incremental value for indentation.
        """
        assert (len(self.m_indent) >= 1)
        self.m_indent.pop()

    def indent(self, indent_add: int):
        """
        Returns a new instance with an incremented indentation value.
        """
        assert ('pre: indent_add must be positive or zero' and indent_add >= 0)
        return KDILog(self.m_name, self.m_state, self.m_indent[-1] + indent_add)

    def log(self, *args):
        """
        Make a log of the parameters in standard output with a standardized
        prefix according to the value of the activation state.
        """
        if self.m_state or (len(args) > 0 and args[0] in ['TODO', 'WARNING', 'FATAL EXCEPTION']):
            print(self.kdi_log_process(), self.m_name, ' ' * self.m_indent[-1], *args)

    def todo(self, *args):
        """
        Always show todo().
        """
        self.log('TODO', *args)

    def warning(self, *args):
        """
        Always show warning().
        """
        self.log('WARNING')
        self.log('WARNING', *args)
        self.log('WARNING')

    def fatal_exception_header(self):
        self.log('FATAL EXCEPTION')
        self.log('FATAL EXCEPTION')

    def fatal_exception_body(self, *args):
        self.log('FATAL EXCEPTION', *args)

    def fatal_exception_footer(self):
        self.fatal_exception_header()

    def fatal_exception(self, *args):
        """
        Always show fatal_exception().
        """
        self.fatal_exception_header()
        self.fatal_exception_body(*args)
        self.fatal_exception_footer()


kdi_log_python = KDILog("PYTHON", False)
