import time

from .kdi_log import KDILog


class KDILogElapse:

    __slots__ = 'm_do_nothing', 'm_tic_tac', 'm_kdi_log_summary', 'm_kdi_log_tic_tac'

    def __init__(self, key: str):
        self.m_tic_tac = dict()
        # tic_tac:
        #   [0] counter, increment on the tac
        #   [1] begin time after tic, last increment after tac (before tic)
        #   [2] cumul time
        self.m_kdi_log_summary = KDILog(key + '_TICTAC_SUMMARY', False)
        self.m_kdi_log_tic_tac = KDILog(key + '_TICTAC', False)
        self.m_do_nothing = not self.m_kdi_log_summary.state and not self.m_kdi_log_tic_tac.state

    def tic(self, key: str) -> None:
        if self.m_do_nothing:
            return
        if key in self.m_tic_tac:
            self.m_tic_tac[key][1] = time.time()
        else:
            self.m_tic_tac[key] = [0, time.time(), 0]
        self.m_kdi_log_tic_tac.log('TIC', key, '#' + str(self.m_tic_tac[key][0]))

    def tac(self, key: str) -> None|int:
        """
        Returns the elapsed time between tic and toc for this key, time expressed in seconds.
        """
        if self.m_do_nothing:
            return
        add = time.time() - self.m_tic_tac[key][1]
        self.m_tic_tac[key] = [self.m_tic_tac[key][0] + 1, add, self.m_tic_tac[key][2] + add]
        self.m_kdi_log_tic_tac.log('TAC', key, '#' + str(self.m_tic_tac[key][0]),
                                   str(self.m_tic_tac[key][1]), str(self.m_tic_tac[key][2]))
        return add

    def summary(self, key: str = None) -> None|int:
        """
        Returns the cumulative elapsed time between tic and toc for this key, time expressed in seconds.
        """
        if not self.m_kdi_log_summary.state:
            return
        if key:
            if key in self.m_tic_tac:
                # self.m_tic_tac[key][0] always non zero
                self.m_kdi_log_summary.log('SUMMARY \'' + key + '\'',
                                           '##' + str(self.m_tic_tac[key][0]),
                                           str(self.m_tic_tac[key][2]), 's (',
                                           str(self.m_tic_tac[key][2] / self.m_tic_tac[key][0]), 's/1)')
                return self.m_tic_tac[key][2]
            self.m_kdi_log_summary.log('SUMMARY KeyError \'' + key + '\' not in ' + str(sorted(self.m_tic_tac.keys())))
            return None

        self.m_kdi_log_summary.log('SUMMARY TIC-TAC')
        for _key, _value in self.m_tic_tac.items():
            # _value[0] always non zero
            print(self.m_kdi_log_summary.kdi_log_process(), '   \'' + _key + '\'',
                  '##' + str(_value[0]),
                  str(_value[2]), 's (',
                  str(_value[2] / _value[0]), 's/1)')
        return None
