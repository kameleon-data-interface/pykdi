import os
from .kdi_log import KDILog


kdi_log_get_env = KDILog('GET_ENV', False)


def reset_kdi_log_get_env():
    global kdi_log_get_env
    kdi_log_get_env = KDILog('GET_ENV', False)


def kdi_get_env_log_reset():
    """
    Just used in test
    """
    global kdi_log_get_env
    kdi_log_get_env = KDILog('GET_ENV', False)


def kdi_get_env(key: str, default: bool) -> bool:
    if val := os.getenv(key):
        if val == '0':
            kdi_log_get_env.log('key:', key, False)
            return False
        kdi_log_get_env.log('key:', key, True)
        return True
    kdi_log_get_env.log('key:', key, default)
    return default


def kdi_get_env_int(key: str, default: int) -> int:
    val = os.getenv(key)
    if val is not None:
        kdi_log_get_env.log('key:', key, int(val))
        return int(val)
    assert (isinstance(default, int))
    kdi_log_get_env.log('key:', key, default)
    return default


def kdi_get_env_str(key: str, default: str) -> str:
    val = os.getenv(key)
    if val is not None:
        kdi_log_get_env.log('key:', key, val)
        return val
    assert (default is None or isinstance(default, str))
    kdi_log_get_env.log('key:', key, default)
    return default
