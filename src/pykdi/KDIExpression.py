import re
from .KDIException import KDIException
from .KDIDatas import KDIDatas


class KDIExpression(KDIDatas):
    """
    Fr: Mécanisme gérant les expressions simples:
            [[number|@key@][+ - * /]]*
        number décrit un entier ou un réél
        @key@ étant une clef du dictionnaire correspondant à un objet numpy ou un entier ou un réél
    """
    __slots__ = 'base', 'expression'

    def __init__(self, base: dict[str, any], expression: str):
        super().__init__()
        self.base = base
        assert (isinstance(expression, str))
        self.expression = expression

    def insert(self, data, chunk=None) -> None:  # Set have been view as Append (KDIList)
        raise NotImplementedError()

    def erase(self, chunk=None) -> None:
        raise NotImplementedError()

    def get(self, chunk=None) -> any:
        # Extract numbers and operators from the expression string
        elements = re.findall(r'(\d+|\+|\-|\*|\/|\(|\)|@|\w+)', self.expression)

        def get_value(i):
            value = ''
            if elements[i] != '@':
                end = len(elements)
                while elements[i] not in '+-*/()':
                    value += elements[i]
                    i += 1
                    if i == end:
                        break
                if value == '':
                    return get_value(i+1)
                return float(value), i

            name = ''
            i += 1
            while elements[i] != '@':
                name += elements[i]
                i += 1
            try:
                return self.base[name].get(chunk).copy(), i + 1
            except AttributeError:
                return self.base[name].get(chunk), i + 1

        # Initialize the result to the first number
        result, i = get_value(0)

        # Apply each operator to the previous result and the current number
        while i < len(elements):
            operator = elements[i]
            if operator == ')':
                i += 1
                if i >= len(elements):
                    break
                operator = elements[i]
            num, i = get_value(i + 1)
            if operator == '+':
                result += num
            elif operator == '-':
                result -= num
            elif operator == '*':
                result *= num
        return result

    def __repr__(self):
        if self.base:
            return 'KDIExpression(base=KDIBase, expression=\'' + self.expression + '\')'
        return 'KDIExpression(expression=\'' + self.expression + '\')'

    def serialize(self) -> str:
        raise KDIException('Serialize forbidden!')
