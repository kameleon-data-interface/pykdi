from .KDIDatas import KDIDatas


class KDIComplexType(KDIDatas):
    """
    Fr: Cet objet est informatif et permet de connaître le type d'objet complexe dont il
        est une instance.
    """
    __slots__ = 'complextype'

    def __init__(self, _complextype: str):
        super().__init__()
        self.complextype = _complextype

    def insert(self, data, chunk=None) -> None:  # Set have been view as Append (KDIList)
        raise NotImplementedError()

    def erase(self, chunk=None) -> None:
        raise NotImplementedError()

    def get(self, chunk=None) -> any:
        raise NotImplementedError()

    def get_complex_type(self) -> str:
        return self.complextype

    def __repr__(self) -> str:
        """
        La description de cet
        The description via the string thus returned is equivalent to that of a serialization.
        That is to say that applying an eval on it is possible... to a certain extent.
        :return: a string representing the instance of this object
        """
        return 'KDIComplexType(\'' + self.complextype + '\')'
