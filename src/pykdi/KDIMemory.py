import numpy as np

from pykdi.tools.kdi_log import KDILog

from .KDIDatas import KDIDatas
from .KDIException import KDIException


kdi_log_memory = KDILog('MEMORY', False)


class KDIMemory(KDIDatas):
    __slots__ = 'base', 'key_base', 'datas', 'sizeof', 'variants'

    step_less_part_less = ['stepless-partless']

    @staticmethod
    def _compute_sizeof(variants, datas):
        if variants is None or len(variants) == 0:
            if datas is None:
                kdi_log_memory.log('init _compute_sizeof return 0')
                return 0
            kdi_log_memory.log('init _compute_sizeof return', datas.__sizeof__())
            return datas.__sizeof__()
        if variants == KDIMemory.step_less_part_less:
            kdi_log_memory.log('init _compute_sizeof return', datas.__sizeof__())
            return datas.__sizeof__()
        assert (isinstance(datas, dict))
        sizes = 0
        for key, val in datas.items():
            kdi_log_memory.log('init _compute_sizeof key:', key, 'val:', val)
            sizes += KDIMemory._compute_sizeof(variants[1:], val)
        return sizes

    def __init__(self, base: dict[str, any], key_base: str, variants: any = None, datas: any = None):
        """
        # TODO La création de KDIMemory devrait prendre en compte
        basic_dtype, shape_dtype = dico.simple_type_parse(real_simple_type)
        """
        kdi_log_memory.log('init key_base:', key_base, 'datas:', datas)
        super().__init__()
        self.base = base
        self.key_base = key_base
        if datas is None:
            self.datas = dict()
            self.sizeof = 0
        else:
            self.datas = datas
            self.sizeof = self._compute_sizeof(variants, datas)
        self.variants = variants

    def reset(self):
        kdi_log_memory.log('reset key_base:', self.key_base)
        self.datas = dict()
        self.sizeof = 0

    def insert(self, data, chunk=None) -> None:
        kdi_log_memory.log('insert key_base:', self.key_base)

        # True for create mode
        current_chunk = self.base['/chunks'].string_chunk(chunk, True)

        store = self.datas
        variants = []
        values = []
        for variant in self.base['/chunks'].order:
            try:
                str_current_chunk = current_chunk['#' + variant]
                try:
                    store = store[str_current_chunk]
                    values.append(str_current_chunk)
                    variants.append(variant)
                except KeyError:
                    store[str_current_chunk] = dict()
                    values.append(str_current_chunk)
                    store = store[str_current_chunk]
                    variants.append(variant)
                except IndexError:
                    kdi_log_memory.fatal_exception('insert no constant variant before:', self.variants, 'chunk:',
                                                   chunk)
                    raise KDIException('insert no constant variant before:', self.variants, 'chunk:', chunk)

            except (TypeError, KeyError):
                break

        kdi_log_memory.log('insert    self.variants:', self.variants, 'variants:', variants)

        if len(variants) == 0:
            if not (self.variants is None or self.variants == self.step_less_part_less):
                kdi_log_memory.fatal_exception('insert no constant variant before:', self.variants, 'current:', variants)
                raise KDIException('insert no constant variant before:', self.variants, 'current:', variants)
            self.variants = self.step_less_part_less
            kdi_log_memory.log('insert    variants:', self.variants)
        else:
            if not (self.variants == variants or self.variants is None):
                kdi_log_memory.fatal_exception('insert no constant variant before:', self.variants, 'current:', variants)
                raise KDIException('insert no constant variant before:', self.variants, 'current:', variants)
            self.variants = variants

        if len(values) == 0:
            kdi_log_memory.log('insert    setpless-partless')
            self.datas = data
            self.sizeof = data.__sizeof__()
            kdi_log_memory.log('insert    sizeof:', self.sizeof, 'datas:', self.datas)
            return

        kdi_log_memory.log('insert    variants:', variants, 'values:', values, 'datas:', self.datas)

        store = self.datas
        for value in values[:-1]:
            store = store[value]

        kdi_log_memory.log('insert    current store:', store)

        if values[-1] not in store:
            kdi_log_memory.fatal_exception('insert impossible internal error')

        store[values[-1]] = data
        self.sizeof += data.__sizeof__()
        kdi_log_memory.log('insert    sizeof:', self.sizeof, 'datas:', self.datas)

    def erase(self, chunk=None) -> None:
        kdi_log_memory.log('erase key_base:', self.key_base)

        # False for access mode
        current_chunk = self.base['/chunks'].string_chunk(chunk, False)

        before_dict, before_key = None, None
        store = self.datas
        variants = []
        for variant in self.base['/chunks'].order:
            try:
                str_current_chunk = current_chunk['#' + variant]
                variants.append(variant)
            except KeyError:
                break
            if isinstance(store, dict):
                if str_current_chunk in store:
                    before_dict, before_key = store, str_current_chunk
                    store = store[str_current_chunk]
                else:
                    raise KDIException('There are no stored values common or not with the \'' +
                                       self.key_base + '\' key for this chunk (' + str_current_chunk + ') !')
            else:
                break

        kdi_log_memory.log('erase    self.variants:', self.variants, 'variants:', variants)
        assert (self.variants == variants or (len(variants) == 0 and self.variants == self.step_less_part_less))

        kdi_log_memory.log('erase    variants:', variants)

        if isinstance(store, dict) and len(store) != 0:
            kdi_log_memory.log('erase    store:', store, 'is dict and not empty')
            if self.datas != store:
                kdi_log_memory.warning('erase    store:', store, 'is dict and NON STEPLESS-PARTLESS (IGNORED ERASE)')
                return
            kdi_log_memory.log('erase    is dict and STEPLESS-PARTLESS (NO ERASE)')
            return

        kdi_log_memory.log('erase    before_dict:', before_dict, 'before_key:', before_key, type(before_key))
        if before_dict is not None:
            kdi_log_memory.log('erase    ', before_dict[before_key])
            kdi_log_memory.log('erase    self.sizeof:', self.sizeof, ' -= ', before_dict[before_key].__sizeof__())
            self.sizeof -= before_dict[before_key].__sizeof__()
            kdi_log_memory.log('erase    self.sizeof:', self.sizeof)
            del before_dict[before_key]

        # TODO l'init avec un data
        if self.sizeof < 0:
            self.sizeof = 0

        kdi_log_memory.log('erase    end', self.datas)

    def get(self, chunk=None) -> any:
        current_chunk = self.base['/chunks'].string_chunk(chunk, False)

        store = self.datas
        for variant in self.base['/chunks'].order:
            try:
                str_current_chunk = current_chunk['#' + variant]
            except KeyError:
                break
            try:
                store = store[str_current_chunk]
            except IndexError:
                break
            except KeyError:
                break
        if isinstance(store, dict):
            raise KDIException(
                'There are no stored values common or not with the \'' + self.key_base + '\' key for this chunk (' + str(current_chunk) + ') (-less)!')
        return store

    def __recursive_light_dump(self, store) -> str:
        if isinstance(store, dict):
            msg = '{'
            for key, value in store.items():
                msg += '\'' + str(key) + '\':'
                msg += self.__recursive_light_dump(value)
            msg += '},'
            return msg
        return 'np.array(shape=' + str(store.shape) + '),'

    def light_dump(self) -> str:
        msg = 'KDIMemory(base=KDIBase, key_base=\''
        msg += self.key_base
        msg += '\', datas='
        msg += self.__recursive_light_dump(self.datas)
        msg += ')'
        return msg

    def __recursive(self, store) -> str:
        if isinstance(store, dict):
            msg = '{'
            for key, value in store.items():
                msg += '\'' + str(key) + '\':'
                msg += self.__recursive(value)
            msg += '},'
            return msg
        return 'np.array(' + str(np.array(store).tolist()) + '),'

    def __repr__(self) -> str:
        msg = 'KDIMemory(base=KDIBase, key_base=\''
        msg += self.key_base
        msg += '\', variants='
        msg += str(self.variants)
        msg += ', datas='
        msg += self.__recursive(self.datas)
        msg += ')'
        return msg

    def __sizeof__(self) -> int:
        return self.sizeof
