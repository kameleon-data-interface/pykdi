# pykdi.dictionary
from pykdi.dictionary import Dictionary
# pykdi
from .KDIComplexType import KDIComplexType
from .KDIChunks import KDIChunks
from .KDIException import KDIException
from .KDIFields import KDIFields
from .KDIMemory import KDIMemory
from .KDISubMeshes import KDISubMeshes
from .KDITyping import KDIDataBaseTyping, KDIDataBaseAttributeRestrictionTyping

__kdi_dictionary__ = None

def kdi_update_attribute_restriction(base: KDIDataBaseTyping,
                                     typename: str,
                                     name: str,
                                     attribute_restriction: KDIDataBaseAttributeRestrictionTyping) -> None:
    """
    Fr: Cette méthode ne doit pas être accessible via l'API publique.
        Elle permet de compléter la database existante en créant une nouvelle instance d'objet
        en donnant son type, type qui doit être défini dans le dictionnaire, et le nom que
        prendra cette instance.
        La création de cette instance inclut la création de ses attributs qui sont décrits dans
        ce même dictionnaire.
        Il est possible de restreindre cette liste d'attributs en précisant la liste des clefs
        des attributs que l'on souhaite conserver.

        :param base: (in/out) la base existante qui sera complétée
        :param typename: (in) le type de l'objet à instancier
        :param name: (in) le nom que prendra cette instance d'objet
        :param attribute_restriction: (in) si ce paramètre vaut None, on instanciera tous les attributs sinon
          on se limitera à la liste des noms des attributs décrit dans ce paramètre
        :return: None

        Testé indirectement par test_kdi_base.py
    """
    global __kdi_dictionary__
    for attr_name, attr_typename in __kdi_dictionary__.get(typename).items():
        if attribute_restriction and attr_name not in attribute_restriction:
            continue
        attr_basic_type, attr_attribute_restriction = __kdi_dictionary__.complex_type_parse(attr_typename)
        kdi_update(base, attr_basic_type, name + attr_name, True, attr_attribute_restriction)
    if name not in base:
        base[name] = KDIComplexType(typename)

def kdi_update(base: KDIDataBaseTyping,
               typename: str,
               name: str = '',
               is_attribute: bool = False,
               attribute_restriction: KDIDataBaseAttributeRestrictionTyping = None) -> KDIDataBaseTyping:
    """
    Fr: Cette méthode fait partie de l'API publique.
        Elle permet de compléter la database existante en créant une nouvelle instance d'objet
        en donnant son type, type qui doit être défini dans le dictionnaire, et le nom que
        prendra cette instance.
        Dans le cas de type complex, on peut restreindre la création des attributs en fixant
        la liste des clefs des attributs à conserver.

        :param base: (in/out) la base existante qui sera complétée
        :param typename: (in) le type de l'objet à instancier
        :param name: (in) le nom que prendra cette instance d'objet
        :param is_attribute: (in) indique si l'instanciation de cet objet se fait dans le cadre
            de la création d'un attribut ; dans ce cas, le nom comporte plusieurs '/', sinon il
            ne doit en contenir qu'un, au début. Néanmoins, il y a l'exception à la règle lors de
            l'instanciation d'un SubMeshes dont le nom comprend le nom du maillage sur lequel se
            base le sous-maillage ('/mesh_name/submesh_name')
        :param attribute_restriction: (in) si ce paramètre vaut None, on instanciera tous les
            attributs sinon on se limitera à la liste des noms des attributs décrits dans
            ce paramètre
        :return: le dictionnaire créé (uniquement lorsque le type passé est 'Base' avec un nom '')
            ou complété (meême si le paramètre base est en input/output).

        Testé par test_kdi_base.py
    """
    global __kdi_dictionary__
    if not __kdi_dictionary__:
        __kdi_dictionary__ = Dictionary()

    if not is_attribute:
        # FDr :
        if name.count('/') > 1:
            # kdi_update_sub passe par ici ce que ne fait pas kdi_update_fields
            # ceci explique pourquoi on ne vérifie que pour submeshes
            if name.split('/')[2] != 'submeshes':
                raise KDIException('An user can only instantiate an object at the root level. ' +
                                   'In fact, the name of the latter must necessarily begin with \'/\' and ' +
                                   'not contain other characters of this type.')
            assert (name[0] == '/')

    if typename == 'Base':
        # Description of the Base semantic object including:
        # - 'Glu' type '/glu'
        # - 'Study' type '/study'
        # - 'Chunks' type '/chunks', etc
        assert (base is None)
        assert (name == '')
        cbase = base
        if cbase is None:
            cbase = dict()
        kdi_update_attribute_restriction(cbase, typename, name, None)
        return cbase

    if base is None:
        raise KDIException('pre: the base must be created before defining object instances')

    if name == '':
        raise KDIException('pre: the key name cannot be the empty string.')

    # Fr: Si la clef existe dans la dictionnaire alors on ne la re-crée pas.
    #     Les attributs d'une instance d'un objet complexe sont créés avec le dit objet.
    # En: If the key exists in the dictionary then we do not re-create it.
    #     The attributes of an instance of a complex object are created with the said object.
    if name in base.keys():
        return base

    try:
        real_simple_type = __kdi_dictionary__.get(typename)  # Supported Simple and Complex Type
    except KeyError:
        if typename == 'Chunks':
            # Fr: Description de cet objet sémantique Chunks incluant en tant que KID Chunks,
            #     un objet spécifique permettant de décrire les variantes associées aux valeurs
            #     de base, généralement le pas de temps et la partie.
            # En: Description of this Chunks semantic object including as KDIChunks, a specific
            #     object for to describe the variants associated with the base values, generally
            #     timestep and part.
            if name not in base:
                base[name] = KDIChunks(base)
            return base
        if typename == 'Fields':
            # Fr: Description de cet objet sémantique Fields incluant des KDIFields en tant que
            #     KDIList spécifique.
            # En: Description of this Fields semantic object including as KDIFields as a specific
            #     KDIList.
            if name not in base:
                base[name] = KDIFields(set())
            return base
        if typename == 'SubMeshes':
            # Fr: Description de cet objet sémantique SubMeshes incluant des KDISubMeshes en tant que
            #     KDIList spécifique.
            # En: Description of this SubMeshes semantic object including as KDISubMeshes as a specific
            #     KDIList.
            if name not in base:
                base[name] = KDISubMeshes(set())
            return base
        raise KDIException('Expected Chunks, Fields or SubMeshes instead of \'' + typename + '\'!')

    try:
        assert (real_simple_type[0] == "@")  # Cas Simple Type
        base[name] = KDIMemory(base=base, key_base=name)
        return base
    except KeyError:
        pass

    kdi_update_attribute_restriction(base, typename, name, attribute_restriction)

    return base
