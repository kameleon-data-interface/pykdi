import numpy as np

from pykdi.agreement.step_part.kdi_agreement_steppart import KDI_AGREEMENT_STEPPART_VARIANT_PART

from pykdi.evaluation_string.KDIEvalString import KDIEvalString
from pykdi.evaluation_string.kdi_update_eval_offsets import kdi_update_eval_offsets
from pykdi.evaluation_string.kdi_update_eval_selection_1_to_n import kdi_update_eval_selection_1_to_n
from pykdi.evaluation_string.kdi_update_eval_selection_connectivity_indexes_points_1_to_n import kdi_update_eval_selection_connectivity_indexes_points_1_to_n

from pykdi.kdi_update_sub import kdi_update_sub

from pykdi.KDITyping import KDIDataBaseTyping
from pykdi.KDIChunks import KDI_CONSTANT_VARIANT
from pykdi.KDIDatas import KDIDatas
from pykdi.KDIFields import KDIFields
from pykdi.KDIMemory import KDIMemory
from pykdi.KDIProxyData import KDIProxyData

from .KDIMutationChunks import KDIMutationChunks

def KDI1toN(base_1: KDIDataBaseTyping,
            nb_parts: int=1,
            partition_key: str=None) -> KDIDataBaseTyping:
    """

      :param base_1: (in) la base en entrée partless (ou nb_parts=1)
      :param nb_parts: (in) nombre de partitions en sortie
      :param partition_key: (in, optionnel) le nom du champ de valeurs
        sur les cellules qui servira pour la partition. A défaut, c'est
        le globalIndexContinuous qui est défini sur les cellules.
    """
    assert ('pre: base_1 must be \'' + KDI_AGREEMENT_STEPPART_VARIANT_PART + '\' constant and equal 1' and
            base_1['/chunks'].variants[KDI_AGREEMENT_STEPPART_VARIANT_PART]['data'] == 1 and
            base_1['/chunks'].variants[KDI_AGREEMENT_STEPPART_VARIANT_PART]['type'] == KDI_CONSTANT_VARIANT)

    if nb_parts == 1:
        return base_1

    if partition_key is None:
        fields = '/globalIndexContinuous'
    else:
        fields = '/fields/' + partition_key

    # It's multi-mil?
    key_meshes = set()
    key_parent_meshes = []
    is_assembly = False
    if '/assembly' in base_1:
        assembly = base_1['/assembly']
        assert (len(assembly) == 1)  # un seul maillage avec sous-maillage
        for key, value in assembly.items():
            # key: le nom du maillage parent (sans le préfixe '/')
            # value: un set de nom de sous-maillage (sans le préfixe ?!?)
            if isinstance(value, set):
                for key_mesh in value:
                    key_meshes.add(key_mesh)
            key_parent_meshes.append('/' + key)
        is_assembly = True
    else:
        for key, item in base_1.items():
            try:
                if item.get_complex_type() == 'UnstructuredGrid':
                    key_meshes.add(key)
                    key_parent_meshes.append(key)
            except:
                continue
    key_meshes = sorted(key_meshes)
    print('sorted key_meshes', key_meshes)

    ignored = []
    base_N = dict()

    base_N['/chunks'] = KDIMutationChunks(base_N, base_1['/chunks'], {KDI_AGREEMENT_STEPPART_VARIANT_PART: {'data': nb_parts} } )

    base_N['/chunks/current'] = base_1['/chunks/current']

    for key, val in base_1.items():
        if key == '/chunks':
            continue
        if key == '/chunks/current':
            ignored.append('/chunks/current')
            continue
        if key == '/KVTKHDF/types':
            # TODO extend name /KVTKHDF/mymesh/types
            ignored.append('/KVTKHDF/types')
            continue
        if key in ['',
                   '/agreement',
                   '/code2kdi', '/code2nbPoints',
                   '/fields',
                   '/glu', '/glu/version', '/glu/name',
                   '/study', '/study/date', '/study/name',
                   '/vtkhdf', '/vtkhdf/version', '/vtkhdf/force_common_data_offsets',]:

            if isinstance(base_1[key], KDIDatas):
                base_N[key] = KDIProxyData(base_N, base_1, key, {'part': 0})
                continue

            base_N[key] = base_1[key]
            continue
        if key in base_1['/fields'].get():
            base_N[key] = KDIProxyData(base_N, base_1, key)
            continue

        # - mono-mesh
        # - multi-mesh avec un assembly
        # - QUID mono-mesh et submeshes ?
        if key in key_meshes:
            print('Mesh', key, 'in progress')

            try:
                base_1[key + '/cells' + fields]
                print(key + '/cells' + fields)
                assert ('A faire' and False)
                # - si partition_key existe alors la valeur décrite l'index de position de chaque cellule
                #   il est alors nécessaire de construire le tableau inverse qui nous donne la position de la
                #   ième cellule.
                cells_indexes = None
            except KeyError:
                try:
                    base_1[key + '/cells/fields/' + fields]
                    print(key + '/cells/fields' + fields)
                    assert ('A faire' and False)
                    # - si partition_key existe alors la valeur décrite l'index de position de chaque cellule
                    #   il est alors nécessaire de construire le tableau inverse qui nous donne la position de la
                    #   ième cellule.
                    cells_indexes = None
                except KeyError:
                    command = """
part_rank = Base['/chunks/current']['part'] 
part_number = Base['/chunks'].variants['part']['data']
inBase = Params[0]
data = inBase[Params[1]].get()
nb = data.shape[0]//part_number
reste = data.shape[0]%part_number
if part_rank < reste:
   first_in_rank = part_rank * (nb + 1)
   nb_in_rank = nb + 1
else:
   first_in_rank = reste * (nb + 1) + (part_rank - reste) * nb
   nb_in_rank = nb
Ret = np.arange(first_in_rank, first_in_rank+nb_in_rank)
                    """
                    cells_indexes = key + '/cells/globalIndexContinuous'
                    base_N[cells_indexes] = KDIEvalString(
                        base_N, cells_indexes,
                        command, [base_1, key + '/cells/types'])

                base_N[key] = KDIProxyData(base_N, base_1, key)

                try:
                    base_1[key + '/cells/offsets']
                    kdi_update_eval_offsets(base_N, key + '/cells')
                except:
                    pass

                base_N[key + '/fields'] = base_1[key + '/fields']
                for field in base_1[key + '/fields'].get():
                    base_N[field] = KDIProxyData(base_N, base_1, field)

                # ('Cells', '/cells', ),
                base_N[key + '/cells'] = KDIProxyData(base_N, base_1, key + '/cells')
                #     ('Types', '/types',),
                #TODO A REVOIR
                #ON UTILISE CHUNK step-part pour base_N
                #ON UTILISE CHUNK step-partless pour base_1
                kdi_update_eval_selection_1_to_n(
                    base_N, key + '/cells/types',
                    base_N, cells_indexes,
                    base_1, key + '/cells/types')
                #     ('Connectivity', '/connectivity',),
                points_indexes = key + '/points/globalIndexContinuous'
                kdi_update_eval_selection_connectivity_indexes_points_1_to_n(
                    base_N, key + '/cells/connectivity',
                    points_indexes,
                    base_N, cells_indexes,
                    base_1, key + '/cells/types', key + '/cells/connectivity')

                # key + '/cells/offsets'

                # Cause insert new field : PartId
                base_N[key + '/cells/fields'] = KDIFields(base_1[key + '/cells/fields'].get())

                for field in base_N[key + '/cells/fields'].get():
                    kdi_update_eval_selection_1_to_n(
                        base_N, field,
                        base_N, cells_indexes,
                        base_1, field)

                command = """
part_rank = Base['/chunks/current']['part']
data = Base[Params[0]].get()
Ret = np.full(data.shape[0], part_rank)
                """
                base_N[key + '/cells/fields/PartId'] = KDIEvalString(
                    base_N, key + '/cells/fields/PartId',
                    command, [key + '/cells/globalIndexContinuous'])
                base_N[key + '/cells/fields'].insert(key + '/cells/fields/PartId')

                # # ('Points', '/points', ),
                base_N[key + '/points'] = base_1[key + '/points']
                #     ('CartesianCoordinates', '/cartesianCoordinates',),
                kdi_update_eval_selection_1_to_n(base_N, key + '/points/cartesianCoordinates',
                                                 base_N, points_indexes,
                                                 base_1, key + '/points/cartesianCoordinates')

                #     ('Fields', '/fields',),
                base_N[key + '/points/fields'] = base_1[key + '/points/fields']
                for field in base_N[key + '/points/fields'].get():
                    kdi_update_eval_selection_1_to_n(
                        base_N, field,
                        base_N, points_indexes,
                        base_1, field)

                #--------------------- SUBMESHES
                if key + '/submeshes' not in base_1:
                    continue

                base_N[key + '/submeshes'] = KDIProxyData(base_N, base_1, key + '/submeshes')
                for sub_mesh_key in base_N[key + '/submeshes'].get():
                    base_N[sub_mesh_key] = KDIProxyData(base_N, base_1, sub_mesh_key)

                    base_N[sub_mesh_key + '/fields'] = KDIFields(base_1[sub_mesh_key + '/fields'].get())
                    for field in base_N[sub_mesh_key + '/fields'].get():
                        base_N[field] = KDIProxyData(base_N, base_1, field)

                    base_N[sub_mesh_key + '/cells'] = base_1[sub_mesh_key + '/cells']

                    base_N[sub_mesh_key + '/cells/fields'] = KDIFields(base_1[sub_mesh_key + '/cells/fields'].get())

                    # version ou le materiau est distribue comme le maillage principal
                    # TODO quitter ce mode pour avoir une distribution propre par matériau : nécessite un nouveau
                    #  1toN qui inclut en quelque sorte le CMM (plus la peine d'appeler le CMM ensuite) puis on
                    #  appelle le writer actuel VTK HDF

                    cells_indexes = '/cache' + sub_mesh_key + '/indexescells'

                    # with chunk 1
                    command = """
inBase = Params[0]
inIndexescells = inBase[Params[1]].get({'step': Base['/chunks/current']['step'], 'part': 0})
outBase = Params[2]
outIndexespart = outBase[Params[3]].get(Base['/chunks/current'])
Ret = np.where(np.in1d(inIndexescells, outIndexespart))[0]
                    """

                    base_N[cells_indexes] = KDIEvalString(
                        base_N, cells_indexes,
                        command, [base_1, sub_mesh_key + '/indexescells',
                                  base_N, key + '/cells/globalIndexContinuous',
                                  ])

                    kdi_update_eval_selection_1_to_n(
                        base_N, sub_mesh_key + '/cells/globalIndexContinuous',
                        base_N, cells_indexes,
                        base_1, sub_mesh_key + '/indexescells')

                    # with chunk N
                    command = """
inBase = Params[0]
inIndexescells = inBase[Params[1]].get()
outBase = Params[2]
outIndexespart = outBase[Params[3]].get(Base['/chunks/current'])
Ret = np.where(np.in1d(inIndexescells, outIndexespart))[0]
                    """
                    base_N[sub_mesh_key + '/indexescells'] = KDIEvalString(
                        base_N, sub_mesh_key + '/indexescells',
                        command, [base_N, key + '/cells/globalIndexContinuous',
                                  base_N, sub_mesh_key + '/cells/globalIndexContinuous',])

                    for field in base_N[sub_mesh_key + '/cells/fields'].get():
                        kdi_update_eval_selection_1_to_n(
                            base_N, field,
                            base_N, cells_indexes,
                            base_1, field)

            print('Mesh', key, 'terminated')
            continue

    number_of_forget_key = 0
    for key in base_1.keys():
        if key not in base_N:
            if key in ignored:
                continue
            print('  FORGET', key, base_1[key])
            number_of_forget_key += 1
    if number_of_forget_key != 0:
        print('EXIST FORGET KEY',number_of_forget_key)

    if partition_key is None:
        base_N['/before_write'] = (base_1, 'KDI1toN(KDIBase, nb_parts=' + str(nb_parts) + ')')
    else:
        base_N['/before_write'] = (base_1, 'KDI1toN(KDIBase, nb_parts=' + str(nb_parts) + ', partition_key=\'' + partition_key + '\')')

    return base_N
