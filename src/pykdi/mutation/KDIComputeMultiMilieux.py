# pykdi.evaluation_string
from pykdi.evaluation_string.kdi_update_eval_offsets import kdi_update_eval_offsets
from pykdi.evaluation_string.kdi_update_eval_selection import kdi_update_eval_selection
from pykdi.evaluation_string.kdi_update_eval_selection_connectivity_indexes_points import kdi_update_eval_selection_connectivity_indexes_points
# pykdi
from pykdi.kdi_update import kdi_update
from pykdi.KDIComplexType import KDIComplexType
from pykdi.KDIException import KDIException
from pykdi.KDIMemory import KDIMemory
from pykdi.KDIProxyData import KDIProxyData
from pykdi.KDIFields import KDIFields
from pykdi.KDITyping import KDIDataBaseTyping

# TODO Ajouter l'option pour avoir le maillage "global" comme un sous maillage, le nom du maillage est alors 'global'
# TODO Ajouter l'option de pouvoir ou non projete tout ou partie des champs du mailalge global... ils seront préfixés par 'glob_'

def KDIComputeMultiMilieux(base: KDIDataBaseTyping,
                           add_origin_mesh: bool=True,
                           add_fields_origin: bool=True,
                           add_prefixe_fields_origin: str='glob_') -> KDIDataBaseTyping:
    """

    """
    # -------------------------
    # Realise une copie... mais en modifiant la reference à base afin que le positionnement du
    # chunk soit associé à cette version de la base
    new_base = {}

    # Ignored keys
    for key, item in base.items():
        if key not in ['/chunks', '/chunks/current', '/code2kdi', '/code2nbPoints']:
            new_base[key] = KDIProxyData(new_base, base, key)

    # Specific treatement keys
    kdi_update(new_base, 'Chunks', '/chunks', True)
    new_base['/chunks'].reset(base['/chunks'])
    try:
        new_base['/chunks/current'] = base['/chunks/current']
    except:
        pass
    new_base['/code2kdi'] = base['/code2kdi']
    new_base['/code2nbPoints'] = base['/code2nbPoints']

    # The key of the assembly varible of dictionary type is the name of a mesh and the value associated
    # with it is a set of sub-mesh name (mesh by selection) which can include the name of the original
    # mesh (add_origin_mesh option).
    assembly = dict()

    # -------------------------
    # Identification des SubUnstructuredGrid
    for key, item in base.items():  # on base and not new_base cause KDIProxyData
        try:
            if item.get_complex_type() == 'UnstructuredGrid':
                if key[1:] not in assembly:
                    assembly[key[1:]] = set()
                if add_origin_mesh:
                    assembly[key[1:]].add(key)

                newkdifields = KDIFields(set())
                for field in base[key + '/cells/fields'].get():
                    listfields = field.split('/')
                    relatif_path = '/' + '/'.join(listfields[2:-1])
                    del new_base[field]
                    globkey = key + relatif_path + '/glob_' + listfields[-1]
                    new_base[globkey] = KDIProxyData(new_base, base, field)
                    newkdifields.insert(globkey)
                new_base[key + '/cells/fields'] = newkdifields

                newkdifields = KDIFields(set())
                for field in base[key + '/points/fields'].get():
                    listfields = field.split('/')
                    relatif_path = '/' + '/'.join(listfields[2:-1])
                    del new_base[field]
                    globkey = key + relatif_path + '/glob_' + listfields[-1]
                    new_base[globkey] = KDIProxyData(new_base, base, field)
                    newkdifields.insert(globkey)
                new_base[key + '/points/fields'] = newkdifields

                # It's modern description
                try:
                    sub_meshes = base[key + '/submeshes'].get()
                except:
                    continue

                for in_key_sub in sub_meshes:
                    item = base[in_key_sub]

                    if item.get_complex_type() != 'SubUnstructuredGrid':
                        raise KDIException('Non coherence base .../submeshes contains keys of type SubUnstructuredGrid')

                    list_out_key_sub = in_key_sub.split('/')
                    del list_out_key_sub[1:3]
                    out_key_sub = '/'.join(list_out_key_sub)

                    try:
                        new_base[in_key_sub + '/indexescells']
                    except:
                        raise KDIException('Attribut missing \'indexescells\' in SubUnstructuredGrid \'' + in_key_sub + '\'!')
                    del new_base[in_key_sub + '/indexescells']

                    new_base[out_key_sub] = KDIComplexType('UnstructuredGrid')
                    # ('Cells', '/cells', ),
                    new_base[out_key_sub + '/cells'] = KDIComplexType('Cells')
                    #     ('Types', '/types',),
                    kdi_update_eval_selection(
                        new_base, out_key_sub + '/cells/types',
                        base, in_key_sub + '/indexescells',
                        base, key + '/cells/types')
                    #     ('Connectivity', '/connectivity',),
                    kdi_update_eval_selection_connectivity_indexes_points(
                        new_base, out_key_sub + '/cells/connectivity',
                        '/caching' + out_key_sub + '/indexespoints', base,
                        in_key_sub + '/indexescells', base,
                        key + '/cells/types', key + '/cells/connectivity')
                    #     ('GlobalIndexContinuous', '/globalIndexContinuous',),
                    kdi_update_eval_selection(
                        new_base, out_key_sub + '/cells/globalIndexContinuous',
                        base, in_key_sub + '/indexescells',
                        base, key + '/cells/globalIndexContinuous')
                    #     ('Fields', '/fields',),
                    new_base[out_key_sub + '/cells/fields'] = KDIFields(())
                    try:
                        list_in_key_sub_field = base[in_key_sub + '/cells/fields'].get()
                    except:
                        list_in_key_sub_field = []
                    for in_key_sub_field in list_in_key_sub_field:
                        list_out_key_sub_field = in_key_sub_field.split('/')
                        del list_out_key_sub_field[1:3]
                        out_key_sub_field = '/'.join(list_out_key_sub_field)
                        new_base[out_key_sub_field] = new_base[in_key_sub_field]
                        del new_base[in_key_sub_field]
                        new_base[out_key_sub + '/cells/fields'].insert(out_key_sub_field)

                    if add_fields_origin:
                        for field in base[key + '/cells/fields'].get():
                            listfields = field.split('/')
                            relatif_path = '/' + '/'.join(listfields[2:-1])
                            kdi_update_eval_selection(new_base, out_key_sub + relatif_path + '/' + add_prefixe_fields_origin + listfields[-1], base,
                                                      in_key_sub + '/indexescells',
                                                      base,
                                                      key + relatif_path + '/' + listfields[-1])
                            new_base[out_key_sub + '/cells/fields'].insert(out_key_sub + relatif_path + '/glob_' + listfields[-1])
                    # ('Points', '/points',),
                    new_base[out_key_sub + '/points'] = KDIComplexType('Points')
                    #     ('CartesianCoordinates', '/cartesianCoordinates',),
                    kdi_update_eval_selection(new_base, out_key_sub + '/points/cartesianCoordinates', new_base,
                                              '/caching' + out_key_sub + '/indexespoints',
                                              base, key + '/points/cartesianCoordinates')
                    #     ('GlobalIndexContinuous', '/globalIndexContinuous',),
                    # TODO NECESSITE UN CALCUL PLUS COMPLEXE EN COMPACTANT LA SELECTION
                    kdi_update_eval_selection(new_base, out_key_sub + '/points/globalIndexContinuous', new_base,
                                              '/caching' + out_key_sub + '/indexespoints',
                                              base, key + '/points/globalIndexContinuous')
                    #     ('Fields', '/fields',),
                    new_base[out_key_sub + '/points/fields'] = KDIFields(())
                    try:
                        list_in_key_sub_field = base[in_key_sub + '/points/fields'].get()
                    except:
                        list_in_key_sub_field = []
                    for in_key_sub_field in list_in_key_sub_field:
                        list_out_key_sub_field = in_key_sub_field.split('/')
                        del list_out_key_sub_field[1:3]
                        out_key_sub_field = '/'.join(list_out_key_sub_field)
                        new_base[out_key_sub_field] = new_base[in_key_sub_field]
                        del new_base[in_key_sub_field]
                        new_base[out_key_sub + '/points/fields'].insert(out_key_sub_field)

                    if add_fields_origin:
                        for field in base[key + '/points/fields'].get():
                            listfields = field.split('/')
                            relatif_path = '/' + '/'.join(listfields[2:-1])
                            kdi_update_eval_selection(new_base, out_key_sub + relatif_path + '/' + add_prefixe_fields_origin + listfields[-1],
                                                      new_base, '/caching' + out_key_sub + '/indexespoints',
                                                      base,
                                                      key + relatif_path + '/' + listfields[-1])
                            new_base[out_key_sub + '/points/fields'].insert(out_key_sub + relatif_path + '/glob_' + listfields[-1])

                    kdi_update_eval_offsets(new_base, out_key_sub + '/cells')

                    new_base[out_key_sub + '/fields'] = new_base[in_key_sub + '/fields']

                    del new_base[in_key_sub + '/cells']
                    try:
                        del new_base[in_key_sub + '/cells/globalIndexContinuous']
                    except:
                        pass
                    del new_base[in_key_sub + '/cells/fields']
                    del new_base[in_key_sub + '/fields']
                    del new_base[in_key_sub]

                    assembly[key[1:]].add(out_key_sub)

                del new_base[key + '/submeshes']
                continue

        except:
            pass

    for key, value in assembly.items():
        if len(value) == 0:
            value.add('/' + key)

    new_base['/assembly'] = assembly

    # The presence of the before_write key indicates that the current description is not JSONizable.
    # We then indicate the previous description, potentially JSONizable as well as the associated recipe
    # (the method with parameter to apply on parent description).
    new_base['/before_write'] = \
        (base, 'KDIComputeMultiMilieux(KDIBase, add_origin_mesh=' + str(add_origin_mesh) +
         ', add_fields_origin=' + str(add_fields_origin) +
         ', add_prefixe_fields_origin=\'' + add_prefixe_fields_origin + '\')')

    return new_base
