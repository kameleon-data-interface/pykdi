from pykdi.KDIChunks import KDIChunks
from pykdi.KDITyping import KDIDataBaseTyping
from pykdi.KDIChunks import KDI_CONSTANT_VARIANT

class KDIMutationChunks(KDIChunks):
    def __init__(self, base: KDIDataBaseTyping, chunks: KDIChunks, chg_variant: dict[str, any]):
        """
        Initializes an instance of KDIChunks.
        The user will not call this method directly which will be called:
        - by kdi_update when creating the database than with the parameter `base`; or
        - by loadJSON when deserialize an instance of this object necessary to pass all these parameters.
        :param base: the current base
        :param order: only via loadJSON, an ordered list of variant names
        :param chg_variant: only via loadJSON, a dictionary describing the specificities of the variant

             Fr: Cette méthode permet de muter la valeur d'un variant constant en une autre.
                i.e. variant = {'part': {'data': 3}, }
        """
        assert ('pre: variant contain one key' and len(chg_variant) == 1)
        for key, val in chg_variant.items():
            assert ('pre: variant key must to be in order' and key in chunks.order)
            assert ('pre: variant key must to be a KDI_CONSTANT_VARIANT' and chunks.variants[key][
                'type'] == KDI_CONSTANT_VARIANT)
            assert ('pre: variant key val contain one key' and len(val) == 1)
            for key2 in val.keys():
                assert ('pre: variant key val key must to be \'data\'' and key2 == 'data')

        variants = dict()
        for variant_key, variant_value in chunks.variants.items():
            if variant_key not in chg_variant:
                variants[variant_key] = chunks.variants[variant_key]
                continue
            variants[variant_key] = {'type': KDI_CONSTANT_VARIANT, 'data': chg_variant[variant_key]['data']}

        super().__init__(base, chunks.order, variants)
