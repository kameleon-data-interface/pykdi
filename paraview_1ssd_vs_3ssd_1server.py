# state file generated using paraview version 5.13.2-1031-g4134b75421
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 13

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.Set(
    ViewSize=[409, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.Set(
    ViewSize=[348, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.Set(
    ViewSize=[409, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.Set(
    ViewSize=[348, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView5 = CreateView('RenderView')
renderView5.Set(
    ViewSize=[346, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView6 = CreateView('RenderView')
renderView6.Set(
    ViewSize=[346, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView7 = CreateView('RenderView')
renderView7.Set(
    ViewSize=[346, 480],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

# Create a new 'Render View'
renderView8 = CreateView('RenderView')
renderView8.Set(
    ViewSize=[346, 479],
    InteractionMode='2D',
    AxesGrid='Grid Axes 3D Actor',
    OrientationAxesVisibility=0,
    CenterOfRotation=[1.0, 0.0, 0.0],
    StereoType='Crystal Eyes',
    CameraPosition=[1.0, 0.0, 8.639503235220042],
    CameraFocalPoint=[1.0, 0.0, 0.0],
    CameraFocalDisk=1.0,
    CameraParallelScale=2.236067977499789,
    LegendGrid='Legend Grid Actor',
    PolarGrid='Polar Grid Actor',
    BackEnd='OSPRay raycaster',
    OSPRayMaterialLibrary=materialLibrary1,
)

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.SplitHorizontal(0, 0.280630)
layout1.SplitVertical(1, 0.500000)
layout1.AssignView(3, renderView1)
layout1.AssignView(4, renderView3)
layout1.SplitVertical(2, 0.500000)
layout1.SplitHorizontal(5, 0.333333)
layout1.AssignView(11, renderView2)
layout1.SplitHorizontal(12, 0.500000)
layout1.AssignView(25, renderView5)
layout1.AssignView(26, renderView7)
layout1.SplitHorizontal(6, 0.333333)
layout1.AssignView(13, renderView4)
layout1.SplitHorizontal(14, 0.500000)
layout1.AssignView(29, renderView6)
layout1.AssignView(30, renderView8)
layout1.SetSize(1452, 960)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView4)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Annotate Time'
a3partitionssansMergeParts = AnnotateTime(registrationName='3 partitions (sans Merge Parts)')
a3partitionssansMergeParts.Set(
    Format='3 partitions (sans Merge Parts)',
)

# create a new 'Annotate Time'
annotateTime1 = AnnotateTime(registrationName='AnnotateTime1')
annotateTime1.Set(
    Format='3 partitions (sans Merge Parts) + Merge Blocks',
)

# create a new 'VTKHDF Reader'
a3pe_1uns_3ssd_10tps__onevtkhdf = VTKHDFReader(registrationName='3pe_1uns_3ssd_10tps__one.vtkhdf', FileName=['/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/tests/EXPECTED/3pe_1uns_3ssd_10tps__one.vtkhdf'])
a3pe_1uns_3ssd_10tps__onevtkhdf.Set(
    CellArrayStatus=['myFieldCell', 'OriginalPartId', 'myFieldCell2'],
    PointArrayStatus=['myFieldPoint', 'OriginalPartId'],
)

# create a new 'VTKHDF Reader'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf = VTKHDFReader(registrationName='1pe_1uns_3ssd_10tps__hard__GIC.vtkhdf', FileName=['/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/tests/EXPECTED/1pe_1uns_3ssd_10tps__hard__GIC.vtkhdf'])
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf.Set(
    CellArrayStatus=['myFieldCell', 'myFieldCell2'],
    PointArrayStatus=['myFieldPoint'],
)

# create a new 'Cell Data to Point Data'
cellDatatoPointData1 = CellDatatoPointData(registrationName='CellDatatoPointData1', Input=a3pe_1uns_3ssd_10tps__onevtkhdf)
cellDatatoPointData1.Set(
    CellDataArraytoprocess=['OriginalPartId', 'myFieldCell', 'myFieldCell2'],
)

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=cellDatatoPointData1)
contour1.Set(
    ContourBy=['POINTS', 'myFieldCell'],
    Isosurfaces=[60.0],
    PointMergeMethod='Uniform Binning',
)

# create a new 'Cell Data to Point Data'
cellDatatoPointData2 = CellDatatoPointData(registrationName='CellDatatoPointData2', Input=a1pe_1uns_3ssd_10tps__hard__GICvtkhdf)
cellDatatoPointData2.Set(
    CellDataArraytoprocess=['myFieldCell', 'myFieldCell2'],
)

# create a new 'Contour'
contour2 = Contour(registrationName='Contour2', Input=cellDatatoPointData2)
contour2.Set(
    ContourBy=['POINTS', 'myFieldCell'],
    Isosurfaces=[60.0],
    PointMergeMethod='Uniform Binning',
)

# create a new 'VTKHDF Reader'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1 = VTKHDFReader(registrationName='1pe_1uns_3ssd_10tps__hard__GIC.vtkhdf', FileName=['/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/tests/EXPECTED/1pe_1uns_3ssd_10tps__hard__GIC.vtkhdf'])
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1.Set(
    CellArrayStatus=['myFieldCell', 'myFieldCell2'],
    PointArrayStatus=['myFieldPoint'],
    MergeParts=0,
)

# create a new 'Cell Data to Point Data'
cellDatatoPointData3 = CellDatatoPointData(registrationName='CellDatatoPointData3', Input=a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1)
cellDatatoPointData3.Set(
    CellDataArraytoprocess=['myFieldCell', 'myFieldCell2'],
)

# create a new 'Contour'
contour3 = Contour(registrationName='Contour3', Input=cellDatatoPointData3)
contour3.Set(
    ContourBy=['POINTS', 'myFieldCell'],
    Isosurfaces=[60.0],
    PointMergeMethod='Uniform Binning',
)

# create a new 'Merge Blocks'
mergeBlocks1 = MergeBlocks(registrationName='MergeBlocks1', Input=a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1)

# create a new 'Cell Data to Point Data'
cellDatatoPointData4 = CellDatatoPointData(registrationName='CellDatatoPointData4', Input=mergeBlocks1)
cellDatatoPointData4.Set(
    CellDataArraytoprocess=['myFieldCell', 'myFieldCell2'],
)

# create a new 'Contour'
contour4 = Contour(registrationName='Contour4', Input=cellDatatoPointData4)
contour4.Set(
    ContourBy=['POINTS', 'myFieldCell'],
    Isosurfaces=[60.0],
    PointMergeMethod='Uniform Binning',
)

# create a new 'Annotate Time'
a1partition = AnnotateTime(registrationName='1 partition')
a1partition.Set(
    Format='1 partition',
)

# create a new 'Annotate Time'
a3partitionsMergeParts = AnnotateTime(registrationName='3 partitions (Merge Parts)')
a3partitionsMergeParts.Set(
    Format='3 partitions (Merge Parts)',
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from a3pe_1uns_3ssd_10tps__onevtkhdf
a3pe_1uns_3ssd_10tps__onevtkhdfDisplay = Show(a3pe_1uns_3ssd_10tps__onevtkhdf, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'myFieldCell'
myFieldCellTF2D = GetTransferFunction2D('myFieldCell')

# get color transfer function/color map for 'myFieldCell'
myFieldCellLUT = GetColorTransferFunction('myFieldCell')
myFieldCellLUT.Set(
    TransferFunction2D=myFieldCellTF2D,
    RGBPoints=[42.0, 0.0564, 0.0564, 0.47, 50.064824, 0.243, 0.46035, 0.81, 56.029077, 0.356814, 0.745025, 0.954368, 62.310063, 0.6882, 0.93, 0.91791, 65.5, 0.899496, 0.944646, 0.768657, 69.64662200000001, 0.957108, 0.833819, 0.508916, 75.188627, 0.927521, 0.621439, 0.315357, 81.83908, 0.8, 0.352, 0.16, 89.0, 0.59, 0.0767, 0.119475],
    ScalarRangeInitialized=1.0,
)

# get opacity transfer function/opacity map for 'myFieldCell'
myFieldCellPWF = GetOpacityTransferFunction('myFieldCell')
myFieldCellPWF.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
    ScalarRangeInitialized=1,
)

# trace defaults for the display properties.
a3pe_1uns_3ssd_10tps__onevtkhdfDisplay.Set(
    Representation='Surface',
    ColorArrayName=['CELLS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a3pe_1uns_3ssd_10tps__onevtkhdfDisplay.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a3pe_1uns_3ssd_10tps__onevtkhdfDisplay.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# show data from a1partition
a1partitionDisplay = Show(a1partition, renderView1, 'TextSourceRepresentation')

# ----------------------------------------------------------------
# setup the visualization in view 'renderView2'
# ----------------------------------------------------------------

# show data from a1pe_1uns_3ssd_10tps__hard__GICvtkhdf
a1pe_1uns_3ssd_10tps__hard__GICvtkhdfDisplay = Show(a1pe_1uns_3ssd_10tps__hard__GICvtkhdf, renderView2, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a1pe_1uns_3ssd_10tps__hard__GICvtkhdfDisplay.Set(
    Representation='Surface',
    ColorArrayName=['CELLS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldPoint'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdfDisplay.ScaleTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdfDisplay.OpacityTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# show data from a3partitionsMergeParts
a3partitionsMergePartsDisplay = Show(a3partitionsMergeParts, renderView2, 'TextSourceRepresentation')

# ----------------------------------------------------------------
# setup the visualization in view 'renderView3'
# ----------------------------------------------------------------

# show data from contour1
contour1Display = Show(contour1, renderView3, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.2,
    SelectScaleArray='myFieldCell',
    GlyphType='Arrow',
    GlyphTableIndexArray='myFieldCell',
    GaussianRadius=0.01,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# show data from cellDatatoPointData1
cellDatatoPointData1Display = Show(cellDatatoPointData1, renderView3, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cellDatatoPointData1Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='OriginalPartId',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'OriginalPartId'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'OriginalPartId'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'OriginalPartId'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
cellDatatoPointData1Display.ScaleTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
cellDatatoPointData1Display.OpacityTransferFunction.Set(
    Points=[0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView4'
# ----------------------------------------------------------------

# show data from cellDatatoPointData2
cellDatatoPointData2Display = Show(cellDatatoPointData2, renderView4, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cellDatatoPointData2Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldCell'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
cellDatatoPointData2Display.ScaleTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
cellDatatoPointData2Display.OpacityTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# show data from contour2
contour2Display = Show(contour2, renderView4, 'GeometryRepresentation')

# trace defaults for the display properties.
contour2Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.16666666666666669,
    SelectScaleArray='myFieldCell',
    GlyphType='Arrow',
    GlyphTableIndexArray='myFieldCell',
    GaussianRadius=0.008333333333333333,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour2Display.ScaleTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour2Display.OpacityTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView5'
# ----------------------------------------------------------------

# show data from a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1Display = Show(a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1, renderView5, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1Display.Set(
    Representation='Surface',
    ColorArrayName=['CELLS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldPoint'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1Display.ScaleTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
a1pe_1uns_3ssd_10tps__hard__GICvtkhdf_1Display.OpacityTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# show data from a3partitionssansMergeParts
a3partitionssansMergePartsDisplay = Show(a3partitionssansMergeParts, renderView5, 'TextSourceRepresentation')

# ----------------------------------------------------------------
# setup the visualization in view 'renderView6'
# ----------------------------------------------------------------

# show data from cellDatatoPointData3
cellDatatoPointData3Display = Show(cellDatatoPointData3, renderView6, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cellDatatoPointData3Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldCell'],
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
cellDatatoPointData3Display.ScaleTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
cellDatatoPointData3Display.OpacityTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# show data from contour3
contour3Display = Show(contour3, renderView6, 'GeometryRepresentation')

# trace defaults for the display properties.
contour3Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='Hierarchy',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.16666666666666669,
    SelectScaleArray='myFieldCell',
    GlyphType='Arrow',
    GlyphTableIndexArray='myFieldCell',
    GaussianRadius=0.008333333333333333,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=['POINTS', ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour3Display.ScaleTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour3Display.OpacityTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView7'
# ----------------------------------------------------------------

# show data from mergeBlocks1
mergeBlocks1Display = Show(mergeBlocks1, renderView7, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
mergeBlocks1Display.Set(
    Representation='Surface',
    ColorArrayName=['CELLS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldPoint',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldPoint'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldPoint'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldPoint'],
    SelectInputVectors=[None, ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
mergeBlocks1Display.ScaleTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
mergeBlocks1Display.OpacityTransferFunction.Set(
    Points=[1.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0],
)

# show data from annotateTime1
annotateTime1Display = Show(annotateTime1, renderView7, 'TextSourceRepresentation')

# ----------------------------------------------------------------
# setup the visualization in view 'renderView8'
# ----------------------------------------------------------------

# show data from cellDatatoPointData4
cellDatatoPointData4Display = Show(cellDatatoPointData4, renderView8, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cellDatatoPointData4Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.4,
    SelectScaleArray='None',
    GlyphType='Arrow',
    GlyphTableIndexArray='None',
    GaussianRadius=0.02,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    ScalarOpacityFunction=myFieldCellPWF,
    ScalarOpacityUnitDistance=2.3378446623041422,
    OpacityArrayName=['POINTS', 'myFieldCell'],
    SelectInputVectors=[None, ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
cellDatatoPointData4Display.ScaleTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
cellDatatoPointData4Display.OpacityTransferFunction.Set(
    Points=[42.0, 0.0, 0.5, 0.0, 89.0, 1.0, 0.5, 0.0],
)

# show data from contour4
contour4Display = Show(contour4, renderView8, 'GeometryRepresentation')

# trace defaults for the display properties.
contour4Display.Set(
    Representation='Surface',
    ColorArrayName=['POINTS', 'myFieldCell'],
    LookupTable=myFieldCellLUT,
    LineWidth=3.0,
    SelectNormalArray='None',
    SelectTangentArray='None',
    SelectTCoordArray='None',
    TextureTransform='Transform2',
    OSPRayScaleArray='myFieldCell',
    OSPRayScaleFunction='Piecewise Function',
    Assembly='',
    SelectedBlockSelectors=[''],
    SelectOrientationVectors='None',
    ScaleFactor=0.2,
    SelectScaleArray='myFieldCell',
    GlyphType='Arrow',
    GlyphTableIndexArray='myFieldCell',
    GaussianRadius=0.01,
    SetScaleArray=['POINTS', 'myFieldCell'],
    ScaleTransferFunction='Piecewise Function',
    OpacityArray=['POINTS', 'myFieldCell'],
    OpacityTransferFunction='Piecewise Function',
    DataAxesGrid='Grid Axes Representation',
    PolarAxes='Polar Axes Representation',
    SelectInputVectors=[None, ''],
    WriteLog='',
)

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
contour4Display.ScaleTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
contour4Display.OpacityTransferFunction.Set(
    Points=[60.0, 0.0, 0.5, 0.0, 60.0078125, 1.0, 0.5, 0.0],
)

# ----------------------------------------------------------------
# setup color maps and opacity maps used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup animation scene, tracks and keyframes
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# initialize the timekeeper

# get time animation track
timeAnimationCue1 = GetTimeTrack()

# initialize the animation track

# get animation scene
animationScene1 = GetAnimationScene()

# initialize the animation scene
animationScene1.Set(
    ViewModules=[renderView1, renderView2, renderView3, renderView4, renderView5, renderView6, renderView7, renderView8],
    Cues=timeAnimationCue1,
    AnimationTime=0.0,
    EndTime=9.0,
    PlayMode='Snap To TimeSteps',
)

# initialize the animation scene

# ----------------------------------------------------------------
# restore active source
SetActiveSource(contour2)
# ----------------------------------------------------------------


##--------------------------------------------
## You may need to add some code at the end of this python script depending on your usage, eg:
#
## Render all views to see them appears
# RenderAllViews()
#
## Interact with the view, usefull when running from pvpython
# Interact()
#
## Save a screenshot of the active view
# SaveScreenshot("path/to/screenshot.png")
#
## Save a screenshot of a layout (multiple splitted view)
# SaveScreenshot("path/to/screenshot.png", GetLayout())
#
## Save all "Extractors" from the pipeline browser
# SaveExtracts()
#
## Save a animation of the current active view
# SaveAnimation()
#
## Please refer to the documentation of paraview.simple
## https://www.paraview.org/paraview-docs/latest/python/paraview.simple.html
##--------------------------------------------