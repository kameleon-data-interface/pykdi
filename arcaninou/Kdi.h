#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <numpy/arrayobject.h>

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace KDI {
#define KASSERT(cnd) if (!(cnd)) { std::cout << "ASSERT line:" << __LINE__ << std::endl; exit(1);}
#define KTRACE(trace, msg) if(trace) { std::cout << "KTRACE " << msg << std::endl; }

class KDIChunk
{
    private:
        const PyObject* m_pInstanceChunk;
        const bool m_trace;

    public:
        KDIChunk(const PyObject* _pInstanceChunk, bool _trace = false) : m_pInstanceChunk(_pInstanceChunk), m_trace(_trace)
        {
            KTRACE(m_trace, "KDIChunk::KDIChunk IN/OUT");
        }

        ~KDIChunk() {
            KTRACE(m_trace, "KDIChunk::~KDIChunk IN");
            if(m_pInstanceChunk) Py_DECREF(m_pInstanceChunk);
            KTRACE(m_trace, "KDIChunk::~KDIChunk OUT");
        }

    private:
        PyObject* _simple_call(const std::string _method, PyObject* _pArgs)
        {
            KTRACE(m_trace, "KDIChunk:_simple_call IN");
            PyObject *pValue = PyUnicode_FromString("pykdi"); KASSERT(pValue);
            PyObject *pModule = PyImport_Import(pValue); KASSERT(pModule);
            PyObject* pClass = PyObject_GetAttrString(pModule, "KDIAgreementStepPartChunk"); KASSERT(pClass);
            PyObject* pMethod = PyObject_GetAttrString(pClass, _method.c_str()); KASSERT(pMethod);
            KASSERT(PyCallable_Check(pMethod));
            KASSERT(_pArgs);
            PyObject* pResult = PyObject_CallObject(pMethod, _pArgs);
            Py_DECREF(pMethod);
            Py_DECREF(pClass);
            Py_DECREF(pModule);
            Py_DECREF(pValue);
            KTRACE(m_trace, "KDIChunk:_simple_call OUT");
            return pResult;
        }

    public:
        void dump()
        {
            KTRACE(m_trace, "KDIChunk:dump IN");
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O)", m_pInstanceChunk);
            _simple_call( "dump", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:dump OUT");
        }

        // TODO Change in Arcane set to insert
        void insert(const std::string _absname, PyArrayObject *_array)
        {
            KTRACE(m_trace, "KDIChunk:insert " << _absname << " IN");
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O, z, O)", m_pInstanceChunk, _absname.c_str(), _array);
            _simple_call( "insert", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:insert " << _absname << " OUT");
        }

        void garbageCollector()
        {
            // TODO On pourrait vérifier que l'on est partless au moins en 0.4.0
            KTRACE(m_trace, "KDIChunk:garbageCollector (...) IN");
            // TODO Verifier que l'on est bien partless
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O)", m_pInstanceChunk); KASSERT(pArgs);
            _simple_call( "garbageCollector", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:garbageCollector OUT");
        }

        void saveVTKHDF(const std::string _absfilename)
        {
            // TODO On pourrait vérifier que l'on est partless au moins en 0.4.0
            KTRACE(m_trace, "KDIChunk:saveVTKHDF (...) IN");
            // TODO Verifier que l'on est bien partless
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O, z)", m_pInstanceChunk, _absfilename.c_str()); KASSERT(pArgs);
            _simple_call( "saveVTKHDF", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:saveVTKHDF OUT");
        }

        void saveVTKHDFCompute(const std::string _absfilename)
        {
            KTRACE(m_trace, "KDI KDIChunk saveVTKHDFCompute " << _absfilename);
            // TODO On pourrait vérifier que l'on est partless au moins en 0.4.0
            // TODO ON pourrait trouver une facon plus elegante d'activté des traitements ici KDIComputeMultiMilieux
            KTRACE(m_trace, "KDIChunk:saveVTKHDFCompute (...) IN");
            // TODO Verifier que l'on est bien partless
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O, z)", m_pInstanceChunk, _absfilename.c_str()); KASSERT(pArgs);
            _simple_call( "saveVTKHDFCompute", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDI KDIChunk saveVTKHDFCompute");
        }

        void saveVTKHDFNto1(const std::string _absfilename)
        {
            // TODO On pourrait vérifier que l'on est partless au moins en 0.4.0
            KTRACE(m_trace, "KDIChunk:saveVTKHDFNto1 (...) IN");
            // TODO Verifier que l'on est bien partless
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O, z)", m_pInstanceChunk, _absfilename.c_str()); KASSERT(pArgs);
            _simple_call( "saveVTKHDFNto1", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:saveVTKHDFNto1 OUT");
        }

        void saveVTKHDFComputeNto1(const std::string _absfilename)
        {
            // TODO On pourrait vérifier que l'on est partless au moins en 0.4.0
            KTRACE(m_trace, "KDIChunk:saveVTKHDFComputeNto1 (...) IN");
            // TODO Verifier que l'on est bien partless
            KASSERT(m_pInstanceChunk);
            PyObject* pArgs = Py_BuildValue("(O, z)", m_pInstanceChunk, _absfilename.c_str()); KASSERT(pArgs);
            _simple_call( "saveVTKHDFComputeNto1", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIChunk:saveVTKHDFComputeNto1 OUT");
        }
};

class KDIBase
{
    private:
        const PyObject* m_pInstanceBase;
        const bool m_trace;

    public:
        KDIBase(const PyObject* _pInstanceBase, const bool _trace) : m_pInstanceBase(_pInstanceBase), m_trace(_trace)
        {
            KTRACE(m_trace, "KDIBase::KDIBase IN/OUT");
        }

        ~KDIBase() {
            KTRACE(m_trace, "KDIBase::~KDIBase IN");
            if(m_pInstanceBase) Py_DECREF(m_pInstanceBase);
            KTRACE(m_trace, "KDIBase::~KDIBase OUT");
        }

        void set_nb_parts(unsigned int _nb_parts)
        {
            KTRACE(m_trace, "KDIBase:set_nb_parts IN");
            KASSERT(m_pInstanceBase);
            std::string key {"nb_parts"};
            PyObject* pArgs = Py_BuildValue("(O, z, i)", m_pInstanceBase, key.c_str(), (int)_nb_parts);
            _simple_call( "set_conf_int", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:set_nb_parts OUT");
        }

        void initialize(bool mode_read)
        {
            KTRACE(m_trace, "KDIBase:initialize IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O, O)", m_pInstanceBase, mode_read ? Py_True: Py_False);
            _simple_call( "initialize", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:initialize OUT");
        }

    private:
        PyObject* _simple_call(const std::string _method, PyObject* _pArgs)
        {
            KTRACE(m_trace, "KDIBase:_simple_call IN");
            PyObject *pValue = PyUnicode_FromString("pykdi"); KASSERT(pValue);
            PyObject *pModule = PyImport_Import(pValue); KASSERT(pModule);
            PyObject* pClass = PyObject_GetAttrString(pModule, "KDIAgreementStepPartBase"); KASSERT(pClass);
            PyObject* pMethod = PyObject_GetAttrString(pClass, _method.c_str()); KASSERT(pMethod);
            KASSERT(PyCallable_Check(pMethod));
            KASSERT(_pArgs);
            PyObject* pResult = PyObject_CallObject(pMethod, _pArgs);
            Py_DECREF(pMethod);
            Py_DECREF(pClass);
            Py_DECREF(pModule);
            Py_DECREF(pValue);
            KTRACE(m_trace, "KDIBase:_simple_call OUT");
            return pResult;
        }

    public:
        void light_dump()
        {
            KTRACE(m_trace, "KDIBase:light_dump IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O)", m_pInstanceBase);
            _simple_call( "light_dump", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:light_dump OUT");
        }

        void dump()
        {
            KTRACE(m_trace, "KDIBase:dump IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O)", m_pInstanceBase);
            _simple_call( "dump", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:dump OUT");
        }

        void update(const std::string &_typename, const std::string &_absname)
        {
            KTRACE(m_trace, "KDIBase:update IN typename=" << _typename << " absname=" << _absname);
            KTRACE(m_trace, "KDIBase:update A");
            KASSERT(m_pInstanceBase);
            KTRACE(m_trace, "KDIBase:update B");
            PyObject* pArgs = Py_BuildValue("(O, z, z)", m_pInstanceBase, _typename.c_str(), _absname.c_str()); KASSERT(pArgs);
            KTRACE(m_trace, "KDIBase:update C");
            _simple_call( "update", pArgs);
            KTRACE(m_trace, "KDIBase:update D");
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:update OUT typename=" << _typename << " absname=" << _absname);
        }

        const std::string update_fields(const std::string &_nameParentMesh, const std::string &_nameField)
        {
            KTRACE(m_trace, "KDIBase:update_fields IN nameParentMesh=" << _nameParentMesh << " nameField=" << _nameField);
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O, z, z)", m_pInstanceBase, _nameParentMesh.c_str(), _nameField.c_str()); KASSERT(pArgs);
            _simple_call( "update_fields", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:update_fields OUT nameParentMesh=" << _nameParentMesh << " nameField=" << _nameField);
            // TODO Ceci est une verole... il serait preferable que ce soit le retour de la fonction Python qui soit exploitee
            return _nameParentMesh + "/" + _nameField;
        }

        const std::string update_sub(const std::string &_nameParentMesh, const std::string &_nameSubMesh)
        {
            KTRACE(m_trace, "KDIBase:update_sub IN nameParentMesh=" << _nameParentMesh << " nameSubMesh=" << _nameSubMesh);
            KASSERT(m_pInstanceBase);
            KASSERT(_nameParentMesh[0] == '/');
            KASSERT(_nameSubMesh[0] == '/');
            PyObject* pArgs = Py_BuildValue("(O, z, z)", m_pInstanceBase, _nameParentMesh.c_str(), _nameSubMesh.c_str()); KASSERT(pArgs);
            _simple_call( "update_sub", pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:update_sub OUT nameParentMesh=" << _nameParentMesh << " nameSubMesh=" << _nameSubMesh);
            // TODO Ceci est une verole... il serait preferable que ce soit le retour de la fonction Python qui soit exploitee
            return _nameParentMesh + "/submeshes" + _nameSubMesh;
        }

    private:
        std::shared_ptr<KDIChunk> _chunk(PyObject* pArgs)
        {
            KTRACE(m_trace, "KDIBase:_chunk IN");
            PyObject* pResult =_simple_call( "chunk", pArgs);
            Py_DECREF(pArgs);
            std::shared_ptr<KDIChunk> chunk = std::make_shared<KDIChunk>(pResult, m_trace);
            KTRACE(m_trace, "KDIBase:_chunk OUT");
            return chunk;
        }

    public:
        std::shared_ptr<KDIChunk> chunk()
        {
            KTRACE(m_trace, "KDIBase:chunk IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O)", m_pInstanceBase);
            std::shared_ptr<KDIChunk> chunk = _chunk(pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:chunk OUT (" << chunk << ")");
            return chunk;
        }

        std::shared_ptr<KDIChunk> chunk(float _vstep)
        {
            KTRACE(m_trace, "KDIBase:chunk (" << _vstep << ") IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O, f)", m_pInstanceBase, _vstep);
            std::shared_ptr<KDIChunk> chunk = _chunk(pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:chunk OUT (" << chunk << ")");
            return chunk;
        }

        std::shared_ptr<KDIChunk> chunk(float _vstep, int _ipart)
        {
            KTRACE(m_trace, "KDIBase:chunk (" << _vstep << ") IN");
            KASSERT(m_pInstanceBase);
            PyObject* pArgs = Py_BuildValue("(O, f, i)", m_pInstanceBase, _vstep, _ipart);
            std::shared_ptr<KDIChunk> chunk = _chunk(pArgs);
            Py_DECREF(pArgs);
            KTRACE(m_trace, "KDIBase:chunk OUT (" << chunk << ")");
            return chunk;
        }
};

std::shared_ptr<KDIBase> _createBase(const std::string _absfilename, bool _trace = false)
{
    KTRACE(_trace, "KDI C++ createBase IN");
    PyObject *pValue = PyUnicode_FromString("pykdi"); KASSERT(pValue);
    KTRACE(_trace, "KDI   FromString pValue=" << pValue);
    KTRACE(_trace, "KDI   Getenv PYTHONPATH=" << getenv("PYTHONPATH"));
    KTRACE(_trace, "KDI   Getenv KDI_DICTIONARY_PATH=" << getenv("KDI_DICTIONARY_PATH"));
    // Si on a un plantage, c'est parce que les commandes :
    //      Py_Initialize();
    //      import_array();
    // n'ont pas été exécutées avant.
    PyObject *pModule = PyImport_Import(pValue);
    KTRACE(_trace, "KDI   Import ?");
    KTRACE(_trace, "KDI   pModule=" << pModule);
    KASSERT(pModule);
    KTRACE(_trace, "KDI   Import");
    PyObject* pClass = PyObject_GetAttrString(pModule, "KDIAgreementStepPartBase"); KASSERT(pClass);
    KTRACE(_trace, "KDI   GetAttrString");
    KASSERT(PyCallable_Check(pClass));
    KTRACE(_trace, "KDI   Check");
    PyObject* pArgs = Py_BuildValue("(z)", _absfilename.c_str()); KASSERT(pArgs);
    KTRACE(_trace, "KDI   BuildValue");
    PyObject* pResult = PyObject_CallObject(pClass, pArgs); KASSERT(pResult);
    KTRACE(_trace, "KDI   CallObject");
    Py_DECREF(pArgs);
    Py_DECREF(pClass);
    Py_DECREF(pModule);
    Py_DECREF(pValue);
    // VALGRIN --17247-- REDIR: 0x50d3600 (libstdc++.so.6:operator new(unsigned long))
    // redirected to 0x4843f30 (operator new(unsigned long))
    std::shared_ptr<KDIBase> base = std::make_shared<KDIBase>(pResult, _trace);
    KTRACE(_trace, "KDI C++ createBase OUT");
    return base;
}

std::shared_ptr<KDIBase> createBase(const std::string _absfilename, unsigned int _nb_parts, bool _trace = false)
{
    KTRACE(_trace, "KDI C++ createBase (" << _nb_parts << ") IN");
    std::shared_ptr<KDIBase> base = _createBase(_absfilename, _trace);
    base->set_nb_parts(_nb_parts);
    base->initialize(false);
    KTRACE(_trace, "KDI C++ createBase OUT");
    return base;
}

std::shared_ptr<KDIBase> loadVTKHDF(const std::string _absfilename, bool _trace = false)
{
    KTRACE(_trace, "loadBase () IN");
    std::shared_ptr<KDIBase> base = _createBase(_absfilename, _trace);
    base->initialize(true);
    KTRACE(_trace, "loadVTKHDF OUT");
    return base;
}

/** Convert a c++ 2D vector into a numpy array
 *
 * @param const vector< vector<T> >& vec : 2D vector data
 * @return PyArrayObject* array : converted numpy array
 *
 * Transforms an arbitrary 2D C++ vector into a numpy array. Throws in case of
 * unregular shape. The array may contain empty columns or something else, as
 * long as it's shape is square.
 *
 * Warning this routine makes a copy of the memory!
 */
template<typename T>
static PyArrayObject* vector_to_nparray(const std::vector< std::vector<T> >& vec, int type_num = PyArray_FLOAT){

   // rows not empty
   if( !vec.empty() ){

      // column not empty
      if( !vec[0].empty() ){

        size_t nRows = vec.size();
        size_t nCols = vec[0].size();
        npy_intp dims[2] = {nRows, nCols};
        PyArrayObject* vec_array = (PyArrayObject *) PyArray_SimpleNew(2, dims, type_num);

        T *vec_array_pointer = (T*) PyArray_DATA(vec_array);

        // copy vector line by line ... maybe could be done at one
        for (size_t iRow=0; iRow < vec.size(); ++iRow){

          if( vec[iRow].size() != nCols){
             Py_DECREF(vec_array); // delete
             throw(std::string("Can not convert vector<vector<T>> to np.array, since c++ matrix shape is not uniform."));
          }

          copy(vec[iRow].begin(),vec[iRow].end(),vec_array_pointer+iRow*nCols);
        }

        return vec_array;

     // Empty columns
     } else {
        npy_intp dims[2] = {vec.size(), 0};
        return (PyArrayObject*) PyArray_ZEROS(2, dims, PyArray_FLOAT, 0);
     }


   // no data at all
   } else {
      npy_intp dims[2] = {0, 0};
      return (PyArrayObject*) PyArray_ZEROS(2, dims, PyArray_FLOAT, 0);
   }

}


/** Convert a c++ vector into a numpy array
 *
 * @param const vector<T>& vec : 1D vector data
 * @return PyArrayObject* array : converted numpy array
 *
 * Transforms an arbitrary C++ vector into a numpy array. Throws in case of
 * unregular shape. The array may contain empty columns or something else, as
 * long as it's shape is square.
 *
 * Warning this routine makes a copy of the memory!
 *
 *
 *
 * https://pyo3.github.io/rust-numpy/numpy/npyffi/types/enum.NPY_TYPES.html
 */
template<typename T>
static PyArrayObject* vector_to_nparray(const std::vector<T>& vec, int type_num = PyArray_FLOAT, int comp_num = 1)
{
    bool trace { true };
    KTRACE(trace, "vector_to_nparray begin")
    // rows not empty
    if( !vec.empty() ){
        PyArrayObject* vec_array = nullptr;
        if (comp_num == 1)
        {
            int nd = 1;
            npy_intp dims[] {vec.size()};
            vec_array = (PyArrayObject *) PyArray_SimpleNew(nd, dims, type_num);
        } else {
            int nd = 2;
            npy_intp dims[] { int(vec.size() / comp_num), comp_num};
            vec_array = (PyArrayObject *) PyArray_SimpleNew(nd, dims, type_num);
        }
        T *vec_array_pointer = (T*) PyArray_DATA(vec_array);
        // KTRACE(trace, vec_array_pointer);
        // A bannir
        copy(vec.begin(), vec.end(), vec_array_pointer);
        KTRACE(trace, "vector_to_nparray copy")
        KTRACE(trace, "vector_to_nparray end")
        return vec_array;
    // no data at all
    }
    npy_intp dims[1] = {0};
    KTRACE(trace, "vector_to_nparray empty")
    KTRACE(trace, "vector_to_nparray end")
    return (PyArrayObject*) PyArray_ZEROS(1, dims, PyArray_FLOAT, 0);
}
}