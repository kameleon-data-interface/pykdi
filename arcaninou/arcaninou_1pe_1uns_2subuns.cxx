#include "mpi.h"

#include "Kdi.h"

using namespace KDI;

namespace
{
    template <typename DataType> class KDITraits;

    template <> class KDITraits<long>
    {
        public:

        static int64_t kdiType() { std::cout << "KDI DTYPE C++ PyArray_INT64" << std::endl; return PyArray_INT64; }
    };

    template <> class KDITraits<int>
    {
        public:

        static int64_t kdiType() { std::cout << "KDI DTYPE C++ PyArray_INT32" << std::endl; return PyArray_INT32; }
    };

    template <> class KDITraits<float>
    {
        public:

        static int64_t kdiType() { std::cout << "KDI DTYPE C++ PyArray_FLOAT32" << std::endl; return PyArray_FLOAT32; }
    };

    template <> class KDITraits<double>
    {
        public:

        static int64_t kdiType() { std::cout << "KDI DTYPE C++ PyArray_FLOAT64" << std::endl; return PyArray_FLOAT64; }
    };

    template <> class KDITraits<unsigned char>
    {
        public:

        static int64_t kdiType() { std::cout << "KDI DTYPE C++ PyArray_UINT8" << std::endl; return PyArray_UINT8; }
    };

    template <typename DataType> PyArrayObject*
    _numpyDataSet1D(std::vector<DataType>& values)
    {
        npy_intp dims[] {values.size()};
        const int64_t py_type = KDITraits<DataType>::kdiType();
        PyArrayObject* vec_array = (PyArrayObject *) PyArray_SimpleNew(1, dims, py_type);
        // PyArray_DATA n'est qu'une facilité d'accès à l'attribut data d'un PyArray
        // cela n'affecte donc pas un compteur de référence
        DataType *vec_array_pointer = (DataType*) PyArray_DATA(vec_array);
        std::copy(values.data(), values.data()+values.size(), vec_array_pointer);
        return vec_array;
    }

    template <typename DataType> PyArrayObject*
    _numpyDataSet2D(std::vector<DataType>& values, std::vector<int> shape)
    {
        npy_intp dims[2] = {shape[0], shape[1]};
        const int64_t py_type = KDITraits<DataType>::kdiType();
        PyArrayObject* vec_array = (PyArrayObject *) PyArray_SimpleNew(2, dims, py_type);
        // PyArray_DATA n'est qu'une facilité d'accès à l'attribut data d'un PyArray
        // cela n'affecte donc pas un compteur de référence
        DataType *vec_array_pointer = (DataType*) PyArray_DATA(vec_array);
        std::copy(values.data(), values.data()+shape[0]*shape[1], vec_array_pointer);
        return vec_array;
    }
} // namespace

/*
PyArrayObject*
_numpyDataSetReal3D(IData* data)
{
  std::cout << "KDI _numpyDataSetReal3D var/data begin";
  auto* true_data = dynamic_cast<IArrayDataT<Real3>*>(data);
  ARCANE_CHECK_POINTER(true_data);
  SmallSpan<const Real3> values(true_data->view());
  Int32 nb_value = values.size();
  // TODO: optimiser cela sans passer par un tableau temporaire
  UniqueArray2<Real> scalar_values;
  scalar_values.resize(nb_value, 3);
  for (Int32 i = 0; i < nb_value; ++i) {
    Real3 v = values[i];
    scalar_values[i][0] = v.x;
    scalar_values[i][1] = v.y;
    scalar_values[i][2] = v.z;
  }

  PyArrayObject* vec_array = _numpyDataSet2D<Real>(scalar_values);
  std::cout << "KDI _numpyDataSetReal3D var/data end";
  return vec_array;
}

PyArrayObject*
_numpyDataSetReal2D(IData* data)
{
  std::cout << "KDI _numpyDataSetReal2D var/data begin";
  // Converti en un tableau de 3 composantes dont la dernière vaudra 0.
  auto* true_data = dynamic_cast<IArrayDataT<Real2>*>(data);
  ARCANE_CHECK_POINTER(true_data);
  SmallSpan<const Real2> values(true_data->view());
  Int32 nb_value = values.size();
  UniqueArray2<Real> scalar_values;
  scalar_values.resize(nb_value, 3);
  for (Int32 i = 0; i < nb_value; ++i) {
    Real2 v = values[i];
    scalar_values[i][0] = v.x;
    scalar_values[i][1] = v.y;
    scalar_values[i][2] = 0.0;
  }

  PyArrayObject* vec_array = _numpyDataSet2D<Real>(scalar_values);
  std::cout << "KDI _numpyDataSetReal2D var/data end";
  return vec_array;
}
*/

int
test_1pe_1uns_2subuns(int argc, char *argv[])
{
    bool with_compute_sub_uns { false };
    bool with_log { false };
    bool with_log_main { false };
    long with_nb_cells { -1 };
    bool with_nto1 { false };
    bool with_write { false };

    std::cout << "test_1pe_1uns_2subuns\n";
    std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> in progress\n";
    for (int i = 1; i < argc ; ++i)
    {
        std::cout << "> Parameter #" << i << " " << argv[i] << "\n";
        if (std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h")
        {
            std::cout << "Use of " << argv[0] << "\n\n";
            std::cout << "   -h | --help\n";
            std::cout << "     Obtains help\n\n";
            std::cout << "   -c | --computeSubUns\n";
            std::cout << "     Activates mutation ComputeMultiMilieux\n\n";
            std::cout << "   -l | --log\n";
            std::cout << "     Activates log (just glu)\n\n";
            std::cout << "   -m | --logmain\n";
            std::cout << "     Activates log main\n\n";
            std::cout << "   -n | --nbCells #\n";
            std::cout << "     Sets the number of cells per partition\n\n";
            std::cout << "   -nto1\n";
            std::cout << "     Enables grouping into a single partition\n\n";
            std::cout << "   -w | --write\n";
            std::cout << "     Activates the writing of a VTK HDF database\n\n";
            exit(0);
        }
        if (std::string(argv[i]) == "--computeSubUns" || std::string(argv[i]) == "-c")
        {
            with_compute_sub_uns = true;
            std::cout << ">   Enable computeSubUns\n";
        }
        if (std::string(argv[i]) == "--log" || std::string(argv[i]) == "-l")
        {
            with_log = true;
            std::cout << ">   Enable log\n";
        }
        if (std::string(argv[i]) == "--logmain" || std::string(argv[i]) == "-m")
        {
            with_log_main = true;
            std::cout << ">   Enable log main\n";
        }
        if (std::string(argv[i]) == "--nbCells" || std::string(argv[i]) == "-n")
        {
            i++;
            with_nb_cells = atol(argv[i]);
            std::cout << ">   Fixed nb cells " << with_nb_cells << "\n";
        }
        if (std::string(argv[i]) == "-nto1")
        {
            with_nto1 = true;
            std::cout << ">   Enable Nto1\n";
        }
        if (std::string(argv[i]) == "--write" || std::string(argv[i]) == "-w")
        {
            with_write = true;
            std::cout << ">   Enable write\n";
        }
    }

    int mpi_size {1};
    int mpi_rank {0};

    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    int nbParts {mpi_size};

    std::shared_ptr<KDIBase> kdi_base = createBase("/tmp/arcaninou_1pe_1uns_2subuns", nbParts, with_log); // true/false permet d'activer les traces

    if (with_log_main)
    {
        std::cout << "> after createBase (" << kdi_base << ")\n";
        // kdi_base->light_dump();
    }

    kdi_base->update("UnstructuredGrid", "/mymesh");
    // VALGRIN --18409-- REDIR: 0x50d1870 (libstdc++.so.6:operator delete(void*, unsigned long))
    // redirected to 0x4846a90 (operator delete(void*, unsigned long))
    // IDEM update_fields

    if (with_log_main)
    {
        std::cout << "> after update UnstructuredGrid\n";
        // kdi_base->light_dump();
    }

    std::shared_ptr<KDIChunk> kdi_chunk_stepless = kdi_base->chunk();
    std::vector<long> version({3, 0});
    PyArrayObject* parray = _numpyDataSet1D<long>(version);
    kdi_chunk_stepless->insert("/glu/version", parray);
    Py_DECREF(parray);

    // for(int iStep=0; iStep<34; ++iStep)
    for(int iStep=0; iStep<4; ++iStep)
    {
        float vStep {iStep};

        std::shared_ptr<KDIChunk> kdi_chunk_partless = kdi_base->chunk(vStep);

        if (with_log_main)
        {
            std::cout << "> after kdi_chunk_partless\n";
        }

        int iPart = mpi_rank;

        std::shared_ptr<KDIChunk> kdi_chunk = kdi_base->chunk(vStep, iPart);

        if (with_log_main)
        {
            std::cout << "> after kdi_chunk\n";
        }

        long nCells {30000000};

        if (with_nb_cells > 0)
        {
            nCells = with_nb_cells;
        } else if(with_write)
        {
            nCells = 30000000;

            if(with_nto1)
            {
                nCells = 100000;
            }
        }

        long nNodes = 2 * (nCells + 1);
        long firstNode = mpi_rank * nNodes;
        long firstColumn = mpi_rank * (nCells + 1);

        {
            std::vector<long> cells_connectivity(4 * nCells);
            long* current = cells_connectivity.data();
            const long* end = current + 4 * nCells - 1; // FOR VALGRIND
            for(long iCell = 0; iCell < nCells; ++iCell)
            {
                *current = 2 * iCell; ++current;
                *current = 2 * iCell + 1; ++current;
                *current = 2 * (iCell + 1) + 1; ++current;
                *current = 2 * (iCell + 1);
                if(current != end) // FOR VALGRIN ?
                    ++current;
            }
            // VALGRIN ==18409== Warning: set address range perms: large range [0x10b65040, 0x49eec040) (undefined)
            assert(current == cells_connectivity.data() + 4 * nbCells);
            PyArrayObject* parray = _numpyDataSet1D<long>(cells_connectivity);
            // VALGRIN ==18409== Warning: set address range perms: large range [0x59c8e040, 0x93015040) (undefined)
            kdi_chunk->insert("/mymesh/cells/connectivity", parray);
            Py_DECREF(parray);
        }
        // VALGRIN ==28265== Warning: set address range perms: large range [0x10908028, 0x49c8f058) (noaccess)
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/cells/connectivity\n";
        }

        {
            std::vector<unsigned char> cells_types(nCells);
            for(unsigned char &val : cells_types)
            {
                val = 9; // KVTK_QUAD
                // val = 12; // KVTK_HEXAHEDRON
            }
            PyArrayObject* parray = _numpyDataSet1D<unsigned char>(cells_types);
            kdi_chunk->insert("/mymesh/cells/types", parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/cells/types\n";
        }

        {
            std::vector<long> cells_globalIndexContinuous(nCells);
            long gic = mpi_rank * nCells;
            for(long &val : cells_globalIndexContinuous)
            {
                val = gic++;
            }
            PyArrayObject* parray = _numpyDataSet1D<long>(cells_globalIndexContinuous);
            kdi_chunk->insert("/mymesh/cells/globalIndexContinuous", parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/cells/globalIndexContinuous\n";
        }

        {
            std::vector<double> points(nNodes * 3);
            double* current = points.data();
            for(long iColumn = 0; iColumn < nCells + 1; ++iColumn)
            {
                *current = firstColumn + iColumn; ++current;
                *current = 0; ++current;
                *current = 0; ++current;

                *current = firstColumn + iColumn; ++current;
                *current = 1; ++current;
                *current = 0; ++current;
            }
            std::vector<int> shape {nNodes, 3};
            PyArrayObject* parray = _numpyDataSet2D<double>(points, shape);
            kdi_chunk->insert("/mymesh/points/cartesianCoordinates", parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/points/cartesianCoordinates\n";
        }

        {
            std::vector<long> points_globalIndexContinuous(nNodes);
            std::cout << "test_1pe_1uns_2subuns points GIC A\n";
            long gic = firstNode;
            for(long &val : points_globalIndexContinuous)
            {
                val = gic++;
            }
            PyArrayObject* parray = _numpyDataSet1D<long>(points_globalIndexContinuous);
            kdi_chunk->insert("/mymesh/points/globalIndexContinuous", parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/points/globalIndexContinuous\n";
        }

        // Fields points
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/points/fields", "LocalNodeId");
            std::vector<long> nodes_uid(nNodes);
            long iNode = 0;
            for(long &val : nodes_uid)
            {
                val = iNode++;
            }
            assert(iNode == nNodes);
            assert(namefield == "/mymesh/points/fields/LocalNodeId");
            PyArrayObject* parray = _numpyDataSet1D<long>(nodes_uid);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/points/fields", "GlobalNodeId");
            std::vector<long> nodes_uid(nNodes);
            long iNode = mpi_rank * nNodes;
            for(long &val : nodes_uid)
            {
                val = iNode++;
            }
            assert(iNode == nNodes);
            assert(namefield == "/mymesh/points/fields/GlobalNodeId");
            PyArrayObject* parray = _numpyDataSet1D<long>(nodes_uid);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/points/fields", "vtkGhostType");
            std::vector<unsigned char> nodes_ghost_type(nNodes);
            for(unsigned char &val : nodes_ghost_type)
            {
                val = 0;
            }
            assert(namefield == "/mymesh/points/fields/vtkGhostType");
            PyArrayObject* parray = _numpyDataSet1D<unsigned char>(nodes_ghost_type);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/points/fields/...\n";
        }

        // Fields cells
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/cells/fields", "LocalCellId");
            std::vector<long> cells_uid(nCells);
            long iCell = 0;
            for(long &val : cells_uid)
            {
                val = iCell++;
            }
            assert(iCell == nCells);
            assert(namefield == "/mymesh/cells/fields/LocalCellId");
            PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/cells/fields", "GlobalCellId");
            std::vector<long> cells_uid(nCells);
            long iCell = mpi_rank * nCells;
            for(long &val : cells_uid)
            {
                val = iCell++;
            }
            assert(iCell == nCells);
            assert(namefield == "/mymesh/cells/fields/GlobalCellId");
            PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        {
            const std::string namefield = kdi_base->update_fields("/mymesh/cells/fields", "vtkGhostType");
            std::vector<unsigned char> cells_ghost_type(nCells);
            for(unsigned char &val : cells_ghost_type)
            {
                val = 0;
            }
            assert(namefield == "/mymesh/cells/fields/vtkGhostType");
            PyArrayObject* parray = _numpyDataSet1D<unsigned char>(cells_ghost_type);
            kdi_chunk->insert(namefield, parray);
            Py_DECREF(parray);
        }
        if (with_log_main)
        {
            std::cout << "> after kdi_chunk /mymesh/cells/fields/...\n";
        }

        {
            const std::string namesubmesh = kdi_base->update_sub("/mymesh", "/mymilA");

            if (with_log_main)
            {
                std::cout << "> after kdi_base update_sub /mymilA\n";
            }

            long half_nCells = long(nCells / 2);
            {
                std::vector<long> cells_uid(half_nCells);
                if (mpi_rank % 2 == 0)
                {
                    long iCell = 0;
                    for(long &val : cells_uid)
                    {
                        val = iCell++;
                    }
                } else {
                    long iCell = nCells - half_nCells;
                    for(long &val : cells_uid)
                    {
                        val = iCell++;
                    }
                }
                assert(namesubmesh == "/mymesh/mymilA");
                PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
                kdi_chunk->insert(namesubmesh + "/indexescells", parray);
                Py_DECREF(parray);
            }
            if (with_log_main)
            {
                std::cout << "> after kdi_chunk insert /mymilA /indexescells\n";
            }

            // Fields cells
            {
                const std::string namefield = kdi_base->update_fields(namesubmesh + "/cells/fields", "LocalCellId");
                std::vector<long> cells_uid(half_nCells);
                long iCell = 0;
                for(long &val : cells_uid)
                {
                    val = iCell++;
                }
                assert(namesubmesh == "/mymesh/mymilA");
                assert(namefield == namesubmesh + "/cells/fields/LocalCellId");
                PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
                kdi_chunk->insert(namefield, parray);
                Py_DECREF(parray);
            }
            if (with_log_main)
            {
                std::cout << "> after kdi_chunk insert /mymilA /cells/fields/LocalCellId\n";
            }
        }

        {
            const std::string namesubmesh = kdi_base->update_sub("/mymesh", "/mymilB");

            if (with_log_main)
            {
                std::cout << "> after kdi_base update_sub /mymilB\n";
            }

            long half_nCells = long(nCells / 2);
            {
                std::vector<long> cells_uid(half_nCells);
                if (mpi_rank % 2 != 0)
                {
                    long iCell = 0;
                    for(long &val : cells_uid)
                    {
                        val = iCell++;
                    }
                } else {
                    long iCell = nCells - half_nCells;
                    for(long &val : cells_uid)
                    {
                        val = iCell++;
                    }
                }
                assert(namesubmesh == "/mymesh/mymilB");
                PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
                kdi_chunk->insert(namesubmesh + "/indexescells", parray);
                Py_DECREF(parray);
            }
            if (with_log_main)
            {
                std::cout << "> after kdi_chunk insert /mymilB /cells/fields\n";
            }

            // Fields cells
            {
                const std::string namefield = kdi_base->update_fields(namesubmesh + "/cells/fields", "LocalCellId");
                std::vector<long> cells_uid(half_nCells);
                long iCell = 0;
                for(long &val : cells_uid)
                {
                    val = iCell++;
                }
                assert(namesubmesh == "/mymesh/mymilB");
                assert(namefield == namesubmesh + "/cells/fields/LocalCellId");
                PyArrayObject* parray = _numpyDataSet1D<long>(cells_uid);
                kdi_chunk->insert(namefield, parray);
                Py_DECREF(parray);
            }
            if (with_log_main)
            {
                std::cout << "> after kdi_chunk insert /mymilB /cells/fields/LocalCellId\n";
            }
        }

        if (with_log_main)
        {
            std::cout << "> after step description\n";
            // kdi_base->light_dump();
        }

        if(with_write)
        {
            if(with_nto1)
            {
                if (with_compute_sub_uns)
                {
                    std::cout << ">>> saveVTKHDFComputeNto1 \"/tmp/arcaninou_subuns_computeNto1\"\n";
                    kdi_chunk_partless->saveVTKHDFComputeNto1("/tmp/arcaninou_subuns_computeNto1");
                } else {
                    std::cout << ">>> saveVTKHDFNto1 \"/tmp/arcaninou_subuns_Nto1\"\n";
                    std::cout << ">>>    WITHOUT SUBMESHES\n";
                    kdi_chunk_partless->saveVTKHDFNto1("/tmp/arcaninou_subuns_Nto1");
                }
            } else {
                if (with_compute_sub_uns)
                {
                    std::cout << ">>> saveVTKHDFCompute \"/tmp/arcaninou_subuns_compute\"\n";
                    kdi_chunk_partless->saveVTKHDFCompute("/tmp/arcaninou_subuns_compute");
                } else {
                    std::cout << ">>> saveVTKHDF \"/tmp/arcaninou_subuns\"\n";
                    std::cout << ">>>    WITHOUT SUBMESHES\n";
                    kdi_chunk_partless->saveVTKHDF("/tmp/arcaninou_subuns");
                }
            }
        } else {
            kdi_chunk_partless->garbageCollector();
        }

        // kdi_base->light_dump();

        std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #" << iStep << " terminated\n";
    }

    std::cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End Fully (mono) terminated\n";

    return 0;
}

int
main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    Py_Initialize();
    import_array();

    test_1pe_1uns_2subuns(argc, argv);
    std::cout << "Test test_1pe_1uns_2subuns terminated\n";

    Py_Finalize();

    MPI_Finalize();
    return 0;
}
