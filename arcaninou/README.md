./1p    dans le répertoire arcaninou :

    export PYTHONPATH=${PWD}/../src
	export KDI_DICTIONARY_PATH=${PWD}/../src/pykdi/dictionary
    export KDI_AGREEMENT_CODE2KDI=VTK

    rm /tmp/arcaninou*.*

    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns -w
    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns -w -nto1
    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns -w -n 10
    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns -w -nto1 -n 10

    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns_2subuns -w -n 10
    /usr/bin/mpiexec -n 4 --output-filename /tmp/toto ./1pe_1uns_2subuns -w -nto1 -n 10

    h5ls -vlr /tmp/arcaninou.vtkhdf


Arcane:
ARCANE_DEBUGGER=memcheck ctest -I 5,5,1 -V
pour lancer le test 5 avec valgrind.

Quelconque executable:
lekienj@UN00314761:~/PyCharmProjects/pykdi_master_0_8_0/arcaninou$ /usr/bin/valgrind --tool=memcheck -v --leak-check=full --track-origins=yes --num-callers=15 --show-reachable=yes --log-file=/tmp/arcaninou.txt ./1pe_1uns > /tmp/arcaninou.listing 2>&1


export PYTHONPATH=/home/lekienj/PyCharmProjects/pykdi_master_0_16_0/src
export KDI_TRACE=1
export KDI_AGREEMENT_CODE2KDI=VTK
export KDI_DICTIONARY_PATH=/home/lekienj/PyCharmProjects/pykdi_master_0_8_0/arcaninou/../src/pykdi/dictionary
export KDI_VTK_HDF_TRACE=1

rm /home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi.json
rm /home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi.vtkhdf

/home/lekienj/Projects/framework_build/lib/arcane_tests_exec -arcane_opt max_iteration 50 /home/lekienj/Projects/framework/arcane/tests/testHydro-1-vtkhdf.arc
