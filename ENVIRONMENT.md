**Auteur** : *Jacques-Bernard Lekien*, jacques-bernard.lekien@cea.fr

# KDI : **K**ameleon **D**ata **I**nterface

Des variables d'environment permettent de piloter de l'extérieur
l'exécution de la plate-forme KDI.

# Sommaire

- [KDI_PYTHON_LOG](#kdi_python_log)
- [VTK HDF](#vtkhdf)
  - [KDI_VTK_HDF_STEPS_CHUNK_SIZE](#kdi_vtk_hdf_steps_chunk_size)
  - [KDI_VTK_HDF_CHUNK_SIZE](#kdi_vtk_hdf_chunk_size)
  - [KDI_VTK_HDF_LOG](#kdi_vtk_hdf_log)
  - [KDI_VTK_HDF_TICTAC_LOG](#kdi_vtk_hdf_tictac_log)
  - [KDI_VTK_HDF_TICTAC_SUMMARY_LOG](#kdi_vtk_hdf_tictac_summary_log)
  - [KDI_VTK_HDF_CHECK](#kdi_vtk_hdf_check)

- [KDI_MEMORY_LOG](#kdi_memory_log)
- [KDI_CREATE_JSON_LOG](#kdi_create_json_log)
- [KDI_VTK_HTF_GARBAGE_COLLECTOR_LOG](#kdi_vtk_hdf_garbage_collector_log)

# <a name="#kdi_python_log"></a>KDI_PYTHON_LOG

La variable `KDI_PYTHON_LOG` permet de tracer les méthodes qui sont sollicitées
depuis l'interface C++. La trace commence par `PYTHON >>>`.

# <a name="#vtkhdf"></a>VTK HDF

Certaines variables d'environment concernent l'aspect VTK HDF.

## <a name="#kdi_vtk_hdf_steps_chunk_size"></a>KDI_VTK_HDF_STEPS_CHUNK_SIZE

La variable `KDI_VTK_HDF_STEPS_CHUNK_SIZE` permet de fixer la taille du chunk des
tableaux STEPS VTK HDF dont la valeur par défaut est ```1024``` d'éléments.
Cela correspond au dimensionnement des tableaux :
- [mesh/]STEPS/Values
- [mesh/]STEPS/NumberOfParts
- [mesh/]STEPS/PartOffsets
- [mesh/]STEPS/PointOffsets
- [mesh/]STEPS/CellOffsets
- [mesh/]STEPS/ConnectivityIdOffsets
- [mesh/]NumberOfPoints
- [mesh/]NumberOfCells
- [mesh/]ConnectivityIdOffsets

## <a name="#kdi_vtk_hdf_chunk_size"></a>KDI_VTK_HDF_CHUNK_SIZE

La variable `KDI_VTK_HDF_CHUNK_SIZE` permet de fixer la taille du chunk des
tableaux VTK HDF dont la valeur par défaut est ```1048576``` d'éléments (1024²).

## <a name="#kdi_vtk_hdf_log"></a>KDI_VTK_HDF_LOG

La variable `KDI_VTK_HDF_LOG` permet d'activer toutes les traces concernant
l'aspect VTK HDF.

## <a name="#kdi_vtk_hdf_tictac_log"></a>KDI_VTK_HDF_TICTAC_LOG

La variable `KDI_VTK_HDF_TICTAC_LOG` permet d'activer les traces fines
d'évaluation du temps passé concernant l'aspect VTK HDF.

## <a name="#kdi_vtk_hdf_tictac_summary_log"></a>KDI_VTK_HDF_TICTAC_SUMMARY_LOG

La variable `KDI_VTK_HDF_TICTAC_SUMMARY_LOG` permet d'activer les traces
cumulées d'évaluation du temps passé concernant l'aspect VTK HDF.

## <a name="#kdi_vtk_hdf_check_log"></a>KDI_VTK_HDF_CHECK_LOG

La variable `KDI_VTK_HDF_CHECK_LOG` permet d'activer les traces
lors de la validation du contenu de la description afin de détecter
toute non conformité (information qui ne peut être comprise) avec
l'aspect VTK HDF.
