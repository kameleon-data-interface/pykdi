import numpy as np

from pykdi import *

datas = {
    '/glu/version': np.array([1, 0]),
    '/study/name': 'MyEtude',
    '/study/date': '2024/05',
    '/mymesh/points/cartesianCoordinates': np.array([[0., 0., 0.], [1., 0., 0.], [1., 1., 0.], [0., 1., 0.], ]),
    '/mymesh/points/fields/myFieldPoint': np.array([0., 3., 2., 1.]),
    '/mymesh/cells/types': np.array([KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_TRIANGLE_TYPE_VALUE]),
    '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
    '/mymesh/cells/fields/myFieldCell': np.array([42, 69]),

    '/mymesh/cells/offsets': np.array([0, 3, 6]),
}


def GetDatas(base, name):
    if name == '/study/name':
        return np.array([datas[name]])
    if name == '/study/date':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return np.array(['{}/{:02d}'.format(datas[name], itps + 1)])
    if name == '/mymesh/points/cartesianCoordinates':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return np.array([[float(-itps) / 10., float(-itps) / 10., 0.], [1., 0., 0.], [1., 1., 0.], [0., 1., 0.], ])
    return datas[name]


def checked(msg, base, first=True):
    # if first:
    #    print('>>>>> ' + msg + ' current time')
    # else:
    #    print('>>>>> ' + msg + ' first time')
    ret = True
    for key in datas.keys():
        if isinstance(datas[key], str):
            if GetDatas(base, key) != base[key].get():
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDatas(base, key))
                print('----------')
        else:
            if not np.all(GetDatas(base, key) == base[key].get()):
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDatas(base, key))
                print('----------')
                assert (np.all(GetDatas(base, key) == base[key].get()))

    if not first:
        return

    current = base['/chunks/current']
    del base['/chunks/current']
    base['/chunks'].set({'step': 0., 'part': 0})
    checked(msg, base, False)
    base['/chunks/current'] = current

    return ret


def test_uns_1pe_1ssd_1uns_10tps():
    # == IN MEMORY =================================================
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 1})

    for itps in range(0, 10):
        print('################ ITPS:', itps)

        float_step = float(itps)

        base['/chunks'].set({})

        for key in ('/glu/version', '/study/name',):
            base[key].insert(GetDatas(base, key))

        base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step})

        key = '/study/date'
        base[key].insert(GetDatas(base, key))

        kdi_update(base, 'UnstructuredGrid', '/mymesh')

        base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

        for key in ('/mymesh/points/cartesianCoordinates', '/mymesh/cells/types',  '/mymesh/cells/connectivity',):
            base[key].insert(GetDatas(base, key))

        parent_field_key = '/mymesh/points/fields'
        for field_key in ('myFieldPoint',):
            kdi_update_fields(base, parent_field_key, field_key)
            key = parent_field_key + '/' + field_key
            base[key].insert(GetDatas(base, key))

        parent_field_key = '/mymesh/cells/fields'
        for field_key in ('myFieldCell',):
            kdi_update_fields(base, parent_field_key, field_key)
            key = parent_field_key + '/' + field_key
            base[key] .insert(GetDatas(base, key))

            kdi_update_eval_offsets(base, '/mymesh/cells')

        checked('Init by memory', base)

        # == SAVE VTK HDF + JSON, LOAD JSON ============================
        base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step})
        write_vtk_hdf(base, 'uns_1pe_1ssd_1uns_10tps_writer.vtkhdf')
        base = read_json('uns_1pe_1ssd_1uns_10tps_writer.json')

        # Set /chunks/current
        base['/chunks'].set({'step': float(itps), 'part': 0})
        checked('Init by load lazy VTK HDF by JSON', base)


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    test_uns_1pe_1ssd_1uns_10tps()
    print('\ntest_uns_1pe_1ssd_1uns_10tps: checked')
