import os
import time

from pykdi import kdi_get_env_log_reset, kdi_get_env, KDILog, KDILogElapse, kdi_get_env_int, kdi_get_env_str

"""
Fr: Ce test valide les mécanismes de récupération de la valeur d'une variable d'environnement
    mais aussi de la gestion des logs ou des logs elapse.

Use pykdi.tools:
- kdi_get_env_log_reset, kdi_get_env
- KDILog .log
- KDILogElapse .tic, .tac., .summary
"""

def test_kdi_env():
    assert (kdi_get_env_int('ENV_INT', 42) == 42)
    assert (kdi_get_env_str('ENV_INT', '42') == '42')
    os.environ['ENV_INT'] = str(12)
    assert (kdi_get_env_int('ENV_INT', 42) == 12)
    assert (kdi_get_env_str('ENV_INT', '42') == '12')
    del os.environ['ENV_INT']

    for option in ['0', '1']:  # '0' False, '1' True
        # With or without log on get_env
        os.environ['KDI_GET_ENV_LOG'] = option
        kdi_get_env_log_reset()  # only used in this test

        state = kdi_get_env('SOUS_LE_SOLEIL', False)
        assert (not state)  # cause default False

        state = kdi_get_env('SOUS_LE_SOLEIL', True)
        assert state  # cause default True

        os.environ['SOUS_LE_SOLEIL'] = '0'

        state = kdi_get_env('SOUS_LE_SOLEIL', False)
        assert (not state)  # cause var env 0

        state = kdi_get_env('SOUS_LE_SOLEIL', True)
        assert (not state)  # cause var env 0

        os.environ['SOUS_LE_SOLEIL'] = '1'

        state = kdi_get_env('SOUS_LE_SOLEIL', False)
        assert state  # cause var env 1

        state = kdi_get_env('SOUS_LE_SOLEIL', True)
        assert state  # cause var env 1

        del os.environ['SOUS_LE_SOLEIL']
    del os.environ['KDI_GET_ENV_LOG']


def test_kdi_log():
    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('FATAL ERROR CAUSE NO OUTPUT')

    kdi_log = KDILog('COUCOU', True)
    kdi_log.log('output A')  # cause default True

    os.environ['COUCOU'] = "1"

    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('FATAL ERROR cause name is not COUCOU')

    os.environ['COUCOU_LOG'] = "1"

    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('FATAL ERROR cause name is not COUCOU_LOG')

    os.environ['KDI_COUCOU'] = "1"

    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('FATAL ERROR cause name is not KDI_COUCOU')

    os.environ['KDI_COUCOU_LOG'] = "1"

    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('output B')  # cause name is KDI_COUCOU_LOG

    kdi_log = KDILog('COUCOU', True)
    kdi_log.log('output C')  # cause default True

    os.environ['KDI_LOG'] = "1"

    kdi_log = KDILog('COUCOU', False)
    kdi_log.log('output D')  # cause name is KDI_COUCOU_LOG

    kdi_log = KDILog('COCO', False)
    kdi_log.log('FATAL ERROR cause name not exist KDI_COCO_LOG')

    kdi_log = KDILog('COCO', True)
    kdi_log.log('output E')  # cause default True

    os.environ['KDI_LOG'] = "0"

    kdi_log = KDILog('KDI_LOG', False)
    kdi_log.log('FATAL ERROR cause False')

    assert (kdi_log.state == False)

    kdi_log.state = True
    kdi_log.log('output F')

    assert (kdi_log.state == True)

    kdi_log = KDILog('KDI_LOG', True)
    kdi_log.log('output G')  # cause default True

    assert (kdi_log.state == True)

    kdi_log.state = False
    kdi_log.log('FATAL ERROR cause False')

    assert (kdi_log.state == False)

    kdi_log = KDILog('KDI_LOG', True)
    kdi_log.log('msg')  # cause default True
    kdi_log.todo('msg todo')  # cause no optional
    kdi_log.warning('msg warning')  # cause no optional
    kdi_log.fatal_exception('msg fatal exception')  # cause no optional

    kdi_log = KDILog('KDI_LOG', False)
    kdi_log.log('FATAL ERROR cause False')
    kdi_log.todo('msg todo')  # cause no optional
    kdi_log.warning('msg warning')  # cause no optional
    kdi_log.fatal_exception('msg fatal exception')  # cause no optional

    del os.environ['COUCOU']
    del os.environ['COUCOU_LOG']
    del os.environ['KDI_COUCOU']
    del os.environ['KDI_COUCOU_LOG']
    del os.environ['KDI_LOG']


def test_kdi_log_elapse():
    print('phase A: nothing')
    kdi_log_elapse = KDILogElapse('VTK_HTG')

    kdi_log_elapse.tic('main')
    print('\n   No TIC cause no var env KDI_VTK_HTG_TICTAC_LOG')
    time.sleep(1)
    delta = kdi_log_elapse.tac('main')
    assert (delta is None)
    print('\n   No TAC cause no var env KDI_VTK_HTG_TICTAC_LOG')
    kdi_log_elapse.summary('main')
    kdi_log_elapse.summary()
    print('\n   No SUMMARY cause no var env KDI_VTK_HTG_TICTAC_SUMMARY_LOG')

    print('\nphase B: just tic and tac')
    os.environ['KDI_VTK_HTG_TICTAC_LOG'] = '1'
    kdi_log_elapse = KDILogElapse('VTK_HTG')
    kdi_log_elapse.tic('main')

    print('\n   TIC indicates:')
    print('      - # value of the current step\n')

    time.sleep(1)

    delta = kdi_log_elapse.tac('main')
    assert (0.99 < delta < 1.01)
    print('\n   TAC indicates')
    print('      - # value of the current step')
    print('      - current elapse on the last TIC-TAC')
    print('      - cumulative elapse on all TIC-TAC\n')

    kdi_log_elapse.summary('main')

    print('\n   No SUMMARY cause no var env KDI_VTK_HTG_TICTAC_SUMMARY_LOG')

    print('\nphase C: tic, tac and summary')
    os.environ['KDI_VTK_HTG_TICTAC_SUMMARY_LOG'] = '1'
    os.environ['KDI_VTK_HTG_TICTAC_LOG'] = '1'
    kdi_log_elapse = KDILogElapse('VTK_HTG')
    kdi_log_elapse.tic('main')
    time.sleep(1)
    delta = kdi_log_elapse.tac('main')
    assert (0.99 < delta < 1.01)
    print('phase C: specific summary \'main\'')
    sum = kdi_log_elapse.summary('main')
    assert (0.99 < sum < 1.01)
    print('phase C: general summary')
    sum = kdi_log_elapse.summary()
    assert (sum is None)  # cause a lot of keys

    print('\n   SUMMARY indicates')
    print('      - ## number of TIC-TAC')
    print('      - cumulative elapse on all TIC-TAC')
    print('      - average elapse on all TIC-TAC\n')

    print('phase D: just summary')
    os.environ['KDI_VTK_HTG_TICTAC_SUMMARY_LOG'] = '1'
    os.environ['KDI_VTK_HTG_TICTAC_LOG'] = '0'
    kdi_log_elapse = KDILogElapse('VTK_HTG')
    kdi_log_elapse.tic('main')
    time.sleep(1)
    delta = kdi_log_elapse.tac('main')
    assert (0.99 < delta < 1.01)
    sum = kdi_log_elapse.summary('main')
    assert (0.99 < sum < 1.01)
    print('phase D: just summary not named')
    kdi_log_elapse.summary()

    print('\nphase E:')
    os.environ['KDI_VTK_HTG_TICTAC_SUMMARY_LOG'] = '1'
    os.environ['KDI_VTK_HTG_TICTAC_LOG'] = '0'
    kdi_log_elapse = KDILogElapse('VTK_HTG')
    kdi_log_elapse.tic('main A')
    kdi_log_elapse.tic('main B')
    time.sleep(1)
    delta = kdi_log_elapse.tac('main A')
    assert (0.99 < delta < 1.01)
    time.sleep(1)
    delta = kdi_log_elapse.tac('main B')
    assert (1.99 < delta < 2.01)
    kdi_log_elapse.tic('main B')
    kdi_log_elapse.tic('main A')
    time.sleep(1)
    delta = kdi_log_elapse.tac('main B')
    assert (0.99 < delta < 1.01)
    delta = kdi_log_elapse.tac('main A')
    assert (0.99 < delta < 1.01)
    print('phase E: all summary (not named)')
    sum = kdi_log_elapse.summary()
    assert (sum is None)
    print('phase E: selected a summary (\'main B\')')
    sum = kdi_log_elapse.summary('main B')
    assert (2.99 < sum < 3.01)
    print('phase E: error on name')
    sum = kdi_log_elapse.summary('main')
    assert (sum is None)
    print('phase E: selected a summary (\'main A\')')
    sum = kdi_log_elapse.summary('main A')
    assert (1.99 < sum < 2.01)


if __name__ == '__main__':
    test_kdi_env()
    print('\ntest_kdi_env: checked\n')
    test_kdi_log()
    print('\ntest_kdi_log: checked\n')
    test_kdi_log_elapse()
    print('\ntest_kdi_log_elapse: checked')
