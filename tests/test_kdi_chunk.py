import numpy as np

from pykdi import (kdi_base,
                   KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT,
                   KDI_AGREEMENT_STEPPART,
                   KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART,
                   KDIMemory, KDIException, KDIProxyData)

"""
Fr: Ce test valide les différents modes de création d'une base ainsi que des erreurs qui peuvent être déclenchées.

Use pykdi:
- kdi_base
- KDIChunks .set

Use pykdi.agreement:
- KDI_AGREEMENT_STEPPART, KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART 
"""


def test_kdi_chunk_error():
    base = kdi_base()

    try:
        base['/chunks'].SetVariants(
            [['gloups', {'type': KDI_GROWNING_VARIANT, 'max': 4, }, ], ])
    except KDIException as e:
        assert (str(e.args[0]) == "pre: variant `gloups` must be have a key `dtype`")

    try:
        base['/chunks'].SetVariants(
            [['step', {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ], ])
    except KDIException as e:
        assert (str(e.args[0]) == "pre: not empty variants. Just a call to create the base!")

    base = kdi_base()

    try:
        base['/chunks'].SetVariants(
            [['beurk', {'type': KDI_CONSTANT_VARIANT, 'dtype': 'f8', }, ], ])
    except KDIException as e:
        assert (str(e.args[0]) == "pre: variant `beurk` must be have a key `max`")


def test_kdi_chunk():
    nb_parts = 4

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     avec une valeur CORRECTE associée à 'nb_parts' de cette configuration
    #     décrit sous la forme d'un entier
    base_4 = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': nb_parts})

    str_base_4_reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base_4) == str_base_4_reference)

    assert (4 == base_4['/chunks'].GetMaxConstantValue('part'))

    try:
        base_4['/chunks'].GetMaxConstantValue('choubi')
    except KDIException as e:
        assert(str(e.args[0]) == "pre: variant `choubi` is not a variant of the base (['step', 'part'])")

    assert (4 == base_4['/chunks'].GetAll('part'))
    assert (np.all(np.array([]) == np.array(base_4['/chunks'].GetAll('step'))))

    try:
        base_4['/chunks'].GetAll('choubi')
    except KDIException as e:
        assert(str(e.args[0]) == "pre: variant `choubi` is not a variant of the base (['step', 'part'])")

    float_step = 2.3
    int_part = 2

    base_4['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step, KDI_AGREEMENT_STEPPART_VARIANT_PART: int_part})

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_4['/chunks'].set([float_step, int_part])

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_4['/chunks'].set([float_step, float(int_part)])

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_1 = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 1})

    str_base_1_reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':1},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base_1) == str_base_1_reference)

    assert (1 == base_1['/chunks'].GetMaxConstantValue('part'))

    assert (np.all(np.array([]) == np.array(base_4['/chunks'].GetAll('step'))))

    base_1['truc'] = KDIMemory(base_1, 'truc')
    base_1['truc'].insert(654321, {'step': 2.3, 'part': 0,})

    assert ({'step': 0, 'part': 0,} == base_1['/chunks'].GetOffsets({'step': 2.3, 'part': 0,}))
    assert (0 == base_1['/chunks'].GetOffset('step', 2.3))
    assert (0 == base_1['/chunks'].GetOffset('part', 0))

    base_1['truc'].insert(654321, {'step': 2.3, 'part': 0,})
    base_1['truc'].insert(654322, {'step': 4.3, 'part': 0,})
    base_1['truc'].insert(654323, {'step': 6.3, 'part': 0,})

    assert ({'step': 1, 'part': 0,} == base_1['/chunks'].GetOffsets({'step': 4.3, 'part': 0,}))
    assert (1 == base_1['/chunks'].GetOffset('step', 4.3))
    assert (0 == base_1['/chunks'].GetOffset('part', 0))

    print(str(base_1['truc']))
    assert (str(base_1['truc']) == "KDIMemory(base=KDIBase, key_base='truc', variants=['step', 'part'], datas={'2.3':{'0':np.array(654321),},'4.3':{'0':np.array(654322),},'6.3':{'0':np.array(654323),},},)")

    assert (np.all(np.array([2.3, ]) == np.array(base_4['/chunks'].GetAll('step'))))

    base_1['trac'] = KDIMemory(base_1, 'trac')
    base_1['trac'].insert(np.array([6., 5., 4., 3., 2., 1.]), {'step': 2.3, 'part': 0,})

    assert (str(base_1['trac']) == "KDIMemory(base=KDIBase, key_base='trac', variants=['step', 'part'], datas={'2.3':{'0':np.array([6.0, 5.0, 4.0, 3.0, 2.0, 1.0]),},},)")

    try:
        print(base_1['truc'].get({'step': 2.3,}))
        assert (False)
    except KDIException:
        pass
    assert (base_1['truc'].get({'step': 2.3, 'part': 0,}) == 654321)

    try:
        print(base_1['trac'].get({'step': 2.3,}))
        assert (False)
    except KDIException:
        pass
    assert (np.all(base_1['trac'].get({'step': 2.3, 'part': 0,}) == np.array([6., 5., 4., 3., 2., 1.])))

    base_4['gluuu'] = KDIProxyData(base_4, base_1, '/glu', {'part': 0})

    try:
        base_4['gluuu'].insert(data=None, chunk=None)
        assert (False)
    except NotImplementedError:
        pass

    try:
        base_4['gluuu'].erase(chunk=None)
        assert (False)
    except NotImplementedError:
        pass

    assert ( base_4['gluuu'].get_complex_type() == 'Glu')

    base_4['truc'] = KDIProxyData(base_4, base_1, 'truc', {'part': 0})

    try:
        base_4['truc'].get_complex_type()
        assert (False)
    except NotImplementedError:
        pass

    for ipart in range(4):
        assert (base_4['truc'].get({'step': 2.3, 'part': ipart,}) == 654321)

    base_4['trac'] = KDIProxyData(base_4, base_1, 'trac', {'part': 0})

    for ipart in range(4):
        assert (np.all(base_4['trac'].get({'step': 2.3, 'part': ipart,}) == np.array([6., 5., 4., 3., 2., 1.])))
        assert (base_4['truc'].get({'step': 2.3, 'part': ipart,}) == 654321)

    try:
        base_4['tric'] = KDIProxyData(base_4, base_1, 'trac', {'part': 0, 'vulcain': 12})
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "KDIChunks:check_del_variants del_variants ({'part': 0, 'vulcain': 12}) incompatible avec self.order (['step', 'part'])")

    try:
        base_4['tric'] = KDIProxyData(base_4, base_1, 'trac', {'vulcain': 12})
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "KDIChunks:check_del_variants del_variants ({'vulcain': 12}) incompatible avec self.order (['step', 'part'])")


if __name__ == '__main__':
    test_kdi_chunk_error()
    test_kdi_chunk()
    print('\ntest_kdi_chunk: checked')
