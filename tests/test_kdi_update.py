from pykdi import (kdi_base, KDI_AGREEMENT_STEPPART, kdi_update, kdi_update_fields,
                   kdi_update_eval_offsets, kdi_update_sub,
                   KDIException)

"""
Fr: Ce test valide la creation des différents objets du dictionnaire

Use pykdi:
- kdi_base
- kdi_update
- kdi_update_sub
- kdi_update_fields
- kdi_update_eval_offsets

Use pykdi.agreement:
- KDI_AGREEMENT_STEPPART
"""


def evaluation(base, expected):
    if len(base.keys()) != expected:
        print('### ERROR ### Unexpected value ', str(len(base.keys())), ' (expected ', str(expected), ')')
        for key, value in base.items():
            print('  \'' + key + '\'', value)
        assert (len(base.keys()) == expected)


def test_kdi_update():
    # Fr: Création d'une base
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})

    assert (str(base['']) == "KDIComplexType('Base')")

    assert (str(base['/agreement']) ==
            "KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),)")

    assert (str(base['/glu']) == "KDIComplexType('Glu')")
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)")
    assert (str(base['/glu/name']) == "KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},)")

    assert (str(base['/study']) == "KDIComplexType('Study')")
    assert (str(base['/study/name']) == "KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},)")
    assert (str(base['/study/date']) == "KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},)")

    assert (str(base['/fields']) == "KDIFields({})")

    assert (str(base['/chunks']) ==
            "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")

    assert (str(base['/vtkhdf']) == "KDIComplexType('VTKHDF')")
    assert (str(base['/vtkhdf/version']) == "KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),)")
    assert (str(base['/vtkhdf/force_common_data_offsets']) == "True")

    assert (str(base['/code2kdi']) == "[ 0  1 13 13 13 13 13 13 13 13 13 13 12]")
    assert (str(base['/code2nbPoints']) == "[3 4 0 0 0 0 0 0 0 0 0 0 8]")

    evaluation(base, 15)

    try:
        kdi_update(base, 'TrucBidule', '/bla_bla')
    except KDIException as e:
        assert (str(e.args[0]) == 'Expected Chunks, Fields or SubMeshes instead of \'TrucBidule\'!')

    kdi_update(base, 'UnstructuredGrid', '/mymesh')

    assert (str(base['/mymesh']) == "KDIComplexType('UnstructuredGrid')")
    assert (str(base['/mymesh/points']) == "KDIComplexType('Points')")
    assert (str(base['/mymesh/points/cartesianCoordinates']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/points/cartesianCoordinates', variants=None, datas={},)")
    assert (str(base[
                    '/mymesh/points/globalIndexContinuous']) == "KDIMemory(base=KDIBase, key_base='/mymesh/points/globalIndexContinuous', variants=None, datas={},)")
    assert (str(base['/mymesh/points/fields']) == "KDIFields({})")
    assert (str(base['/mymesh/cells']) == "KDIComplexType('Cells')")
    assert (str(base['/mymesh/cells/types']) == "KDIMemory(base=KDIBase, key_base='/mymesh/cells/types', variants=None, datas={},)")
    assert (str(base[
                    '/mymesh/cells/connectivity']) == "KDIMemory(base=KDIBase, key_base='/mymesh/cells/connectivity', variants=None, datas={},)")
    assert (str(base[
                    '/mymesh/cells/globalIndexContinuous']) == "KDIMemory(base=KDIBase, key_base='/mymesh/cells/globalIndexContinuous', variants=None, datas={},)")
    assert (str(base['/mymesh/cells/fields']) == "KDIFields({})")
    assert (str(base['/mymesh/fields']) == "KDIFields({})")

    assert (len(base.keys()) == 26)

    try:
        kdi_update_fields(base, 'n_importe_quoi', 'myFieldPoint1')
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "pre: field_parent_name `n_importe_quoi` must be exist!")

    kdi_update_fields(base, '/mymesh/points/fields', 'myFieldPoint1')
    kdi_update_fields(base, '/mymesh/points/fields', 'myFieldPoint2')

    assert (str(base['/mymesh/points/fields']) ==
            "KDIFields({'/mymesh/points/fields/myFieldPoint1','/mymesh/points/fields/myFieldPoint2',})")
    assert (str(base['/mymesh/points/fields/myFieldPoint1']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/points/fields/myFieldPoint1', variants=None, datas={},)")
    assert (str(base['/mymesh/points/fields/myFieldPoint2']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/points/fields/myFieldPoint2', variants=None, datas={},)")

    assert (len(base.keys()) == 28)

    kdi_update_eval_offsets(base, '/mymesh/cells')

    assert (str(base['/mymesh/cells/offsets']) ==
            "KDIEvalBase64(base=KDIBase, key_base='/mymesh/cells/offsets', lines='CmJhc2UgPSBCYXNlCmNvZGUybmJQb2ludHMgPSBQYXJhbXNbMF0KdHlwZXNDb2RlID0gYmFzZVtQYXJhbXNbMV1dLmdldCgpCmlmIHR5cGVzQ29kZS5kdHlwZSA9PSAnfFMxJzoKICAgIG5iUG9pbnRzID0gY29kZTJuYlBvaW50c1t0eXBlc0NvZGUudmlldyhkdHlwZT1ucC5pbnQ4KV0KZWxzZToKICAgIG5iUG9pbnRzID0gY29kZTJuYlBvaW50c1t0eXBlc0NvZGVdClJldCA9IG5wLmVtcHR5KChsZW4obmJQb2ludHMpICsgMSksIGR0eXBlPW5wLmludDY0KQpSZXRbMF0gPSAwCm5wLmN1bXN1bShuYlBvaW50cywgb3V0PVJldFsxOl0pCg==', params=[array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8), '/mymesh/cells/types'])")

    assert (len(base.keys()) == 29)

    kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell1')
    kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell2')

    assert (str(base['/mymesh/cells/fields']) ==
            "KDIFields({'/mymesh/cells/fields/myFieldCell1','/mymesh/cells/fields/myFieldCell2',})")
    assert (str(base['/mymesh/cells/fields/myFieldCell1']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/cells/fields/myFieldCell1', variants=None, datas={},)")
    assert (str(base['/mymesh/cells/fields/myFieldCell2']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/cells/fields/myFieldCell2', variants=None, datas={},)")

    assert (len(base.keys()) == 31)

    try:
        kdi_update_sub(base, 'n_importe_quoi', '/mymil1')
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "pre: mesh_parent_name 'n_importe_quoi' must be exist!")

    try:
        kdi_update_sub(base, '/mymesh/cells/fields/myFieldCell1', '/mymil1')
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "pre: mesh_parent_name '/mymesh/cells/fields/myFieldCell1' must be an ComplexType!")

    try:
        kdi_update_sub(base, '', '/mymil1')
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "pre: mesh_parent_name '' must be an UnstructuredGrid (not Base)!")

    try:
        kdi_update(base, 'SubMeshes', '/mymesh/bla_bla')
        asssert (False)
    except KDIException as e:
        assert (e.args[0] == 'An user can only instantiate an object at the root level. ' +
                'In fact, the name of the latter must necessarily begin with \'/\' and ' +
                'not contain other characters of this type.')

    try:
        kdi_update(None, 'SubMeshes', '/mymesh/submeshes')
        asssert (False)
    except KDIException as e:
        assert (e.args[0] == 'pre: the base must be created before defining object instances')

    try:
        kdi_update(base, 'SubMeshes', '')
        asssert (False)
    except KDIException as e:
        assert (e.args[0] == 'pre: the key name cannot be the empty string.')

    str_my_mil1 = kdi_update_sub(base, '/mymesh', '/mymil1')

    assert (str(base['/mymesh/submeshes']) == "KDISubMeshes({'/mymesh/submeshes/mymil1',})")

    assert (str_my_mil1 == '/mymesh/submeshes/mymil1')

    assert (str(base[str_my_mil1]) == "KDIComplexType('SubUnstructuredGrid')")
    assert (str(base[str_my_mil1 + '/indexescells']) ==
            "KDIMemory(base=KDIBase, key_base=\'" + str_my_mil1 + "/indexescells\', variants=None, datas={},)")
    assert (str(base[str_my_mil1 + '/cells']) == "KDIComplexType('Cells')")
    assert (str(base[str_my_mil1 + '/cells/fields']) == "KDIFields({})")
    assert (str(base[str_my_mil1 + '/fields']) == "KDIFields({})")

    assert (len(base.keys()) == 37)

    kdi_update_fields(base, str_my_mil1 + '/cells/fields', 'loc_myFieldCell3')
    kdi_update_fields(base, str_my_mil1 + '/cells/fields', 'loc_myFieldCell4')
    assert (str(base[str_my_mil1 + '/cells/fields']) ==
            "KDIFields({'/mymesh/submeshes/mymil1/cells/fields/loc_myFieldCell3','/mymesh/submeshes/mymil1/cells/fields/loc_myFieldCell4',})")
    assert (str(base[str_my_mil1 + '/cells/fields/loc_myFieldCell3']) ==
            "KDIMemory(base=KDIBase, key_base='" + str_my_mil1 + "/cells/fields/loc_myFieldCell3', variants=None, datas={},)")
    assert (str(base[str_my_mil1 + '/cells/fields/loc_myFieldCell4']) ==
            "KDIMemory(base=KDIBase, key_base='" + str_my_mil1 + "/cells/fields/loc_myFieldCell4', variants=None, datas={},)")

    assert (len(base.keys()) == 39)
    str_my_mil2 = kdi_update_sub(base, '/mymesh', '/mymil2')

    assert (str(base['/mymesh/submeshes']) == "KDISubMeshes({'/mymesh/submeshes/mymil1','/mymesh/submeshes/mymil2',})")

    assert (str_my_mil2 == '/mymesh/submeshes/mymil2')

    assert (str(base[str_my_mil2]) == "KDIComplexType('SubUnstructuredGrid')")
    assert (str(base[str_my_mil2 + '/indexescells']) ==
            "KDIMemory(base=KDIBase, key_base='" + str_my_mil2 + "/indexescells', variants=None, datas={},)")
    assert (str(base[str_my_mil2 + '/cells']) == "KDIComplexType('Cells')")
    assert (str(base[str_my_mil2 + '/cells/fields']) == "KDIFields({})")
    assert (str(base[str_my_mil2 + '/fields']) == "KDIFields({})")

    assert (len(base.keys()) == 44)

    kdi_update_fields(base, '/mymesh/submeshes/mymil2/cells/fields', 'loc_myFieldCell5')
    assert (str(base["/mymesh/submeshes/mymil2/cells/fields"]) ==
            "KDIFields({'/mymesh/submeshes/mymil2/cells/fields/loc_myFieldCell5',})")
    assert (str(base['/mymesh/submeshes/mymil2/cells/fields/loc_myFieldCell5']) ==
            "KDIMemory(base=KDIBase, key_base='/mymesh/submeshes/mymil2/cells/fields/loc_myFieldCell5', variants=None, datas={},)")

    evaluation(base, 45)


if __name__ == '__main__':
    test_kdi_update()
    print('\ntest_kdi_update: checked')
