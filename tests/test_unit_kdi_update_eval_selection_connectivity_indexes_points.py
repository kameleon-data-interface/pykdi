import time
import numpy as np

from pykdi import KDI_SIZE


def test_unit_kdi_update_eval_selection_connectivity_indexes_points():
    SIMULATION_TRIANGLE_TYPE_VALUE = 11
    SIMULATION_QUADRANGLE_TYPE_VALUE = 12
    SIMULATION_HEXAHEDRON_TYPE_VALUE = 13

    KDI_TRIANGLE = 0
    KDI_QUADRANGLE = 1
    KDI_HEXAHEDRON = 12

    class KDIMemoryMOC:
        __slots__ = '_datas'
        def __init__(self, datas=None):
            if datas is None:
                self._datas = np.full(shape=(2500000,), fill_value=SIMULATION_HEXAHEDRON_TYPE_VALUE, dtype=np.int8)
                return
            self._datas = datas

        def get(self):
            return self._datas


    command_eval_selection_connectivty_indexes_points = """
# Begin coding
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points')
base = Base
print('     ', '/chunks/current',' in base:', str('/chunks/current' in base))
chunk = base['/chunks/current']
print('  IN base')
# indexes
indexesbase = Params[0]
print('  IN /chunks/current')
print('     /chunks/current in indexesbase:', str('/chunks/current' in indexesbase))
indexesbasechunk = None
if '/chunks/current' in indexesbase:
    indexesbasechunk = indexesbase['/chunks/current']
indexesbase['/chunks/current'] = chunk
indexescellskey = Params[1]
print('  IN indexescellskey:', indexescellskey)
indexescellvalues = indexesbase[indexescellskey].get()
print('     indexescellvalues:', indexescellvalues)
if indexesbasechunk:
    indexesbase['/chunks/current'] = indexesbasechunk
# in values
inbase = Params[2]
inbasechunk = inbase['/chunks/current']
inbase['/chunks/current'] = chunk
intypeskey = Params[3]
print('  IN intypeskey:', intypeskey)
intypesvalues = inbase[intypeskey].get()
print('     intypesvalues:', intypesvalues)
inconnectivitykey = Params[4]
print('  IN inconnectivitykey:', inconnectivitykey)
inconnectivityvalues = inbase[inconnectivitykey].get()
print('     inconnectivityvalues:', inconnectivityvalues)
inbase['/chunks/current'] = inbasechunk
# code2nbPoints
code2nbPoints = Params[5]
print('  IN code2nbPoints:', code2nbPoints)
# out key
outconnectivitykey = Params[6]
print('  IN outconnectivitykey:', outconnectivitykey)
outindexespointskey = Params[7]
print('  IN outindexespointskey:', outindexespointskey)
outconnectivity = np.zeros(shape=(inconnectivityvalues.shape[0],), dtype=inconnectivityvalues.dtype)
points = {}
OUTpoints = []
crt_INindexescellvalues = 0
crt_INconnectivity = 0
crt_OUTconnectivity = 0
crt_OUTpoints = 0
for icell, intypevalue in enumerate(intypesvalues):
    nbPoints = code2nbPoints[intypevalue]
    if indexescellvalues[crt_INindexescellvalues] != icell:
        #print('  ', False, '(', indexescellvalues[crt_INindexescellvalues], ')')
        crt_INconnectivity += nbPoints
        #print('  crt_INconnectivity:', crt_INconnectivity)
    else:
        #print('  ', True, '(', indexescellvalues[crt_INindexescellvalues], ')')
        crt_INindexescellvalues += 1
        #print('  crt_INindexescellvalues:', crt_INindexescellvalues)
        end_INconnectivity = crt_INconnectivity + nbPoints
        #print('  crt_INconnectivity:', crt_INconnectivity)
        #print('  end_INconnectivity:', end_INconnectivity)
        while crt_INconnectivity != end_INconnectivity:
            iPoint = inconnectivityvalues[crt_INconnectivity]
            #print('  iPoint:', iPoint)
            if iPoint not in points:
                points[iPoint] = crt_OUTpoints
                OUTpoints.append(iPoint)
                iPoint = crt_OUTpoints
                crt_OUTpoints += 1
            else:
                iPoint = points[iPoint]
            outconnectivity[crt_OUTconnectivity] = iPoint
            crt_INconnectivity += 1
            #print('  crt_INconnectivity', crt_INconnectivity)
            crt_OUTconnectivity += 1
            #print('  crt_OUTconnectivity:', crt_OUTconnectivity)
    if crt_INindexescellvalues == indexescellvalues.shape[0]:
        break
outconnectivity = np.resize(outconnectivity, crt_OUTconnectivity)
Ret = {outconnectivitykey: outconnectivity, outindexespointskey: np.array(OUTpoints), }
print('  OUT Ret:', Ret)
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points terminated')"""

    SIMULATION_TRIANGLE_TYPE_VALUE = 11
    SIMULATION_QUADRANGLE_TYPE_VALUE = 12
    SIMULATION_HEXAHEDRON_TYPE_VALUE = 13

    KDI_TRIANGLE = 0
    KDI_QUADRANGLE = 1
    KDI_HEXAHEDRON = 12

    code2kdi = {
        SIMULATION_TRIANGLE_TYPE_VALUE: KDI_TRIANGLE,
        SIMULATION_QUADRANGLE_TYPE_VALUE: KDI_QUADRANGLE,
        SIMULATION_HEXAHEDRON_TYPE_VALUE: KDI_HEXAHEDRON }

    kdi2nbPoints = {KDI_TRIANGLE: 3, KDI_QUADRANGLE: 4, KDI_HEXAHEDRON: 8}

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/chunks/current': {},
        '/indexes': KDIMemoryMOC(np.array([i for i in range(15, 912343, 2)])),
        '/types_in': KDIMemoryMOC(np.array([SIMULATION_HEXAHEDRON_TYPE_VALUE for i in range(1000000)])),
        '/connectivity_in': KDIMemoryMOC(np.array([i  for _ in range(1000000) for i in range(0, 8, 1)])),
    }

    code2nbPoints = {}
    for key, val in CurrentBase['/code2kdi'].items():
        code2nbPoints[key] = kdi2nbPoints[val]

    CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/types_in', '/connectivity_in',
                     code2nbPoints, '/outconnectivitykey', '/outindexespointskey',]

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_selection_connectivty_indexes_points, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'])
    from hashlib import sha1
    print(sha1(variables['Ret']['/outconnectivitykey']).hexdigest())
    assert (sha1(variables['Ret']['/outconnectivitykey']).hexdigest() == 'ed3786b00b475cb7c9c9f0c465835d780dde99d4')
    print(sha1(variables['Ret']['/outindexespointskey']).hexdigest())
    assert (sha1(variables['Ret']['/outindexespointskey']).hexdigest() == '786cc70d505bbd1783dc08700b8bf06eb057f422')

    # initial 0.98

    print('------------------------')

    command_eval_selection_connectivty_indexes_points_2 = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points 2')
out_base = Base
## assert ('/chunks/current' in out_base)
chunk = out_base['/chunks/current']
del out_base
# indexes
indexes_base = Params[0]
indexes_base_chunk = None
if '/chunks/current' in indexes_base:
    indexes_base_chunk = indexes_base['/chunks/current']
indexes_base['/chunks/current'] = chunk
indexes_cells_key = Params[1]
## assert (indexes_cells_key in indexes_base)
indexes_cells_values = indexes_base[indexes_cells_key].get()
if indexes_base_chunk is None:
    del indexes_base['/chunks/current']
else:
    indexes_base['/chunks/current'] = indexes_base_chunk
del indexes_cells_key, indexes_base_chunk, indexes_base
# in values
in_base = Params[2]
## assert ('/chunks/current' in in_base)
in_base_chunk = in_base['/chunks/current']
in_base['/chunks/current'] = chunk
in_types_key = Params[3]
## assert (in_types_key in in_base)
in_types_values = in_base[in_types_key].get()
in_connectivity_key = Params[4]
## assert (in_connectivity_key in in_base)
in_connectivity_values = in_base[in_connectivity_key].get()
in_base['/chunks/current'] = in_base_chunk
del in_types_key, in_connectivity_key, in_base_chunk, in_base
# code2nbPoints
code2nbPoints = Params[5]
# out key
out_connectivity_key = Params[6]
out_indexes_points_key = Params[7]
# compute in_nbpoints by cells and in_offsets
vfunc = np.vectorize(lambda x: code2nbPoints[x], otypes=[np.int64])
in_nbpoints = vfunc(in_types_values)
# ????? NO del code2nbPoints
in_offsets = np.concatenate([[0], np.cumsum(in_nbpoints)])
# compute out_nbpoints, out_nbconnectivity
out_nbpoints = in_nbpoints[indexes_cells_values]
del in_nbpoints
out_nbconnectivity = np.sum(out_nbpoints)
# compute out_connectivity_values with old indexes on points
out_connectivity_values_with_old = np.empty((out_nbconnectivity,), dtype=np.int64)
del out_nbconnectivity
begin = 0
for out_index_cell, in_index_cell in enumerate(indexes_cells_values):
    nbpoints = out_nbpoints[out_index_cell]
    end = begin + nbpoints
    begin2 = in_offsets[in_index_cell]
    end2 = begin2 + nbpoints
    out_connectivity_values_with_old[begin:end] = in_connectivity_values[begin2:end2]
    begin = end
del begin, end, begin2, end2, nbpoints, out_index_cell, in_index_cell
# actually, out_connectivity_with_in but with old indexes points
# compute out_indexes_points_values
out_indexes_points_values = np.array(list(set(out_connectivity_values_with_old)))
# compute old index to (new) index points
oldtonewindexpoints = {}
for out_pt, in_pt in enumerate(out_indexes_points_values):
    oldtonewindexpoints[in_pt] = out_pt
# compute out_connectivity_values with (new) indexes on points
vfunc = np.vectorize(lambda x: oldtonewindexpoints[x], otypes=[np.int64])
out_connectivity_values = vfunc(out_connectivity_values_with_old)
# return results
Ret = {out_connectivity_key: out_connectivity_values, out_indexes_points_key: out_indexes_points_values, }
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points terminated')"""

    CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/types_in', '/connectivity_in',
                     code2nbPoints, '/outconnectivitykey', '/outindexespointskey',]

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_selection_connectivty_indexes_points_2, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'])
    from hashlib import sha1
    print(sha1(variables['Ret']['/outconnectivitykey']).hexdigest())
    assert (sha1(variables['Ret']['/outconnectivitykey']).hexdigest() == 'ed3786b00b475cb7c9c9f0c465835d780dde99d4')
    print(sha1(variables['Ret']['/outindexespointskey']).hexdigest())
    assert (sha1(variables['Ret']['/outindexespointskey']).hexdigest() == '786cc70d505bbd1783dc08700b8bf06eb057f422')

    # Discriminant case

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/chunks/current': {},
        '/indexes': KDIMemoryMOC(np.array([0, 6, 4, 2, 7,])),
        '/types_in': KDIMemoryMOC(np.array([
            SIMULATION_TRIANGLE_TYPE_VALUE,
            SIMULATION_QUADRANGLE_TYPE_VALUE,
            SIMULATION_HEXAHEDRON_TYPE_VALUE,
            SIMULATION_TRIANGLE_TYPE_VALUE,
            SIMULATION_QUADRANGLE_TYPE_VALUE,
            SIMULATION_HEXAHEDRON_TYPE_VALUE,
            SIMULATION_TRIANGLE_TYPE_VALUE,
            SIMULATION_QUADRANGLE_TYPE_VALUE,
            SIMULATION_HEXAHEDRON_TYPE_VALUE])),
        '/connectivity_in': KDIMemoryMOC(np.array([
            3, 2, 1, ##
            0, 2, 5, 3,
            4, 3, 2, 6, 9, 7, 8, 7, ##
            13, 12, 11,
            10, 12, 15, 13, ##
            14, 13, 12, 16, 19, 17, 18, 17,
            23, 22, 21, ##
            20, 22, 25, 23, ##
            24, 23, 22, 26, 29, 27, 28, 27,
        ])),
    }

    # [ 3, 2, 1,
    #   23, 22, 21,
    #   10, 12, 15, 13,
    #   4, 3, 2, 6, 9, 7, 8, 7,
    #   20, 22, 25, 23, ]
    #
    # [ 0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 13, 14, 15, 16,] NEW
    # [ 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13, 15, 20, 21, 22, 23, 25,] OLD
    #
    # [ 2  1  0
    #   15 14 13
    #   8  9 11 10
    #   3  2  1  4  7  5  6  5
    #   12 14 16 15]

    CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/types_in', '/connectivity_in',
                     code2nbPoints, '/outconnectivitykey', '/outindexespointskey',]

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_selection_connectivty_indexes_points_2, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'])
    from hashlib import sha1
    print(variables['Ret']['/outconnectivitykey'])
    print(sha1(variables['Ret']['/outconnectivitykey']).hexdigest())
    assert (sha1(variables['Ret']['/outconnectivitykey']).hexdigest() == 'e3c98dc54c4c905ed58e3a7afd43e95f5d8da10f')
    print(variables['Ret']['/outindexespointskey'])
    print(sha1(variables['Ret']['/outindexespointskey']).hexdigest())
    assert (sha1(variables['Ret']['/outindexespointskey']).hexdigest() == '9a56d6f47f105e43ce7b188c13da2826f0db7ace')

    print('------------------------')

    command_eval_selection_connectivty_indexes_points_3 = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points 3')
tic = time.time()
out_base = Base
## assert ('/chunks/current' in out_base)
chunk = out_base['/chunks/current']
# indexes
indexes_base = Params[0]
indexes_base_chunk = None
if '/chunks/current' in indexes_base:
    indexes_base_chunk = indexes_base['/chunks/current']
indexes_base['/chunks/current'] = chunk
indexes_cells_key = Params[1]
## assert (indexes_cells_key in indexes_base)
indexes_cells_values = indexes_base[indexes_cells_key].get()
if indexes_base_chunk is None:
    del indexes_base['/chunks/current']
else:
    indexes_base['/chunks/current'] = indexes_base_chunk
# in values
in_base = Params[2]
## assert ('/chunks/current' in in_base)
in_base_chunk = in_base['/chunks/current']
in_base['/chunks/current'] = chunk
in_types_key = Params[3]
## assert (in_types_key in in_base)
in_types_values = in_base[in_types_key].get()
in_connectivity_key = Params[4]
## assert (in_connectivity_key in in_base)
in_connectivity_values = in_base[in_connectivity_key].get()
in_base['/chunks/current'] = in_base_chunk
# code2nbPoints
code2nbPoints = Params[5]
# out key
out_connectivity_key = Params[6]
out_indexes_points_key = Params[7]
in_nbpoints = code2nbPoints[in_types_values]
in_offsets = np.empty((len(in_nbpoints)+1), dtype=np.int64)
in_offsets[0] = 0
np.cumsum(in_nbpoints, out=in_offsets[1:])
# compute out_nbpoints, out_nbconnectivity
out_nbpoints = in_nbpoints[indexes_cells_values]
out_nbconnectivity = np.sum(out_nbpoints)
print('Time step 1: ', time.time() - tic)

tic = time.time()
# compute out_connectivity_values with old indexes on points
out_connectivity_values_with_old = np.empty((out_nbconnectivity,), dtype=np.int64)
begin = np.int64(0)
for out_index_cell, in_index_cell in enumerate(indexes_cells_values):
    #assert (0 <= out_index_cell < len(out_nbpoints))
    nbpoints = out_nbpoints[out_index_cell]
    end = begin + nbpoints
    #assert (0 <= in_index_cell < len(in_offsets))
    begin2 = in_offsets[in_index_cell]
    end2 = begin2 + nbpoints
    #assert (0 <= begin <= len(out_connectivity_values_with_old))
    #assert (0 <= end <= len(out_connectivity_values_with_old))
    #assert (0 <= begin2 <= len(in_connectivity_values))
    #assert (0 <= end2 <= len(in_connectivity_values))
    out_connectivity_values_with_old[begin:end] = in_connectivity_values[begin2:end2]
    begin = end
assert (begin == out_nbconnectivity)
print('Time step 2: ', time.time() - tic)

tic = time.time()
# actually, out_connectivity_with_in but with old indexes points
# compute out_indexes_points_values
out_indexes_points_values = np.array(list(set(out_connectivity_values_with_old)))
print('Time step 3: ', time.time() - tic)

tic = time.time()
# compute old index to (new) index points
oldtonewindexpoints = np.empty(out_indexes_points_values.max() + 1, dtype=np.int64)
for out_pt, in_pt in enumerate(out_indexes_points_values):
    oldtonewindexpoints[in_pt] = out_pt
# compute out_connectivity_values with (new) indexes on points
out_connectivity_values = oldtonewindexpoints[out_connectivity_values_with_old]
print('Time step 4: ', time.time() - tic)

# return results
Ret = {out_connectivity_key: out_connectivity_values, out_indexes_points_key: out_indexes_points_values, }
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points terminated 3')"""

    def RunTestGros(command):
        code2kdi = np.full(shape=15, fill_value=0, dtype=np.int8)
        assert (SIMULATION_TRIANGLE_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_TRIANGLE_TYPE_VALUE] = KDI_TRIANGLE
        assert (SIMULATION_QUADRANGLE_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_QUADRANGLE_TYPE_VALUE] = KDI_QUADRANGLE
        assert (SIMULATION_HEXAHEDRON_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_HEXAHEDRON_TYPE_VALUE] = KDI_HEXAHEDRON

        CurrentBase = {
            '/code2kdi': code2kdi,
            '/chunks/current': {},
            '/indexes': KDIMemoryMOC(np.array([i for i in range(15, 912343, 2)], dtype=np.int64)),
            '/types_in': KDIMemoryMOC(np.array([SIMULATION_HEXAHEDRON_TYPE_VALUE for i in range(1000000)], dtype=np.int8)),
            '/connectivity_in': KDIMemoryMOC(np.array([i  for _ in range(1000000) for i in range(0, 8, 1)], dtype=np.int64)),
        }

        kdi2nbPoints = np.full(shape=KDI_SIZE, fill_value=0, dtype=np.int8)
        assert(KDI_TRIANGLE < len(kdi2nbPoints))
        kdi2nbPoints[KDI_TRIANGLE] = 3
        assert(KDI_QUADRANGLE < len(kdi2nbPoints))
        kdi2nbPoints[KDI_QUADRANGLE] = 4
        assert(KDI_HEXAHEDRON < len(kdi2nbPoints))
        kdi2nbPoints[KDI_HEXAHEDRON] = 8

        code2nbPoints = kdi2nbPoints[CurrentBase['/code2kdi']]

        CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/types_in', '/connectivity_in',
                         code2nbPoints, '/outconnectivitykey', '/outindexespointskey',]

        tic = time.time()

        variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
        try:
            variables.update(dict(locals(), **globals()))
            exec(command, variables, variables)
        except Exception as e:
            raise Exception('KDI Exception Error in execution exec!')

        print('Time: ', time.time() - tic)

        print(variables['Ret'])
        from hashlib import sha1
        print(sha1(variables['Ret']['/outconnectivitykey']).hexdigest())
        assert (sha1(variables['Ret']['/outconnectivitykey']).hexdigest() == 'ed3786b00b475cb7c9c9f0c465835d780dde99d4')
        print(sha1(variables['Ret']['/outindexespointskey']).hexdigest())
        assert (sha1(variables['Ret']['/outindexespointskey']).hexdigest() == '786cc70d505bbd1783dc08700b8bf06eb057f422')

    # Time step 1:  0.0030488967895507812
    # Time step 2:  0.2431166172027588
    # Time step 3:  0.0990152359008789
    # Time step 4:  0.004918575286865234
    # Time:  0.3504645824432373

    # Discriminant case

    def RunTestPrecis(command):
        code2kdi = np.full(shape=15, fill_value=0, dtype=np.int8)
        assert (SIMULATION_TRIANGLE_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_TRIANGLE_TYPE_VALUE] = KDI_TRIANGLE
        assert (SIMULATION_QUADRANGLE_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_QUADRANGLE_TYPE_VALUE] = KDI_QUADRANGLE
        assert (SIMULATION_HEXAHEDRON_TYPE_VALUE < len(code2kdi))
        code2kdi[SIMULATION_HEXAHEDRON_TYPE_VALUE] = KDI_HEXAHEDRON

        CurrentBase = {
            '/code2kdi': code2kdi,
            '/chunks/current': {},
            '/indexes': KDIMemoryMOC(np.array([0, 6, 4, 2, 7, ])),
            '/types_in': KDIMemoryMOC(np.array([
                SIMULATION_TRIANGLE_TYPE_VALUE,
                SIMULATION_QUADRANGLE_TYPE_VALUE,
                SIMULATION_HEXAHEDRON_TYPE_VALUE,
                SIMULATION_TRIANGLE_TYPE_VALUE,
                SIMULATION_QUADRANGLE_TYPE_VALUE,
                SIMULATION_HEXAHEDRON_TYPE_VALUE,
                SIMULATION_TRIANGLE_TYPE_VALUE,
                SIMULATION_QUADRANGLE_TYPE_VALUE,
                SIMULATION_HEXAHEDRON_TYPE_VALUE])),
            '/connectivity_in': KDIMemoryMOC(np.array([
                3, 2, 1,  ##
                0, 2, 5, 3,
                4, 3, 2, 6, 9, 7, 8, 7,  ##
                13, 12, 11,
                10, 12, 15, 13,  ##
                14, 13, 12, 16, 19, 17, 18, 17,
                23, 22, 21,  ##
                20, 22, 25, 23,  ##
                24, 23, 22, 26, 29, 27, 28, 27,
            ])),
        }

        # [ 3, 2, 1,
        #   23, 22, 21,
        #   10, 12, 15, 13,
        #   4, 3, 2, 6, 9, 7, 8, 7,
        #   20, 22, 25, 23, ]
        #
        # [ 0, 1, 2, 3, 4, 5, 6, 7,  8,  9, 10, 11, 12, 13, 14, 15, 16,] NEW
        # [ 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13, 15, 20, 21, 22, 23, 25,] OLD
        #
        # [ 2  1  0
        #   15 14 13
        #   8  9 11 10
        #   3  2  1  4  7  5  6  5
        #   12 14 16 15]

        kdi2nbPoints = np.full(shape=KDI_SIZE, fill_value=0, dtype=np.int8)
        assert(KDI_TRIANGLE < len(kdi2nbPoints))
        kdi2nbPoints[KDI_TRIANGLE] = 3
        assert(KDI_QUADRANGLE < len(kdi2nbPoints))
        kdi2nbPoints[KDI_QUADRANGLE] = 4
        assert(KDI_HEXAHEDRON < len(kdi2nbPoints))
        kdi2nbPoints[KDI_HEXAHEDRON] = 8

        code2nbPoints = kdi2nbPoints[CurrentBase['/code2kdi']]

        CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/types_in', '/connectivity_in',
                         code2nbPoints, '/outconnectivitykey', '/outindexespointskey',]

        tic = time.time()

        variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
        try:
            variables.update(dict(locals(), **globals()))
            exec(command, variables, variables)
        except Exception as e:
            raise Exception('KDI Exception Error in execution exec!')

        print('Time: ', time.time() - tic)

        print(variables['Ret'])
        from hashlib import sha1
        print(variables['Ret']['/outconnectivitykey'])
        print(sha1(variables['Ret']['/outconnectivitykey']).hexdigest())
        assert (sha1(variables['Ret']['/outconnectivitykey']).hexdigest() == 'e3c98dc54c4c905ed58e3a7afd43e95f5d8da10f')
        print(variables['Ret']['/outindexespointskey'])
        print(sha1(variables['Ret']['/outindexespointskey']).hexdigest())
        assert (sha1(variables['Ret']['/outindexespointskey']).hexdigest() == '9a56d6f47f105e43ce7b188c13da2826f0db7ace')

    RunTestGros(command_eval_selection_connectivty_indexes_points_3)
    RunTestPrecis(command_eval_selection_connectivty_indexes_points_3)

    print('------------------------')

    command_eval_selection_connectivty_indexes_points_4 = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points 4')
tic = time.time()
out_base = Base
## assert ('/chunks/current' in out_base)
chunk = out_base['/chunks/current']
# indexes
indexes_base = Params[0]
indexes_base_chunk = None
if '/chunks/current' in indexes_base:
    indexes_base_chunk = indexes_base['/chunks/current']
indexes_base['/chunks/current'] = chunk
indexes_cells_key = Params[1]
## assert (indexes_cells_key in indexes_base)
indexes_cells_values = indexes_base[indexes_cells_key].get()
if indexes_base_chunk is None:
    del indexes_base['/chunks/current']
else:
    indexes_base['/chunks/current'] = indexes_base_chunk
# in values
in_base = Params[2]
## assert ('/chunks/current' in in_base)
in_base_chunk = in_base['/chunks/current']
in_base['/chunks/current'] = chunk
in_types_key = Params[3]
## assert (in_types_key in in_base)
in_types_values = in_base[in_types_key].get()
in_connectivity_key = Params[4]
## assert (in_connectivity_key in in_base)
in_connectivity_values = in_base[in_connectivity_key].get()
in_base['/chunks/current'] = in_base_chunk
# code2nbPoints
code2nbPoints = Params[5]
# out key
out_connectivity_key = Params[6]
out_indexes_points_key = Params[7]
in_nbpoints = code2nbPoints[in_types_values]
in_offsets = np.empty((len(in_nbpoints)+1), dtype=np.int64)
in_offsets[0] = 0
np.cumsum(in_nbpoints, out=in_offsets[1:])
# compute out_nbpoints, out_nbconnectivity
out_nbpoints = in_nbpoints[indexes_cells_values]
out_nbconnectivity = np.sum(out_nbpoints)
print('Time step 1: ', time.time() - tic)

tic = time.time()
# compute out_connectivity_values with old indexes on points
out_connectivity_values_with_old = np.empty((out_nbconnectivity,), dtype=np.int64)

begin = np.int64(0)
for out_index_cell, in_index_cell in enumerate(indexes_cells_values):
    #assert (0 <= out_index_cell < len(out_nbpoints))
    nbpoints = out_nbpoints[out_index_cell]
    end = begin + nbpoints
    #assert (0 <= in_index_cell < len(in_offsets))
    begin2 = in_offsets[in_index_cell]
    end2 = begin2 + nbpoints
    #assert (0 <= begin <= len(out_connectivity_values_with_old))
    #assert (0 <= end <= len(out_connectivity_values_with_old))
    #assert (0 <= begin2 <= len(in_connectivity_values))
    #assert (0 <= end2 <= len(in_connectivity_values))
    out_connectivity_values_with_old[begin:end] = in_connectivity_values[begin2:end2]
    begin = end
assert (begin == out_nbconnectivity)
print('Time step 2: ', time.time() - tic)

if False:
    # Mauvaise "bonne idée", cela marche en Nto1 à cause du coût important IO
    # mais cela ne marche pas ici.
    tic = time.time()
    # compute out_connectivity_values with old indexes on points
    out_connectivity_values_with_old = np.empty((out_nbconnectivity,), dtype=np.int64)
    
    indexes = np.empty((out_nbconnectivity,), dtype=np.int64)
    current = np.int64(0)
    for in_index_cell in indexes_cells_values:
        #assert (0 <= current < out_nbconnectivity)
        #assert (0 <= in_index_cell < len(in_offsets))
        nbpoints = in_nbpoints[in_index_cell]
        index = in_offsets[in_index_cell]
        #assert (0 <= index <= len(in_connectivity_values))
        for inPt in range(nbpoints):
            #assert (0 <= index + inPt < len(in_connectivity_values))
            indexes[current] = index + inPt
            current += 1
    assert (current == out_nbconnectivity)
    out_connectivity_values_with_old = in_connectivity_values[indexes]
    print('Time step 2bis: ', time.time() - tic)

tic = time.time()
# actually, out_connectivity_with_in but with old indexes points
# compute out_indexes_points_values
out_indexes_points_values = np.array(list(set(out_connectivity_values_with_old)))
print('Time step 3: ', time.time() - tic)

tic = time.time()
# compute old index to (new) index points
oldtonewindexpoints = np.empty(out_indexes_points_values.max() + 1, dtype=np.int64)
for out_pt, in_pt in enumerate(out_indexes_points_values):
    oldtonewindexpoints[in_pt] = out_pt
# compute out_connectivity_values with (new) indexes on points
out_connectivity_values = oldtonewindexpoints[out_connectivity_values_with_old]
print('Time step 4: ', time.time() - tic)

# return results
Ret = {out_connectivity_key: out_connectivity_values, out_indexes_points_key: out_indexes_points_values, }
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection_connectivty_indexes_points terminated 4')"""

    RunTestPrecis(command_eval_selection_connectivty_indexes_points_4)
    print('AVANT Time: 0.00047')
    RunTestGros(command_eval_selection_connectivty_indexes_points_4)
    print('AVANT Time: 0.454')


if __name__ == '__main__':
    test_unit_kdi_update_eval_selection_connectivity_indexes_points()
    print('\ntest_unit_kdi_update_eval_offsets: checked')
