from mpi4py import MPI
import numpy as np

from pykdi import *

SIMULATION_TRIANGLE_TYPE_VALUE = 0


def test_example_uns_1pe_4ssd_1uns_10tps_multi_server():
    mpi_rank = MPI.COMM_WORLD.Get_rank()
    mpi_size = MPI.COMM_WORLD.Get_size()

    # Fr: Création de la base
    #     Description explicite de la valeur associée par le code (ici 59) au type d'une
    #     cellule décrivant un triangle
    base = KDIAgreementBaseStepPart('filepath')
    base.set_conf({KDI_AGREEMENT_CONF_NB_PARTS: mpi_size,
                   KDI_AGREEMENT_CONF_CODE_2_KDI: {59: KDI_TRIANGLE}})
    base.initialize()

    # Fr: Création d'un maillage et d'un champ de valeurs aux points et aux cellules
    #     A faire remarquer que cela peut se faire à tout moment de la création de la base
    base.update('UnstructuredGrid', '/mymesh')
    base.update_fields('/mymesh/points/fields', 'myFieldPoint')
    base.update_fields('/mymesh/cells/fields', 'myFieldCell')

    # Fr: Définition d'un chunk stepless (donc partless)
    chunk_stepless_partless = base.chunk()

    # Fr: Association à une valeur stepless de certains champs (valeurs globales intemporelles)
    chunk_stepless_partless.insert('/glu/version', np.array([1, 0], dtype='i1'))

    chunk_stepless_partless.insert('/study/name', 'MyEtude')

    # Fr: Déroulement de la simulation avec des temps croissant
    #     Dans le cas où il y a régression temporelle au cours de la simulation, les temps
    #     sauvegardés de valeurs supérieures seront détruites.
    for int_step in range(10):

        # Fr: Le pas de temps est un réel
        float_step = float(int_step * 1.1)

        # Fr: Définition d'un chunk partless
        chunk_partless = base.chunk(float_step)

        # Fr: Ici, la simulation s'exécute en parallèle, le rang du processus correspond à celui de la partition
        int_part = mpi_rank

        # Fr: Définition d'un chunk fin de la base décrivant une partition pour un
        #     temps de simulation donné
        chunk = base.chunk(float_step, int_part)

        cartesianCoordinates = np.array(
            [[0. + 2 * int_part, 0. - 0.1 * float_step, 0.],
             [1. + 2 * int_part, 0. - 0.1 * float_step, 0.],
             [1. + 2 * int_part, 1. + 0.1 * float_step, 0.],
             [0. + 2 * int_part, 1. + 0.1 * float_step, 0.], ])
        # Fr: Enregistrement des coordonnées des 4 points variant au cours de la simulation
        chunk.insert('/mymesh/points/cartesianCoordinates', cartesianCoordinates)

        fields = np.array([1., 2., 3., 2.], dtype='f8')
        # Fr: A tout moment, il est possible de construire un champ de valeurs.
        #     Si il existe, il ne se passera rien.
        #     La clef correspondant à ce champ dans le dictionnaire de la base sera retournée.
        fieldkey = base.update_fields('/mymesh/points/fields', 'myFieldPoint')
        # Fr: Enregistrement des valeurs du champ de valeurs variant au cours de la
        #     simulation pour ces 4 points
        chunk.insert(fieldkey, fields)

        fields = np.array([1, 2, 0, 3], dtype='i4')
        # Fr: A tout moment, il est possible de créer un nouveau champ de valeurs.
        fieldkey = base.update_fields('/mymesh/points/fields', 'myLocalIndexPoint')
        chunk.insert(fieldkey, fields)

        fields = fields + 4 * int_part
        # Fr: A tout moment, il est possible de créer un nouveau champ de valeurs.
        fieldkey = base.update_fields('/mymesh/points/fields', 'myGlobalIndexPoint')
        chunk.insert(fieldkey, fields)

        types = np.array([59, 59], dtype='i1')
        # Fr: Enregistrement du type de cellules, dans cet exemple, suivant la convention du code
        chunk.insert('/mymesh/cells/types', types)

        connectivity = np.array([0, 1, 2, 2, 3, 0])
        # Fr: Enregistrement de la connectivité des cellules à travers un index de points
        chunk.insert('/mymesh/cells/connectivity', connectivity)

        fields = np.array([42, 69], dtype='f8')
        chunk.insert('/mymesh/cells/fields/myFieldCell', fields)

        fields = np.array([1, 0], dtype='i4')
        fieldkey = base.update_fields('/mymesh/cells/fields', 'myLocalIndexCell')
        chunk.insert(fieldkey, fields)

        fields = fields + 2 * int_part
        fieldkey = base.update_fields('/mymesh/cells/fields', 'myGlobalIndexCell')
        chunk.insert(fieldkey, fields)

        # == SAVE VTK HDF + JSON, LOAD JSON ============================
        # Fr: Pour déclencher une écriture, il est nécessaire de définir toutes les partitions
        #     d'un temps de simulation.

        chunk_partless.saveVTKHDF('ex_uns_1pe_4ssd_1uns_10tps__multi_server')

        # Fr: L'écriture d'un fichier VTK HDF se traduit par l'écriture d'un fichier .vtkhdf
        #     et d'un fichier .json qui décrit la sémantique compléte de la base et ce qui
        #     n'a pu être écrit dans la base VTK HDF.

        base = KDIAgreementBaseStepPart('ex_uns_1pe_4ssd_1uns_10tps__multi_server')
        base.initialize(True)


if __name__ == '__main__':
    # Fr: Exemple de création d'une base produite en mode d'exécution distribuée,
    #     décrivant P partitions ou sous-domaines de calcul et ayant 10 sorties de
    #     sauvegarde temporelles ; P égal au nombre de processus.
    test_example_uns_1pe_4ssd_1uns_10tps_multi_server()
    print('\ntest_example_uns_1pe_4ssd_1uns_10tps_multi_server: checked')
