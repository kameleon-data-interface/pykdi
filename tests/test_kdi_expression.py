from pykdi import KDIExpression, KDIException
from pykdi import kdi_base, KDI_AGREEMENT_STEPPART
from pykdi import kdi_update_fields

import  numpy as np

"""
Fr: Ce test valide la classe KDIExpression

Use pykdi:
- KDIExpression
"""


def test_kdi_expression():
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 3})

    kdi_update_fields(base, '/fields', 'A').insert(np.array([1., 2., 3.]), {})
    assert np.all(base['/fields/A'].get({}) == np.array([1., 2., 3.]))

    kdi_update_fields(base, '/fields', 'B').insert(42., {})
    assert np.all(base['/fields/B'].get({}) == 42.)

    kdi_update_fields(base, '/fields', 'C').insert(np.array([3., 2., 1.]), {})
    assert np.all(base['/fields/C'].get({}) == np.array([3., 2., 1.]))

    kdi_expression = KDIExpression(base, '@/fields/A@*@/fields/B@*@/fields/C@')

    try:
        kdi_expression.insert(None)
        assert (False)
    except NotImplementedError:
        pass

    try:
        kdi_expression.erase()
        assert (False)
    except NotImplementedError:
        pass

    try:
        kdi_expression.serialize()
        assert (False)
    except KDIException as e:
        assert (e.args[0] == 'Serialize forbidden!')

    assert (str(kdi_expression) == "KDIExpression(base=KDIBase, expression='@/fields/A@*@/fields/B@*@/fields/C@')")

    assert np.all(kdi_expression.get({}) == np.array([126., 168., 126.]))
    assert np.all(base['/fields/A'].get({}) == np.array([1., 2., 3.]))
    assert np.all(base['/fields/B'].get({}) == 42.)
    assert np.all(base['/fields/C'].get({}) == np.array([3., 2., 1.]))

    kdi_update_fields(base, '/fields', 'D').insert(12., {'step':1., 'part':0})
    kdi_update_fields(base, '/fields', 'D').insert(24., {'step':1., 'part':1})
    kdi_update_fields(base, '/fields', 'D').insert(42., {'step':1., 'part':2})

    kdi_expression = KDIExpression(base, '@/fields/A@*@/fields/D@*@/fields/C@')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (np.all(kdi_expression.get({'step':1., 'part':0}) == np.array([36., 48., 36.])))
    assert (np.all(kdi_expression.get({'step':1., 'part':1}) == np.array([72., 96., 72.])))
    assert (np.all(kdi_expression.get({'step':1., 'part':2}) == np.array([126., 168., 126.])))

    kdi_expression = KDIExpression(base, '@/fields/A@*10*@/fields/D@*@/fields/C@')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (np.all(kdi_expression.get({'step':1., 'part':0}) == np.array([360., 480., 360.])))
    assert (np.all(kdi_expression.get({'step':1., 'part':1}) == np.array([720., 960., 720.])))
    assert (np.all(kdi_expression.get({'step':1., 'part':2}) == np.array([1260., 1680., 1260.])))

    # TODO ATTENTION Le calcul ne tient pas compte d'un ordre de priorité dans les operateurs

    kdi_expression = KDIExpression(base, '10.+2-3*4/5.')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36.)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36.)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36.)

    expected = 9.6
    assert (kdi_expression.get({'step':1., 'part':0}) != expected)
    assert (kdi_expression.get({'step':1., 'part':1}) != expected)
    assert (kdi_expression.get({'step':1., 'part':2}) != expected)

    assert (str(kdi_expression) == "KDIExpression(base=KDIBase, expression='10.+2-3*4/5.')")

    kdi_expression = KDIExpression(None, '10.+2-3*4/5.')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36.)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36.)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36.)

    expected = 9.6
    assert (kdi_expression.get({'step':1., 'part':0}) != expected)
    assert (kdi_expression.get({'step':1., 'part':1}) != expected)
    assert (kdi_expression.get({'step':1., 'part':2}) != expected)

    assert (str(kdi_expression) == "KDIExpression(expression='10.+2-3*4/5.')")

    # TODO Réflexion autour du passage à la notation polonaise

    from collections import deque

    def compute_notation_polonaise(sequence: list) -> int:
        d = deque()
        for touche in sequence:
            if isinstance(touche, int) or isinstance(touche, float):
                d.append(touche)
            elif isinstance(touche, str):
                b, a = d.pop(), d.pop()
                expr = f"{a} {touche} {b}"
                d.append(eval(expr))
            else:
                raise ValueError(f"Expression invalide: {touche}")
            # print(f"{d}  # {touche}")
        return d.pop()

    result = compute_notation_polonaise([3, 1, 2., "+", 4, "*", "+"])
    assert(result == 15.0)

    # TODO En l'état, le calcul ne tient pas compte des parenthèses
    # TODO Ce qui explique que c'est deux expressions donnent un résultat
    # TODO identique

    kdi_expression = KDIExpression(base, '10.+2-(3*4/5.)+0')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36)

    expected = 9.6
    assert (kdi_expression.get({'step':1., 'part':0}) != expected)
    assert (kdi_expression.get({'step':1., 'part':1}) != expected)
    assert (kdi_expression.get({'step':1., 'part':2}) != expected)

    kdi_expression = KDIExpression(base, '10.+2-3*4/5.')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36)

    kdi_expression = KDIExpression(base, '10.+2-3*(4/5.)')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36)

    kdi_expression = KDIExpression(base, '10.+(2-3*4)/5.')

    #for ipart in range(3):
    #    print(kdi_expression.get({'step':1., 'part':ipart}))
    assert (kdi_expression.get({'step':1., 'part':0}) == 36)
    assert (kdi_expression.get({'step':1., 'part':1}) == 36)
    assert (kdi_expression.get({'step':1., 'part':2}) == 36)

    expected = 8.0
    assert (kdi_expression.get({'step':1., 'part':0}) != expected)
    assert (kdi_expression.get({'step':1., 'part':1}) != expected)
    assert (kdi_expression.get({'step':1., 'part':2}) != expected)


if __name__ == '__main__':
    test_kdi_expression()
    print('\ntest_kdi_expression: checked')
