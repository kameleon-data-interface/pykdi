import numpy as np

from pykdi import *

KDI_SIMULATION_TRIANGLE_TYPE_VALUE = 0

TRIANGLE_NB_POINTS = 3

data_common = {
    '/glu/version': np.array([1, 0]),
    '/study/name': 'MyEtude',
    '/study/date': '2024/05',
}

data_ssd = [
    {
        '/mymesh/points/cartesianCoordinates': np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [1., 1., 0.], [0., -1., 0.], [1., -1., 0.], ]),
        '/mymesh/points/fields/myFieldPoint': np.array([1., 2., 2., 3., 1., 1., ]),
        '/mymesh/cells/types': np.array([KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_QUADRANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0, ]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69, 59, ]),
        '/mymesh/cells/fields/myFieldCell2': np.array([]),

        '/mymesh/cells/offsets': np.array([0, 3, 6, 10, ]),
    },
    {
        '/mymesh/points/cartesianCoordinates': np.array([[1., 0., 0.], [2, 0., 0.], [2., 1., 0.], [1., 1., 0.], ]),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 1., 2., 3.]),
        '/mymesh/cells/types': np.array([KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69]),
        '/mymesh/cells/fields/myFieldCell2': np.array([]),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),
    },
    {
        '/mymesh/points/cartesianCoordinates': np.array([[0., 1., 0.], [1., 1., 0.], [1., 2., 0.], [0., 2., 0.], ]),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 3., 2., 1.]),
        '/mymesh/cells/types': np.array([KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69]),
        '/mymesh/cells/fields/myFieldCell2': np.array([]),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),
    }
]


def GetDataCommon(base, name):
    if name == '/study/name':
        return np.array([data_common[name]])
    if name == '/study/date':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return np.array(['{}/{:02d}'.format(data_common[name], itps + 1)])
    return data_common[name]


def GetDataSssd(base, name):
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    issd = base['/chunks'].GetOffset('part', chunks['part'])
    if name == '/mymesh/cells/fields/myFieldCell2':
        array = np.zeros(data_ssd[issd]['/mymesh/cells/fields/myFieldCell'].shape, dtype=float)
        array += itps + issd / 100.
        return array
    if name == '/mymesh/points/cartesianCoordinates':
        delta = float(itps) / 10.
        if issd == 0:
            return (data_ssd[issd]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[-delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [0., 0., 0.], ]))
        if issd == 1:
            return (data_ssd[issd]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], ]))
        if issd == 2:
            return (data_ssd[issd]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [-delta, delta, 0.], ]))
    return data_ssd[issd][name]


def checked(msg, base, first=True):
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    issd = base['/chunks'].GetOffset('part', chunks['part'])
    # if first:
    #    print('>>>>> ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(issd) + ')')
    # else:
    #    print('>>>>> ' + msg + ' prev time (itps: ' + str(itps - 1) + ', part: ' + str(issd) + ')')
    ret = True
    for key in data_common.keys():
        if isinstance(data_common[key], str):
            if GetDataCommon(base, key) != base[key].get():
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDataCommon(base, key))
                print('----------')
        else:
            if not np.all(GetDataCommon(base, key) == base[key].get()):
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDataCommon(base, key))
                print('----------')
                assert (np.all(GetDataCommon(base, key) == base[key].get()))
    for key in data_ssd[issd].keys():
        if isinstance(data_ssd[issd][key], str):
            if GetDataSssd(base, key) != base[key].get():
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDataSssd(base, key))
                print('----------')
        else:
            if not np.all(GetDataSssd(base, key) == base[key].get()):
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDataSssd(base, key))
                print('----------')
                assert (np.all(GetDataSssd(base, key) == base[key].get()))

    if not first:
        return

    if itps > 0.:
        current = base['/chunks/current']
        del base['/chunks/current']

        base['/chunks'].set({'step': itps - 1, 'part': issd})
        checked(msg, base, False)
        base['/chunks/current'] = current

    return ret


def checkedAll(msg, base):
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    for issd in range(3):
        base['/chunks'].set({'step': itps, 'part': issd})
        checked(msg, base)


def test_uns_1pe_3ssd_1uns_10tps_C():
    # == IN MEMORY =================================================
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 3})

    for itps in range(10):

        for issd in range(3):

            float_step = float(itps)

            base['/chunks'].set({'step': float_step, 'part': issd})

            key = '/glu/version'
            base[key].insert(GetDataCommon(base, key))

            key = '/study/name'
            base[key].insert(GetDataCommon(base, key))

            key = '/study/date'
            base[key].insert(GetDataCommon(base, key))

            kdi_update(base, 'UnstructuredGrid', '/mymesh')

            key = '/mymesh/points/cartesianCoordinates'
            base[key].insert(GetDataSssd(base, key))

            key = '/mymesh/points/fields/myFieldPoint'
            if key not in base.keys():
                if key in base['/mymesh/points/fields'].get():
                    raise KDIException('\'' + key + '\' is already listed in \'/mymesh/points/fields\'')
                base['/mymesh/points/fields'].insert(key)  # here, Set = Append
                fieldPoint = KDIMemory(base=base, key_base=key)
                base[key] = fieldPoint
            elif key not in base['/mymesh/points/fields'].get():
                raise KDIException('\'' + key + '\' is not listed in \'/mymesh/points/fields\'')

            base[key].insert(GetDataSssd(base, key))

            key = '/mymesh/cells/types'
            base[key].insert(GetDataSssd(base, key))

            key = '/mymesh/cells/connectivity'
            base[key].insert(GetDataSssd(base, key))

            kdi_update_eval_offsets(base, '/mymesh/cells')

            for key in ['/mymesh/cells/fields/myFieldCell', '/mymesh/cells/fields/myFieldCell2']:
                if key not in base.keys():
                    if key in base['/mymesh/cells/fields'].get():
                        raise KDIException('\'' + key + '\' is already listed in \'/mymesh/cells/fields\'')
                    base['/mymesh/cells/fields'].insert(key)  # here, Set = Append
                    fieldPoint = KDIMemory(base=base, key_base=key)
                    base[key] = fieldPoint
                elif key not in base['/mymesh/cells/fields'].get():
                    raise KDIException('\'' + key + '\' is not listed in \'/mymesh/cells/fields\'')

                print('/mymesh/cells/fields', 'step:', itps, 'issd:', issd, GetDataSssd(base, key))
                base[key].insert(GetDataSssd(base, key))

            checked('Init by memory', base)

        checkedAll('Init by memory', base)

        # == SAVE VTK HDF + JSON, LOAD JSON ============================
        base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step})
        write_vtk_hdf(base, 'uns_1pe_3ssd_1uns_10tps_C_writer.vtkhdf')
        base = read_json('uns_1pe_3ssd_1uns_10tps_C_writer.json')
        ## Set /chunks/current
        base['/chunks'].set({'step': float(itps), 'part': 0})
        checkedAll('Init by load lazy VTK HDF by JSON', base)


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    test_uns_1pe_3ssd_1uns_10tps_C()
    print('\ntest_uns_1pe_3ssd_1uns_10tps_C: checked')
