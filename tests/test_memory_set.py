import gc
import numpy as np

from pykdi import *

"""
Fr: Ce test valide les différents modes d'accès dans un KDIMemory en fonction des chunks :

test_memory_set_chunk_step_part :
   - insert() step_part value
        - en passant un chunk en paramètre à insert()
        - avec un chunk prédéfini dans la base
        - en surchargeant le chunk prédéfini par un chunk en paramètre de insert()
   - get()
        - vérification que l'on obtient bien la valeur pour le bon step_part
        - incorrect en stepless-partless, step-partless
        - incorrect en step-part incorrect
          - step non déjà défini lors d'un get()
          - part incorrect lors d'un get()
          - pas de valeur associé à un step existant ou part existant

de façon similaire dans les tests :
- test_memory_set_chunk_step avec un insert() step_partless value
- test_memory_set_chunk avec un insert() stepless-partless value

test_memory_constant_variants :
   - suite à un insert, on détermine le mode d'insert()
     step-part, step-partless ou stepless-partless
     puis aux insert() suivant on vérifie que l'on reste bien dans ce mode
     de déclaration
     (il est interdit de définir une valeur globale puis une valeur spéficique pour
     un step-part donné)
    
Il faut positionner KDI_TEST_MEMORY_LEAKS=1 pour activer le passage des tests leaks.
- test_memory_polygon_leaks
  test en brut de force avec allocation de tableau brut (empty) sans vérification
  des valeurs, appliquer de la méthode erase()
  
- test_memory_quadrangle_leaks
  la même chose sur des quadrangles mais avec KVTKHDFGarbageCollector()  

- test_memory_quadrangle_leaks_CPP
  la même chose mais à la façon d'un accès C++
"""


test_memory_leaks = kdi_get_env('KDI_TEST_MEMORY_LEAKS', False)


def test_memory_set_chunk_step_part():
    """
    Fr: Test de l'affectation pour un chunk donné et des interrogations inadaptées.
    """

    # Fr: Création d'une base
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})

    # Fr: Vérification de la représentation de '/glu/Version'
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)")

    # Fr: Affectation d'une valeur en passant un chunk complétement défini
    #     {KVTKHDF_VARIANT_STEP: 1.1, KVTKHDF_VARIANT_PART: 0}
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'),
                                chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

    # Fr: Vérification de la représentation de '/glu/Version'
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=['step', 'part'], datas={'1.1':{'0':np.array([1, 0]),},},)")

    # Fr: On vérifie que l'on obtient bien la valeur pour ce chunk
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 2.2}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 2.2, })
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (2.2) not exist in mode get()")

    # Fr: Reinitialisation en dur avant un insert via un chunk prédéfini dans la base
    base['/glu/version'] = KDIMemory(base=base, key_base='/glu/version', variants=None, datas={},)

    # Fr: Affectation d'une valeur en passant un chunk global complétement défini
    #     {KVTKHDF_VARIANT_STEP: 1.1, KVTKHDF_VARIANT_PART: 0}
    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

    # Fr: On vérifie que l'on obtient bien la valeur pour ce chunk
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'))

    # Fr: On vérifie que l'on obtient bien la valeur pour ce chunk
    assert (str(base['/glu/version'].get()) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien la valeur pour ce chunk
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {}, step-less et part-less
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "There are no stored values common or not with the '/glu/version' key for this chunk ({}) (-less)!")

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 1.1}, part-less
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, })
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "There are no stored values common or not with the '/glu/version' key for this chunk ({'step': 1.1, '#step': '1.1'}) (-less)!")

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk complétement défini mais ne correspondant
    #     pas à celui du set
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 1})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "There are no stored values common or not with the '/glu/version' key for this chunk ({'step': 1.1, 'part': 1, '#step': '1.1', '#part': '1'}) (-less)!")

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 4})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == 'pre: variant (part) is KDI_CONSTANT_VARIANT but value (4) not in [0, 4[')

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 5})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == 'pre: variant (part) is KDI_CONSTANT_VARIANT but value (5) not in [0, 4[')

    # Fr: Affectation d'une valeur en passant un chunk complétement défini
    #        {KVTKHDF_VARIANT_STEP: 1.1, KVTKHDF_VARIANT_PART: 0}
    #     étant pris en compte en surchage de celui défini dans la base
    base['/glu/version'].insert(np.array([32, 45], dtype='i1'),
                                chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

    # Fr: On vérifie que l'on obtient bien la valeur pour ce chunk
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.0, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[32 45]')

    # TODO Fr: Tester la réécriture pour un même temps, part


def test_memory_set_chunk_step():
    """
    Fr: Test de l'affectation pour un chunk donné et des interrogations inadaptées.
    """

    # Fr: Création d'une base
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})

    # Fr: Vérification de la représentation de '/glu/Version'
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)")

    # Fr: Affectation d'une valeur en passant un chunk {KVTKHDF_VARIANT_STEP: 1.1}
    #     afin de définir une valeur globale pour le temps 1.1 mais part-less
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1})

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk {KVTKHDF_VARIANT_STEP: 1.1}
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, })) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 2.2}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 2.2, })
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (2.2) not exist in mode get()")

    # Fr: En mode écriture, on définit la clef KVTKHDF_VARIANT_STEP à 1.1 et KVTKHDF_VARIANT_PART à 0
    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})
    assert (str(base['/glu/version'].get()) == '[1 0]')

    # Fr: On vérifie que l'on obtient une erreur pour le chunk {}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "There are no stored values common or not with the '/glu/version' key for this chunk ({}) (-less)!")

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk
    #     {KVTKHDF_VARIANT_STEP: 1.1, KVTKHDF_VARIANT_PART: 0}
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk
    #     {KVTKHDF_VARIANT_STEP: 1.1, KVTKHDF_VARIANT_PART: 1}
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 1})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 4})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (part) is KDI_CONSTANT_VARIANT but value (4) not in [0, 4[")

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 5})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (part) is KDI_CONSTANT_VARIANT but value (5) not in [0, 4[")

    # Fr: On vérifie que l'on obtient une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 2.2}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 2.2})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (2.2) not exist in mode get()")

    # TODO Fr: Tester la réécriture pour un même temps part-less


def test_memory_set_chunk():
    """
    Fr: Test de l'affectation pour un chunk donné et des interrogations inadaptées.
    """

    # Fr: Création d'une base
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})

    # Fr: Vérification de la représentation de '/glu/Version'
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)")

    # Fr: Affectation d'une valeur en passant un chunk {}
    #     afin de définir une valeur globale step-less et part-less
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={})

    # Fr: On vérifie que l'on arrive à charger une valeur avec le chunk {}
    assert (str(base['/glu/version'].get(chunk={})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 1.1}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, })
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (1.1) not exist in mode get()")

    # Fr: En mode écriture, on définit la clef KVTKHDF_VARIANT_STEP à 1.1 et KVTKHDF_VARIANT_PART à 0
    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk passé en paramètre step-partless
    #     Le fait d'avoir créer le chunk KVTKHDF_VARIANT_STEP à 1.1, cette clef a prise cette valeur
    #     (en mode écriture)
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, })) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 2.2}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 2.2, })
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (2.2) not exist in mode get()")

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk passé en paramètre step-part
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien la valeur globale pour le chunk {KVTKHDF_VARIANT_STEP: 1.1}
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour le chunk {KVTKHDF_VARIANT_STEP: 2.2, KVTKHDF_VARIANT_PART: 0}
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 2.2, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == "pre: variant (step) is KDI_GROWNING_VARIANT but value (2.2) not exist in mode get()")

    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 1})) == '[1 0]')

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 4})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == 'pre: variant (part) is KDI_CONSTANT_VARIANT but value (4) not in [0, 4[')

    # Fr: On vérifie que l'on obtient bien une erreur pour un chunk incorrect, ici part >= 4
    #     Test en erreur
    try:
        base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 5})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[
                    0] == 'pre: variant (part) is KDI_CONSTANT_VARIANT but value (5) not in [0, 4[')

    # TODO Fr: Tester la réécriture pour step-less part-less


def test_memory_constant_variants():
    """
    Fr: Test de l'affectation pour un chunk donné et des interrogations inadaptées.
    """

    # Fr: Création d'une base
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})

    # Fr: Vérification de la représentation de '/glu/Version'
    assert (str(base['/glu/version']) == "KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},)")

    # Fr: Cas chunk stepless-partless

    # Fr: Affectation d'une valeur en passant un chunk {}
    #     afin de définir une valeur globale step-less et part-less
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={})

    # Fr: On vérifie que l'on arrive à charger une valeur avec le chunk {}
    assert (str(base['/glu/version'].get(chunk={})) == '[1 0]')

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-less et part-less, ici step-partless
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-less et part-less, ici step-part
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 2})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")

    # Fr: Cas chunk step-partless
    # Fr: Destruction du KDIMemory nécessaire
    base['/glu/version'] = KDIMemory(base=base, key_base='/glu/version', datas={},)

    # Fr: Affectation d'une valeur step-partless
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1})
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1})) == '[1 0]')

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-partless, ici stepless-partless
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-partless, ici step-part
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 2})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")

    # Fr: Cas chunk step-part
    # Fr: Destruction du KDIMemory nécessaire
    base['/glu/version'] = KDIMemory(base=base, key_base='/glu/version', datas={},)

    # Fr: Affectation d'une valeur step-part
    base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 2})
    assert (str(base['/glu/version'].get(chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1, KDI_AGREEMENT_STEPPART_VARIANT_PART: 2})) == '[1 0]')

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-part, ici stepless-partless
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")

    # Fr: Il est donc interdit d'avoir un chunk d'un autre type de step-part, ici step-partless
    try:
        base['/glu/version'].insert(np.array([1, 0], dtype='i1'), chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 1.1})
        raise Exception('Guru Meditation: Internal Error')
    except Exception as e:
        assert (e.args[0] == "insert no constant variant before:")


def test_memory_polygon_leaks():
    """
    Fr: Test sur une fuite mémoire potentielle.
    """
    if not test_memory_leaks:
        return

    # Fr: Création d'une base
    base = kdi_update(None, 'Base', '')
    base['/chunks'].SetVariants([[KDI_AGREEMENT_STEPPART_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
                                 [KDI_AGREEMENT_STEPPART_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': 4, }, ], ])

    MY_SIMULATION_POLYGON_TYPE_VALUE = 0
    dict_code2kdi = {MY_SIMULATION_POLYGON_TYPE_VALUE: KDI_POLYGON, }

    array_code2kdi = np.full(shape=(max(dict_code2kdi.keys()) + 1), fill_value=KDI_PIT, dtype=np.int8)
    # TODO Verifier que np.int8 max <= KDI_PIT
    for k, v in dict_code2kdi.items():
        assert (k < len(array_code2kdi))
        assert (v < KDI_SIZE)
        array_code2kdi[k] = v

    base['/code2kdi'] = array_code2kdi
    # In case KDI_POLYGON, the user must explicitly give the number of points in each cell.
    # base['/code2nbPoints'] = kdi2nbPoints[array_code2kdi]

    for iStep in range(2):

        vStep = float(iStep)

        for iPart in [0, 1, 2, 3]:

            # temporal and partition declinaison
            base['/chunks'].set({'step': vStep, 'part': iPart})

            kdi_update(base, 'UnstructuredGrid', '/mymesh')

            nPoints = 300000000

            key = '/mymesh/points/cartesianCoordinates'
            base[key].insert(np.empty((nPoints, 3,), dtype=np.float64))

            nFields = 100

            for iField in range(nFields):
                kdi_update_fields(base, '/mymesh/points/fields', 'myFieldPoint_' + str(iField))
                key = '/mymesh/points/fields/myFieldPoint_' + str(iField)
                base[key].insert(np.empty((nPoints,), dtype=np.float64))

            # TODO Les POLYGON ne sont pas actuellement considérés au niveau saveVTKHDF
            #      Il faut soit :
            #      - définir un nouveau champ /mymesh/cells/nbpoints
            #      - définir explicitement /mymesh/cells/offsets
            #      En mode standard, c'est /mymesh/cells/types qui définit /mymesh/cells/nbpoints
            #      qui définit /mymesh/cells/offsets.
            key = '/mymesh/cells/types'
            base[key].insert(np.array([MY_SIMULATION_POLYGON_TYPE_VALUE]))

            key = '/mymesh/cells/connectivity'
            base[key].insert(np.array([iPoint for iPoint in range(nPoints)]))

            key = '/mymesh/cells/offsets'
            if key not in base:
                base[key] = KDIMemory(base, key)
            base[key].insert(np.array([0, nPoints]))

            print('#', iStep, '#', iPart, 'terminated')
            gc.collect()

        print('#', iStep, 'terminated')
        gc.collect()

        for iPart in [0, 1, 2, 3]:
            # temporal and partition declinaison
            base['/chunks'].set({'step': vStep, 'part': iPart})
            base['/mymesh/points/cartesianCoordinates'].erase()
            for iField in range(nFields):
                base['/mymesh/points/fields/myFieldPoint_' + str(iField)].erase()
            base['/mymesh/cells/types'].erase()
            base['/mymesh/cells/connectivity'].erase()
            base['/mymesh/cells/offsets'].erase()
        gc.collect()

    pass


def test_memory_quadrangle_leaks():
    """
    Fr: Test sur une fuite mémoire potentielle.
    """
    if not test_memory_leaks:
        return

    # Fr: Création d'une base
    base = kdi_update(None, 'Base', '')
    base['/chunks'].SetVariants([[KDI_AGREEMENT_STEPPART_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
                                 [KDI_AGREEMENT_STEPPART_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': 4, }, ], ])

    dict_code2kdi = {KDI_SIMULATION_QUADRANGLE_TYPE_VALUE: KDI_QUADRANGLE, }

    array_code2kdi = np.full(shape=(max(dict_code2kdi.keys()) + 1), fill_value=KDI_PIT, dtype=np.int8)
    # TODO Verifier que np.int8 max <= KDI_PIT
    for k, v in dict_code2kdi.items():
        assert (k < len(array_code2kdi))
        assert (v < KDI_SIZE)
        array_code2kdi[k] = v

    base['/code2kdi'] = array_code2kdi
    base['/code2nbPoints'] = kdi2nbPoints[array_code2kdi]

    for iStep in range(3):

        vStep = float(iStep)

        for iPart in [0, 1, 2, 3]:

            # temporal and partition declinaison
            base['/chunks'].set({'step': vStep, 'part': iPart})

            kdi_update(base, 'UnstructuredGrid', '/mymesh')

            nCells = 300000000
            nPoints = 2 * (nCells + 1)

            key = '/mymesh/points/cartesianCoordinates'
            base[key].insert(np.empty((nPoints, 3,), dtype=np.float64))

            nFields = 100

            for iField in range(nFields):
                kdi_update_fields(base, '/mymesh/points/fields', 'myFieldPoint_' + str(iField))
                key = '/mymesh/points/fields/myFieldPoint_' + str(iField)
                base[key].insert(np.empty((nPoints,), dtype=np.float64))

            key = '/mymesh/cells/types'
            base[key].insert(np.array([KDI_SIMULATION_QUADRANGLE_TYPE_VALUE for _ in range(nCells)]))

            key = '/mymesh/cells/connectivity'
            base[key].insert(np.empty((4 * nCells,), dtype=np.int64))

            kdi_update_eval_offsets(base, '/mymesh/cells')

            print('#', iStep, '#', iPart, 'terminated')
            gc.collect()

        print('#', iStep, 'terminated')
        gc.collect()

        base['/chunks'].set({'step': vStep})
        if False:
            for iPart in [0, 1, 2, 3]:
                # temporal and partition declinaison
                base['/chunks'].set({'step': vStep, 'part': iPart})
                base['/mymesh/points/cartesianCoordinates'].erase()
                for iField in range(nFields):
                    base['/mymesh/points/fields/myFieldPoint_' + str(iField)].erase()
                base['/mymesh/cells/types'].erase()
                base['/mymesh/cells/connectivity'].erase()
            gc.collect()
        else:
            step_partless = {KDI_AGREEMENT_STEPPART_VARIANT_STEP: vStep}
            KVTKHDFGarbageCollector(base, step_partless, True, force=True)


def test_memory_quadrangle_leaks_cpp():
    """
    Fr: Test sur une fuite mémoire potentielle.
    """
    if not test_memory_leaks:
        return

    # Fr: Création d'une base
    kdi_base = KDIAgreementBaseStepPart('filepath')
    kdi_base.set_conf({KDI_AGREEMENT_CONF_NB_PARTS: 4,
                       KDI_AGREEMENT_CONF_CODE_2_KDI: {KDI_SIMULATION_QUADRANGLE_TYPE_VALUE: KDI_QUADRANGLE, }})
    kdi_base.initialize()

    for iStep in range(3):

        vStep = float(iStep)

        chunk_partless = kdi_base.chunk(vStep)

        for iPart in [0, 1, 2, 3]:

            # temporal and partition declinaison
            chunk = kdi_base.chunk(vStep, iPart)

            kdi_base.update("UnstructuredGrid", "/mymesh");

            nCells = 300000000
            nPoints = 2 * (nCells + 1)

            key = '/mymesh/points/cartesianCoordinates'
            chunk.insert(key, np.empty((nPoints, 3,), dtype=np.float64))

            nFields = 100

            for iField in range(nFields):
                kdi_base.update_fields('/mymesh/points/fields', 'myFieldPoint_' + str(iField))
                key = '/mymesh/points/fields/myFieldPoint_' + str(iField)
                chunk.insert(key, np.empty((nPoints,), dtype=np.float64))

            key = '/mymesh/cells/types'
            chunk.insert(key, np.array([KDI_SIMULATION_QUADRANGLE_TYPE_VALUE for _ in range(nCells)]))

            key = '/mymesh/cells/connectivity'
            chunk.insert(key, np.empty((4 * nCells,), dtype=np.int64))

            print('#', iStep, '#', iPart, 'terminated')
            gc.collect()
        print('#', iStep, 'terminated')
        gc.collect()

        # Cet appel n'est pas à faire
        chunk_partless.garbageCollector()


if __name__ == '__main__':
    test_memory_set_chunk_step_part()
    print('\ntest_memory_set_chunk_step_part: checked\n')
    test_memory_set_chunk_step()
    print('\ntest_memory_set_chunk_step: checked\n')
    test_memory_set_chunk()
    print('\ntest_memory_set_chunk: checked\n')
    test_memory_constant_variants()
    print('\ntest_memory_constant_variants: checked\n')

    # default, not test memory leaks
    # for activate, set var env

    try:
        print('\ntest_memory_polygon_leaks\n')
        test_memory_polygon_leaks()
        print('\ntest_memory_polygon_leaks: checked\n')
    except:
        raise KDIException('KDI TEST MEMORY SET: FATAL ERROR test_memory_polygon_leaks')

    try:
        print('\ntest_memory_quadrangle_leaks\n')
        test_memory_quadrangle_leaks()
        print('\ntest_memory_quadrangle_leaks: checked\n')
    except:
        raise KDIException('KDI TEST MEMORY SET: FATAL ERROR test_memory_quadrangle_leaks')

    try:
        print('\ntest_memory_quadrangle_leaks_CPP\n')
        test_memory_quadrangle_leaks_cpp()
        print('\ntest_memory_quadrangle_leaks_CPP: checked\n')
    except:
        raise KDIException('KDI TEST MEMORY SET: FATAL ERROR test_memory_quadrangle_leaks_CPP')

    print('\ntest_memory_leaks: checked')
