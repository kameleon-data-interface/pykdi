import numpy as np

from pykdi import *

datas = {
    '/glu/version': np.array([1, 0]),
    '/study/name': 'MyEtude',
    '/study/date': '2024/05',
    '/mymesh/points/cartesianCoordinates': np.array([[0., 0., 0.], [1., 0., 0.], [1., 1., 0.], [0., 1., 0.], ]),
    '/mymesh/points/fields/myFieldPoint': np.array([0., 3., 2., 1.]),
    '/mymesh/cells/types': np.array([KDI_SIMULATION_TRIANGLE_TYPE_VALUE, KDI_SIMULATION_TRIANGLE_TYPE_VALUE]),
    '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
    '/mymesh/cells/fields/myFieldCell': np.array([42, 69]),

    '/mymesh/cells/offsets': np.array([0, 3, 6]),
}


def GetDatas(base, name):
    if name == '/study/name':
        return np.array([datas[name]])
    if name == '/study/date':
        return np.array(['{}/{:02d}'.format(datas[name], 0)])
    return datas[name]


def checked(msg, base, first=True):
    # if first:
    #    print('>>>>> ' + msg + ' current time')
    # else:
    #    print('>>>>> ' + msg + ' first time')
    ret = True
    for key in datas.keys():
        if isinstance(datas[key], str):
            if GetDatas(base, key) != base[key].get():
                print('Expected value key:' + key + ':')
                print(base[key].get())
                print('... vs ...')
                print(GetDatas(base, key))
                print('----------')
            assert (GetDatas(base, key) == base[key].get())
            # TODO avec test invariant (temporel et part)
            # TODO avec test invariant part
            # TODO avec chunk faux
        else:
            if not np.all(GetDatas(base, key) == base[key].get()):
                print('Expected value key \'' + key + '\':')
                print(base[key].get())
                print('... vs ...')
                print(GetDatas(base, key))
                print('----------')
                assert (np.all(GetDatas(base, key) == base[key].get()))
            assert (np.all(GetDatas(base, key) == base[key].get()))
            # TODO avec test invariant (temporel et part)
            # TODO avec test invariant part
            # TODO avec chunk faux

    if not first:
        return

    current = base['/chunks/current']
    del base['/chunks/current']
    base['/chunks'].set({'step': 0., 'part': 0})
    checked(msg, base, False)
    base['/chunks/current'] = current

    return ret


def test_uns_1pe_1ssd_1uns_1tps():
    # == IN MEMORY =================================================
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 1})

    base['/chunks'].set({})

    for key in ('/glu/version', '/study/name',):
        base[key].insert(GetDatas(base, key))

    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.0})

    key = '/study/date'
    base[key].insert(GetDatas(base, key))

    kdi_update(base, 'UnstructuredGrid', '/mymesh')

    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.0, KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})

    for key in ('/mymesh/points/cartesianCoordinates', '/mymesh/cells/types',  '/mymesh/cells/connectivity',):
        base[key].insert(GetDatas(base, key))

    parent_field_key = '/mymesh/points/fields'
    for field_key in ('myFieldPoint',):
        kdi_update_fields(base, parent_field_key, field_key)
        key = parent_field_key + '/' + field_key
        base[key].insert(GetDatas(base, key))

    parent_field_key = '/mymesh/cells/fields'
    for field_key in ('myFieldCell',):
        kdi_update_fields(base, parent_field_key, field_key)
        key = parent_field_key + '/' + field_key
        base[key] .insert(GetDatas(base, key))

    kdi_update_eval_offsets(base, '/mymesh/cells')

    checked('Init by memory', base)

    # == SAVE VTK HDF + JSON, LOAD JSON ============================
    base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.0})
    write_vtk_hdf(base, 'uns_1pe_1ssd_1uns_1tps_writer.vtkhdf')
    load_base = read_json('uns_1pe_1ssd_1uns_1tps_writer.json')

    checked('Init by load lazy VTK HDF by JSON', load_base)


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    test_uns_1pe_1ssd_1uns_1tps()
    print('\ntest_uns_1pe_1ssd_1uns_1tps: checked')
