import time
import numpy as np

from pykdi import KDI_SIZE


def test_unit_kdi_update_eval_offsets():
    SIMULATION_TRIANGLE_TYPE_VALUE = 11
    SIMULATION_QUADRANGLE_TYPE_VALUE = 12
    SIMULATION_HEXAHEDRON_TYPE_VALUE = 13

    SIMULATION_TYPE_VALUE_PIT = 14
    SIMULATION_TYPE_VALUE_SIZE = 15

    KDI_TRIANGLE = 0
    KDI_QUADRANGLE = 1
    KDI_HEXAHEDRON = 12

    class KDIMemoryMOC:
        __slots__ = '_datas'
        def __init__(self):
            self._datas = np.full(shape=(2500000,), fill_value=SIMULATION_HEXAHEDRON_TYPE_VALUE, dtype=np.int8)

        def get(self):
            return self._datas

    print('First implementation: numpy arrays')

    command_eval_offsets_1 = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets')
base = Base
code2kdi = base['/code2kdi']
kdi2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    vfunc = np.vectorize(lambda x: kdi2nbPoints[code2kdi[int.from_bytes(x)]], otypes=[np.int64])
else:
    vfunc = np.vectorize(lambda x: kdi2nbPoints[code2kdi[x]], otypes=[np.int64])
nbPoints = vfunc(typesCode)
Ret = np.concatenate([[0], np.cumsum(nbPoints)])
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets terminated')"""

    code2kdi = np.full(shape=15, fill_value=0, dtype=np.int8)
    assert(SIMULATION_TRIANGLE_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_TRIANGLE_TYPE_VALUE] = KDI_TRIANGLE
    assert(SIMULATION_QUADRANGLE_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_QUADRANGLE_TYPE_VALUE] = KDI_QUADRANGLE
    assert(SIMULATION_HEXAHEDRON_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_HEXAHEDRON_TYPE_VALUE] = KDI_HEXAHEDRON

    kdi2nbPoints = np.empty(15, np.int8)
    assert(KDI_TRIANGLE < len(kdi2nbPoints))
    kdi2nbPoints[KDI_TRIANGLE] = 3
    assert(KDI_QUADRANGLE < len(kdi2nbPoints))
    kdi2nbPoints[KDI_QUADRANGLE] = 4
    assert(KDI_HEXAHEDRON < len(kdi2nbPoints))
    kdi2nbPoints[KDI_HEXAHEDRON] = 8

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/current/types': KDIMemoryMOC()
    }
    CurrentParams = [kdi2nbPoints, '/current/types']

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_offsets_1, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'], len(variables['Ret']))

    # Second ================================================================================

    print('Second implementation: dictionnairies')

    command_eval_offsets_2 = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets')
base = Base
code2kdi = base['/code2kdi']
kdi2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    vfunc = np.vectorize(lambda x, y, z: z[y[int.from_bytes(x)]], otypes=[np.int64])
else:
    vfunc = np.vectorize(lambda x, y, z: z[y[x]], otypes=[np.int64])
nbPoints = vfunc(typesCode, code2kdi, kdi2nbPoints)
Ret = np.concatenate([[0], np.cumsum(nbPoints)])
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets terminated')"""

    code2kdi = {
        SIMULATION_TRIANGLE_TYPE_VALUE: KDI_TRIANGLE,
        SIMULATION_QUADRANGLE_TYPE_VALUE: KDI_QUADRANGLE,
        SIMULATION_HEXAHEDRON_TYPE_VALUE: KDI_HEXAHEDRON }

    kdi2nbPoints = {KDI_TRIANGLE: 3, KDI_QUADRANGLE: 4, KDI_HEXAHEDRON: 8}

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/current/types': KDIMemoryMOC()
    }
    CurrentParams = [kdi2nbPoints, '/current/types']

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_offsets_2, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'], len(variables['Ret']))

    # Third ================================================================================

    print('Third implementation: dictionnairies')

    command_eval_offsets_mt = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets')
base = Base
code2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    vfunc = np.vectorize(lambda x: code2nbPoints[int.from_bytes(x)], otypes=[np.int64])
else:
    vfunc = np.vectorize(lambda x: code2nbPoints[x], otypes=[np.int64])
nbPoints = vfunc(typesCode)
Ret = np.concatenate([[0], np.cumsum(nbPoints)])
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets terminated')"""

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/current/types': KDIMemoryMOC()
    }
    code2nbPoints = {}
    for key, val in CurrentBase['/code2kdi'].items():
        code2nbPoints[key] = kdi2nbPoints[val]
    CurrentParams = [code2nbPoints, '/current/types']

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_offsets_mt, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'], len(variables['Ret']))

    # Fourth ================================================================================

    print('Fourth implementation: dictionnairies')

    command_eval_offsets_mt = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets')
base = Base
code2nbPoints = Params[0]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    nbPoints = code2nbPoints[typesCode.view(dtype=np.int8)]
else:
    nbPoints = code2nbPoints[typesCode]
Ret = np.empty((len(nbPoints)+1), dtype=np.int64)
Ret[0] = 0
np.cumsum(nbPoints, out=Ret[1:])
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_offsets terminated')"""

    code2kdi = np.full(shape=SIMULATION_TYPE_VALUE_SIZE, fill_value=0, dtype=np.int8)
    assert(SIMULATION_TRIANGLE_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_TRIANGLE_TYPE_VALUE] = KDI_TRIANGLE
    assert(SIMULATION_QUADRANGLE_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_QUADRANGLE_TYPE_VALUE] = KDI_QUADRANGLE
    assert(SIMULATION_HEXAHEDRON_TYPE_VALUE < len(code2kdi))
    code2kdi[SIMULATION_HEXAHEDRON_TYPE_VALUE] = KDI_HEXAHEDRON

    kdi2nbPoints = np.full(shape=KDI_SIZE, fill_value=0, dtype=np.int8)
    assert(KDI_TRIANGLE < len(kdi2nbPoints))
    kdi2nbPoints[KDI_TRIANGLE] = 3
    assert(KDI_QUADRANGLE < len(kdi2nbPoints))
    kdi2nbPoints[KDI_QUADRANGLE] = 4
    assert(KDI_HEXAHEDRON < len(kdi2nbPoints))
    kdi2nbPoints[KDI_HEXAHEDRON] = 8

    CurrentBase = {
        '/code2kdi': code2kdi,
        '/current/types': KDIMemoryMOC()
    }

    code2nbPoints = kdi2nbPoints[CurrentBase['/code2kdi']]

    CurrentParams = [code2nbPoints, '/current/types']

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_offsets_mt, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'], len(variables['Ret']))

    # Time:  0.3081662654876709
    # Time:  0.1419830322265625
    # Time:  0.11855173110961914
    # Time:  0.009302854537963867


if __name__ == '__main__':
    test_unit_kdi_update_eval_offsets()
    print('\ntest_unit_kdi_update_eval_offsets: checked')
