class Test(object):

    def __init__(self):
        self.count = 0

    def test(self):
        self.count += 1
        print('incr count', self.count)
        return self.count


from rpc.zmqrpcserver import ZMQRPCServer, LISTEN, CONNECT

server = ZMQRPCServer(Test)
server.queue('tcp://0.0.0.0:5000', thread=True)
server.work(workers=2)

# L'objet de ce test c'est de lancer en multi thread 2 serveurs
#