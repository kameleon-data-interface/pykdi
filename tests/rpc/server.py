def add(a, b):
    print('compute add', a, b)
    return a+b

def sub(a, b):
    print('compute sub', a, b)
    return a-b

from rpc.rpcserver import RPCServer

server = RPCServer()

server.registerFunction(add)
server.registerFunction(sub)

server.run()

# Dans le repertoire ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc:
# ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc$ ~/Téléchargements/PaDaWAn-Legacy-master/venv/bin/python3 server.py
# ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc$ ~/Téléchargements/PaDaWAn-Legacy-master/venv/bin/python3 client.py
#
# Le comportement semble conforme :
# - le client demande un calcul en transmettant deux valeurs
# - le serveur le reçoit et réalise le calcul et transmet le résultat
# - le client affiche le résultat
#
# La declaration de la méthode se fait par méthode au niveau du serveur.
