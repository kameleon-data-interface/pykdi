class Mathematics:
    def add(a, b):
        print('compute Mathematics.add', a, b)
        return a+b

    def sub(a, b):
        print('compute Mathematics.sub', a, b)
        return a-b

from rpc.rpcserver import RPCServer

server = RPCServer()

server.registerInstance(Mathematics)

server.run()

# Dans le repertoire ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc:
# ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc$ ~/Téléchargements/PaDaWAn-Legacy-master/venv/bin/python3 server_by_registration.py
# ~/PyCharmProjects/pykdi_master_0_16_0/tests/rpc$ ~/Téléchargements/PaDaWAn-Legacy-master/venv/bin/python3 client.py
#
# Le comportement semble conforme :
# - le client demande un calcul en transmettant deux valeurs
# - le serveur le reçoit et réalise le calcul et transmet le résultat
# - le client affiche le résultat
#
# La declaration de la méthode se fait par méthode d'une instance de classe au niveau du serveur.
#
# Il n'y a pas d'enregistrement de l'instance de classe et d'identification du côté client.
