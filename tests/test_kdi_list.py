from pykdi import KDIList, KDIFields, KDISubMeshes

"""
Fr: Ce test valide la classe KDIList et ses dérivées

Use pykdi:
- KDIList, KDIFields, KDISubMeshes
"""


def evaluation(kdi_list, expected_1, expected_2, expected_3):

    try:
        assert (kdi_list.get() == expected_1)
    except NotImplementedError:
        assert (False)

    try:
        kdi_list.erase()
    except NotImplementedError:
        assert (False)

    try:
        assert (kdi_list.get() == expected_1)
    except NotImplementedError:
        assert (False)

    try:
        kdi_list.insert('a')
        kdi_list.insert('b')
        kdi_list.insert('d')
    except NotImplementedError:
        assert (False)

    try:
        assert (kdi_list.get() == expected_2)
    except NotImplementedError:
        assert (False)

    try:
        kdi_list.erase()
    except NotImplementedError:
        assert (False)

    try:
        assert (kdi_list.get() == expected_2)
    except NotImplementedError:
        assert (False)

    assert (str(kdi_list) == expected_3)


def test_kdi_list_a():
    expected_1 = []
    expected_2 = ['a', 'b', 'd']
    expected_3 = "({'a','b','d',})"
    kdi_list = KDIList(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDIList' + expected_3)
    kdi_list = KDIFields(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDIFields' + expected_3)
    kdi_list = KDISubMeshes(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDISubMeshes' + expected_3)

def test_kdi_list_b():
    expected_1 = []
    expected_2 = ['a', 'b', 'd']
    expected_3 = "({'a','b','d',})"
    kdi_list = KDIList(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDIList' + expected_3)
    kdi_list = KDIFields(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDIFields' + expected_3)
    kdi_list = KDISubMeshes(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDISubMeshes' + expected_3)

def test_kdi_list_c():
    expected_1 = ['c', 'e']
    expected_2 = ['a', 'b', 'c', 'd', 'e']
    expected_3 = "({'a','b','c','d','e',})"
    kdi_list = KDIList(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDIList' + expected_3)
    kdi_list = KDIFields(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDIFields' + expected_3)
    kdi_list = KDISubMeshes(expected_1)
    evaluation(kdi_list, expected_1, expected_2, 'KDISubMeshes' + expected_3)

def test_kdi_list_d():
    expected_1 = ['c', 'e']
    expected_2 = ['a', 'b', 'c', 'd', 'e']
    expected_3 = "({'a','b','c','d','e',})"
    kdi_list = KDIList(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDIList' + expected_3)
    kdi_list = KDIFields(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDIFields' + expected_3)
    kdi_list = KDISubMeshes(set(expected_1))
    evaluation(kdi_list, expected_1, expected_2, 'KDISubMeshes' + expected_3)


if __name__ == '__main__':
    test_kdi_list_a()
    test_kdi_list_b()
    test_kdi_list_c()
    test_kdi_list_d()
    print('\ntest_kdi_list: checked')
