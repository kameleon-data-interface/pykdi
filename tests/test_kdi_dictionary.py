from pykdi import Dictionary

"""
Fr: Ce test valide la creation des différents objets du dictionnaire

Use pykdi.dictionary:
- dictionary .check, .get, .simple_type_parse .complex_type_parse
"""


def test_kdi_dictionary():
    dico = Dictionary()
    dico.check()

    assert (str(dico.get('Base')) == "{'/agreement': 'Agreement', '/glu': 'Glu', '/study': 'Study', '/chunks': 'Chunks', '/fields': 'Fields'}")

    assert (str(dico.get('SubUnstructuredGrid')['/cells']) == "Cells{/fields}")

    basic_type, restricted_attributes = dico.complex_type_parse("Cells{/fields,/globalIndexContinue}")
    assert (basic_type == "Cells")
    assert (str(restricted_attributes) == "['/fields', '/globalIndexContinue']")

    assert (dico.get("Date") == "@string")
    basic_dtype, shape_dtype = dico.simple_type_parse(dico.get("Date"))
    assert (basic_dtype == "string")
    assert (shape_dtype == [])

    assert (dico.get("Connectivity") == "@int[]")
    basic_dtype, shape_dtype = dico.simple_type_parse(dico.get("Connectivity"))
    assert (basic_dtype == "int")
    assert (shape_dtype == [None])

    assert (dico.get("CartesianCoordinates") == "@float[][3]")
    basic_dtype, shape_dtype = dico.simple_type_parse(dico.get("CartesianCoordinates"))
    assert (basic_dtype == "float")
    assert (shape_dtype == [None, 3])


if __name__ == '__main__':
    test_kdi_dictionary()
    print('\ntest_kdi_dictionary: checked')
