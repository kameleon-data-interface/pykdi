"""
Fr: Ce test valide la description d'un maillage de 6 cellules distribuées sur trois partitions.
"""

import copy
from mpi4py import MPI
import numpy as np
import os

from pykdi import *

kdi_test_with_global_index_continuous = kdi_get_env('KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS', False)
kdi_test_with_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_VTK_HDF', True)
kdi_test_with_nto1_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF', False)
kdi_test_with_saving_json = kdi_get_env('KDI_TEST_WITH_SAVING_JSON', False)
kdi_test_with_loading_json = kdi_get_env('KDI_TEST_WITH_LOADING_JSON', False)


def reset_kdi_test_1uns_2subuns_3ss_10tps():
    global kdi_test_with_global_index_continuous
    global kdi_test_with_saving_vtkhdf
    global kdi_test_with_nto1_saving_vtkhdf
    global kdi_test_with_saving_json
    global kdi_test_with_loading_json
    kdi_test_with_global_index_continuous = kdi_get_env('KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS', False)
    kdi_test_with_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_VTK_HDF', True)
    kdi_test_with_nto1_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF', False)
    kdi_test_with_saving_json = kdi_get_env('KDI_TEST_WITH_SAVING_JSON', False)
    kdi_test_with_loading_json = kdi_get_env('KDI_TEST_WITH_LOADING_JSON', False)

kdi_test_log = KDILog('TEST', True)

SIMULATION_TRIANGLE_TYPE_VALUE: int = 0
SIMULATION_QUADRANGLE_TYPE_VALUE: int = 1

data_common = {
    '/glu/version': np.array([1, 0]),
    '/study/name': 'MyEtude',
    '/study/date': '2024/05',
    '/fields/myGlobScalarField': np.array([0]),
    '/fields/myGlobVectorField': np.array([2, 1, 0]),
}

data_ssd = [
    {
        '/mymesh/points/globalIndexContinuous': np.array([5, 6, 2, 3, 8, 9]),
        '/mymesh/points/cartesianCoordinates': np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [1., 1., 0.],
                                                         [0., -1., 0.], [1., -1., 0.]], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([1., 2., 2., 3., 1., 1.], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([3, 2, 6]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE,
                                         SIMULATION_QUADRANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0,], dtype=int),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69, 89], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/myGIC': np.array([3, 2, 6], dtype=int),
        '/mymesh/cells/fields/mySSD': np.array([0, 0, 0], dtype=int),

        # copie glob_
        '/mymesh/points/fields/glob_myFieldPoint': np.array([1., 2., 2., 3., 1., 1.], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell': np.array([42, 69, 89], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/glob_myGIC': np.array([3, 2, 6], dtype=int),
        '/mymesh/cells/fields/glob_mySSD': np.array([0, 0, 0], dtype=int),

        '/mymesh/cells/offsets': np.array([0, 3, 6, 10, ]),  # compute

        # compute
        '/mymil1/points/cartesianCoordinates': np.array(
            [[0., 0., 0.], [1., 0., 0.], [1., 1., 0.], [0., -1., 0.], [1., -1., 0.]], dtype=float),
        '/mymil1/points/fields/glob_myFieldPoint': np.array([1., 2., 3., 1., 1.], dtype=float),
        '/mymil1/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_QUADRANGLE_TYPE_VALUE]),
        '/mymil1/cells/connectivity': np.array([0, 1, 2, 3, 4, 1, 0], dtype=int),
        '/mymil1/cells/fields/glob_myFieldCell': np.array([42, 89], dtype=float),
        '/mymil1/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil1/cells/fields/glob_myGIC': np.array([3, 6], dtype=int),
        '/mymil1/cells/fields/glob_mySSD': np.array([0, 0], dtype=int),
        '/mymil1/cells/fields/myFieldCell3': np.array([30.01, 30.02], dtype=float),

        '/mymil1/cells/offsets': np.array([0, 3, 7, ]),

        # compute
        '/mymil2/points/cartesianCoordinates': np.array(
            [[0., 0., 0.], [1., 1., 0.],[0., 1., 0.], ], dtype=float),
        '/mymil2/points/fields/glob_myFieldPoint': np.array([1., 3., 2., ], dtype=float),
        '/mymil2/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE,]),
        '/mymil2/cells/connectivity': np.array([0, 1, 2], dtype=int),
        '/mymil2/cells/fields/glob_myFieldCell': np.array([69, ], dtype=float),
        '/mymil2/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil2/cells/fields/glob_myGIC': np.array([2, ], dtype=int),
        '/mymil2/cells/fields/glob_mySSD': np.array([0, ], dtype=int),
        '/mymil2/cells/fields/myFieldCell3': np.array([40.01], dtype=float),

        '/mymil2/cells/offsets': np.array([0, 3, ]),
    },
    {
        '/mymesh/points/globalIndexContinuous': np.array([6, 7, 4, 3]),
        '/mymesh/points/cartesianCoordinates': np.array([[1., 0., 0.], [2, 0., 0.], [2., 1., 0.], [1., 1., 0.]], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 1., 2., 3.], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([5, 4]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0], dtype=int),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/myGIC': np.array([5, 4], dtype=int),
        '/mymesh/cells/fields/mySSD': np.array([1, 1], dtype=int),

        # copie glob_
        '/mymesh/points/fields/glob_myFieldPoint': np.array([2., 1., 2., 3.], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/glob_myGIC': np.array([5, 4], dtype=int),
        '/mymesh/cells/fields/glob_mySSD': np.array([1, 1], dtype=int),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),

        # compute
        '/mymil1/points/cartesianCoordinates': np.array(
            [[1., 0., 0.], [2., 0., 0.], [2., 1., 0.], ], dtype=float),
        '/mymil1/points/fields/glob_myFieldPoint': np.array([2., 1., 2., ], dtype=float),
        '/mymil1/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymil1/cells/connectivity': np.array([0, 1, 2, ], dtype=int),
        '/mymil1/cells/fields/glob_myFieldCell': np.array([42, ], dtype=float),
        '/mymil1/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil1/cells/fields/glob_myGIC': np.array([5, ], dtype=int),
        '/mymil1/cells/fields/glob_mySSD': np.array([1, ], dtype=int),
        '/mymil1/cells/fields/myFieldCell3': np.array([30.01], dtype=float),

        '/mymil1/cells/offsets': np.array([0, 3, ]),

        # compute
        '/mymil2/points/cartesianCoordinates': np.array(
            [[2., 1., 0.], [1., 1., 0.], [1., 0., 0.], ], dtype=float),
        '/mymil2/points/fields/glob_myFieldPoint': np.array([2., 3., 2., ], dtype=float),
        '/mymil2/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymil2/cells/connectivity': np.array([0, 1, 2, ], dtype=int),
        '/mymil2/cells/fields/glob_myFieldCell': np.array([69, ], dtype=float),
        '/mymil2/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil2/cells/fields/glob_myGIC': np.array([4, ], dtype=int),
        '/mymil2/cells/fields/glob_mySSD': np.array([1, ], dtype=int),
        '/mymil2/cells/fields/myFieldCell3': np.array([40.01], dtype=float),

        '/mymil2/cells/offsets': np.array([0, 3, ]),
    },
    {
        '/mymesh/points/globalIndexContinuous': np.array([2, 3, 1, 0]),
        '/mymesh/points/cartesianCoordinates': np.array([[0., 1., 0.], [1., 1., 0.], [1., 2., 0.], [0., 2., 0.]], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 3., 2., 1.], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([1, 0, ]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0], dtype=int),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/myGIC': np.array([1, 0], dtype=int),
        '/mymesh/cells/fields/mySSD': np.array([2, 2], dtype=int),

        # copie glob_
        '/mymesh/points/fields/glob_myFieldPoint': np.array([2., 3., 2., 1.], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymesh/cells/fields/glob_myGIC': np.array([1, 0], dtype=int),
        '/mymesh/cells/fields/glob_mySSD': np.array([2, 2], dtype=int),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),

        # compute
        '/mymil1/points/cartesianCoordinates': np.array(
            [[0., 1., 0.], [1., 1., 0.], [1., 2., 0.], ], dtype=float),
        '/mymil1/points/fields/glob_myFieldPoint': np.array([2., 3., 2., ], dtype=float),
        '/mymil1/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymil1/cells/connectivity': np.array([0, 1, 2, ], dtype=int),
        '/mymil1/cells/fields/glob_myFieldCell': np.array([42, ], dtype=float),
        '/mymil1/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil1/cells/fields/glob_myGIC': np.array([1, ], dtype=int),
        '/mymil1/cells/fields/glob_mySSD': np.array([2, ], dtype=int),
        '/mymil1/cells/fields/myFieldCell3': np.array([30.01], dtype=float),

        '/mymil1/cells/offsets': np.array([0, 3, ]),

        # compute
        '/mymil2/points/cartesianCoordinates': np.array(
            [[1., 2., 0.], [0., 2., 0.], [0., 1., 0.], ], dtype=float),
        '/mymil2/points/fields/glob_myFieldPoint': np.array([2., 1., 2., ], dtype=float),
        '/mymil2/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymil2/cells/connectivity': np.array([0, 1, 2, ], dtype=int),
        '/mymil2/cells/fields/glob_myFieldCell': np.array([69, ], dtype=float),
        '/mymil2/cells/fields/glob_myFieldCell2': np.array([], dtype=float),
        '/mymil2/cells/fields/glob_myGIC': np.array([0, ], dtype=int),
        '/mymil2/cells/fields/glob_mySSD': np.array([2, ], dtype=int),
        '/mymil2/cells/fields/myFieldCell3': np.array([40.01], dtype=float),

        '/mymil2/cells/offsets': np.array([0, 3, ]),
    }
]


def get_common_data(base, name):
    if name == '/study/name':
        return np.array([data_common[name]])
    if name == '/study/date':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return np.array(['{}/{:02d}'.format(data_common[name], itps + 1)])
    if name == '/fields/myGlobScalarField':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return data_common[name] * 2 + itps
    if name == '/fields/myGlobVectorField':
        chunks = base['/chunks/current']
        itps = base['/chunks'].GetOffset('step', chunks['step'])
        return data_common[name] + np.array([1, 2, 3]) * itps
    return data_common[name]


def get_step_part__data(base, name):
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    ipart = base['/chunks'].GetOffset('part', chunks['part'])
    if name == '/mymesh/cells/fields/myFieldCell2' or name == '/mymesh/cells/fields/glob_myFieldCell2':
        array = np.zeros(data_ssd[ipart]['/mymesh/cells/fields/myFieldCell'].shape, dtype=float)
        array += itps + ipart / 100.
        return array
    if name == '/mymil1/cells/fields/glob_myFieldCell2':
        array = np.zeros(data_ssd[ipart]['/mymil1/cells/fields/glob_myFieldCell'].shape, dtype=float)
        array += itps + ipart / 100.
        return array
    if name == '/mymil2/cells/fields/glob_myFieldCell2':
        array = np.zeros(data_ssd[ipart]['/mymil2/cells/fields/glob_myFieldCell'].shape, dtype=float)
        array += itps + ipart / 100.
        return array
    if name == '/mymesh/points/cartesianCoordinates':
        delta = float(itps) / 10.
        if ipart == 0:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array(
                        [[-delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [-delta, -1 - 2 * delta, 0.],
                         [0., 0., 0.], ]))
        if ipart == 1:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], ]))
        if ipart == 2:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [-delta, delta, 0.], ]))
    if name == '/mymil1/points/cartesianCoordinates':
        delta = float(itps) / 10.
        if ipart == 0:
            return (data_ssd[ipart]['/mymil1/points/cartesianCoordinates'] +
                    np.array(
                        [[-delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], [-delta, -1 - 2 * delta, 0.],
                         [0., 0., 0.], ]))
        if ipart == 1:
            return (data_ssd[ipart]['/mymil1/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [delta, -delta, 0.], [0., 0., 0.], ]))
        if ipart == 2:
            return (data_ssd[ipart]['/mymil1/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.], ]))
    if name == '/mymil2/points/cartesianCoordinates':
        delta = float(itps) / 10.
        if ipart == 0:
            return (data_ssd[ipart]['/mymil2/points/cartesianCoordinates'] +
                    np.array([[-delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.],]))
        if ipart == 1:
            return (data_ssd[ipart]['/mymil2/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.], ]))
        if ipart == 2:
            return (data_ssd[ipart]['/mymil2/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [-delta, delta, 0.],  [0., 0., 0.], ]))
    return data_ssd[ipart][name]


def checked(msg, base, ignored, renamed,
            first: bool = True, with_study_date: bool = True, with_global_fields: bool = True,
            ignored_data_key: set = None):
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    ipart = base['/chunks'].GetOffset('part', chunks['part'])
    str_chunks = '(itps: ' + str(itps) + ', part: ' + str(ipart) + ')'
    sum_checked = 0
    sum_ignored = 0
    for data_key in data_common.keys():
        splitkey = data_key.split('/')
        if len(splitkey) > 2 and splitkey[1] in ignored:
            if ignored_data_key is not None:
                ignored_data_key.add(data_key)
            sum_ignored += 1
            continue
        if not with_study_date and data_key == '/study/date':
            if ignored_data_key is not None:
                ignored_data_key.add(data_key)
            sum_ignored += 1
            continue
        if not with_global_fields and data_key[:8] == '/fields/':
            if ignored_data_key is not None:
                ignored_data_key.add(data_key)
            sum_ignored += 1
            continue
        key = data_key
        for renkey, renvalue in renamed.items():
            if renkey[:len(renkey)] == key[:len(renkey)]:
                key = renvalue + key[len(renkey):]

        if isinstance(data_common[key], str):
            if get_common_data(base, key) != base[key].get():
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> A. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_common_data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Differents values ' + key + '!' + str_chunks)
        else:
            if not np.all(get_common_data(base, key) == base[key].get()):
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> B. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_common_data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Unexpected values ' + key + '!' + str_chunks)
        sum_checked += 1

    for data_key in data_ssd[ipart].keys():
        splitkey = data_key.split('/')
        if len(splitkey) > 2 and splitkey[1] in ignored:
            continue

        key = data_key

        if not kdi_test_with_global_index_continuous:
            if (pos := key.find('/points/globalIndexContinuous')) != -1:
                assert (len(key) == pos + len('/points/globalIndexContinuous'))
                if ignored_data_key is not None:
                    ignored_data_key.add(data_key)
                sum_ignored += 1
                continue
            if (pos := key.find('/cells/globalIndexContinuous')) != -1:
                assert (len(key) == pos + len('/cells/globalIndexContinuous'))
                if ignored_data_key is not None:
                    ignored_data_key.add(data_key)
                sum_ignored += 1
                continue
            sum_ignored += 1
            continue

        for renkey, renvalue in renamed.items():
            if renkey[:len(renkey)] == key[:len(renkey)]:
                key = renvalue + key[len(renkey):]

        if isinstance(data_ssd[ipart][key], str):
            if get_step_part__data(base, key) != base[key].get():
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> C. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_step_part__data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Differents values ' + key + '!' + str_chunks)
        else:
            try:
                if not np.all(get_step_part__data(base, key) == base[key].get()):
                    kdi_test_log.fatal_exception_header()
                    kdi_test_log.fatal_exception_body('>>>>> D. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                    kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                    kdi_test_log.fatal_exception_body(base[key].get())
                    kdi_test_log.fatal_exception_body('... vs ...')
                    kdi_test_log.fatal_exception_body(get_step_part__data(base, key))
                    kdi_test_log.fatal_exception_footer()
                    raise KDIException('Unexpected values \'' + key + '\'!')
            except KeyError:
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> E. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(get_step_part__data(base, key))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(key, 'in base ?', (key in base))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Unexpected key \'' + key + '\' in base!')
            except KDIException:
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> F. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(get_step_part__data(base, key))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(key, 'in base ?', (key in base))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_footer()
                raise KDIException()
        sum_checked += 1

    if not first or itps == 0.:
        return sum_checked, sum_ignored

    current = base['/chunks/current']
    del base['/chunks/current']
    base['/chunks'].set({'step': itps - 1, 'part': ipart})
    add_checked, add_ignored = checked(msg, base, ignored, renamed, first=False, with_study_date=with_study_date,
                                       with_global_fields=with_global_fields, ignored_data_key=ignored_data_key)
    base['/chunks/current'] = current

    return sum_checked + add_checked, sum_ignored + add_ignored


def checked_all(msg, partitions_list, base, ignored, renamed, with_study_date=True, with_global_fields=True,
                ignored_data_key: set = None):
    sum_checked, sum_ignored = 0, 0
    chunks = base['/chunks/current']
    itps = base['/chunks'].GetOffset('step', chunks['step'])
    for ipart in partitions_list:
        base['/chunks'].set({'step': itps, 'part': ipart})
        add_checked, add_ignored = checked(msg, base, ignored, renamed, with_study_date=with_study_date,
                                           with_global_fields=with_global_fields,
                                           ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored
    return sum_checked, sum_ignored


def test_1uns_2subuns_3ssd_10tps():
    ignored_data_key = set()
    sum_checked, sum_ignored = 0, 0

    mpi_rank = MPI.COMM_WORLD.Get_rank()
    mpi_size = MPI.COMM_WORLD.Get_size()

    nb_parts = 3

    if mpi_size == 1:
        partitions_list = [0, 1, 2]
        vtkhdf_filename = '1pe_1uns_2subuns_3ssd_10tps'
    elif mpi_size == nb_parts:
        partitions_list = [mpi_rank]
        vtkhdf_filename = '3pe_1uns_2subuns_3ssd_10tps'
    else:
        raise Exception("This test works in sequential and with 3 processes in parallel.")

    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': nb_parts})

    for itps in range(10):

        float_step = float(itps)

        kdi_test_log.log('steps', float_step)
        kdi_test_log.push_indent(2)

        for ipart  in partitions_list:

            kdi_test_log.log('ipart', ipart)
            kdi_test_log.push_indent(2)

            # a-temporel and a-partition declinaison
            base['/chunks'].set({'step': float_step})

            key = '/glu/version'
            base[key].insert(get_common_data(base, key))

            key = '/study/name'
            base[key].insert(get_common_data(base, key))

            # temporel and a-partition declinaison
            base['/chunks'].set({'step': float_step})

            key = '/study/date'
            base[key].insert(get_common_data(base, key))

            kdi_update_fields(base, '/fields', 'myGlobScalarField')
            key = '/fields/myGlobScalarField'
            base[key].insert(get_common_data(base, key))

            kdi_update_fields(base, '/fields', 'myGlobVectorField')
            key = '/fields/myGlobVectorField'
            base[key].insert(get_common_data(base, key))

            # temporal and partition declinaison
            base['/chunks'].set({'step': float_step, 'part': ipart})

            kdi_update(base, 'UnstructuredGrid', '/mymesh')
            
            if kdi_test_with_global_index_continuous:
                kdi_test_log.log('defined', '/mymesh/points/globalIndexContinuous')
                key = '/mymesh/points/globalIndexContinuous'
                base[key].insert(get_step_part__data(base, key))

            key = '/mymesh/points/cartesianCoordinates'
            base[key].insert(get_step_part__data(base, key))

            kdi_update_fields(base, '/mymesh/points/fields', 'myFieldPoint')
            key = '/mymesh/points/fields/myFieldPoint'
            base[key].insert(get_step_part__data(base, key))

            if kdi_test_with_global_index_continuous:
                kdi_test_log.log('defined', '/mymesh/cells/globalIndexContinuous')
                key = '/mymesh/cells/globalIndexContinuous'
                base[key].insert(get_step_part__data(base, key))

            key = '/mymesh/cells/types'
            base[key].insert(get_step_part__data(base, key))

            key = '/mymesh/cells/connectivity'
            base[key].insert(get_step_part__data(base, key))

            kdi_update_eval_offsets(base, '/mymesh/cells')

            kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell')
            key = '/mymesh/cells/fields/myFieldCell'
            base[key].insert(get_step_part__data(base, key))

            kdi_update_fields(base, '/mymesh/cells/fields', 'myFieldCell2')
            key = '/mymesh/cells/fields/myFieldCell2'
            base[key].insert(get_step_part__data(base, key))

            kdi_update_fields(base, '/mymesh/cells/fields', 'myGIC')
            key = '/mymesh/cells/fields/myGIC'
            base[key].insert(get_step_part__data(base, key))

            kdi_update_fields(base, '/mymesh/cells/fields', 'mySSD')
            key = '/mymesh/cells/fields/mySSD'
            base[key].insert(get_step_part__data(base, key))

            add_checked, add_ignored = checked('Init by memory (without mymil#)', base,
                                               ignored=['mymil1', 'mymil2'],
                                               renamed={'/mymesh/cells/fields/glob_my': '/mymesh/cells/fields/my',
                                                        '/mymesh/points/fields/glob_my': '/mymesh/points/fields/my'},
                                               ignored_data_key=ignored_data_key)
            sum_checked += add_checked
            sum_ignored += add_ignored

            str_my_mil1 = kdi_update_sub(base, '/mymesh', '/mymil1')
            kdi_update_fields(base, str_my_mil1+'/cells/fields', 'mylocFieldCell')
            kdi_update_fields(base, str_my_mil1+'/cells/fields', 'mylocFieldCell3')
            if ipart == 0:
                base[str_my_mil1+'/indexescells'].insert(np.array([0, 2]))
                base[str_my_mil1+'/cells/fields/mylocFieldCell'].insert(np.array([10.01, 10.02]))
                base[str_my_mil1+'/cells/fields/mylocFieldCell3'].insert(np.array([30.01, 30.02]))
            else:
                base[str_my_mil1+'/indexescells'].insert(np.array([0]))
                base[str_my_mil1+'/cells/fields/mylocFieldCell'].insert(np.array([10.01]))
                base[str_my_mil1+'/cells/fields/mylocFieldCell3'].insert(np.array([30.01]))

            str_my_mil2 = kdi_update_sub(base, '/mymesh', '/mymil2')
            base[str_my_mil2+'/indexescells'].insert(np.array([1]))
            kdi_update_fields(base, str_my_mil2+'/cells/fields', 'mylocFieldCell')
            base[str_my_mil2+'/cells/fields/mylocFieldCell'].insert(np.array([20.01, ]))
            kdi_update_fields(base, str_my_mil2+'/cells/fields', 'mylocFieldCell3')
            base[str_my_mil2+'/cells/fields/mylocFieldCell3'].insert(np.array([40.01]))
            kdi_update_fields(base, str_my_mil2+'/cells/fields', 'mylocFieldCell4')
            base[str_my_mil2+'/cells/fields/mylocFieldCell4'].insert(np.array([68]))

            add_checked, add_ignored = checked('Init by memory (with mymil# but ignored)', base,
                                               ignored=['mymil1', 'mymil2'],
                                               renamed={'/mymesh/cells/fields/glob_my': '/mymesh/cells/fields/my',
                                                        '/mymesh/points/fields/glob_my': '/mymesh/points/fields/my'},
                                               ignored_data_key=ignored_data_key)
            sum_checked += add_checked
            sum_ignored += add_ignored

            newbase = KDIComputeMultiMilieux(base)

            add_checked, add_ignored = checked('Init by memory (with ComputeMultiMilieux, mymil# no ignored)', newbase,
                    ignored=['mymil1', 'mymil2'],
                    renamed={'/mymesh/cells/fields/my': '/mymesh/cells/fields/glob_my',
                             '/mymesh/points/fields/my': '/mymesh/points/fields/glob_my'},
                    ignored_data_key=ignored_data_key)
            sum_checked += add_checked
            sum_ignored += add_ignored

            kdi_test_log.pop_indent()

        add_checked, add_ignored = checked_all('Init by memory', partitions_list, base,
                                               ignored=['mymil1', 'mymil2'],
                                               renamed={'/mymesh/cells/fields/glob_my': '/mymesh/cells/fields/my',
                                                        '/mymesh/points/fields/glob_my': '/mymesh/points/fields/my'},
                                               ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored

        # == SAVE VTK HDF + JSON, LOAD JSON ============================
        base['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step})

        if KDIVTKHDFCheck(base):
            raise Exception('Checked base anormaly cause existence SubUnstructuredGrid!')

        if kdi_test_with_nto1_saving_vtkhdf:
            assert ('pre: mpi_size>1 to saveVTKHDF_Nto1' and mpi_size > 1)
            assert ('pre: with GIC to saveVTKHDF_Nto1' and kdi_test_with_global_index_continuous)
            kdi_test_log.log('saveVTKHDF_Nto1', vtkhdf_filename + '__one.vtkhdf')
            write_vtk_hdf_n_to_1(base, vtkhdf_filename + '__one.vtkhdf')
            # This method does not modify base and does not save the JSON file.

            assert ('pre: mpi_size>1 to saveVTKHDF_Nto1' and mpi_size > 1)
            assert ('pre: with GIC to saveVTKHDF_Nto1' and kdi_test_with_global_index_continuous)
            kdi_test_log.log('saveVTKHDF_Nto1', vtkhdf_filename + '__multi__one.vtkhdf')
            write_vtk_hdf_n_to_1(newbase, vtkhdf_filename + '__multi__one.vtkhdf')
            # This method does not modify base and does not save the JSON file.

        if kdi_test_with_saving_vtkhdf:
            if mpi_size > 1 and kdi_test_with_global_index_continuous:
                kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__hard.vtkhdf')
                write_vtk_hdf(base, vtkhdf_filename + '__hard.vtkhdf', with_json=kdi_test_with_saving_json)

                newbase_cp = KDIComputeMultiMilieux(base)

                kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__multi.vtkhdf')
                write_vtk_hdf(newbase_cp, vtkhdf_filename + '__multi.vtkhdf', with_json=False)
            else:
                kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__hard.vtkhdf')
                write_vtk_hdf(base, vtkhdf_filename + '__hard.vtkhdf', with_json=kdi_test_with_saving_json)

                newbase_cp = KDIComputeMultiMilieux(base)

                kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__multi.vtkhdf')
                write_vtk_hdf(newbase_cp, vtkhdf_filename + '__multi.vtkhdf', with_json=False)

            if kdi_test_with_loading_json:
                del base
                kdi_test_log.log('loadJSON', vtkhdf_filename + '__hard.json')
                base = read_json(vtkhdf_filename + '__hard.json')

        add_checked, add_ignored = checked_all('Init by memory', partitions_list, base,
                                               ignored=['mymil1', 'mymil2'],
                                               renamed={'/mymesh/cells/fields/glob_my': '/mymesh/cells/fields/my',
                                                        '/mymesh/points/fields/glob_my': '/mymesh/points/fields/my'},
                                               ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored

        add_checked, add_ignored = checked('Init by memory (with ComputeMultiMilieux, mymil# no ignored)', newbase,
                                           ignored=['mymil1', 'mymil2'],
                                           renamed={'/mymesh/cells/fields/my': '/mymesh/cells/fields/glob_my',
                                                    '/mymesh/points/fields/my': '/mymesh/points/fields/glob_my'},
                                           ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored

        kdi_test_log.pop_indent()

        # saveVTKHDF(base, vtkhdf_filename + '__hard.vtkhdf')
        #
        # base_cp = copy.deepcopy(base)
        # saveVTKHDF(base_cp, vtkhdf_filename + '__hard_B.vtkhdf', with_clean_data_submeshes=True)
        # del base_cp
        #
        # if kdi_test_with_loading_json:
        #     base_after_load = loadJSON(vtkhdf_filename + '__hard.json')
        #     assert (base.keys() == base_after_load.keys())
        #
        # newbase['/chunks'].set({KVTKHDF_VARIANT_STEP: float_step})
        #
        # if not KDIVTKHDFCheck(newbase):
        #     raise Exception('No check base anormaly for VTK HDF!')
        # try:
        #     if mpi_size > 1 and kdi_test_with_global_index_continuous:
        #         saveVTKHDF_Nto1(base, vtkhdf_filename + '__hard__one.vtkhdf')
        #         # Cette méthode ne modifie pas base et ne sauvegarde pas le fichier JSON
        #
        #     newbase = KDIComputeMultiMilieux(base)
        #
        #     if mpi_size > 1 and kdi_test_with_global_index_continuous:
        #         saveVTKHDF_Nto1(newbase, vtkhdf_filename + '__multi__one.vtkhdf')
        #         # Cette méthode ne modifie pas base et ne sauvegarde pas le fichier JSON
        #
        #     newbase_after_load = KDIComputeMultiMilieux(base)
        #
        #     saveVTKHDF(newbase, vtkhdf_filename + '__multimil.vtkhdf')
        #
        #     saveVTKHDF(newbase_after_load, vtkhdf_filename + '__multimil_bis.vtkhdf')
        #
        #     # TODO COmpare h5dump ces bases
        #
        #     # TODO Not exist this file normally cause     new_base['/before_write']
        #     #newbase_after_load = loadJSON(vtkhdf_filename + '__multimil.json')
        #
        #     for key in newbase_after_load.keys():
        #         if key in ['/chunks/current']:
        #             continue
        #         before_load = str(newbase[key])
        #         after_load = str(newbase_after_load[key])
        #         assert (before_load == after_load)
        #
        # except KDIExceptionNoWriteFileJson:
        #     pass
        #
        # if kdi_test_with_loading_json:
        #     base = base_after_load

    kdi_test_log.log('sum_checked', sum_checked)
    kdi_test_log.log('sum_ignored', sum_ignored)
    kdi_test_log.log('ignored_data_key', ignored_data_key)

    if kdi_test_with_global_index_continuous:
        if mpi_size == 3:
            assert (sum_checked == 570 + 1824)
            assert (sum_ignored == 0)
        else:
            assert (sum_checked == 1520 + 4864)
            assert (sum_ignored == 0)
        assert (ignored_data_key == set())
    else:
        if mpi_size == 3:
            assert (sum_checked == 570)
            assert (sum_ignored == 1824)
        else:
            assert (sum_checked == 1520)
            assert (sum_ignored == 4864)
        assert (ignored_data_key == {'/mymesh/cells/globalIndexContinuous', '/mymesh/points/globalIndexContinuous'})


def test_1uns_2subuns_3ssd_10tps__save_json():
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    reset_kdi_test_1uns_2subuns_3ss_10tps()
    test_1uns_2subuns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']


def test_1uns_2subuns_3ssd_10tps__save_and_load_json():
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    os.environ['KDI_TEST_WITH_LOADING_JSON'] = '1'
    reset_kdi_test_1uns_2subuns_3ss_10tps()
    test_1uns_2subuns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']
    del os.environ['KDI_TEST_WITH_LOADING_JSON']


def test_1uns_2subuns_3ssd_10tps_json():
    os.environ['KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS'] = '1'
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    os.environ['KDI_TEST_WITH_LOADING_JSON'] = '1'
    reset_kdi_test_1uns_2subuns_3ss_10tps()
    test_1uns_2subuns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']
    del os.environ['KDI_TEST_WITH_LOADING_JSON']
    del os.environ['KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS']


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    test_1uns_2subuns_3ssd_10tps()
    kdi_test_log.log('>>> test_1uns_2subuns_3ssd_10tps: checked')
    import sys
    if len(sys.argv) > 1:
        assert( sys.argv[1] == 'SIMPLE')
        exit(0)
    test_1uns_2subuns_3ssd_10tps__save_json()
    kdi_test_log.log('>>> test_1uns_2subuns_3ssd_10tps__save_json: checked')
    test_1uns_2subuns_3ssd_10tps__save_and_load_json()
    kdi_test_log.log('>>> test_1uns_2subuns_3ssd_10tps__save_and_load_json: checked')
    test_1uns_2subuns_3ssd_10tps_json()
    kdi_test_log.log('>>> test_1uns_2subuns_3ssd_10tps_json: checked')
