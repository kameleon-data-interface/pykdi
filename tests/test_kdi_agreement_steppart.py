import numpy as np

from pykdi import (KDIAgreementBaseStepPart, KDI_AGREEMENT_CONF_NB_PARTS, KDIException)

"""
Fr: Ce test valide les différents modes de création d'une base ainsi que des erreurs qui peuvent être déclenchées.

Use pykdi:
- KDIException

Use pykdi.agreement:
- KDIAgreementStepPartBase .chunk
- KDIAgreementStepPartChunks .step_part
"""


def test_kdi_agreement_steppart():
    nb_parts = 4

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     avec une valeur CORRECTE associée à 'nb_parts' de cette configuration
    #     décrit sous la forme d'un entier
    base = KDIAgreementBaseStepPart('filepath')
    base.set_conf_int(KDI_AGREEMENT_CONF_NB_PARTS, nb_parts)
    base.initialize()

    assert (str(base) == "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}")

    float_step = 2.3
    int_part = 2

    chunk = base.chunk(float_step, int_part)

    reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base) == reference)
    assert (str(chunk) == reference)

    assert (str(chunk._chunk) == '{\'step\': 2.3, \'part\': 2, \'#step\': \'2.3\', \'#part\': \'2\'}')

    chunk = base.chunk(float_step)

    assert (str(base) == reference)
    assert (str(chunk) == reference)

    assert (str(chunk._chunk) == '{\'step\': 2.3, \'#step\': \'2.3\'}')

    chunk = base.chunk()

    assert (str(base) == reference)
    assert (str(chunk) == reference)

    assert (str(chunk._chunk) == '{}')

    try:
        base.chunk(int_part=int_part)
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[0] == 'pre: float_step must be not None if int_part is not None!')

    float_step = 3.42

    chunk = base.chunk(float_step)

    reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3, 3.42])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base) == reference)
    assert (str(chunk) == reference)

    assert (str(chunk._chunk) == '{\'step\': 3.42, \'#step\': \'3.42\'}')


if __name__ == '__main__':
    test_kdi_agreement_steppart()
    print('\ntest_kdi_agreement_steppart: checked')
