import numpy as np

from pykdi import kdi_base, KDI_AGREEMENT_STEPPART, KDIEvent, KDIException

"""
Fr: Ce test valide la classe KDIEvent

Use pykdi:
- KDIEvent
"""

def test_kdi_event():
    class KDIEvalStringMOC:
        __slots__ = '_datas'
        def __init__(self):
            self._datas = {
                '/pair': np.array([2., 4., 6., 8., ]),
                '/impair': np.array([1., 3., 5., 7., 9., ]),
            }

        def get(self, chunk):
            return self._datas

    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 3})
    base['/chunks/current'] = {}

    caching = '/caching'
    base[caching] = KDIEvalStringMOC()

    kdi_event_pair = KDIEvent(base, caching, '/pair')

    try:
        kdi_event_pair.insert(data=None)
        assert (False)
    except NotImplementedError:
        pass

    kdi_event_pair.erase()

    assert (np.all(kdi_event_pair.get() == np.array([2., 4., 6., 8., ])))

    kdi_event_impair = KDIEvent(base, caching, '/impair')
    assert (np.all(kdi_event_impair.get() == np.array([1., 3., 5., 7., 9., ])))

    try:
        kdi_event_pair.serialize()
        assert (False)
    except NotImplementedError:
        pass

    # Fr: La clef n'existe pas pour la clef du cache
    kdi_event_pere = KDIEvent(base, caching, '/pere')
    try:
        kdi_event_pere.get()
    except KDIException as e:
        assert (e.args[0] == "pre: name key '/pere' is not in caching dict dict_keys(['/pair', '/impair'])")

    # Fr: La valeur de la clef du cache n'est pas un dictionnaire.

    class FailedMOC:
        __slots__ = '_datas'
        def __init__(self):
            self._datas = np.array([2., 4., 6., 8., ])

        def get(self, chunk):
            return self._datas

    caching2 = '/caching2'
    base[caching2] = FailedMOC()

    kdi_event_failed = KDIEvent(base, caching2, '/pair')
    try:
        kdi_event_failed.get()
    except KDIException as e:
        assert (e.args[0] == "pre: name caching '/caching2' is not dict")

    # Fr: La valeur de la clef du cache n'est pas KDIDatas proof.

    caching3 = '/caching3'
    base[caching3] = np.array([1, 2, 3,])

    kdi_event_failed = KDIEvent(base, caching3, '/pair')
    try:
        kdi_event_failed.get()
    except KDIException as e:
        assert (e.args[0] == "pre: name caching '/caching3' is key in base but non conform")

    # Fr: La valeur de la clef du cache n'existe pas.

    kdi_event_failed = KDIEvent(base, '/gloups', '/pair')
    try:
        kdi_event_failed.get()
    except KDIException as e:
        assert (e.args[0] == "pre: name caching '/gloups' is not key in base")


if __name__ == '__main__':
    test_kdi_event()
    print('\ntest_kdi_event: checked')
