from pykdi import KDIComplexType

"""
Fr: Ce test valide la classe KDIComplexType

Use pykdi:
- KDIComplexType
"""

def test_kdi_complex_type():
    kdi_complex_type = KDIComplexType('Dandelion')

    try:
        kdi_complex_type.insert(data=None)
        assert (False)
    except NotImplementedError:
        pass

    try:
        kdi_complex_type.erase()
        assert (False)
    except NotImplementedError:
        pass

    try:
        kdi_complex_type.get()
        assert (False)
    except NotImplementedError:
        pass

    assert (kdi_complex_type.get_complex_type() == 'Dandelion')

    assert (str(kdi_complex_type) == 'KDIComplexType(\'Dandelion\')')


if __name__ == '__main__':
    test_kdi_complex_type()
    print('\ntest_kdi_complex_type: checked')
