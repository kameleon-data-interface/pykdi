import json
import numpy as np
import os

from pykdi import *

from test_1uns_3ssd_10tps import checked_all, test_1uns_3ssd_10tps

main_program = False


def test_kdi_vtk_hdf_create_json_a():
    if not main_program:
        return
    # A executer que son mon portable
    kdi_vtk_hdf_create_json('/home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi_one.vtkhdf')
    print('test_kdi_vtk_hdf_create_json_a: create JSON')
    read_json('/home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi_one_CREATE.json')


def __compare_json_file(a_file_name: str, b_file_name: str):
    print('---------- Study on the difference between JSON files ----------')
    print('Open old JSON')
    with open(a_file_name, 'r') as fd:
        old_json = json.load(fd)
    print('Open new JSON')
    with open(b_file_name, 'r') as fd:
        new_json = json.load(fd)
    print('Compare')
    for key, new_val in new_json.items():
        try:
            old_val = old_json[key]
        except:
            print('WARNING key \'' + key + '\' not in old!')
            continue
        if new_val == old_val:
            continue
        print('VALUES IS DIFFERENT FOR', key)
        print('-> new_val:', new_val)
        print('-> old_val:', old_val)
    for key, old_val in old_json.items():
        try:
            new_json[key]
        except:
            print('WARNING key \'' + key + '\' not in new!')
            continue
    print('---------- terminated')


def __checked_all(file_name: str):
    print('---------- Study on the differences when reading VTK HDF data via this new JSON ----------')
    with_log = False
    base = read_json(file_name)
    ignored_data_key = set()
    sum_checked, sum_ignored = 0, 0
    for itps in range(10):
        float_step = float(itps)
        base['/chunks'].set({'step': float_step})
        add_checked, add_ignored = checked_all('test_kdi_vtk_hdf_create_json_b', [0, 1, 2], base,
                                               with_study_date=False, with_global_fields=False,
                                               ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored
    if with_log:
        print('sum_checked', sum_checked)
        print('sum_ignored', sum_ignored)
        print('ignored_data_key', ignored_data_key)
    assert (sum_checked == 627)
    assert (sum_ignored == 171)
    assert (ignored_data_key == {'/fields/myGlobScalarField', '/fields/myGlobVectorField',
                                 '/study/date'})
    print('---------- terminated')


def test_kdi_vtk_hdf_create_json_b():
    os.environ['KDI_GET_ENV_LOG'] = '0'
    reset_kdi_log_get_env()
    os.environ['KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS'] = '1'
    os.environ['KDI_TEST_WITH_SAVING_VTK_HDF'] = '1'
    os.environ['KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF'] = '0'
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    os.environ['KDI_TEST_WITH_LOADING_JSON'] = '0'
    test_1uns_3ssd_10tps()
    kdi_vtk_hdf_create_json('1pe_1uns_3ssd_10tps__hard.vtkhdf', prefix_before_extension='_NEW')
    print('test_kdi_vtk_hdf_create_json_b: create JSON')
    __compare_json_file('1pe_1uns_3ssd_10tps__hard.json', '1pe_1uns_3ssd_10tps__hard_NEW.json')
    print('test_kdi_vtk_hdf_create_json_b: compare JSON checked')
    __checked_all('1pe_1uns_3ssd_10tps__hard_NEW.json')
    print('test_kdi_vtk_hdf_create_json_b: compare read values checked')
    base = read_json('1pe_1uns_3ssd_10tps__hard.json')

def test_kdi_vtk_hdf_create_json_c():
    """
    Fr: Les résultats sont les mêmes que pour le test d
    """
    if not main_program:
        return
    os.system('cp tests/EXPECTED/3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf')
    kdi_vtk_hdf_create_json('3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf', prefix_before_extension='_NEW')
    print('test_kdi_vtk_hdf_create_json_c: create JSON')
    # TODO Ce qui doit différer c'est les variants et les valeurs pour nb_parts
    __compare_json_file('1pe_1uns_3ssd_10tps__hard.json', '3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    print('test_kdi_vtk_hdf_create_json_c: compare JSON checked')
    # TODO Ce qui doit différer c'est les variants et les valeurs pour nb_parts
    # TODO NE FONCTIONNE PAS __checked_all('3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    print('test_kdi_vtk_hdf_create_json_c: compare read values checked')

    base_1 = read_json('3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    base_N = KDI1toN(base_1, nb_parts=3)
    expected = {'/mymesh/cells/globalIndexContinuous':
                    {0: [0, 1, 2],
                     1: [3, 4],
                     2: [5, 6],
                     },
                '/mymesh/cells/types':
                    {0: [0, 0, 0],
                     1: [0, 0],
                     2: [0, 1],
                    },
                '/mymesh/cells/connectivity':
                    {0: [1, 0, 2, 2, 3, 1, 4, 3, 2],
                     1: [2, 3, 0, 1, 0, 3],
                     2: [2, 3, 0, 4, 5, 2, 1],
                     },
                '/mymesh/cells/fields/OriginalPartId':
                    {0: [2., 2., 0.],
                     1: [0., 1.],
                     2: [1., 0.],
                    },
                '/mymesh/cells/fields/PartId':
                    {0: [0, 0, 0],
                     1: [1, 1],
                     2: [2, 2],
                    },
                '/mymesh/cells/fields/myFieldCell':
                    {0: [69., 42., 69.],
                     1: [42., 69.],
                     2: [42., 89.],
                    },
                '/mymesh/cells/fields/myFieldCell2':
                    {0: [0.02, 0.02, 0.],
                     1: [0., 0.01],
                     2: [0.01, 0.],
                    },
                '/mymesh/cells/fields/myGIC':
                    {0: [0, 1, 2],
                     1: [3, 4],
                     2: [5, 6],
                    },
                '/mymesh/cells/fields/mySSD':
                    {0: [2, 2, 0],
                     1: [0, 1],
                     2: [1, 0],
                    },
                '/mymesh/points/globalIndexContinuous':
                    {0: [0, 1, 2, 3, 5],
                     1: [3, 4, 5, 6],
                     2: [4, 5, 6, 7, 8, 9],
                     },
                '/mymesh/points/cartesianCoordinates':
                    {0: [[0., 2., 0.], [1., 2., 0.], [0., 1., 0.], [1., 1., 0.], [0., 0., 0.]],
                     1: [[1., 1., 0.], [2., 1., 0.], [0., 0., 0.], [1., 0., 0.]],
                     2: [[2., 1., 0.], [0., 0., 0.], [1., 0., 0.], [2., 0., 0.], [0., -2., 0.], [1., -1., 0.]]
                     },
                '/mymesh/points/fields/OriginalPartId':
                    {0: [2., 2., 2., 1., 0.],
                     1: [1., 1., 0., 1.],
                     2: [1., 0., 1., 1., 0., 0.],
                     },
                '/mymesh/points/fields/myFieldPoint':
                    {0: [1., 2., 2., 3., 1.],
                     1: [3., 2., 1., 2.],
                     2: [2., 1., 2., 1., 1., 1.],
                     },
                }

    for field in ['/mymesh/cells/globalIndexContinuous', '/mymesh/cells/types', '/mymesh/cells/connectivity', '/mymesh/points/globalIndexContinuous']:  #, '/points/cartesianCoordinates']:
        print('>', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_N[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    for field in base_N['/mymesh/cells/fields'].get():
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    field = '/mymesh/points/cartesianCoordinates'
    print('>', field)
    for ipart in range(3):
        print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
        assert (np.all(expected[field][ipart] == base_N[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    for field in base_N['/mymesh/points/fields'].get():
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    write_vtk_hdf(base_N,
                  '3pe_1uns_2subuns_3ssd_10tps__PARA.vtkhdf',
                  chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.},
                  with_clean_data_submeshes=True)


def test_kdi_vtk_hdf_create_json_d():
    """
    Fr: Les résultats sont les mêmes que pour le test c
    """
    if not main_program:
        return
    os.system('cp tests/EXPECTED/3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf')
    kdi_vtk_hdf_create_json('3pe_1uns_2subuns_3ssd_10tps__one.vtkhdf', prefix_before_extension='_NEW')
    print('test_kdi_vtk_hdf_create_json_d: create JSON')
    # TODO Ce qui doit différer c'est les variants et les valeurs pour nb_parts
    # TODO Materiaux en plus
    __compare_json_file('1pe_1uns_3ssd_10tps__hard.json', '3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    print('test_kdi_vtk_hdf_create_json_d: compare JSON checked')
    # TODO Ce qui doit différer c'est les variants et les valeurs pour nb_parts
    # TODO Materiaux en plus
    # TODO NE FONCTIONNE PAS __checked_all('3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    print('test_kdi_vtk_hdf_create_json_d: compare read values checked')

    base_1 = read_json('3pe_1uns_2subuns_3ssd_10tps__one_NEW.json')
    base_N = KDI1toN(base_1, nb_parts=3)

    write_vtk_hdf(base_N,
                  '3pe_1uns_2subuns_3ssd_10tps__PARA.vtkhdf',
                  chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.},
                  with_clean_data_submeshes=True)

def test_kdi_vtk_hdf_create_json_e():
    """
    Ce test ne s'execute pas en CI sur GitLab.

    Ce test par d'une base de référence :
        EXPECTED/3pe_1uns_2subuns_3ssd_10tps__multi__one.vtkhdf
    qui décrit un maillage regroupé via HDF MPI-IO
    """
    if not main_program:
        return
    os.system('cp tests/EXPECTED/3pe_1uns_2subuns_3ssd_10tps__multi__one.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__multi__one.vtkhdf')
    kdi_vtk_hdf_create_json('3pe_1uns_2subuns_3ssd_10tps__multi__one.vtkhdf', prefix_before_extension='_NEW')
    print('test_kdi_vtk_hdf_create_json_e: create JSON')

    __compare_json_file('1pe_1uns_3ssd_10tps__hard.json', '3pe_1uns_2subuns_3ssd_10tps__multi__one_NEW.json')
    print('test_kdi_vtk_hdf_create_json_e: compare JSON checked')

    #### __checked_all('3pe_1uns_2subuns_3ssd_10tps__multi__one_NEW.json') ### TODO A REVOIR
    print('test_kdi_vtk_hdf_create_json_e: compare read values checked')

    base_1 = read_json('3pe_1uns_2subuns_3ssd_10tps__multi__one_NEW.json')


    expected = {
        '/mymesh': None,
        '/mymesh/fields': None,  # TODO
        '/mymesh/cells': None,
        '/KVTKHDF/Assembly/mymesh/mymesh/types':  # TODO Pourquoi Assembly/mymesh/mymesh
            {0: [5, 5, 5, 5, 5, 5, 9],
            },
        '/mymesh/cells/types':
            {0: [0, 0, 0, 0, 0, 0, 1],
            },
        '/mymesh/cells/offsets':
            {0: [0, 3, 6, 9, 12, 15, 18, 22],
            },
        '/mymesh/cells/connectivity':
            {0: [1, 0, 2, 2, 3, 1, 5, 3, 2, 5, 6, 3, 4, 3, 6, 6, 7, 4, 8, 9, 6, 5]
            },
        '/mymesh/cells/fields': None,
        '/mymesh/cells/fields/OriginalPartId':
            {0: [2., 2., 0., 0., 1., 1., 0.],
            },
        '/mymesh/cells/fields/myFieldCell':
            {0: [69., 42., 69., 42., 69., 42., 89.],
            },
        '/mymesh/cells/fields/myFieldCell2':
            {0: [0.02, 0.02, 0., 0., 0.01, 0.01, 0.],
            },
        '/mymesh/cells/fields/myGIC':
            {0: [0, 1, 2, 3, 4, 5, 6],
            },
        '/mymesh/cells/fields/mySSD':
            {0: [2, 2, 0, 0, 1, 1, 0],
            },
        '/mymesh/points': None,
        '/mymesh/points/cartesianCoordinates':
            {0: [[0.,  2.,  0.], [1.,  2.,  0.], [0.,  1.,  0.], [1.,  1.,  0.], [2.,  1.,  0.],
                 [0.,  0.,  0.], [1.,  0.,  0.], [2.,  0.,  0.], [0., - 2.,  0.], [1., - 1.,  0.]]
             },
        '/mymesh/points/fields': None,
        '/mymesh/points/fields/OriginalPartId':
            {0: [2., 2., 2., 2., 1., 0., 1., 1., 0., 0.],
             },
        '/mymesh/points/fields/myFieldPoint':
            {0: [1., 2., 2., 3., 2., 1., 2., 1., 1., 1.],
             },
        '/mymesh/submeshes': None,
        '/mymesh/submeshes/mymil1': None,
        '/mymesh/submeshes/mymil1/fields': None,
        '/mymesh/submeshes/mymil1/indexescells':
            {0: [1, 3, 5, 6]
             },
        '/mymesh/submeshes/mymil1/cells': None,
        '/mymesh/submeshes/mymil1/cells/fields': None,
        '/mymesh/submeshes/mymil1/cells/fields/OriginalPartId':
            {0: [2., 0., 1., 0.],
             },
        '/mymesh/submeshes/mymil1/cells/fields/mylocFieldCell':
            {0: [10.01, 10.01, 10.01, 10.02],
             },
        '/mymesh/submeshes/mymil1/cells/fields/mylocFieldCell3':
            {0: [30.01, 30.01, 30.01, 30.02],
             },
        '/mymesh/submeshes/mymil2': None,
        '/mymesh/submeshes/mymil2/fields': None,
        '/mymesh/submeshes/mymil2/indexescells':
            {0: [0, 2, 4]
             },
        '/mymesh/submeshes/mymil2/cells': None,
        '/mymesh/submeshes/mymil2/cells/fields': None,
        '/mymesh/submeshes/mymil2/cells/fields/OriginalPartId':
            {0: [2., 0., 1.],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell':
            {0: [20.01, 20.01, 20.01],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell3':
            {0: [40.01, 40.01, 40.01],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell4':
            {0: [68, 68, 68],
             },
        '': None,
        '/chunks': None,
        '/chunks/current': None,
        '/agreement': None,
        '/glu': None,
        '/glu/version':
            {0: [2, 0],
             },
        '/glu/name': None,  # TODO
        '/study': None,
        '/study/date': None,  # TODO
        '/study/name': None,  # TODO
        '/fields': None,
        '/vtkhdf': None,
        '/vtkhdf/version':
            {0: [2, 0],
             },
        '/vtkhdf/force_common_data_offsets': None,
        '/code2kdi': None,
        '/code2nbPoints': None,
    }

    assert (len(expected) == len(base_1))

    nb_expected = len(expected)

    do = set()

    for field in ['', '/chunks', '/chunks/current', '/agreement', '/glu', '/glu/version', '/glu/name',
                  '/study', '/study/date', '/study/name', '/fields',
                  '/vtkhdf', '/vtkhdf/version', '/vtkhdf/force_common_data_offsets',
                  '/code2kdi', '/code2nbPoints',

                  '/mymesh', '/mymesh/fields', '/mymesh/cells', '/mymesh/cells/fields', '/mymesh/points', '/mymesh/points/fields',
                  '/mymesh/submeshes', '/mymesh/submeshes/mymil1', '/mymesh/submeshes/mymil1/fields', '/mymesh/submeshes/mymil1/cells', '/mymesh/submeshes/mymil1/cells/fields',
                  '/mymesh/submeshes/mymil2', '/mymesh/submeshes/mymil2/fields', '/mymesh/submeshes/mymil2/cells', '/mymesh/submeshes/mymil2/cells/fields',
                  ]:
        if expected[field] is None:
            assert (field in base_1)
            nb_expected -= 1
            do.add(field)
            continue
        print('>', field)
        assert (np.all(expected[field][0] == base_1[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
        do.add(field)
        nb_expected -= 1

    for field in ['/KVTKHDF/Assembly/mymesh/mymesh/types', '/mymesh/cells/types', '/mymesh/cells/offsets', '/mymesh/cells/connectivity']:
        print('>', field)
        print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
        assert (np.all(expected[field][0] == base_1[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
        do.add(field)
        nb_expected -= 1

    for field in base_1['/mymesh/cells/fields'].get():
        print('>', 'FIELD', field)
        print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
        assert(np.all(expected[field][0] == base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
        do.add(field)
        nb_expected -= 1

    field = '/mymesh/points/cartesianCoordinates'
    print('>', field)
    print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
    assert (np.all(expected[field][0] == base_1[field].get(
        {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
    do.add(field)
    nb_expected -= 1

    for field in base_1['/mymesh/points/fields'].get():
        print('>', 'FIELD', field)
        print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
        assert(np.all(expected[field][0] == base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
        do.add(field)
        nb_expected -= 1

    for submesh in ['/mymesh/submeshes/mymil1', '/mymesh/submeshes/mymil2']:
        field = submesh + '/indexescells'
        print('>', field)
        print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
        assert (np.all(expected[field][0] == base_1[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
        do.add(field)
        nb_expected -= 1
        for field in base_1[submesh + '/cells/fields'].get():
            print('>', 'FIELD', field)
            print(base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
            assert(np.all(expected[field][0] == base_1[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0})))
            do.add(field)
            nb_expected -= 1

    for key in expected.keys():
        if key not in do:
            print('TODO COMPARE', key)
    for key in base_1.keys():
        if key not in expected:
            print('TODO EXPECTED', key)
    assert (nb_expected == 0)

    base_N = KDI1toN(base_1, nb_parts=3)

    expected = {
        '/mymesh': None,
        '/mymesh/fields': None,  # TODO
        '/mymesh/cells': None,
        '/mymesh/cells/globalIndexContinuous':
            {0: [0, 1, 2],
             1: [3, 4],
             2: [5, 6],
             },
        '/mymesh/cells/types':
            {0: [0, 0, 0],
             1: [0, 0],
             2: [0, 1],
            },
        '/mymesh/cells/offsets':
            {0: [0, 3, 6, 9],
             1: [0, 3, 6],
             2: [0, 3, 7],
            },
        '/mymesh/cells/connectivity':
            {0: [1, 0, 2, 2, 3, 1, 4, 3, 2],
             1: [2, 3, 0, 1, 0, 3],
             2: [2, 3, 0, 4, 5, 2, 1],
             },
        '/mymesh/cells/fields': None,
        '/mymesh/cells/fields/OriginalPartId':
            {0: [2., 2., 0.],
             1: [0., 1.],
             2: [1., 0.],
            },
        '/mymesh/cells/fields/PartId':
            {0: [0, 0, 0],
             1: [1, 1],
             2: [2, 2],
            },
        '/mymesh/cells/fields/myFieldCell':
            {0: [69., 42., 69.],
             1: [42., 69.],
             2: [42., 89.],
            },
        '/mymesh/cells/fields/myFieldCell2':
            {0: [0.02, 0.02, 0.],
             1: [0., 0.01],
             2: [0.01, 0.],
            },
        '/mymesh/cells/fields/myGIC':
            {0: [0, 1, 2],
             1: [3, 4],
             2: [5, 6],
            },
        '/mymesh/cells/fields/mySSD':
            {0: [2, 2, 0],
             1: [0, 1],
             2: [1, 0],
            },
        '/mymesh/points': None,
        '/mymesh/points/globalIndexContinuous':
            {0: [0, 1, 2, 3, 5],
             1: [3, 4, 5, 6],
             2: [4, 5, 6, 7, 8, 9],
             },
        '/mymesh/points/cartesianCoordinates':
            {0: [[0., 2., 0.], [1., 2., 0.], [0., 1., 0.], [1., 1., 0.], [0., 0., 0.]],
             1: [[1., 1., 0.], [2., 1., 0.], [0., 0., 0.], [1., 0., 0.]],
             2: [[2., 1., 0.], [0., 0., 0.], [1., 0., 0.], [2., 0., 0.], [0., -2., 0.], [1., -1., 0.]]
             },
        '/mymesh/points/fields': None,
        '/mymesh/points/fields/OriginalPartId':
            {0: [2., 2., 2., 2., 0.],
             1: [2., 1., 0., 1.],
             2: [1., 0., 1., 1., 0., 0.],
             },
        '/mymesh/points/fields/myFieldPoint':
            {0: [1., 2., 2., 3., 1.],
             1: [3., 2., 1., 2.],
             2: [2., 1., 2., 1., 1., 1.],
             },
        '/mymesh/submeshes': None,
        '/mymesh/submeshes/mymil1': None,
        '/mymesh/submeshes/mymil1/fields': None,
        '/mymesh/submeshes/mymil1/indexescells':
            {0: [1],  # Ca c'est cells global rang index by mil by rang
             1: [0],
             2: [0, 1],
             },
        '/mymesh/submeshes/mymil1/cells': None,
        '/mymesh/submeshes/mymil1/cells/globalIndexContinuous':
            {0: [1], # Ca c'est cells global 1 index by mil by rang
             1: [3],
             2: [5, 6],
             },
        '/mymesh/submeshes/mymil1/cells/fields': None,
        '/mymesh/submeshes/mymil1/cells/fields/OriginalPartId':
            {0: [2.],
             1: [0.],
             2: [1., 0.],
             },
        '/mymesh/submeshes/mymil1/cells/fields/mylocFieldCell':
            {0: [10.01],
             1: [10.01],
             2: [10.01, 10.02],
             },
        '/mymesh/submeshes/mymil1/cells/fields/mylocFieldCell3':
            {0: [30.01],
             1: [30.01],
             2: [30.01, 30.02],
             },
        '/mymesh/submeshes/mymil2': None,
        '/mymesh/submeshes/mymil2/fields': None,
        '/mymesh/submeshes/mymil2/indexescells':
            {0: [0, 2],  # Ca c'est cells global rang index by mil by rang
             1: [1],
             2: [],
             },
        '/mymesh/submeshes/mymil2/cells': None,
        '/mymesh/submeshes/mymil2/cells/globalIndexContinuous':
            {0: [0, 2], # Ca c'est cells global 1 index by mil by rang
             1: [4],
             2: [],
             },
        '/mymesh/submeshes/mymil2/cells/fields': None,
        '/mymesh/submeshes/mymil2/cells/fields/OriginalPartId':
            {0: [2., 0.],
             1: [1.],
             2: [],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell':
            {0: [20.01, 20.01],
             1: [20.01],
             2: [],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell3':
            {0: [40.01, 40.01],
             1: [40.01],
             2: [],
             },
        '/mymesh/submeshes/mymil2/cells/fields/mylocFieldCell4':
            {0: [68, 68],
             1: [68],
             2: [],
             },
        '': None,
        '/chunks': None,
        '/chunks/current': None,
        '/agreement': None,
        '/glu': None,
        '/glu/version':
            {0: [2, 0],
             1: [2, 0],
             2: [2, 0],
             },
        '/glu/name': None,  # TODO
        '/study': None,
        '/study/date': None,  # TODO
        '/study/name': None,  # TODO
        '/fields': None,
        '/vtkhdf': None,
        '/vtkhdf/version':
            {0: [2, 0],
             1: [2, 0],
             2: [2, 0],
             },
        '/vtkhdf/force_common_data_offsets': None,
        '/code2kdi': None,
        '/code2nbPoints': None,

        '/caching/mymesh/cells/connectivity/mymesh/points/globalIndexContinuous': None,  # TODO
        '/cache/mymesh/submeshes/mymil1/indexescells':
            {0: [0],
             1: [1],
             2: [2, 3],
             },
        '/cache/mymesh/submeshes/mymil2/indexescells':
            {0: [0, 1],
             1: [2],
             2: [],
             },

        '/before_write': None,
    }

    if len(expected) != len(base_N):
        for key in base_N:
            if key not in expected:
                print('NOT IN EXPECTED', key)
        for key in expected:
            if key not in base_N:
                print('IN EXPECTED', key, 'BUT NOT BASE_N')

    assert (len(expected) == len(base_N))

    nb_expected = 3 * len(expected)

    do = set()

    for field in ['', '/chunks',
                  '/chunks/current',
                  '/agreement', '/glu', '/glu/version', '/glu/name',
                  '/study', '/study/date', '/study/name', '/fields',
                  '/vtkhdf', '/vtkhdf/version', '/vtkhdf/force_common_data_offsets',
                  '/code2kdi', '/code2nbPoints',

                  '/mymesh', '/mymesh/fields', '/mymesh/cells', '/mymesh/cells/fields', '/mymesh/points', '/mymesh/points/fields',
                  '/mymesh/submeshes', '/mymesh/submeshes/mymil1', '/mymesh/submeshes/mymil1/fields', '/mymesh/submeshes/mymil1/cells', '/mymesh/submeshes/mymil1/cells/fields',
                  '/mymesh/submeshes/mymil2', '/mymesh/submeshes/mymil2/fields', '/mymesh/submeshes/mymil2/cells', '/mymesh/submeshes/mymil2/cells/fields',

                  '/caching/mymesh/cells/connectivity/mymesh/points/globalIndexContinuous',

                  '/before_write']:
        if expected[field] is None:
            assert (field in base_N)
            nb_expected -= 3
            do.add(field)
            continue
        print('>', field)
        for ipart in range(3):
            assert (np.all(expected[field][ipart] == base_N[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for field in ['/mymesh/cells/globalIndexContinuous', '/mymesh/cells/types', '/mymesh/cells/offsets', '/mymesh/cells/connectivity', '/mymesh/points/globalIndexContinuous']:
        print('>', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_N[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for field in base_N['/mymesh/cells/fields'].get():
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    field = '/mymesh/points/cartesianCoordinates'
    print('>', field)
    for ipart in range(3):
        print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
        assert (np.all(expected[field][ipart] == base_N[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
        nb_expected -= 1
        do.add(field)

    for field in base_N['/mymesh/points/fields'].get():
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for field in ['/cache/mymesh/submeshes/mymil1/indexescells', '/cache/mymesh/submeshes/mymil2/indexescells']:
        print('>', field)
        for ipart in range(3):
            print('base_1 ', base_1['/mymesh/submeshes/mymil1/indexescells'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
            print('base_N GIC', base_N['/mymesh/cells/globalIndexContinuous'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            print(field, base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymesh/submeshes/mymil1', '/mymesh/submeshes/mymil2']:
        field = submesh + '/cells/globalIndexContinuous'
        print('>', field)
        for ipart in range(3):
            print(field, base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_N[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)
        field = submesh + '/indexescells'
        print('>', field)
        for ipart in range(3):
            print('>>> ipart', ipart)
            print('/mymesh/cells/globalIndexContinuous', base_N['/mymesh/cells/globalIndexContinuous'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            print(submesh + '/cells/globalIndexContinuous', base_N[submesh + '/cells/globalIndexContinuous'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            print(field, base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_N[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)
        for field in base_N[submesh + '/cells/fields'].get():
            print('>', 'FIELD', field)
            for ipart in range(3):
                print(base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                assert(np.all(expected[field][ipart] == base_N[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
                nb_expected -= 1
                do.add(field)

    for key in expected.keys():
        if key not in do:
            print('TODO COMPARE', key)
    for key in base_N.keys():
        if key not in expected:
            print('TODO EXPECTED', key)
    assert (nb_expected == 0)

    base_MN = KDIComputeMultiMilieux(base_N)

    expected = {
        '/mymesh': None,
        '/mymesh/fields': None,  # TODO
        '/mymesh/cells': None,
        '/mymesh/cells/globalIndexContinuous':
            {0: [0, 1, 2],
             1: [3, 4],
             2: [5, 6],
             },
        '/mymesh/cells/types':
            {0: [0, 0, 0],
             1: [0, 0],
             2: [0, 1],
            },
        '/mymesh/cells/offsets':
            {0: [0, 3, 6, 9],
             1: [0, 3, 6],
             2: [0, 3, 7],
            },
        '/mymesh/cells/connectivity':
            {0: [1, 0, 2, 2, 3, 1, 4, 3, 2],
             1: [2, 3, 0, 1, 0, 3],
             2: [2, 3, 0, 4, 5, 2, 1],
             },
        '/mymesh/cells/fields': None,
        '/mymesh/cells/fields/glob_OriginalPartId':  # rename cause CMM
            {0: [2., 2., 0.],
             1: [0., 1.],
             2: [1., 0.],
            },
        '/mymesh/cells/fields/glob_PartId':  # rename cause CMM
            {0: [0, 0, 0],
             1: [1, 1],
             2: [2, 2],
            },
        '/mymesh/cells/fields/glob_myFieldCell':  # rename cause CMM
            {0: [69., 42., 69.],
             1: [42., 69.],
             2: [42., 89.],
            },
        '/mymesh/cells/fields/glob_myFieldCell2':  # rename cause CMM
            {0: [0.02, 0.02, 0.],
             1: [0., 0.01],
             2: [0.01, 0.],
            },
        '/mymesh/cells/fields/glob_myGIC':  # rename cause CMM
            {0: [0, 1, 2],
             1: [3, 4],
             2: [5, 6],
            },
        '/mymesh/cells/fields/glob_mySSD':  # rename cause CMM
            {0: [2, 2, 0],
             1: [0, 1],
             2: [1, 0],
            },
        '/mymesh/points': None,
        '/mymesh/points/globalIndexContinuous':
            {0: [0, 1, 2, 3, 5],
             1: [3, 4, 5, 6],
             2: [4, 5, 6, 7, 8, 9],
             },
        '/mymesh/points/cartesianCoordinates':
            {0: [[0., 2., 0.], [1., 2., 0.], [0., 1., 0.], [1., 1., 0.], [0., 0., 0.]],
             1: [[1., 1., 0.], [2., 1., 0.], [0., 0., 0.], [1., 0., 0.]],
             2: [[2., 1., 0.], [0., 0., 0.], [1., 0., 0.], [2., 0., 0.], [0., -2., 0.], [1., -1., 0.]]
             },
        '/mymesh/points/fields': None,
        '/mymesh/points/fields/glob_OriginalPartId':  # rename cause CMM
            {0: [2., 2., 2., 2., 0.],
             1: [2., 1., 0., 1.],
             2: [1., 0., 1., 1., 0., 0.],
             },
        '/mymesh/points/fields/glob_myFieldPoint':  # rename cause CMM
            {0: [1., 2., 2., 3., 1.],
             1: [3., 2., 1., 2.],
             2: [2., 1., 2., 1., 1., 1.],
             },
        '/mymil1': None,
        '/mymil1/fields': None,  # TODO
        '/mymil1/cells': None,
        '/mymil1/cells/globalIndexContinuous':
            {0: [1],
             1: [3],
             2: [5, 6],
             },
        '/mymil1/cells/types':
            {0: [0],
             1: [0],
             2: [0, 1],
             },
        '/mymil1/cells/offsets':
            {0: [0, 3],
             1: [0, 3],
             2: [0, 3, 7],
             },
        '/mymil1/cells/connectivity':
            {0: [1, 2, 0],
             1: [1, 2, 0],
             2: [2, 3, 0, 4, 5, 2, 1],
             },
        '/mymil1/cells/fields': None,  # TODO
        '/mymil1/cells/fields/OriginalPartId':
            {0: [2.],
             1: [0.],
             2: [1., 0.],
             },
        '/mymil1/cells/fields/mylocFieldCell':
            {0: [10.01],
             1: [10.01],
             2: [10.01, 10.02],
             },
        '/mymil1/cells/fields/mylocFieldCell3':
            {0: [30.01],
             1: [30.01],
             2: [30.01, 30.02],
             },
        '/mymil1/cells/fields/glob_OriginalPartId':
            {0: [2.],
             1: [0.],
             2: [1., 0.],
             },
        '/mymil1/cells/fields/glob_PartId':
            {0: [0],
             1: [1],
             2: [2, 2],
             },
        '/mymil1/cells/fields/glob_myFieldCell':
            {0: [42.],
             1: [42.],
             2: [42., 89.],
             },
        '/mymil1/cells/fields/glob_myFieldCell2':
            {0: [0.02],
             1: [0.],
             2: [0.01, 0.],
             },
        '/mymil1/cells/fields/glob_myGIC':
            {0: [1],
             1: [3],
             2: [5, 6],
             },
        '/mymil1/cells/fields/glob_mySSD':
            {0: [2],
             1: [0],
             2: [1, 0],
             },
        '/mymil1/points': None,
        '/mymil1/points/globalIndexContinuous':
            {0: [1, 2, 3],
             1: [3, 5, 6],
             2: [4, 5, 6, 7, 8, 9],
             },
        '/mymil1/points/cartesianCoordinates':
            {0: [[1., 2., 0.], [0., 1., 0.], [1., 1., 0.]],
             1: [[1., 1., 0.], [0., 0., 0.], [1., 0., 0.]],
             2: [[2., 1., 0.], [0., 0., 0.], [1., 0., 0.], [2., 0., 0.], [0., -2., 0.], [1., -1., 0.]],
             },
        '/mymil1/points/fields': None,  # TODO
        '/mymil1/points/fields/glob_OriginalPartId':  # rename cause CMM
            {0: [2., 2., 2.],
             1: [2., 0., 1.],
             2: [1., 0., 1., 1., 0., 0.],
             },
        '/mymil1/points/fields/glob_myFieldPoint':  # rename cause CMM
            {0: [2., 2., 3.],
             1: [3., 1., 2.],
             2: [2., 1., 2., 1., 1., 1.],
             },
        '/mymil2': None,
        '/mymil2/fields': None,  # TODO
        '/mymil2/cells': None,
        '/mymil2/cells/globalIndexContinuous':
            {0: [0, 2],
             1: [4],
             2: [],
             },
        '/mymil2/cells/types':
            {0: [0, 0],
             1: [0],
             2: [],
             },
        '/mymil2/cells/offsets':
            {0: [0, 3, 6],
             1: [0, 3],
             2: [],
             },
        '/mymil2/cells/connectivity':
            {0: [1, 0, 2, 4, 3, 2],
             1: [1, 0, 2],
             2: [],
             },
        '/mymil2/cells/fields': None,  # TODO
        '/mymil2/cells/fields/OriginalPartId':
            {0: [2., 0.],
             1: [1.],
             2: [],
             },
        '/mymil2/cells/fields/mylocFieldCell':
            {0: [20.01, 20.01],
             1: [20.01],
             2: [],
             },
        '/mymil2/cells/fields/mylocFieldCell3':
            {0: [40.01, 40.01],
             1: [40.01],
             2: [],
             },
        '/mymil2/cells/fields/mylocFieldCell4':
            {0: [68, 68],
             1: [68],
             2: [],
             },
        '/mymil2/cells/fields/glob_OriginalPartId':
            {0: [2., 0.],
             1: [1.],
             2: [],
             },
        '/mymil2/cells/fields/glob_PartId':
            {0: [0., 0.],
             1: [1.],
             2: [],
             },
        '/mymil2/cells/fields/glob_myFieldCell':
            {0: [69., 69.],
             1: [69.],
             2: [],
             },
        '/mymil2/cells/fields/glob_myFieldCell2':
            {0: [0.02, 0.],
             1: [0.01],
             2: [],
             },
        '/mymil2/cells/fields/glob_myGIC':
            {0: [0, 2],
             1: [4],
             2: [],
             },
        '/mymil2/cells/fields/glob_mySSD':
            {0: [2, 0],
             1: [1],
             2: [],
             },
        '/mymil2/points': None,
        '/mymil2/points/globalIndexContinuous':
            {0: [0, 1, 2, 3, 5],
             1: [3, 4, 6],
             2: [],
             },
        '/mymil2/points/cartesianCoordinates':
            {0: [[0., 2., 0.], [1., 2., 0.], [0., 1., 0.], [1., 1., 0.], [0., 0., 0.]],
             1: [[1., 1., 0.], [2., 1., 0.], [1., 0., 0.]],
             2: np.zeros((0,3), dtype=np.float64),
             },
        '/mymil2/points/fields': None,  # TODO
        '/mymil2/points/fields/glob_OriginalPartId':  # rename cause CMM
            {0: [2., 2., 2., 2., 0.],
             1: [2., 1., 1.],
             2: [],
             },
        '/mymil2/points/fields/glob_myFieldPoint':  # rename cause CMM
            {0: [1., 2., 2., 3., 1.],
             1: [3., 2., 2.],
             2: [],
             },
        '': None,
        '/chunks': None,
        '/chunks/current': None,
        '/agreement': None,
        '/glu': None,
        '/glu/version':
            {0: [2, 0],
             1: [2, 0],
             2: [2, 0],
             },
        '/glu/name': None,  # TODO
        '/study': None,
        '/study/date': None,  # TODO
        '/study/name': None,  # TODO
        '/fields': None,
        '/vtkhdf': None,
        '/vtkhdf/version':
            {0: [2, 0],
             1: [2, 0],
             2: [2, 0],
             },
        '/vtkhdf/force_common_data_offsets': None,
        '/code2kdi': None,
        '/code2nbPoints': None,
        '/assembly': None,
        '/before_write': None,

        '/caching/mymesh/cells/connectivity/mymesh/points/globalIndexContinuous':
            {0: {'/mymesh/cells/connectivity': [1, 0, 2, 2, 3, 1, 4, 3, 2],
                 '/mymesh/points/globalIndexContinuous': [0, 1, 2, 3, 5]},
             1: {'/mymesh/cells/connectivity': [2, 3, 0, 1, 0, 3],
                 '/mymesh/points/globalIndexContinuous': [3, 4, 5, 6]},
             2: {'/mymesh/cells/connectivity': [2, 3, 0, 4, 5, 2, 1],
                 '/mymesh/points/globalIndexContinuous': [4, 5, 6, 7, 8, 9]},
             },
        '/caching/mymil1/cells/connectivity/caching/mymil1/indexespoints':
            {0: {'/mymil1/cells/connectivity': [1, 2, 0],
                 '/caching/mymil1/indexespoints': [1, 2, 3]},
             1: {'/mymil1/cells/connectivity': [1, 2, 0],
                 '/caching/mymil1/indexespoints': [0, 2, 3]},
             2: {'/mymil1/cells/connectivity': [2, 3, 0, 4, 5, 2, 1],
                 '/caching/mymil1/indexespoints': [0, 1, 2, 3, 4, 5]},
             },
        '/caching/mymil2/cells/connectivity/caching/mymil2/indexespoints':
            {0: {'/mymil2/cells/connectivity': [1, 0, 2, 4, 3, 2],
                 '/caching/mymil2/indexespoints': [0, 1, 2, 3, 4]},
             1: {'/mymil2/cells/connectivity': [1, 0, 2],
                 '/caching/mymil2/indexespoints': [0, 1, 3]},
             2: {'/mymil2/cells/connectivity': [],
                 '/caching/mymil2/indexespoints': []},
             },
        '/caching/mymil1/indexespoints':
            {0: [1, 2, 3],
             1: [0, 2, 3],
             2: [0, 1, 2, 3, 4, 5],
             },
        '/caching/mymil2/indexespoints':
            {0: [0, 1, 2, 3, 4],
             1: [0, 1, 3],
             2: [],
             },
        '/cache/mymesh/submeshes/mymil1/indexescells':
            {0: [0],
             1: [1],
             2: [2, 3],
             },
        '/cache/mymesh/submeshes/mymil2/indexescells':
            {0: [0, 1],
             1: [2],
             2: [],
             },

        '/before_write': None,
    }

    assert (len(expected) == len(base_MN))

    nb_expected = 3 * len(expected)

    do = set()

    for field in ['', '/chunks',
                  '/chunks/current',
                  '/agreement', '/glu', '/glu/version', '/glu/name',
                  '/study', '/study/date', '/study/name', '/fields',
                  '/vtkhdf', '/vtkhdf/version', '/vtkhdf/force_common_data_offsets',
                  '/code2kdi', '/code2nbPoints', '/assembly', '/before_write',]:
        if expected[field] is None:
            assert (field in base_MN)
            nb_expected -= 3
            do.add(field)
            continue
        print('>', field)
        for ipart in range(3):
            assert (np.all(expected[field][ipart] == base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        for post_field in ['', '/fields', '/cells', '/cells/fields', '/points', '/points/fields',]:
            field = submesh + post_field
            if expected[field] is None:
                assert (field in base_MN)
                nb_expected -= 3
                do.add(field)
                continue
            print('>', field)
            for ipart in range(3):
                print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                assert (np.all(expected[field][ipart] == base_MN[field].get(
                    {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
                nb_expected -= 1
                do.add(field)

    field = '/caching/mymesh/cells/connectivity/mymesh/points/globalIndexContinuous'
    print('>', field)
    for ipart in range(3):
        print(base_MN[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
        assert (np.all(expected[field][ipart]['/mymesh/cells/connectivity'] == base_MN[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})[
            '/mymesh/cells/connectivity']))
        assert (np.all(expected[field][ipart]['/mymesh/points/globalIndexContinuous'] == base_MN[field].get(
            {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})[
            '/mymesh/points/globalIndexContinuous']))
        nb_expected -= 1
        do.add(field)

    for submesh in ['/mymil1', '/mymil2']:
        field = '/caching' + submesh + '/cells/connectivity/caching' + submesh + '/indexespoints'
        print('>', field)
        for ipart in range(3):
            print(base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart][submesh + '/cells/connectivity'] == base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})[
                submesh + '/cells/connectivity']))
            assert (np.all(expected[field][ipart]['/caching' + submesh + '/indexespoints'] == base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})[
                '/caching' + submesh + '/indexespoints']))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymil1', '/mymil2']:
        field = '/caching' + submesh + '/indexespoints'
        print('>', field)
        for ipart in range(3):
            print(base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        for post_field in ['/cells/globalIndexContinuous', '/cells/types', '/cells/offsets', '/cells/connectivity', '/points/globalIndexContinuous']:
            field = submesh + post_field
            print('>', field)
            for ipart in range(3):
                print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                assert (np.all(expected[field][ipart] == base_MN[field].get(
                    {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
                nb_expected -= 1
                do.add(field)

    for field in base_MN['/mymesh/cells/fields'].get():
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        post_field = '/points/cartesianCoordinates'
        field = submesh + post_field
        print('>', field)
        for ipart in range(3):
            print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[field][ipart] == base_MN[field].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        for field in base_MN[submesh + '/points/fields'].get():
            print('>', 'FIELD', field)
            for ipart in range(3):
                print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                assert(np.all(expected[field][ipart] == base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
                nb_expected -= 1
                do.add(field)

    for field in ['/cache/mymesh/submeshes/mymil1/indexescells', '/cache/mymesh/submeshes/mymil2/indexescells']:
        print('>', field)
        for ipart in range(3):
            print('base_1 ', base_1['/mymesh/submeshes/mymil1/indexescells'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: 0}))
            print('base_MN GIC', base_MN['/mymesh/cells/globalIndexContinuous'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            print(field, base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert(np.all(expected[field][ipart] == base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
            nb_expected -= 1
            do.add(field)

    for submesh in ['/mymil1', '/mymil2']:
        for field in base_MN[submesh + '/cells/fields'].get():
            print('>', 'FIELD', field)
            for ipart in range(3):
                if field == '/mymil1/cells/fields/glob_OriginalPartId':
                    print('/mymesh/submeshes/mymil1/indexescells', base_N['/mymesh/submeshes/mymil1/indexescells'].get(
                        {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                    print('/mymesh/cells/fields/OriginalPartId', base_N['/mymesh/cells/fields/OriginalPartId'].get(
                        {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                print(base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
                assert(np.all(expected[field][ipart] == base_MN[field].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))
                nb_expected -= 1
                do.add(field)

    for key in expected.keys():
        if key not in do:
            print('TODO COMPARE', key)
    for key in base_MN.keys():
        if key not in expected:
            print('TODO EXPECTED', key)
    assert (nb_expected == 0)

    #------- IN WRITER
    expected['/mymesh/points/##points'] = {0: [5], 1: [4], 2: [6],}
    expected['/mymil1/points/##points'] = {0: [3], 1: [3], 2: [6], }
    expected['/mymil2/points/##points'] = {0: [5], 1: [3], 2: [0],}

    commandNumberOf = """
array = Base[Params[0]].get()
numberOf = np.zeros(shape=(1,), dtype=np.int64)
numberOf[0] = array.shape[0]
Ret = numberOf
    """

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        base_MN[submesh + '/points/##points'] = KDIEvalString(base_MN, submesh + '/points/##points', commandNumberOf,
                                                              [submesh + '/points/cartesianCoordinates', ])
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_MN[submesh + '/points/##points'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[submesh + '/points/##points'][ipart] == base_MN[submesh + '/points/##points'].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    for submesh in ['/mymesh', '/mymil1', '/mymil2']:
        print('>', 'FIELD', field)
        for ipart in range(3):
            print(base_MN[submesh + '/points/cartesianCoordinates'].get({KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart}))
            assert (np.all(expected[submesh + '/points/cartesianCoordinates'][ipart] == base_MN[submesh + '/points/cartesianCoordinates'].get(
                {KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0., KDI_AGREEMENT_STEPPART_VARIANT_PART: ipart})))

    print('GO TO WRITE VTK HDF')
    write_vtk_hdf(base_MN,
                  '3pe_1uns_2subuns_3ssd_10tps__multi__PARA.vtkhdf',
                  chunk={KDI_AGREEMENT_STEPPART_VARIANT_STEP: 0.},
                  with_clean_data_submeshes=True)
    # mpirun.mpich -n 3 ~/Téléchargements/ParaView-5.13.1-MPI-Linux-Python3.10-x86_64/bin/pvserver --disable-xdisplay-test
    # ~/Téléchargements/ParaView-5.13.1-MPI-Linux-Python3.10-x86_64/bin/paraview -url cs://UN00314761:11111


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    main_program = True
    # TODO Ajouter un test sur une base HDF non VTK HDF
    test_kdi_vtk_hdf_create_json_a()
    print('test_kdi_vtk_hdf_create_json_a: checked')
    test_kdi_vtk_hdf_create_json_b()
    print('test_kdi_vtk_hdf_create_json_b: checked')
    test_kdi_vtk_hdf_create_json_c()
    print('test_kdi_vtk_hdf_create_json_c: checked')
    test_kdi_vtk_hdf_create_json_d()
    print('test_kdi_vtk_hdf_create_json_d: checked')
    test_kdi_vtk_hdf_create_json_e()
    print('test_kdi_vtk_hdf_create_json_e: checked')
