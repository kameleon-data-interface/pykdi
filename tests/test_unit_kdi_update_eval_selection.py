import time
import numpy as np
import math


def test_unit_kdi_update_eval_selection():
    SIMULATION_TRIANGLE_TYPE_VALUE = 11
    SIMULATION_QUADRANGLE_TYPE_VALUE = 12
    SIMULATION_HEXAHEDRON_TYPE_VALUE = 13

    KDI_TRIANGLE = 0
    KDI_QUADRANGLE = 1
    KDI_HEXAHEDRON = 12

    class KDIMemoryMOC:
        __slots__ = '_datas'
        def __init__(self, datas=None):
            if datas is None:
                self._datas = np.full(shape=(2500000,), fill_value=SIMULATION_HEXAHEDRON_TYPE_VALUE, dtype=np.int8)
                return
            self._datas = datas

        def get(self):
            return self._datas


    command_eval_selection = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection')
base = Base
chunk = base['/chunks/current']
# indexes
indexesbase = Params[0]
indexesbasechunk = indexesbase['/chunks/current']
indexesbase['/chunks/current'] = chunk
indexeskey = Params[1]
indexes = indexesbase[indexeskey].get()
indexesbase['/chunks/current'] = indexesbasechunk
# in values
inbase = Params[2]
inbasechunk = inbase['/chunks/current']
inbase['/chunks/current'] = chunk
inkey = Params[3]
invalues = inbase[inkey].get()
inbase['/chunks/current'] = inbasechunk
# in values shape
Ret = invalues[indexes]
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection terminated')"""

    command_eval_selection = """
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection')
base = Base
chunk = base['/chunks/current']
# indexes
indexesbase = Params[0]
indexesbasechunk = None
if '/chunks/current' in indexesbase:
    indexesbasechunk = indexesbase['/chunks/current']
indexesbase['/chunks/current'] = chunk
indexeskey = Params[1]
indexes = indexesbase[indexeskey].get()
if indexesbasechunk is None:
    del indexesbase['/chunks/current']
else:
    indexesbase['/chunks/current'] = indexesbasechunk
# in values
inbase = Params[2]
inbasechunk = None
if '/chunks/current' in inbase:
    inbasechunk = inbase['/chunks/current']
inbase['/chunks/current'] = chunk
inkey = Params[3]
invalues = inbase[inkey].get()
if inbasechunk is None:
    del inbase['/chunks/current']
else:
    inbase['/chunks/current'] = inbasechunk
# compute
Ret = invalues[indexes]
print('>>>>>>>>>>>>>>>>>>>>>>> KDIEvalString:kdi_update_eval_selection terminated')
"""
    CurrentBase = {
        '/chunks/current': {},
        '/current/types': KDIMemoryMOC(),
        '/indexes': KDIMemoryMOC(np.array([i for i in range(15, 912343, 2)])),
        '/values_in': KDIMemoryMOC(np.array([i*1.1 for i in range(2500000)])),
    }
    CurrentParams = [CurrentBase, '/indexes', CurrentBase, '/values_in']

    print('/indexes', len(CurrentBase['/indexes'].get()), CurrentBase['/indexes'].get()[0], CurrentBase['/indexes'].get()[-1])
    print('/values_in', len(CurrentBase['/values_in'].get()), CurrentBase['/values_in'].get()[0], CurrentBase['/values_in'].get()[-1])

    tic = time.time()

    variables = {'Base': CurrentBase, 'Params': CurrentParams, 'Ret': []}
    try:
        variables.update(dict(locals(), **globals()))
        exec(command_eval_selection, variables, variables)
    except Exception as e:
        raise Exception('KDI Exception Error in execution exec!')

    print('Time: ', time.time() - tic)

    print(variables['Ret'], len(variables['Ret']))
    print('Ret', len(variables['Ret']), variables['Ret'][0], 'expected 16.5', variables['Ret'][-1], 'expected 1.0035751e+06')
    assert(len(variables['Ret']) == 456164)
    assert(math.fabs(variables['Ret'][0] - 16.5) < 0.0001)
    assert(math.fabs(variables['Ret'][-1] - 1.0035751e+06) < 0.0001)

    # Time:  0.00086212158203125


if __name__ == '__main__':
    test_unit_kdi_update_eval_selection()
    print('\ntest_unit_kdi_update_eval_selection: checked')
