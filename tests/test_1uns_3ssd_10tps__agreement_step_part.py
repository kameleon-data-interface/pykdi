"""
Fr: Ce test valide la description d'un maillage de 6 cellules distribuées sur trois partitions.
"""

from mpi4py import MPI
import numpy as np
import os

from pykdi import KDIAgreementBaseStepPart, KDI_AGREEMENT_CONF_NB_PARTS, kdi_get_env, KDILog

kdi_test_with_global_index_continuous = kdi_get_env('KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS', False)
kdi_test_with_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_VTK_HDF', True)
kdi_test_with_nto1_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF', False)
kdi_test_with_saving_json = kdi_get_env('KDI_TEST_WITH_SAVING_JSON', False)
kdi_test_with_loading_json = kdi_get_env('KDI_TEST_WITH_LOADING_JSON', False)


def reset_kdi_test_1uns_3ss_10tps():
    global kdi_test_with_global_index_continuous
    global kdi_test_with_saving_vtkhdf
    global kdi_test_with_nto1_saving_vtkhdf
    global kdi_test_with_saving_json
    global kdi_test_with_loading_json
    kdi_test_with_global_index_continuous = kdi_get_env('KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS', False)
    kdi_test_with_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_VTK_HDF', True)
    kdi_test_with_nto1_saving_vtkhdf = kdi_get_env('KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF', False)
    kdi_test_with_saving_json = kdi_get_env('KDI_TEST_WITH_SAVING_JSON', False)
    kdi_test_with_loading_json = kdi_get_env('KDI_TEST_WITH_LOADING_JSON', False)


kdi_test_log = KDILog('TEST', True)

SIMULATION_TRIANGLE_TYPE_VALUE: int = 0
SIMULATION_QUADRANGLE_TYPE_VALUE: int = 1

data_common = {
    '/glu/version': np.array([2, 0]),
    '/study/name': 'MyEtude',
    '/study/date': '2024/05',
    '/fields/myGlobScalarField': np.array([0]),
    '/fields/myGlobVectorField': np.array([2, 1, 0]),
}

# LEGEND
# gIC, points/cells global index continuous
#
#  gIC:0           gIC:1
#  (0 2 0) +-----+ (1 2 0)
#          | G  /|
#          |   / |
#          |  /  |
#          | / F |
#  gIC:2   |/    | gIC:3        gIC:4
#  (0 1 0) +-----+ (1 1 0) ---+ (2 1 0)
#          | B  /|          / |
#          |   / |   E    /   |
#          |  /  |      /     |
#          | / A |    /   D   |
#          |/    |  /         |
#  gIC:5   +-----+ gIC:6 -----+ gIC:7
#  (0 0 0) |     | (1 0 0)      (2 0 0)
#          |     |
#          |  C  |
#          |     |
#          |     |
#          |     + gIC:9
#          |    /  (1 -1 0)
#          |   /
#          |  /
#          | /
#          |/
#  gIC:8   +
#  (0 -2 0)
#
# A: gIC:3 (ssd 0)
# B: gIC:2 (ssd 0)
# C: gIC:6 (ssd 0)
# D: gIC:5 (ssd 1)
# E: gIC:4 (ssd 1)
# F: gIC:1 (ssd 2)
# G: gIC:0 (ssd 2)
#
# id, local index sub-domain
data_ssd = [
    # SSD 0
    #
    #  id:2            id:3
    #  gIC:2           gIC:3
    #  (0 1 0) +-----+ (1 1 0)
    #          | B  /|
    #          |   / |
    #          |  /  |
    #          | / A |
    #  id:0    |/    | id:1
    #  gIC:5   +-----+ gIC:6
    #  (0 0 0) |     | (1 0 0)
    #          |     |
    #          |  C  |
    #          |     | id:5
    #          |     + gIC:9
    #          |    /  (1 -1 0)
    #          |   /
    #          |  /
    #          | /
    #  id:4    |/
    #  gIC:8   +
    #  (0 -2 0)
    #
    # A: id:0, gIC:3
    # B: id:1, gIC:2
    # C: id:2, gIC:6
    {
        '/mymesh/points/globalIndexContinuous': np.array([5, 6, 2, 3, 8, 9]),
        '/mymesh/points/cartesianCoordinates': np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [1., 1., 0.],
                                                         [0., -1., 0.], [1., -1., 0.], ], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([1., 2., 2., 3., 1., 1., ], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([3, 2, 6]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE,
                                         SIMULATION_QUADRANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0, ]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69, 89, ], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),

        '/mymesh/cells/offsets': np.array([0, 3, 6, 10, ]),  # compute
    },
    # SSD 1
    #
    #  id:3            id:2
    #  gIC:3           gIC:4
    #  (1 1 0) +-----+ (2 1 0)
    #          | E  /|
    #          |   / |
    #          |  /  |
    #          | / D |
    #  id:0    |/    | id:1
    #  gIC:6   +-----+ gIC:7
    #  (1 0 0)         (2 0 0)
    #
    # D: id:0, gIC:5
    # E: id:1, gIC:4
    {
        '/mymesh/points/globalIndexContinuous': np.array([6, 7, 4, 3]),
        '/mymesh/points/cartesianCoordinates': np.array([[1., 0., 0.], [2, 0., 0.], [2., 1., 0.], [1., 1., 0.], ], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 1., 2., 3.], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([5, 4]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),
    },
    # SSD 2
    #
    #  id:3            id:2
    #  gIC:0           gIC:1
    #  (0 2 0) +-----+ (1 2 0)
    #          | G  /|
    #          |   / |
    #          |  /  |
    #          | / F |
    #  id:0    |/    | id:1
    #  gIC:2   +-----+ gIC:3
    #  (0 1 0)         (1 1 0)
    #
    # F: id:0, gIC:1
    # G: id:1, gIC:0
    {
        '/mymesh/points/globalIndexContinuous': np.array([2, 3, 1, 0]),
        '/mymesh/points/cartesianCoordinates': np.array([[0., 1., 0.], [1., 1., 0.], [1., 2., 0.], [0., 2., 0.], ], dtype=float),
        '/mymesh/points/fields/myFieldPoint': np.array([2., 3., 2., 1.], dtype=float),
        '/mymesh/cells/globalIndexContinuous': np.array([1, 0]),
        '/mymesh/cells/types': np.array([SIMULATION_TRIANGLE_TYPE_VALUE, SIMULATION_TRIANGLE_TYPE_VALUE]),
        '/mymesh/cells/connectivity': np.array([0, 1, 2, 2, 3, 0]),
        '/mymesh/cells/fields/myFieldCell': np.array([42, 69], dtype=float),
        '/mymesh/cells/fields/myFieldCell2': np.array([], dtype=float),

        '/mymesh/cells/offsets': np.array([0, 3, 6]),
    }
]


def get_common_data(pkchunk, name):
    base = pkchunk.base
    chunk = pkchunk.chunk

    if name == '/study/name':
        return np.array([data_common[name]])
    if name == '/study/date':
        itps = base['/chunks'].GetOffset('step', chunk['step'])
        return np.array(['{}/{:02d}'.format(data_common[name], itps + 1)])
    if name == '/fields/myGlobScalarField':
        itps = base['/chunks'].GetOffset('step', chunk['step'])
        return data_common[name] * 2 + itps
    if name == '/fields/myGlobVectorField':
        itps = base['/chunks'].GetOffset('step', chunk['step'])
        return data_common[name] + np.array([1, 2, 3]) * itps
    return data_common[name]


def get_step_part__data(pkchunk, name):
    base = pkchunk.base
    chunk = pkchunk.chunk

    itps = base['/chunks'].GetOffset('step', chunk['step'])
    ipart = base['/chunks'].GetOffset('part', chunk['part'])
    if name == '/mymesh/cells/fields/myFieldCell2' or name == '/mymesh/cells/fields/glob_myFieldCell2':
        array = np.zeros(data_ssd[ipart]['/mymesh/cells/fields/myFieldCell'].shape, dtype=float)
        array += itps + ipart / 100.
        return array
    if name == '/mymesh/points/cartesianCoordinates':
        delta = float(itps) / 10.
        if ipart == 0:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array(
                        [[-delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [-delta, -1 - 2 * delta, 0.],
                         [0., 0., 0.], ]))
        if ipart == 1:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [delta, -delta, 0.], [0., 0., 0.], [0., 0., 0.], ]))
        if ipart == 2:
            return (data_ssd[ipart]['/mymesh/points/cartesianCoordinates'] +
                    np.array([[0., 0., 0.], [0., 0., 0.], [0., 0., 0.], [-delta, delta, 0.], ]))
    return data_ssd[ipart][name]


def checked(msg, pkchunk,
            first: bool = True, with_study_date: bool = True, with_global_fields: bool = True,
            ignored_data_key: set = None):
    base = pkchunk.base
    chunk = pkchunk.chunk

    itps = base['/chunks'].GetOffset('step', chunk['step'])
    ipart = base['/chunks'].GetOffset('part', chunk['part'])
    str_chunks = '(itps: ' + str(itps) + ', part: ' + str(ipart) + ')'
    sum_checked = 0
    sum_ignored = 0
    for data_key in data_common.keys():
        if not with_study_date and data_key == '/study/date':
            if ignored_data_key is not None:
                ignored_data_key.add(data_key)
            sum_ignored += 1
            continue
        if not with_global_fields and data_key[:8] == '/fields/':
            if ignored_data_key is not None:
                ignored_data_key.add(data_key)
            sum_ignored += 1
            continue
        key = data_key
        if isinstance(data_common[key], str):
            if get_common_data(pkchunk, key) != pkchunk.get(key): # BEFORE base[key].get(chunk):
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> A. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_common_data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Differents values ' + key + '!' + str_chunks)
        else:
            if not np.all(get_common_data(pkchunk, key) ==  pkchunk.get(key)): # BEFORE base[key].get(chunk):
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> B. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get())
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_common_data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Unexpected values ' + key + '!' + str_chunks)
        sum_checked += 1

    for data_key in data_ssd[ipart].keys():
        key = data_key

        if not kdi_test_with_global_index_continuous:
            if (pos := key.find('/points/globalIndexContinuous')) != -1:
                assert (len(key) == pos + len('/points/globalIndexContinuous'))
                if ignored_data_key is not None:
                    ignored_data_key.add(data_key)
                sum_ignored += 1
                continue
            if (pos := key.find('/cells/globalIndexContinuous')) != -1:
                assert (len(key) == pos + len('/cells/globalIndexContinuous'))
                if ignored_data_key is not None:
                    ignored_data_key.add(data_key)
                sum_ignored += 1
                continue
            sum_ignored += 1
            continue

        if isinstance(data_ssd[ipart][key], str):
            if get_step_part__data(base, key) != base[key].get(chunk):
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> C. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(base[key].get(chunk))
                kdi_test_log.fatal_exception_body('... vs ...')
                kdi_test_log.fatal_exception_body(get_step_part__data(base, key))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Differents values ' + key + '!' + str_chunks)
        else:
            try:
                if not np.all(get_step_part__data(pkchunk, key) == base[key].get(chunk)):
                    kdi_test_log.fatal_exception_header()
                    kdi_test_log.fatal_exception_body('>>>>> D. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                    kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                    kdi_test_log.fatal_exception_body(base[key].get(chunk))
                    kdi_test_log.fatal_exception_body('... vs ...')
                    kdi_test_log.fatal_exception_body(get_step_part__data(pkchunk, key))
                    kdi_test_log.fatal_exception_footer()
                    raise KDIException('Unexpected values \'' + key + '\'!')
            except KeyError:
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> E. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(get_step_part__data(pkchunk, key))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(key, 'in base ?', (key in base))
                kdi_test_log.fatal_exception_footer()
                raise KDIException('Unexpected key \'' + key + '\' in base!')
            except KDIException:
                kdi_test_log.fatal_exception_header()
                kdi_test_log.fatal_exception_body('>>>>> F. ' + msg + ' current time (itps: ' + str(itps) + ', part: ' + str(ipart) + ')')
                kdi_test_log.fatal_exception_body('Expected value key:' + key + ':')
                kdi_test_log.fatal_exception_body(get_step_part__data(pkchunk, key))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(key, 'in base ?', (key in base))
                kdi_test_log.fatal_exception_body('----------')
                kdi_test_log.fatal_exception_body(base[key].get(chunk))
                kdi_test_log.fatal_exception_footer()
                raise KDIException()
        sum_checked += 1

    if not first or itps == 0.:
        return sum_checked, sum_ignored

    new_pkchunk = pkchunk._kdi_base.chunk(itps - 1, ipart)
    add_checked, add_ignored = checked(msg, new_pkchunk, first=False, with_study_date=with_study_date,
                                       with_global_fields=with_global_fields, ignored_data_key=ignored_data_key)

    return sum_checked + add_checked, sum_ignored + add_ignored


def checked_all(msg, partitions_list, pkchunk, with_study_date=True, with_global_fields=True,
                ignored_data_key: set = None):
    sum_checked, sum_ignored = 0, 0

    base = pkchunk._kdi_base.base
    chunk = pkchunk.chunk

    itps = base['/chunks'].GetOffset('step', chunk['step'])
    for ipart in partitions_list:
        new_pkchunk = pkchunk._kdi_base.chunk(itps, ipart)
        add_checked, add_ignored = checked(msg, new_pkchunk, with_study_date=with_study_date,
                                           with_global_fields=with_global_fields,
                                           ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored
    return sum_checked, sum_ignored


def test_1uns_3ssd_10tps():
    reset_kdi_test_1uns_3ss_10tps()

    ignored_data_key = set()
    sum_checked, sum_ignored = 0, 0

    mpi_rank = MPI.COMM_WORLD.Get_rank()
    mpi_size = MPI.COMM_WORLD.Get_size()
    kdi_test_log.log('mpi_rank (', mpi_rank, ') / mpi_size (', mpi_size, ')')

    nb_parts = 3

    if mpi_size == 1:
        partitions_list = [0, 1, 2]
        vtkhdf_filename = '1pe_1uns_3ssd_10tps'
    elif mpi_size == nb_parts:
        partitions_list = [mpi_rank]
        vtkhdf_filename = '3pe_1uns_3ssd_10tps'
    else:
        raise Exception("This test works in sequential and with 3 processes in parallel.")

    pkbase = KDIAgreementBaseStepPart(vtkhdf_filename)
    pkbase.set_conf_int(KDI_AGREEMENT_CONF_NB_PARTS, nb_parts)
    pkbase.initialize()

    for itps in range(10):

        float_step = float(itps)
        
        kdi_test_log.log('steps', float_step)
        kdi_test_log.push_indent(2)

        for ipart  in partitions_list:

            kdi_test_log.log('ipart', ipart)
            kdi_test_log.push_indent(2)

            # a-temporel and a-partition declinaison
            pkchunk = pkbase.chunk()

            key = '/glu/version'
            pkchunk.insert(key, get_common_data(pkchunk, key))

            key = '/study/name'
            pkchunk.insert(key, get_common_data(pkchunk, key))

            # temporel and a-partition declinaison
            pkchunk = pkbase.chunk(float_step=float_step)

            key = '/study/date'
            pkchunk.insert(key, get_common_data(pkchunk, key))

            pkchunk.update_fields('/fields', 'myGlobScalarField')
            key = '/fields/myGlobScalarField'
            pkchunk.insert(key, get_common_data(pkchunk, key))

            pkchunk.update_fields('/fields', 'myGlobVectorField')
            key = '/fields/myGlobVectorField'
            pkchunk.insert(key, get_common_data(pkchunk, key))

            # temporal and partition declinaison
            pkchunk = pkbase.chunk(float_step=float_step, int_part=ipart)
            
            pkchunk.update('UnstructuredGrid', '/mymesh')

            if kdi_test_with_global_index_continuous:
                kdi_test_log.log('defined', '/mymesh/points/globalIndexContinuous')
                key = '/mymesh/points/globalIndexContinuous'
                pkchunk.insert(key, get_step_part__data(pkchunk, key))

            key = '/mymesh/points/cartesianCoordinates'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            pkchunk.update_fields('/mymesh/points/fields', 'myFieldPoint')
            key = '/mymesh/points/fields/myFieldPoint'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            if kdi_test_with_global_index_continuous:
                kdi_test_log.log('defined', '/mymesh/cells/globalIndexContinuous')
                key = '/mymesh/cells/globalIndexContinuous'
                pkchunk.insert(key, get_step_part__data(pkchunk, key))

            key = '/mymesh/cells/types'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            key = '/mymesh/cells/connectivity'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            #TODO kdi_update_eval_offsets(base, '/mymesh/cells')

            #TODO Quid in pkchunk/pkbase

            pkchunk.update_fields('/mymesh/cells/fields', 'myFieldCell')
            key = '/mymesh/cells/fields/myFieldCell'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            pkchunk.update_fields('/mymesh/cells/fields', 'myFieldCell2')
            key = '/mymesh/cells/fields/myFieldCell2'
            pkchunk.insert(key, get_step_part__data(pkchunk, key))

            add_checked, add_ignored = checked('Init by memory', pkchunk, ignored_data_key=ignored_data_key)
            sum_checked += add_checked
            sum_ignored += add_ignored

            kdi_test_log.pop_indent()

        add_checked, add_ignored = checked_all('Init by memory', partitions_list, pkchunk,
                                               ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored

        # == SAVE VTK HDF + JSON, LOAD JSON ============================
        pkchunk = pkbase.chunk(float_step)

        # if not KDIVTKHDFCheck(base):
        #     raise Exception('No check base anormaly for VTK HDF!')
        #
        # if kdi_test_with_nto1_saving_vtkhdf:
        #     assert ('pre: mpi_size>1 to saveVTKHDF_Nto1' and mpi_size > 1)
        #     assert ('pre: with GIC to saveVTKHDF_Nto1' and kdi_test_with_global_index_continuous)
        #     kdi_test_log.log('saveVTKHDF_Nto1', vtkhdf_filename + '__one.vtkhdf')
        #     write_vtk_hdf_n_to_1(base, vtkhdf_filename + '__one.vtkhdf')
        #     # This method does not modify base and does not save the JSON file.
        #
        # if kdi_test_with_saving_vtkhdf:
        #     if mpi_size > 1 and kdi_test_with_global_index_continuous:
        #         kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__hard.vtkhdf')
        #         write_vtk_hdf(base, vtkhdf_filename + '__hard.vtkhdf', with_json=kdi_test_with_saving_json)
        #     else:
        #         kdi_test_log.log('saveVTKHDF', vtkhdf_filename + '__hard.vtkhdf')
        #         write_vtk_hdf(base, vtkhdf_filename + '__hard.vtkhdf', with_json=kdi_test_with_saving_json)
        #
        #     if kdi_test_with_loading_json:
        #         del base
        #         kdi_test_log.log('loadJSON', vtkhdf_filename + '__hard.json')
        #         base = read_json(vtkhdf_filename + '__hard.json')

        add_checked, add_ignored = checked_all('Init by memory', partitions_list, pkchunk,
                                               ignored_data_key=ignored_data_key)
        sum_checked += add_checked
        sum_ignored += add_ignored

        kdi_test_log.pop_indent()

    kdi_test_log.log('sum_checked', sum_checked)
    kdi_test_log.log('sum_ignored', sum_ignored)
    kdi_test_log.log('ignored_data_key', ignored_data_key)

    if kdi_test_with_global_index_continuous:
        if mpi_size == 3:
            assert (sum_checked == 285 + 513)
            assert (sum_ignored == 0)
        else:
            assert (sum_checked == 855 + 1539)
            assert (sum_ignored == 0)
        assert (ignored_data_key == set())
    else:
        if mpi_size == 3:
            assert (sum_checked == 285)
            assert (sum_ignored == 513)
        else:
            assert (sum_checked == 855)
            assert (sum_ignored == 1539)
        assert (ignored_data_key == {'/mymesh/cells/globalIndexContinuous', '/mymesh/points/globalIndexContinuous'})

    # In SEQUENTIAL mode:
    #   The JSON file is the same True or False for kdi_test_with_global_index_continuous,
    #   the attribute exists but there are no values provided:
    #       diff 1pe_1uns_3ssd_10tps__hard.json EXPECTED/1pe_1uns_3ssd_10tps__hard.json
    #
    #   TODO '/study/date' not saved stepless-part on KDI in HDF
    #   TODO '/chunks' not used values in HDF for growning variant
    #
    #   The VTK HDF file is different, we also find for kdi_test_with_global_index_continuous=True
    #   the description in /KDI/mymesh/*Data/globalIndexContinuous:
    #      EXPECTED/1pe_1uns_3ssd_10tps__hard__GIC.vtkhdf
    #      EXPECTED/1pe_1uns_3ssd_10tps__hard__non_GIC.vtkdhf
    #
    # TODO Oddly enough, h5diff indicates that the files are the same... even though the first
    #      one contains the GIC description that the second one doesn't!
    #
    # In PARALLEL mode (3pes):
    #   The JSON file is same, just changes the file name.
    #   The '__hard' VTK HDF file is same.
    #   The product '__one' VTK HDF file depends on kdi_test_with_global_index_continuous=True
    #   else non product:
    #      EXPECTED/3pe_1uns_3ssd_10tps__one.vtkhdf


def test_1uns_3ssd_10tps__save_json():
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    reset_kdi_test_1uns_3ss_10tps()
    test_1uns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']


def test_1uns_3ssd_10tps__save_and_load_json():
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    os.environ['KDI_TEST_WITH_LOADING_JSON'] = '1'
    reset_kdi_test_1uns_3ss_10tps()
    test_1uns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']
    del os.environ['KDI_TEST_WITH_LOADING_JSON']


def test_1uns_3ssd_10tps_json():
    os.environ['KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS'] = '1'
    os.environ['KDI_TEST_WITH_SAVING_JSON'] = '1'
    os.environ['KDI_TEST_WITH_LOADING_JSON'] = '1'
    reset_kdi_test_1uns_3ss_10tps()
    test_1uns_3ssd_10tps()
    del os.environ['KDI_TEST_WITH_SAVING_JSON']
    del os.environ['KDI_TEST_WITH_LOADING_JSON']
    del os.environ['KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS']


if __name__ == '__main__':
    # Fr: ...
    # En: ...
    test_1uns_3ssd_10tps()
    kdi_test_log.log('>>> test_1uns_3ssd_10tps: checked')
    import sys
    if len(sys.argv) > 1:
        assert( sys.argv[1] == 'SIMPLE')
        exit(0)
    test_1uns_3ssd_10tps__save_json()
    kdi_test_log.log('>>> test_1uns_3ssd_10tps__save_json: checked')
    test_1uns_3ssd_10tps__save_and_load_json()
    kdi_test_log.log('>>> test_1uns_3ssd_10tps__save_and_load_json: checked')
    test_1uns_3ssd_10tps_json()
    kdi_test_log.log('>>> test_1uns_3ssd_10tps_json: checked')
