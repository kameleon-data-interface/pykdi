import numpy as np

from pykdi import (kdi_base, KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT, KDI_AGREEMENT_STEPPART,
                   KDI_AGREEMENT_STEPPART_VARIANT_STEP, KDI_AGREEMENT_STEPPART_VARIANT_PART, KDIException)

"""
Fr: Ce test valide les différents modes de création d'une base ainsi que des erreurs qui peuvent être déclenchées.

Use pykdi:
- kdi_base
- KDIChunks .SetVariants and KDI_GROWNING_VARIANT, KDI_CONSTANT_VARIANT
- KDIException
- KDIMemory .insert

Use pykdi.agreement:
- KDI_AGREEMENT_STEPPART_VARIANT_STEP
- KDI_AGREEMENT_STEPPART_VARIANT_PART
- KDI_AGREEMENT_STEPPART
"""


def test_kdi_base():
    nb_parts = 4

    # Fr: Création détaillée de base
    base_reference = kdi_base()
    base_reference['/chunks'].SetVariants(
        [[KDI_AGREEMENT_STEPPART_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
         [KDI_AGREEMENT_STEPPART_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': nb_parts, }, ], ])

    base_reference['/agreement'].insert(np.array([KDI_AGREEMENT_STEPPART]), chunk={})

    str_base_reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base')}"

    assert (str(base_reference) == str_base_reference)

    # Fr: Création détaillée de base avec une configuration vide
    base = kdi_base({})
    base['/chunks'].SetVariants(
        [[KDI_AGREEMENT_STEPPART_VARIANT_STEP, {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', }, ],
         [KDI_AGREEMENT_STEPPART_VARIANT_PART, {'type': KDI_CONSTANT_VARIANT, 'max': nb_parts, }, ], ])

    base['/agreement'].insert(np.array([KDI_AGREEMENT_STEPPART]), chunk={})

    assert (str(base_reference) == str(base))

    # Fr: Création détaillée de base avec un nom 'agreement' INCORRECT décrit dans la configuration
    #     Test en erreur
    try:
        kdi_base({'agreement': 'Soleil'})
    except KDIException as e:
        assert (e.args[0] == 'pre: this agreement \'Soleil\' does not match any of the known agreements. [\'STEPPART\',]!')

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     il MANQUE à préciser pour cette configuration la clef 'nb_parts'
    #     Test en erreur
    try:
        kdi_base({'agreement': KDI_AGREEMENT_STEPPART})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[0] == 'pre: configuration must be have the key \'nb_parts\' in ' +
                'configuration \'agreement\'=\'' + KDI_AGREEMENT_STEPPART + '\', type value int or string describe int')

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     mais la valeur associée à la 'nb_parts' de cette configuration est INCORRECT
    #     Test en erreur
    try:
        kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 'sunshine'})
        raise Exception('Guru Meditation: Internal Error')
    except KDIException as e:
        assert (e.args[0] == 'pre: configuration must be have the key \'nb_parts\' in ' +
                'configuration \'agreement\'=\'' + KDI_AGREEMENT_STEPPART + '\', type value int or string describe int')

    str_base_reference_agreement = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     avec une valeur CORRECTE associée à 'nb_parts' de cette configuration
    #     décrit sous la forme d'une chaîne de caractères

    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': str(nb_parts)})

    assert (str_base_reference_agreement == str(base))

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     avec une valeur CORRECTE associée à 'nb_parts' de cette configuration
    #     décrit sous la forme d'un entier
    base = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': nb_parts})
    assert (str_base_reference_agreement == str(base))

if __name__ == '__main__':
    test_kdi_base()
    print('\ntest_kdi_base: checked')
