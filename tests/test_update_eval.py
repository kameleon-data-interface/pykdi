import numpy as np
import os

from pykdi import *

custom_command_eval_offsets = """
base = Base
code2nbPoints = base[Params[0]]
typesCode = base[Params[1]].get()
if typesCode.dtype == '|S1':
    nbPoints = code2nbPoints[typesCode.view(dtype=np.int8)]
else:
    nbPoints = code2nbPoints[typesCode]
Ret = np.empty((len(nbPoints) + 1), dtype=np.int64)
Ret[0] = 0
np.cumsum(nbPoints, out=Ret[1:])
"""

def kdi_update_eval_string(Base):
    print('kdi_update_eval_string in progress')

    print ('   ', Base['/code2nbPoints'])  # stepless-partless
    print ('   ', Base['/mymesh/cells/types'].get())

    Base['/mymesh/cells/offsets'] = KDIEvalString(Base, '/mymesh/cells/offsets', custom_command_eval_offsets,
                                                  ['/code2nbPoints', '/mymesh/cells/types'],
                                                  serializable=True)
    # Fr: C'est serialisable car seul le premier paramètre base est une structure complexe.
    assert (np.all(Base['/mymesh/cells/offsets'].get() == np.array([0, 3, 6, 10, ])))

    serialize = str(Base['/mymesh/cells/offsets'])

    KDIBase = Base
    Base['/truc'] = eval(serialize)
    assert (np.all(Base['/truc'].get() == np.array([0, 3, 6, 10, ])))

    # Fr: Le fait de serialiser puis d'evaluer en donnant le même résultat démontre par la preuve que c'est serializable.

    assert (Base['/mymesh/cells/offsets'].light_dump() == "KDIEvalString(... , datas=KDIMemory(base=KDIBase, key_base='/mymesh/cells/offsets', datas={},))")

    del Base['/truc']

    Base['/mymesh/cells/offsets'] = KDIEvalString(Base, '/mymesh/cells/offsets', custom_command_eval_offsets,
                                                  ['/code2nbPoints', '/mymesh/cells/types'],
                                                  kmemo=KDIMemory(Base, '/mymesh/cells/offsets'),
                                                  serializable=True)

    try:
        Base['/mymesh/cells/offsets'] = KDIEvalString(Base, '/mymesh/cells/offsets', custom_command_eval_offsets,
                                                      ['/code2nbPoints', '/mymesh/cells/types'],
                                                      kmemo='Sous le soleil des tropiques',
                                                      serializable=True)
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "KDIEvalString FATAL ERROR kmemo is not instance KDIMemory")

    Base['/mymesh/cells/offsets'] = KDIEvalString(Base, '/mymesh/cells/offsets', custom_command_eval_offsets,
                                                  ['truc', 'bidule'],
                                                  serializable=True)
    try:
        assert (np.all(Base['/mymesh/cells/offsets'].get() == np.array([0, 3, 6, 10, ])))
        assert (False)
    except KDIException as e:
        print(str(e.args[0]))
        assert (str(e.args[0]) == "Error in execution '/mymesh/cells/offsets' KDIEvalString!")

    print('... terminated kdi_update_eval_string')


def moc_kdi_update_eval_selection_connectivity_indexes_points(Base, Params):
    # Begin coding
    print('KDIEvalString:moc_kdi_update_eval_selection_connectivity_indexes_points')
    base = Base
    print('     ', '/chunks/current', ' in base:', str('/chunks/current' in base))
    chunk = base['/chunks/current']
    print('  IN base')
    # code2kdi
    code2kdi = base['/code2kdi']
    print('  IN code2kdi:', code2kdi)
    # indexes
    indexesbase = Params[0]
    indexesbasechunk = indexesbase['/chunks/current']
    indexesbase['/chunks/current'] = chunk
    indexescellskey = Params[1]
    print('  IN indexescellskey:', indexescellskey)
    indexescellvalues = indexesbase[indexescellskey].get()
    print('     indexescellvalues:', indexescellvalues)
    indexesbase['/chunks/current'] = indexesbasechunk
    # in values
    inbase = Params[2]
    inbasechunk = inbase['/chunks/current']
    inbase['/chunks/current'] = chunk
    intypeskey = Params[3]
    print('  IN intypeskey:', intypeskey)
    intypesvalues = inbase[intypeskey].get()
    print('     intypesvalues:', intypesvalues)
    inconnectivykey = Params[4]
    print('  IN inconnectivykey:', inconnectivykey)
    inconnectivyvalues = inbase[inconnectivykey].get()
    print('     inconnectivyvalues:', inconnectivyvalues)
    inbase['/chunks/current'] = inbasechunk
    # kdi2nbPoints
    kdi2nbPoints = Params[5]
    print('  IN kdi2nbPoints:', kdi2nbPoints)
    # out key
    outconnectivykey = Params[6]
    print('  IN outconnectivykey:', outconnectivykey)
    outindexespointskey = Params[7]
    print('  IN outindexespointskey:', outindexespointskey)
    outconnectivy = np.empty(shape=(inconnectivyvalues.shape[0],), dtype=inconnectivyvalues.dtype)
    points = {}
    OUTpoints = []
    crt_INindexescellvalues = 0
    crt_INconnectivity = 0
    crt_OUTconnectivity = 0
    crt_OUTpoints = 0
    for icell, intypevalue in enumerate(intypesvalues):
        print('icell:', icell, 'intypevalue:', intypevalue)
        nbPoints = kdi2nbPoints[code2kdi[intypevalue]]
        print('  nbPoints:', nbPoints)
        if indexescellvalues[crt_INindexescellvalues] != icell:
            print('  ', False, '(', indexescellvalues[crt_INindexescellvalues], ')')
            crt_INconnectivity += nbPoints
            print('  crt_INconnectivity:', crt_INconnectivity)
        else:
            print('  ', True, '(', indexescellvalues[crt_INindexescellvalues], ')')
            crt_INindexescellvalues += 1
            print('  crt_INindexescellvalues:', crt_INindexescellvalues)
            end_INconnectivity = crt_INconnectivity + nbPoints
            print('  crt_INconnectivity:', crt_INconnectivity)
            print('  end_INconnectivity:', end_INconnectivity)
            while crt_INconnectivity != end_INconnectivity:
                iPoint = inconnectivyvalues[crt_INconnectivity]
                print('  iPoint:', iPoint)
                if iPoint not in points:
                    points[iPoint] = crt_OUTpoints
                    OUTpoints.append(iPoint)
                    iPoint = crt_OUTpoints
                    crt_OUTpoints += 1
                else:
                    iPoint = points[iPoint]
                outconnectivy[crt_OUTconnectivity] = iPoint
                crt_INconnectivity += 1
                print('  crt_INconnectivity', crt_INconnectivity)
                crt_OUTconnectivity += 1
                print('  crt_OUTconnectivity:', crt_OUTconnectivity)
    outconnectivy = np.resize(outconnectivy, crt_OUTconnectivity)
    Ret = {outconnectivykey: outconnectivy, outindexespointskey: np.array(OUTpoints), }
    print('  OUT Ret:', Ret)
    # End coding
    return Ret


command_eval_selection_connectivity_indexes_points = """
import numpy as np
# Begin coding
print('KDIEvalString:kdi_update_eval_selection_connectivity_indexes_points')
base = Base
print('     ', '/chunks/current',' in base:', str('/chunks/current' in base))
chunk = base['/chunks/current']
print('  IN base')
# code2kdi
code2kdi = base['/code2kdi']
print('  IN code2kdi:', code2kdi)
# indexes
indexesbase = Params[0]
indexesbasechunk = indexesbase['/chunks/current']
indexesbase['/chunks/current'] = chunk
indexescellskey = Params[1]
print('  IN indexescellskey:', indexescellskey)
indexescellvalues = indexesbase[indexescellskey].get()
print('     indexescellvalues:', indexescellvalues)
indexesbase['/chunks/current'] = indexesbasechunk
# in values
inbase = Params[2]
inbasechunk = inbase['/chunks/current']
inbase['/chunks/current'] = chunk
intypeskey = Params[3]
print('  IN intypeskey:', intypeskey)
intypesvalues = inbase[intypeskey].get()
print('     intypesvalues:', intypesvalues)
inconnectivykey = Params[4]
print('  IN inconnectivykey:', inconnectivykey)
inconnectivyvalues = inbase[inconnectivykey].get()
print('     inconnectivyvalues:', inconnectivyvalues)
inbase['/chunks/current'] = inbasechunk
# kdi2nbPoints
kdi2nbPoints = Params[5]
print('  IN kdi2nbPoints:', kdi2nbPoints)
# out key
outconnectivykey = Params[6]
print('  IN outconnectivykey:', outconnectivykey)
outindexespointskey = Params[7]
print('  IN outindexespointskey:', outindexespointskey)
outconnectivy = np.zeros(shape=(inconnectivyvalues.shape[0],), dtype=inconnectivyvalues.dtype)
points = {}
OUTpoints = []
crt_INindexescellvalues = 0
crt_INconnectivity = 0
crt_OUTconnectivity = 0
crt_OUTpoints = 0
for icell, intypevalue in enumerate(intypesvalues):
    print('icell:', icell, 'intypevalue:', intypevalue)
    nbPoints = kdi2nbPoints[code2kdi[intypevalue]]
    print('  nbPoints:', nbPoints)
    if indexescellvalues[crt_INindexescellvalues] != icell:
        print('  ', False, '(', indexescellvalues[crt_INindexescellvalues], ')')
        crt_INconnectivity += nbPoints
        print('  crt_INconnectivity:', crt_INconnectivity)
    else:
        print('  ', True, '(', indexescellvalues[crt_INindexescellvalues], ')')
        crt_INindexescellvalues += 1
        print('  crt_INindexescellvalues:', crt_INindexescellvalues)
        end_INconnectivity = crt_INconnectivity + nbPoints
        print('  crt_INconnectivity:', crt_INconnectivity)
        print('  end_INconnectivity:', end_INconnectivity)
        while crt_INconnectivity != end_INconnectivity:
            iPoint = inconnectivyvalues[crt_INconnectivity]
            print('  iPoint:', iPoint)
            if iPoint not in points:
                points[iPoint] = crt_OUTpoints
                OUTpoints.append(iPoint)
                iPoint = crt_OUTpoints
                crt_OUTpoints += 1
            else:
                iPoint = points[iPoint]
            outconnectivy[crt_OUTconnectivity] = iPoint
            crt_INconnectivity += 1
            print('  crt_INconnectivity', crt_INconnectivity)
            crt_OUTconnectivity += 1
            print('  crt_OUTconnectivity:', crt_OUTconnectivity)
outconnectivy = np.resize(outconnectivy, crt_OUTconnectivity)
Ret = {outconnectivykey: outconnectivy, outindexespointskey: np.array(OUTpoints), }
print('  OUT Ret:', Ret)
"""


def test_kdi_update_eval_selection_connectivity_indexes_points():
    # MOC
    class MOC:
        def __init__(self, data):
            self.data = data

        def get(self, chunk=None):
            return self.data

    outconnectivykey = '/mymil1/cells/connectivity'
    outindexespointskey = '/mymil1/indexespoints'

    Base = {'/code2kdi': {0: 0, 1: 1},
            '/code2nbPoints': np.array([3, 4, ]),
            '/mymil1/indexescells': MOC(np.array([0, 2, ])),
            '/mymesh/cells/types': MOC(np.array([0, 0, 1, ])),
            '/mymesh/cells/connectivity': MOC(np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0, ])),
            '/mymesh/points/cartesianCoordinates': MOC(np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [1., 1., 0.], [0., -1., 0.], [1., -1., 0.], ])),
    }
    Base['/chunks'] = KDIChunks(base=Base, order=['step', 'part'], variants={'step': {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', 'data': np.array([1.0, ])},'part': {'type': KDI_CONSTANT_VARIANT, 'data': 1},})
    Base['/chunks/current'] = {'step': 1.0, 'part': 0, '#step': '1.0', '#part': '0'}
    Params = [Base, '/mymil1/indexescells', Base, '/mymesh/cells/types', '/mymesh/cells/connectivity', {0: 3, 1: 4},
              outconnectivykey, outindexespointskey ]

    cachingkey = '/caching' + outconnectivykey + outindexespointskey
    NewBase = {'/code2kdi': {0: 0, 1: 1}, }
    NewBase['/chunks'] = Base['/chunks']
    NewBase['/chunks/current'] = {'step': 1.0, 'part': 0, '#step': '1.0', '#part': '0'}
    NewBase[cachingkey] = moc_kdi_update_eval_selection_connectivity_indexes_points(Base, Params)

    assert (np.all(NewBase[cachingkey][outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[cachingkey][outindexespointskey] == np.array([0, 1, 3, 4, 5])))

    NewBase[cachingkey] = KDIEvalString(NewBase, cachingkey, command_eval_selection_connectivity_indexes_points,
                                        [Base, '/mymil1/indexescells', Base, '/mymesh/cells/types',
                                         '/mymesh/cells/connectivity', kdi2nbPoints,
                                         outconnectivykey, outindexespointskey])

    assert (np.all(NewBase[cachingkey].get()[outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[cachingkey].get()[outindexespointskey] == np.array([0, 1, 3, 4, 5])))

    NewBase[outconnectivykey] = KDIEvent(NewBase, cachingkey, outconnectivykey)
    NewBase[outindexespointskey] = KDIEvent(NewBase, cachingkey, outindexespointskey)

    assert (np.all(NewBase[outconnectivykey].get() == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[outindexespointskey].get() == np.array([0, 1, 3, 4, 5])))
    kdi_update_eval_selection(NewBase, '/mymil1/points/cartesianCoordinates', NewBase, outindexespointskey,
                              Base, '/mymesh/points/cartesianCoordinates')

    assert (np.all(NewBase['/mymil1/points/cartesianCoordinates'].get() ==
                   np.array([[0., 0., 0.], [1., 0., 0.], [1., 1., 0.], [0., -1., 0.], [1., -1., 0.], ])))

    kdi_update_eval_string(Base)


def test_kdi_update_eval_selection_connectivity_indexes_points_step():
    NewBase = {}
    # MOC
    class MOC:
        def __init__(self, data):
            self.data = data

        def get(self, chunk=None):
            return self.data

    class MOCSTEP:
        def __init__(self, data):
            self.data = data

        def get(self, chunk=None):
            if chunk:
                itps = NewBase['/chunks'].GetOffset('step', chunk['step'])
            else:
                itps = NewBase['/chunks'].GetOffset('step', NewBase['/chunks/current']['step'])
            return self.data[itps]

    outconnectivykey = '/mymil1/cells/connectivity'
    outindexespointskey = '/mymil1/indexespoints'

    Base = {'/code2kdi': {0: 0, 1: 1},
            '/code2nbPoints': np.array([3, 4, ]),
            '/mymil1/indexescells': MOC(np.array([0, 2, ])),
            '/mymesh/cells/types': MOC(np.array([0, 0, 1, ])),
            '/mymesh/cells/connectivity': MOC(np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0, ])),
            '/mymesh/points/cartesianCoordinates': MOCSTEP(
                { 0.0: np.array([[0., 0., 0.], [1., 0., 0.], [0., 1., 0.], [1., 1., 0.], [0., -1., 0.], [1., -1., 0.], ]),
                  1.0: np.array([[-1., 0., 0.], [0., 0., 0.], [-1., 1., 0.], [0., 1., 0.], [-1., -1., 0.], [0., -1., 0.], ]),
                }),
    }
    Base['/chunks'] = KDIChunks(base=Base, order=['step', 'part'], variants={'step': {'type': KDI_GROWNING_VARIANT, 'dtype': 'f8', 'data': np.array([0.0, 1.0, ])},'part': {'type': KDI_CONSTANT_VARIANT, 'data': 1},})
    Base['/chunks/current'] = {'step': 1.0, 'part': 0, '#step': '1.0', '#part': '0'}

    # Begin inconstance declaration test
    Base['/mymesh/cells/offsets'] = KDIEvalString(Base, '/mymesh/cells/offsets', 'bla bla bla',
                                                  [Base['/code2nbPoints'], '/mymesh/cells/types'])
    try:
        kdi_update_eval_offsets(Base, '/mymesh/cells')
    except KDIException as e:
        assert (e.args[0] == 'pre: command already exist but it\'s different!')

    del Base['/mymesh/cells/offsets']

    kdi_update_eval_offsets(Base, '/mymesh/cells')
    # End inconstance declaration test

    assert (np.all(Base['/mymesh/cells/offsets'].get() == np.array([0, 3, 6, 10])))

    Params = [Base, '/mymil1/indexescells', Base, '/mymesh/cells/types', '/mymesh/cells/connectivity', {0: 3, 1: 4},
              outconnectivykey, outindexespointskey ]

    cachingkey = '/caching' + outconnectivykey + outindexespointskey

    NewBase['/code2kdi'] = {0: 0, 1: 1}
    NewBase['/chunks'] = Base['/chunks']
    NewBase['/chunks/current'] = {'step': 1.0, 'part': 0, '#step': '1.0', '#part': '0'}

    NewBase[cachingkey] = moc_kdi_update_eval_selection_connectivity_indexes_points(Base, Params)

    assert (np.all(NewBase[cachingkey][outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[cachingkey][outindexespointskey] == np.array([0, 1, 3, 4, 5])))

    NewBase[cachingkey] = KDIEvalString(NewBase, cachingkey, command_eval_selection_connectivity_indexes_points,
                                        [Base, '/mymil1/indexescells', Base, '/mymesh/cells/types',
                                         '/mymesh/cells/connectivity', kdi2nbPoints,
                                         outconnectivykey, outindexespointskey])

    assert (np.all(NewBase[cachingkey].get()[outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[cachingkey].get()[outindexespointskey] == np.array([0, 1, 3, 4, 5])))

    NewBase[outconnectivykey] = KDIEvent(NewBase, cachingkey, outconnectivykey)
    NewBase[outindexespointskey] = KDIEvent(NewBase, cachingkey, outindexespointskey)

    assert (np.all(NewBase[outconnectivykey].get() == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(NewBase[outindexespointskey].get() == np.array([0, 1, 3, 4, 5])))
    kdi_update_eval_selection(NewBase, '/mymil1/points/cartesianCoordinates', NewBase, outindexespointskey,
                              Base, '/mymesh/points/cartesianCoordinates')

    assert (np.all(NewBase['/mymil1/points/cartesianCoordinates'].get() ==
                   np.array([[-1., 0., 0.], [0., 0., 0.], [0., 1., 0.], [-1., -1., 0.], [0., -1., 0.], ])))


def test_kdi_update_eval_n_to_1():
    nb_parts = 4

    # Fr: Création détaillée de base avec un nom 'agreement' CORRECT décrit dans la configuration
    #     avec une valeur CORRECTE associée à 'nb_parts' de cette configuration
    #     décrit sous la forme d'un entier
    base_4 = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': nb_parts})

    str_base_4_reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base_4) == str_base_4_reference)

    assert (4 == base_4['/chunks'].GetMaxConstantValue('part'))

    try:
        base_4['/chunks'].GetMaxConstantValue('choubi')
    except KDIException as e:
        assert(str(e.args[0]) == "pre: variant `choubi` is not a variant of the base (['step', 'part'])")

    assert (4 == base_4['/chunks'].GetAll('part'))
    assert (np.all(np.array([]) == np.array(base_4['/chunks'].GetAll('step'))))

    try:
        base_4['/chunks'].GetAll('choubi')
    except KDIException as e:
        assert(str(e.args[0]) == "pre: variant `choubi` is not a variant of the base (['step', 'part'])")

    float_step = 2.3
    int_part = 2

    base_4['/chunks'].set({KDI_AGREEMENT_STEPPART_VARIANT_STEP: float_step, KDI_AGREEMENT_STEPPART_VARIANT_PART: int_part})

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_4['/chunks'].set([float_step, int_part])

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_4['/chunks'].set([float_step, float(int_part)])

    assert (str(base_4['/chunks']) == "KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([2.3])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':4},})")
    assert (str(base_4['/chunks/current']) == "{'step': 2.3, 'part': 2, '#step': '2.3', '#part': '2'}")

    base_1 = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 1})

    str_base_1_reference = "{'/agreement': KDIMemory(base=KDIBase, key_base='/agreement', variants=['stepless-partless'], datas=np.array(['STEPPART']),), '/glu/version': KDIMemory(base=KDIBase, key_base='/glu/version', variants=None, datas={},), '/glu/name': KDIMemory(base=KDIBase, key_base='/glu/name', variants=None, datas={},), '/glu': KDIComplexType('Glu'), '/study/date': KDIMemory(base=KDIBase, key_base='/study/date', variants=None, datas={},), '/study/name': KDIMemory(base=KDIBase, key_base='/study/name', variants=None, datas={},), '/study': KDIComplexType('Study'), '/chunks': KDIChunks(base=KDIBase, order=['step', 'part'], variants={'step':{'type':KDI_GROWNING_VARIANT, 'dtype':'f8', 'data':np.array([])},'part':{'type':KDI_CONSTANT_VARIANT, 'data':1},}), '/fields': KDIFields({}), '': KDIComplexType('Base'), '/vtkhdf/version': KDIMemory(base=KDIBase, key_base='/vtkhdf/version', variants=['stepless-partless'], datas=np.array([2, 0]),), '/vtkhdf': KDIComplexType('VTKHDF'), '/vtkhdf/force_common_data_offsets': True, '/code2kdi': array([ 0,  1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12], dtype=int8), '/code2nbPoints': array([3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8], dtype=int8)}"

    assert (str(base_1) == str_base_1_reference)

    assert (1 == base_1['/chunks'].GetMaxConstantValue('part'))

    assert (np.all(np.array([]) == np.array(base_4['/chunks'].GetAll('step'))))

    base_1['truc'] = KDIMemory(base_1, 'truc')
    base_1['truc'].insert(654321, {'step': 2.3, 'part': 0,})

    assert ({'step': 0, 'part': 0,} == base_1['/chunks'].GetOffsets({'step': 2.3, 'part': 0,}))
    assert (0 == base_1['/chunks'].GetOffset('step', 2.3))
    assert (0 == base_1['/chunks'].GetOffset('part', 0))

    base_1['truc'].insert(654321, {'step': 2.3, 'part': 0,})
    base_1['truc'].insert(654322, {'step': 4.3, 'part': 0,})
    base_1['truc'].insert(654323, {'step': 6.3, 'part': 0,})

    assert ({'step': 1, 'part': 0,} == base_1['/chunks'].GetOffsets({'step': 4.3, 'part': 0,}))
    assert (1 == base_1['/chunks'].GetOffset('step', 4.3))
    assert (0 == base_1['/chunks'].GetOffset('part', 0))

    print(str(base_1['truc']))
    assert (str(base_1['truc']) == "KDIMemory(base=KDIBase, key_base='truc', variants=['step', 'part'], datas={'2.3':{'0':np.array(654321),},'4.3':{'0':np.array(654322),},'6.3':{'0':np.array(654323),},},)")

    assert (np.all(np.array([2.3, ]) == np.array(base_4['/chunks'].GetAll('step'))))

    base_1['trac'] = KDIMemory(base_1, 'trac')
    base_1['trac'].insert(np.array([6., 5., 4., 3., 2., 1.]), {'step': 2.3, 'part': 0,})

    assert (str(base_1['trac']) == "KDIMemory(base=KDIBase, key_base='trac', variants=['step', 'part'], datas={'2.3':{'0':np.array([6.0, 5.0, 4.0, 3.0, 2.0, 1.0]),},},)")

    try:
        print(base_1['truc'].get({'step': 2.3,}))
        assert (False)
    except KDIException:
        pass
    assert (base_1['truc'].get({'step': 2.3, 'part': 0,}) == 654321)

    try:
        print(base_1['trac'].get({'step': 2.3,}))
        assert (False)
    except KDIException:
        pass
    assert (np.all(base_1['trac'].get({'step': 2.3, 'part': 0,}) == np.array([6., 5., 4., 3., 2., 1.])))

    base_4['truc'] = KDIProxyData(base_4, base_1, 'truc', {'part': 0})

    for ipart in range(4):
        assert (base_4['truc'].get({'step': 2.3, 'part': ipart,}) == 654321)

    base_4['trac'] = KDIProxyData(base_4, base_1, 'trac', {'part': 0})

    for ipart in range(4):
        assert (np.all(base_4['trac'].get({'step': 2.3, 'part': ipart,}) == np.array([6., 5., 4., 3., 2., 1.])))
        assert (base_4['truc'].get({'step': 2.3, 'part': ipart,}) == 654321)

    try:
        base_4['tric'] = KDIProxyData(base_4, base_1, 'trac', {'part': 0, 'vulcain': 12})
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "KDIChunks:check_del_variants del_variants ({'part': 0, 'vulcain': 12}) incompatible avec self.order (['step', 'part'])")

    try:
        base_4['tric'] = KDIProxyData(base_4, base_1, 'trac', {'vulcain': 12})
        assert (False)
    except KDIException as e:
        assert (str(e.args[0]) == "KDIChunks:check_del_variants del_variants ({'vulcain': 12}) incompatible avec self.order (['step', 'part'])")

    class MOC:
        def __init__(self, data):
            self.data = data

        def get(self, chunk=None):
            # TODO Activer cet assert et passer les methodes en n_to_1
            # assert (base_1['/chunks/current']['part'] == 0)
            return self.data

    outconnectivykey = '/mymil1/cells/connectivity'
    outindexespointskey = '/mymil1/indexespoints'

    base_1['/code2kdi'] = {0: 0, 1: 1}
    base_1['/code2nbPoints']= np.array([3, 4, ])
    base_1['/mymil1/indexescells']= MOC(np.array([0, 2, ]))
    base_1['/mymesh/cells/types']= MOC(np.array([0, 0, 1, ]))
    base_1['/mymesh/cells/connectivity']= MOC(np.array([0, 1, 3, 0, 3, 2, 4, 5, 1, 0, ]))
    base_1['/mymesh/points/cartesianCoordinates']= MOC(np.array([[-1., 0., 0.], [0., 0., 0.], [-1., 1., 0.], [0., 1., 0.], [-1., -1., 0.], [0., -1., 0.], ]))

    base_1['/chunks/current'] = {'step': 2.3, 'part': 0, '#step': '2.3', '#part': '0'}

    kdi_update_eval_offsets(base_1, '/mymesh/cells')

    assert (np.all(base_1['/mymesh/cells/offsets'].get() == np.array([0, 3, 6, 10])))

    Params = [base_1, '/mymil1/indexescells', base_1, '/mymesh/cells/types', '/mymesh/cells/connectivity', {0: 3, 1: 4},
              outconnectivykey, outindexespointskey ]

    cachingkey = '/caching' + outconnectivykey + outindexespointskey

    base_n = kdi_base({'agreement': KDI_AGREEMENT_STEPPART, 'nb_parts': 4})
    base_n['/code2kdi'] = {0: 0, 1: 1}
    base_n['/chunks/current'] = {'step': 2.3, 'part': 1, '#step': '2.3', '#part': '1'}

    # base_n[cachingkey] = moc_kdi_update_eval_selection_connectivity_indexes_points(base_1, Params)
    #
    # assert (np.all(base_n[cachingkey][outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    # assert (np.all(base_n[cachingkey][outindexespointskey] == np.array([0, 1, 3, 4, 5])))
    #
    # del base_n[cachingkey]
    # base_n[cachingkey] = KDIEvalString(base_n, cachingkey, command_eval_selection_connectivity_indexes_points,
    #                                   [base_1, '/mymil1/indexescells', base_1, '/mymesh/cells/types',
    #                                    '/mymesh/cells/connectivity', kdi2nbPoints,
    #                                    outconnectivykey, outindexespointskey])
    #
    # assert (np.all(base_n[cachingkey].get()[outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    # assert (np.all(base_n[cachingkey].get()[outindexespointskey] == np.array([0, 1, 3, 4, 5])))
    #
    # del base_n[cachingkey]
    kdi_update_eval_selection_connectivity_indexes_points_1_to_n(
        base_n, outconnectivykey, outindexespointskey,
        base_1, '/mymil1/indexescells', base_1, '/mymesh/cells/types',
       '/mymesh/cells/connectivity')

    assert (np.all(base_n[cachingkey].get()[outconnectivykey] == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(base_n[cachingkey].get()[outindexespointskey] == np.array([0, 1, 3, 4, 5])))

    base_n[outconnectivykey] = KDIEvent(base_n, cachingkey, outconnectivykey)
    base_n[outindexespointskey] = KDIEvent(base_n, cachingkey, outindexespointskey)

    assert (np.all(base_n[outconnectivykey].get() == np.array([0, 1, 2, 3, 4, 1, 0])))
    assert (np.all(base_n[outindexespointskey].get() == np.array([0, 1, 3, 4, 5])))
    kdi_update_eval_selection_1_to_n(base_n, '/mymil1/points/cartesianCoordinates', base_n, outindexespointskey,
                                     base_1, '/mymesh/points/cartesianCoordinates')

    assert (np.all(base_n['/mymil1/points/cartesianCoordinates'].get() ==
                   np.array([[-1., 0., 0.], [0., 0., 0.], [0., 1., 0.], [-1., -1., 0.], [0., -1., 0.], ])))


def test_kdi_eval_script():
    script_name = 'command_ec'

    command_ec = """
m = Base[Params[0]]
c = Base[Params[1]]
Ret = m * c * c
    """

    try:
        os.remove(script_name)
    except FileNotFoundError:
        pass

    with open(script_name, 'w') as fd:
        fd.write(command_ec)

    base = dict()
    base['m'] = 432
    base['c'] = 299789
    base['ec'] = KDIEvalScript(base, 'gloups', script_name, ['m', 'c'])
    assert(base['ec'].get() == 432 * 299789 * 299789)

    os.remove(script_name)




if __name__ == '__main__':
    test_kdi_update_eval_selection_connectivity_indexes_points()
    test_kdi_update_eval_selection_connectivity_indexes_points_step()
    test_kdi_update_eval_n_to_1()
    test_kdi_eval_script()
    print('\nkdi_update_eval_selection_connectivity_indexes_points: checked')
