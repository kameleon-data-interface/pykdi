**Auteur** : *Jacques-Bernard Lekien*, jacques-bernard.lekien@cea.fr

**Projet gitlab.com** : *pykdi*, *6 mai 2024*.

Mise à jour avec la version 0.13.0.

Ce document KDI VTK HDF est là pour expliquer le contenu additionnel que l'on trouve dans VTK HDF.
Il est à remarquer que toutes les données temporelles doivent être absolument écrites dans le fichier HDF
(suivant la sémantique VTK ou KDI) afin d'alléger le fichier JSON.

# Sommaire

- [Sans Milieu/Matériau](#sans_mat)
- [Sans Milieu/Matériau - Mono-partition Nto1](#sans_mat_nto1)
- [Milieu/Matériau via une sélection](#mat_via_sel)
- [Milieu/Matériau via un maillage](#mat_via_maillage)
- [Milieu/Matériau via un maillage - Mono-partition Nto1](#mat_via_maillage_nto1)

## <a name="#sans_mat"></a>Sans Milieu/Matériau

Ce cas de figure correspond aux tests **1pe_1uns_3ssd_10tps** et **3pe_1uns_3ssd_10tps** (__hard).
Exécution en séquentielle ou parallèle avec la définition de trois partitions et un maillage simple.

A noter que :
- les fichiers JSON sont les mêmes en dehors du référencement du nom de la base qui change.
```bash
tkdiff 1pe_1uns_3ssd_10tps__hard.json 3pe_1uns_3ssd_10tps__hard.json 
```
- les fichiers VTK HDF sont les mêmes.
```bash
h5diff 1pe_1uns_3ssd_10tps__hard.vtkhdf 3pe_1uns_3ssd_10tps__hard.vtkhdf 
```

Il reste 6 ***KDIMemory*** dans le fichier JSON :
- ***/agreement*** fixé au début de la simulation lors du positionnement de l'agrément
- ***/glu/version*** à fixer au début de la simulation
- ***/glu/name*** à fixer au début de la simulation
- ***/study/date*** plus problématique car c'est une valeur qui varie au cours du temps
- ***/study/name*** à fixer au début de la simulation
- ***/vtkhdf/version*** fixé au début de la simulation lors du positionnement de l'agrément

On trouve dans le fichier HDF la sémantique KDI pour stocker le GIC :
- ***/KDI/mymesh/CellData/globalIndexContinuous*** basé sur la description d'offset ***/VTKHDF/Steps/CellOffsets***
- ***/KDI/mymesh/PointData/globalIndexContinuous*** basé sur la description d'offset ***/VTKHDF/Steps/PointOffsets***

Ces GIC auraient pu être stockés comme un champ de valeurs aux cellules/points mais sémantiquement
cela n'avait aucun intérêt pour l'utilisateur de les voir apparaître. Par ailleurs, cela démontre
l'idée que l'on peut avoir des informations propres à KDI qui sont stockées dans le fichier HDF en
complément de la sémantique VTK.

TODO stocker en KDI HDF la date qui est une chaîne de caractères avec une variation temporelle

## <a name="#sans_mat_nto1"></a>Sans Milieu/Matériau - Mono-partition Nto1

Ce cas de figure correspond aux tests **3pe_1uns_3ssd_10tps** (__one) et
**3pe_1uns_2subuns_3ssd_10tps** (__hard_one).
Exécution en parallèle avec la définition de trois partitions mais avec l'écrivain Nto1 et un maillage simple.

A noter que :
- pas de fichier JSON
- les fichiers VTK HDF sont les mêmes car en effet, le cas test **3pe_1uns_2subuns_3ssd_10tps** (__hard_one)
  revient à décrire le maillage global en ignorant les sélections pour produire les sous-maillages.
```bash
h5diff 3pe_1uns_3ssd_10tps__one.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__hard_one.vtkhdf 
```

L'équivalent du GIC c'est le LIC (Local Index Continue), le parcours naturel du tableau, ce qui
explique qu'on n'a pas, pour le moment, de sémantique KDI spécifique.

TODO produire un fichier JSON capable de lire ces fichier VTK HDF Nto1 :
Il est probable que ce fichier JSON n'aura pas connaissance de la description des sous-maillages
dans le cas du test **3pe_1uns_2subuns_3ssd_10tps** (__hard_one) si il est construit à partir du fichier vtkhdf;
sinon le travail serait à faire lors de l'écriture par saveVTKHDF_Nto1.

## <a name="#mat_via_sel"></a>Milieu/Matériau via une sélection

Ce cas de figure correspond aux tests **1pe_1uns_2subuns_3ssd_10tps** et **3pe_1uns_2subuns_3ssd_10tps** (__hard).
Exécution en séquentielle ou parallèle avec la définition de trois partitions et avec deux matériaux décrits
comme une sélection.

A noter que :
- les fichiers JSON sont les mêmes en dehors du référencement du nom de la base qui change... et des valeurs
  différentes associées aux sous-maillages.
```bash
tkdiff 1pe_1uns_2subuns_3ssd_10tps__hard.json 3pe_1uns_2subuns_3ssd_10tps__hard.json 
```
- les fichiers VTK HDF sont les mêmes.
```bash
h5diff 1pe_1uns_2subuns_3ssd_10tps__hard.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__hard.vtkhdf 
```

Seul le maillage "global" est décrit en VTK HDF. 


Dans le fichier JSON, on trouve les mêmes efforts qu'à faire pour ***Sans Milieu/Matériau*** avec
en plus aussi pour l'aspect sous-maillage :
- /mymesh/submeshes/\*/indexescells
- /mymesh/submeshes/\*/cells/fields/\* qui sont les champs aux cellules
- /mymesh/submeshes/\*/points/fields/\* qui sont les champs aux points

TODO Mettre en place des méthodes spécifique au stockage et chargement de la description d'un sous-maillage
et de ses champs suivant KDI VTK.

## <a name="#mat_via_maillage"></a>Milieu/Matériau via un maillage

Ce cas de figure correspond aux tests **1pe_1uns_2subuns_3ssd_10tps** et **3pe_1uns_2subuns_3ssd_10tps** (__multimil).
Exécution en séquentielle ou parallèle avec la définition de trois partitions et avec deux matériaux décrit
comme une sélection mais avec l'application de la mutation KDIComputeMultiMilieux.

A noter que :
- pas de fichier JSON.
- les fichiers VTK HDF sont les mêmes.
```bash
h5diff 1pe_1uns_2subuns_3ssd_10tps__multimil.vtkhdf 3pe_1uns_2subuns_3ssd_10tps__multimil.vtkhdf 
```

TODO Manque le stockage du GIC cellules (resp. points) sur le maillage global suivant la sémantique KDI
mais en exploitant la description d'offset /VTKHDF/Steps/CellOffsets (resp. /VTKHDF/Steps/PointOffsets)
pourtant on trouve /KDI/mymesh/CellData et /KDI/mymesh/PointData

## <a name="#mat_via_maillage_nto1"></a>Milieu/Matériau via un maillage - Mono-partition Nto1

Ce cas de figure correspond aux tests **3pe_1uns_2subuns_3ssd_10tps** (__multi_one).
Exécution en parallèle avec la définition de trois partitions et avec deux matériaux décrit
comme une sélection mais avec l'application de la mutation KDIComputeMultiMilieux
mais avec l'écrivain Nto1.

A noter que :
- pas de fichier JSON.

TODO Produire un fichier JSON capable de lire ce fichier **3pe_1uns_2subuns_3ssd_10tps** (__multi_one)
voir les mêmes contraintes que sans milieux
