# PYKDI

## 1.0.0 (beyong version)

TODO tested in Arcane MPI_size=1 BigSize [1000;100;100]
TODO tested in Arcane MPI_size=4 MiddleSize [200;100;100]
TODO Mutation plan de coupe
TODO clean log in C++ Arcane
TODO checked Arcane multi mil (proto avec une sélection une cellule sur deux)

echo "pykdi ##"`cat src/pykdi/*.py src/pykdi/*/*.py src/pykdi/*/*/*.py src/pykdi/*/*/*/*.py | wc -l`" lines"
echo "   DICO ##"`cat src/pykdi/dictionary/*.json | wc -l`
echo "   TODO ##"`grep TODO src/pykdi/*.py src/pykdi/*/*.py src/pykdi/*/*/*.py src/pykdi/*/*/*/*.py | wc -l`
echo "   MD ##"`cat *.md | wc -l`
echo "      TODO ##"`cat *.md | grep TODO | wc -l`
echo "tests Python ##"`cat tests/*.py tools/*.py | wc -l`" lines"
echo "   TODO ##"`cat tests/*.py tools/*.py | grep TODO | wc -l`
echo "tests C++ ##"`cat main.cxx | wc -l`" lines"
echo "   TODO ##"`grep TODO main.cxx | wc -l`
echo "tests arcaninou ##"`cat arcaninou/*.cxx arcaninou/*.h | wc -l`" lines"
echo "   TODO ##"`grep TODO arcaninou/arcaninou_1pe_1uns.cxx | wc -l`

## 0.19.0
* add pykdi for session (first prototype)
  the runner actually is in pykdi/session/run included two test
  which required the appearance of pykdi/agreement/proxy and to 
  evolve the “standard” builtin version
  limitation:
  - use of first storage only
  - get imposes a complete redefinition of the chunk
    (see exemple pykdi/session/my_reader the pendant of
    pykdi/session/my_writer)
  - housekeeping must be done in this session code as well
    as refactoring
  - tests/rpc offers examples that could be the subject of a
    small demonstrative project for community
  - force that the values are necessarily Numpy
    (in this first step) after resolution of the
    encoding/decoding problem. 
  - tools/run_session.py
* fix test session/run.py

## 0.18.0
* checked arcaninou
* extend arcaninou parameter: -h -w -nto1 -nbCells #
* add tests example mono-server, multi-server
* add documentation USERMANUAL (Python)
* add arcaninou test with submeshes
* extend saveVTKHDFComputeNto1 in KDIAgreementStepPartChunk
* fix compute multi milieux + write VTK HDF via arcaninou
* fix compute multi milieux + write VTK HDF Nto1 via arcaninou
* use shared ptr in Kdi.h
* add documentation USERMANUAL (C++)
* add ersatz Gepeto rewritten in Python
* add kdi_builder with reformat initialisation KDIAgreementStepPartBase
* add test rpc socket under KDI

## 0.17.0
* add mutation 1toN
* extend create JSON with mesh main on assembly or not
* extend KVTKHDFDatas with mesh main description assembly or not
* extend KDIProxyData with modification variants (as 'part' N to 1) 
* script paraview (add and del)
* add check_variants and reduce_chunk on KDIChunks
* complete release on KDIProxyData
* writer VTK HDF support zero size mesh
* add key_base KVTKHDFDatas
* static method data_read_vtk_hdf
* extend support zero size mesh (method reduce max don't supported zero size mesh)
  kdi_update_eval_selection_connectivity_indexes_points.py
  kdi_update_eval_selection_connectivity_indexes_points_1_to_n.py
* extend create JSON on multi-meshes
* extend checked in tests

pykdi ##6313 lines (+846)
   DICO ##52 (-)
   TODO ##70 (-2)
   MD ##1922 (+29)
      TODO ##41 (+5)
tests Python ##6843 lines (+1494)
   TODO ##53 (+32)
tests C++ ##699 lines (-)
   TODO ##10 (-)
tests arcaninou ##803 lines (-)
   TODO ##3 (-)

## 0.16.0
* optimize connectivity compute in Nto1
* release test 1uns (42 conf/50), launch via run_test_1uns :
  - +/- material
  - seq/para 3
  - +/- GIC
  - +/- VTK HDF
  - +/- JSON
  - +/- Nto1 VTK HDF (+GIC)
* add tools/compare_array_hdf5
* chg run_test, launch leaks memory set
* add KVTKHDFStepPartChunks, reader Steps VTK HDF when initializing an instance 
* add tools KDILog, KDILogElapse
* add kdi_vtk_hdf_create_json and test
* first step in release by directory tools, vtkhdf
* propagates developments

pykdi ##5467 lines (+710)
   DICO ##52
   TODO ##72 (+18)
   MD ##1893
      TODO ##36 (+3)
tests Python ##5439 lines (+400)
   TODO ##19 (+3)
tests C++ ##699 lines
   TODO ##10
tests arcaninou ##803 lines
   TODO ##3

## 0.15.0
* add KDI_POLYGON
  with nbPoints=-1
  associated with KVTK_POLYGON
  WARNING Not save with saveVTKHTG
* add KVTKHDFGarbageCollector
* add environment variable KDI_AGREEMENT_CODE2KDI with value VTK
  to associate code cell type value with that of VTK
* use kdi_get_env
* add light_dump and __sizeof__ when specific object
* fix chunk size saveVTKHDF
* add KVTKHDFGarbageCollector after saveVTKHDF
* add gc.collect() after saveVTKHDF
* add arcaninou test
* rewrite KDIMemory
* rewrite/add kdi_get_env, kdi_log, kdi_log_elapse
* creation status on string/build chunk (create or not a new step)
* fix variant control value
* extend code coverage and doc test_memory_set
* checked arcaninou and the Real

pykdi ##4788 lines
   TODO ##54 (-8)
   MD ##1850
      TODO ##27 (+4)
tests Python ##4995 lines
   TODO ##16 (-10)
tests C++ ##699 lines
   TODO ##10

## 0.14.0
* add KDIVTKHDF.md
* add KVTKHDFFieldGlobalDatas
* add saveVTKHDF option with_clean_data_submeshes for clean KDIMemory information
* add test_1uns_2subuns_3ssd_10tps this option has True
* add this default True saveVTKHDF in C++ testing

pykdi ##4135 lines
   TODO ##51
   MD ##1786
      TODO ##23
tests Python ##4430 lines
   TODO ##23
tests C++ ##699 lines
   TODO ##10

## 0.13.0

* previous todos have been processed
* specific KDI_VTK_HDF_STEPS_CHUNK_SIZE (see ENVIRONMENT.md)
* add mySSD in test_1uns_2subuns_3ssd_10tps
* add global in fields in test_1uns_3ssd_10tps and test_1uns_2subuns_3ssd_10tps
* save VTKHDF Nto1 only parallel mode (mpi size == nb parts)
* add FieldData, Steps/FieldDataOffsets et Steps/FieldDataSizes with
  SoftLink for materials to the description of the parent mesh (global)
* extends A and B from C to support empty chunk description
* release MOONSHOT
* validates description standardization on all proc. by code in KVTKHDFCheck
* extend test C++, add run parallel 4 (mono mat)
* add saveVTKHDFNto1 in KDIAgreementStepPartChunk

pykdi ##4024 lines
   TODO ##49
   MD ##1641
tests Python ##4426 lines
   TODO ##24
tests C++ ##699 lines
   TODO ##10

RESET ALL TODO
* it is not possible from KDI's point of view to change the number
  of processes between each output which VTK HDF supports
* optimize evaluation KDIWriteVTKHDFNto1 and connectivity
* reflection to be made on how to set chunk size:
  - either absolutely like here
  - either adaptively from a desired number of times per chunk (T),
    the size in number of elements of the array at the time of its creation (N)
    which requires summing it over all the subdomains: T*N; It remains to be seen
    whether there are implementation constraints in order to obtain the best performance.

## 0.12.0
* extend Nto1 to take into account sub-mesh
  - after GIC selection
  - compute a new GIC by mil
* extend test_1uns_2subuns_3ssd_10tps run_test
* add myGIC in test_1uns_2subuns_3ssd_10tps

TODO
* bad sequential compute a new GIC by mil

## 0.11.0
* add decliniation test with globalIndexContinue
* add remark VTKHDF.md
* add KDIExceptionNoWriteFileJson
* restore after saveVTKHDF the current chunks status in base
* add new saveVTKHDF_Nto1
* add ghost treatment in KDIComputeMultiMilieux
* add ref Nto1 3pe_1uns_3ssd_10tps
* add globalIndexContinue and using in tests/test_1uns_3ssd_10tps.py
* extend run_test (seq., parallel with Nto1 or not)
* add key in instance for dbg
* only processes the JSON file
* set atemporel apartition glu version and study date
* new rules write with/without JSON

TODO
* KDIVTKHDFCheck must, beyond the existence of the key,
  check if a value is associated for a given chunk
* define but without using in tests/test_1uns_2subuns_3ssd_10tps.py

## 0.10.0
* decline 1uns_2submesh_3ssd to 1uns_3ssd (without submesh)
* upgrade fo test to support 1pe or 3pe
* ignored KDI_PIT in code2kdi of KDIVTKHDFCheck
* sorted keys list for parallel execution
* rename test_1uns_1pe_4ssd_10tps_memory_set to test_1uns_1pe_3ssd_10tps_memory_set
* rename test_1uns_2subuns_1pe_3ssd_10tps_memory_set to test_1uns_2subuns_3ssd_10tps.py
* add test_1uns_3ssd_10tps.py (without submeshes)
* add environment variable in KDIWriterVTKHDF
  - KDI_VTK_HDF_CHUNK_SIZE
  - KDI_VTK_HDF_TRACE
  - KDI_VTK_HDF_TRACE_TICTAC
  - KDI_VTK_HDF_TRACE_TICTAC_SUMMARY
  - KDI_VTK_HDF_CHECK_TRACE
* add ENVIRONMENT.md
* adaptation of the writer to execution sequential and parallel writing
  (as many sub-domains of calculation, partitions/parts)
* refactoring of variable and function naming
* sorted key_meshes in writer VTK HDF
* add parallel mode HDF file open with driver MPIO and describe communicator
* desactive saveJSON in parallel
* added mpi4py like requirements
* cleaning code and check method
* add run_test.sh: sequential and parallel (3)

TODO
* sharing semantics
* added a test with a sub-mesh and a field described only on a process
* to be treated /fields

## 0.9.0
* fix by begin=np.int64(0)
  The addition of 2 char gives a char and therefore potentially a false
  result (it is not necessarily false in C/C++ due to an integer conversion).
  So that the result is not false, it must be in the right type and therefore
  calculate in the right type with the right addition operator, which is why one
  of the two members of the operation is of this type (converted according to this type).
* fix explicit type np.array in tests
* fix KDISubMeshes in KDIJson
* method naming standardization
* optimizest eval offsets and selection connectivity indexes points
* add environment variable KVTKHDF_CHUNK_SIZE
* add KDI_PIT, KDI_SIZE
* add KVTKHDF_PIT, KVTKHDF_SIZE
* transformed code2kdi,code2nbPoints into np.array
* transformed kdi2nbPoints into np.array
* transformed kdi2vtk,vtk2kdi into np.array
* use __repr__ for np.array for JSON
* add docstring
* add KDIExceptionNoWriteFileJson
* use kdi_update_eval_offsets
(No tested in Arcade)

## 0.8.0
* add 3 test_unit_kdi_update_eval...
* use the new optimize eval_string_connectivity
* fix: protect access /chunks/current in base for eval_string_selection and connectivity
* warning comment in kdi_update
(Tested in Arcade and on calculator)

## 0.7.0
* Erase print expensive
* add trace lines script and print time for KDIEvalString
* add todo for KDIEvalScript
* KVTKHDF_CHUNK_SIZE to 1M
* set_datas rename datas to datas_0 for iPart=0 (optimize, reuse for len and access datas)
* suppress import in string of KDIEvalScring
* comment reverse command types VTK to types code
* erase import in command eval
* rewrite command_eval_offsets
* rewrite command_eval_selection

TODO
* rewrite command_eval_selection_connectivty_indexes_points

## 0.6.0
* Sets the size of a chunk absolutely

TODO
* reflection to be made on how to set this size:
  - either absolutely like here
  - either adaptively from a desired number of times per chunk (T),
    the size in number of elements of the array at the time of its creation (N)
    which requires summing it over all the subdomains: T*N; It remains to be seen
    whether there are implementation constraints in order to obtain the best performance.

## 0.5.0
* add external dictionary JSON file for describe a communauty semantic
* add KDI_DICTIONARY_PATH to find the dictionary.json file
* del obsolete code on creation sub unstructured (with explicite parent mesh)
* add test on dictionary
* add complex/simple type parser
* add saveVTKHDFCompute C++, it's not very elegant
* chg update_sub C++, return new path milieu, it's not very elegant
* add test C++ with milieux
Note:
* That today the computer type as well as the dimensions are not used by KDIMemory
* Each field is just an array. The computer type is implied during assignment.*
  No particular semantic type.

## 0.4.0
* add semantic SubUnstructuredGrid (milieux)
* add function ComputeMultiMilieux (vision sub to vision multi-uns)
* add VTK HDF pour stocker la vision multi-uns
* add test 1uns_2subuns_1pe_3ssd_10tps, checked with ParaView
* option save original mesh as milieux
* option projection fields points and cells original mesh on each milieu
* documentation MOONSHOT et VTKHDF
* add modern description SubUNS by kdi_update_sub without attribut LinkOnUnstructuredGrid
* extend KDIComputeMilieux for this modern description
* add new test_kdi_compute_multimilieux
* add update_sub for binding C++
* declines test C++ mono and multi milieux
* clean venv
* maj .gitignore
* differentiates the creation of a HDF file (w) from the completion of a HDF file (a)

TODO
* modify test_1uns_2subuns_1pe_10tps_memory for modern description
* extend binding for modern description
* extend test c++ for declare subuns

## 0.3.0
* ajout de nombreux tests
* mise en place du binding pour appeler pykdi en C++ avec un test d'appel
* fix KDIAgreementStepPartChunk qui ne peut pas modifier la
  représentation de la base mais juste mémoriser une configuraiton chunk
* add setChunk KDIAgreementStepPartBase
* modification afin d'être conforme à ce qu'attends ParaView/VTK
  * on ne déclare que l'offset du premier part
  * modification importante dans l'écrivain et dans le lecteur
  * constat: gain en écriture mais surcoût en lecture
* mise en place de chunk stepless, partless ou (step, part) en mode
  agreement VTK HDF
* mise en place CI avec une arborescente qui ne permet plus de tester
  simplement en mode dev.
* simplification de la transmission du code2kdi en le déclarant dans le
  dictionnaire, cela a eu un impact sur le JSON
* mise en place de la notion de SubUNS
* en cours, le traitement pour convertir UNS+SubUNS en une hiérarhcie de UNS
* à faire, la prise en compte pour écrire cette hiérarchie dans VTK HDF

## 0.2.0

wc -lsrc/pykdi/*.py >>> **1843 lignes**

wc -l tests/*.py >>> **1078 lignes**

* apparition de la notion d'agreement STEPPART avec la déclinaison
  * kdi_agreement_steppart appelé dans kdi_base
  * KDIAgreementStepPartBase incluant l'écriture et lecture VTK HDF et JSON
  * KDIAgreementStepPartChunk
* CI + GL avec requirment.txt/setup.py
* tests base détaillés ou intégrés
* tests chunk détaillés ou intégrés
* tests update
* tests 1pe1ssd1uns1tps
* tests 1pe1ssd1uns10tps
* tests 1pe4ssd1uns10tps

## 0.1.0

* VTK/HDF 1uns 1ssd Ntps
