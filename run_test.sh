export PYTHONPATH=$PWD/src
export KDI_DICTIONARY_PATH=$PWD/src/pykdi/dictionary

# In Arcane
#   cd ~/Projects/framework_build
#   cmake --build .
#   export PYTHONPATH=~/PyCharmProjects/pykdi_master_0_8_0/src
#   export KDI_DICTIONARY_PATH=~/PyCharmProjects/pykdi_master_0_8_0/src/pykdi/dictionary
#   make
#   rm /home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi.json
#   rm /home/lekienj/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi.vtkhdf
#   clear
#
# SEQUENTIAL
#   /home/lekienj/Projects/framework_build/lib/arcane_tests_exec -arcane_opt max_iteration 50 /home/lekienj/Projects/framework/arcane/tests/testHydro-1-vtkhdf.arc
#
# PARALELL
#   /usr/bin/mpiexec -n 4 --output-filename /tmp/toto --oversubscribe /home/lekienj/Projects/framework_build/lib/arcane_tests_exec -arcane_opt max_iteration 50 /home/lekienj/Projects/framework/arcane/tests/testHydro-1-vtkhdf.arc
#
#   ctest -R hydro1_vtkhdf_4proc
#
# result in:
#  ~/Projects/framework_build/output/depouillement/vtkhdf/Mesh_kdi.vtkhdf

export KDI_TEST_MEMORY_LEAKS=1
python3 tests/test_memory_set.py