from setuptools import setup, find_namespace_packages

setup(
    name="pykdi",
    version="0.1.0",
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    python_requires=">=3.12",
    install_requires=["numpy", "h5py"],
    test_requires=["pytest", "pytest-cov"],
)