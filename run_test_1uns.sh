#!/bin/bash
#------------------------------------------------------------------------------
# Fr: Ce test évalue précisément :
#
#     - la présence attendue ou non des fichiers .vtkhdf, .json et _one.vtkhdf
#
#     - si le fichier .vtkhdf est présent, il en évalue le contenu attendu...
#       qui est invariant du mode d'exécution... mais qui peut contenir une
#       information additionnelle lorsque l'on déclare un GIC
#
#     - si le fichier .json est présent, il en évalue le contenu attendu
#       qui est invariant dans ce contexte
#       en parallèle, on ignore les informations relatives à submeshes car
#       seules les informations du rang=0 s'y trouve (et non celles des autres rangs)
#
#     - si le fichier _one.vtkhdf est présent (uniquement possible en mode parallèle),
#       il en évalue le contenu attendu en ignorant la variabilité intrinséque
#       à l'écriture concurrente du champ de valeurs sur les noeuds OriginalPartId
#       (en effet, un point peut appartenir à plusieurs partitions)
#------------------------------------------------------------------------------
set -uo pipefail

ESC="\033"
COLOR_RED_BOLD_BLINK="${ESC}[31;1;6m"
COLOR_DEFAULT="${ESC}[0m"

#------------------------------------------------------------------------------
# Executes a test following conditions passed as a parameter
#------------------------------------------------------------------------------
function run_test
{
  local -r cmd=$1
  local -r name=$2
  local -r gic=$3
  local -r write_vtk_hdf=$4
  local -r write_n_to_1vtk_hdf=$5
  local -r write_json=$6
  local -r read_json=$7

  rm -f *.json *.vtkhdf

  export KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS="${gic}"
  export KDI_TEST_WITH_SAVING_VTK_HDF="${write_vtk_hdf}"
  export KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF="${write_n_to_1vtk_hdf}"
  export KDI_TEST_WITH_SAVING_JSON="${write_json}"
  export KDI_TEST_WITH_LOADING_JSON="${read_json}"
  name_test=${KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS}${KDI_TEST_WITH_SAVING_VTK_HDF}${KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF}${KDI_TEST_WITH_SAVING_JSON}${KDI_TEST_WITH_LOADING_JSON}
  export KDI_TEST_LOG=0

  echo '.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.'
  echo '.-.-. '${cmd}' '${name_test}' '${name}
  echo '.-.-.-.-.-. '${KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS}' KDI_TEST_WITH_GLOBAL_INDEX_CONTINUOUS'
  echo '.-.-.-.-.-. '${KDI_TEST_WITH_SAVING_VTK_HDF}' KDI_TEST_WITH_SAVING_VTK_HDF'
  echo '.-.-.-.-.-. '${KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF}' KDI_TEST_WITH_SAVING_N_TO_1_VTK_HDF'
  echo '.-.-.-.-.-. '${KDI_TEST_WITH_SAVING_JSON}' KDI_TEST_WITH_SAVING_JSON'
  echo '.-.-.-.-.-. '${KDI_TEST_WITH_LOADING_JSON}' KDI_TEST_WITH_LOADING_JSON'

  ${cmd} tests/test_${name}.py SIMPLE
  ret=$?
  if [ $ret != 0 ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. !!! TEST EXECUTION '${cmd}' '${name_test}' FAILED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    exit 1
  else
    echo '.-.-.-.-.-. >>> test execution '${cmd}' '${name_test}' passed'
  fi
}

#------------------------------------------------------------------------------
# Checks for non-existence of a .vtkhdf file (excluding _one.vtkhdf)
#------------------------------------------------------------------------------
function non_exist_vtk_hdf
{
  result=$(find . -maxdepth 1 -name '*.vtkhdf' | grep -v '_one.vtkhdf' | wc -l)
  if [ "$result" != "0" ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. !!! ERROR EXIST .VTKHDF INEXPECTED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    exit 1
  else
    echo '.-.-.-.-.-. >>> non exist .vtkhdf'
  fi
}

#------------------------------------------------------------------------------
# Checks for non-existence of a .json file
#------------------------------------------------------------------------------
function non_exist_json
{
  result=$(find . -maxdepth 1 -name '*.json' | wc -l)
  if [ "$result" != "0" ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. !!! ERROR EXIST .JSON INEXPECTED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    exit 1
  else
    echo '.-.-.-.-.-. >>> non exist .json'
  fi
}

#------------------------------------------------------------------------------
# Checks for non-existence of a _one.vtkhdf
#------------------------------------------------------------------------------
function non_exist_nto1_vtk_hdf
{
  result=$(find . -maxdepth 1 -name '*_one.vtkhdf' | wc -l)
  if [ "$result" != "0" ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. !!! ERROR EXIST _ONE;VTKHDF INEXPECTED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    exit 1
  else
    echo '.-.-.-.-.-. >>> non exist _one.vtkhdf'
  fi
}

#------------------------------------------------------------------------------
# Compare two .vtkhdf files (outside of _one.vtkhdf)
# The gic parameter is linked to the definition or not followed by its writing of the GIC.
#------------------------------------------------------------------------------
function compare_vtk_hdf
{
  local -r cmd=$1
  local -r name=$2
  local -r gic=$3

  temporary="/tmp/temporary"
  #rm -rf ${temporary}
  mkdir -p ${temporary}
  if [[ "${cmd}" == "python3" ]]; then
    cp 1pe_${name}__hard.vtkhdf ${temporary}/va.vtkhdf
  else
    cp 3pe_${name}__hard.vtkhdf ${temporary}/va.vtkhdf
  fi
  h5dump ${temporary}/va.vtkhdf > ${temporary}/a.txt
  rm ${temporary}/va.vtkhdf
  cp tests/EXPECTED/1pe_${name}__hard__${gic}.vtkhdf ${temporary}/va.vtkhdf
  h5dump ${temporary}/va.vtkhdf > ${temporary}/b.txt
  rm ${temporary}/va.vtkhdf
  # same reference, just change name
  diff ${temporary}/a.txt ${temporary}/b.txt
  ret=$?
  if [ $ret != 0 ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. !!! ERROR DIFFERENCE .VTKHDF '${cmd}' NOTED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    #rm -rf ${temporary}
    exit 1
  fi
  echo '.-.-.-.-.-. >>> test .vtkhdf '${cmd}' passed'
  #rm -rf ${temporary}
}

#------------------------------------------------------------------------------
# Compare two .json files
#------------------------------------------------------------------------------
function compare_json
{
  local -r cmd=$1
  local -r name=$2

  temporary="/tmp/temporary"
  #rm -rf ${temporary}
  mkdir -p ${temporary}
  cp tests/EXPECTED/1pe_${name}__hard.json ${temporary}/a.json
  if [[ "${cmd}" == "python3" ]]; then
    cp ${temporary}/a.json ${temporary}/aa.json
    cp 1pe_${name}__hard.json ${temporary}/bb.json
    original="1pe_"${name}"__hard.json"
  else
    # Fr: On ignore les submeshes sauvegardés dans JSON en parallèle car
    #     seules les informations du rang=0 s'y trouve
    grep -v submeshes ${temporary}/a.json >  ${temporary}/aa.json
    cp 3pe_${name}__hard.json ${temporary}/b1.json
    sed 's/3pe_'${name}'__hard/1pe_'${name}'__hard/' ${temporary}/b1.json > ${temporary}/b2.json
    grep -v 'submeshes' ${temporary}/b2.json > ${temporary}/bb.json
    original="3pe_"${name}"__hard.json"
  fi
  diff ${temporary}/aa.json ${temporary}/bb.json
  ret=$?
  if [ $ret != 0 ]; then
    echo -e "${COLOR_RED_BOLD_BLINK}" >&2
    echo -e '.-.-.-.-.-. ERROR DIFF 'tests/EXPECTED/1pe_${name}__hard.json' ('${temporary}/aa.json') '$original' ('${temporary}/bb.json')'
    echo -e '.-.-.-.-.-. !!! ERROR DIFFERENCE .JSON '${cmd}' NOTED !!!'
    echo -e "${COLOR_DEFAULT}" >&2
    #rm -rf ${temporary}
    exit 1
  fi
  echo '.-.-.-.-.-. >>> test .json '${cmd}' passed'
  #rm -rf ${temporary}
}

#------------------------------------------------------------------------------
# Compare two _one.vtrkhdf files
# The value of dataset S varies depending on concurrent write conditions,
# which is why we exclude the values of this dataset in the comparison...
# and then only process these values by validating an expected cyclic pattern.
#------------------------------------------------------------------------------
# A copy of this function is present in run_test.sh
#------------------------------------------------------------------------------
function compare_nto1_vtk_hdf
{
  local -r name=$1

  temporary="/tmp/temporary"
  #rm -rf ${temporary}
  mkdir -p ${temporary}
  cp 3pe_${name}__one.vtkhdf ${temporary}/va.vtkhdf
  h5dump ${temporary}/va.vtkhdf > ${temporary}/a.txt
  cp tests/EXPECTED/3pe_${name}__one.vtkhdf ${temporary}/va.vtkhdf
  h5dump ${temporary}/va.vtkhdf > ${temporary}/b.txt
  diff ${temporary}/a.txt ${temporary}/b.txt
  ret=$?
  if [ $ret != 0 ]; then
    echo "It's normal only on OriginalPartId"
    if [ $name == "1uns_3ssd_10tps" ]; then
      sed '137,141d' ${temporary}/a.txt > ${temporary}/da.txt
      sed '137,141d' ${temporary}/b.txt > ${temporary}/db.txt
    elif [ $name == "1uns_2subuns_3ssd_10tps" ]; then
      sed '157,161d' ${temporary}/a.txt > ${temporary}/da.txt
      sed '157,161d' ${temporary}/b.txt > ${temporary}/db.txt
    elif [ $name == "1uns_2subuns_3ssd_10tps__multi" ]; then
      # mymil2
      sed '871,874d' ${temporary}/a.txt > ${temporary}/da1.txt
      sed '871,874d' ${temporary}/b.txt > ${temporary}/db1.txt
      # mymil1
      sed '542,546d' ${temporary}/da1.txt > ${temporary}/da2.txt
      sed '542,546d' ${temporary}/db1.txt > ${temporary}/db2.txt
      # mymesh
      sed '191,196d' ${temporary}/da2.txt > ${temporary}/da.txt
      sed '191,196d' ${temporary}/db2.txt > ${temporary}/db.txt
    else
      echo -e "${COLOR_RED_BOLD_BLINK}" >&2
      echo -e '.-.-.-.-.-. !!! ERROR UNFORSEEN CASE !!!'
      echo -e "${COLOR_DEFAULT}" >&2
    fi
    diff ${temporary}/da.txt ${temporary}/db.txt
    ret=$?
    if [ $ret != 0 ]; then
      echo -e "${COLOR_RED_BOLD_BLINK}" >&2
      echo -e '.-.-.-.-.-. !!! ERROR DIFFERENCE _ONE.VTKHDF NOTED !!!'
      echo -e "${COLOR_DEFAULT}" >&2
      exit 1
    else
      echo '.-.-.-.-.-. >>> test _one.vtkhdf passed'
    fi
    if [ $name == "1uns_3ssd_10tps" ]; then
      python tools/compare_array_hdf5.py tests/EXPECTED/3pe_${name}__one.vtkhdf "/VTKHDF/PointData/OriginalPartId" "[2, 2, [0, 2], [0, 1, 2], 1, [0, 2], [0, 1, 2], 1, 0, 0]"
    elif [ $name == "1uns_2subuns_3ssd_10tps" ]; then
      python tools/compare_array_hdf5.py tests/EXPECTED/3pe_${name}__one.vtkhdf "/VTKHDF/PointData/OriginalPartId" "[2, 2, [0, 2], [0, 1, 2], 1, [0, 2], [0, 1, 2], 1, 0, 0]"
    elif [ $name == "1uns_2subuns_3ssd_10tps__multi" ]; then
      python tools/compare_array_hdf5.py tests/EXPECTED/3pe_${name}__one.vtkhdf "/VTKHDF/mymesh/PointData/OriginalPartId" "[2, 2, [0, 2], [0, 1, 2], 1, [0, 2], [0, 1, 2], 1, 0, 0]"
      python tools/compare_array_hdf5.py tests/EXPECTED/3pe_${name}__one.vtkhdf "/VTKHDF/mymil1/PointData/OriginalPartId" "[2, 2, [0, 1, 2], 1, [0, 2], [0, 1, 2], 1, 0, 0]"
      python tools/compare_array_hdf5.py tests/EXPECTED/3pe_${name}__one.vtkhdf "/VTKHDF/mymil2/PointData/OriginalPartId" "[2, 2, [0, 2], [0, 1, 2], 1, [0, 1, 2], 1]"
    else
      echo -e "${COLOR_RED_BOLD_BLINK}" >&2
      echo -e '.-.-.-.-.-. !!! ERROR UNFORSEEN CASE !!!'
      echo -e "${COLOR_DEFAULT}" >&2
    fi
    ret=$?
    if [ $ret != 0 ]; then
      echo -e "${COLOR_RED_BOLD_BLINK}" >&2
      echo -e '.-.-.-.-.-. !!! ERROR UNEXPECTED _ONE.VTKHDF ORIGINALPARTID VALUES NOTED !!!'
      echo -e "${COLOR_DEFAULT}" >&2
      exit 1
    else
      echo '.-.-.-.-.-. >>> test expected _one.vtkhdf OriginalPartId values passed'
    fi
  fi
}

#------------------------------------------------------------------------------
# Main program
#------------------------------------------------------------------------------

export PYTHONPATH=$PWD/src
export KDI_DICTIONARY_PATH=$PWD/src/pykdi/dictionary

counter=0
for name in '1uns_2subuns_3ssd_10tps' '1uns_3ssd_10tps'
do
  for cmd in 'python3' 'mpirun -n 3 python3'
  do
    if [[ "${cmd}" == "mpirun -n 3 python3" ]]; then
      # Enable save VTK HDF Nto1 only available in parallel mode

      run_test "${cmd}" "${name}" "1" "0" "1" "0" "0"
      non_exist_vtk_hdf
      non_exist_json
      compare_nto1_vtk_hdf "${name}"
      if [ "${name}" == "1uns_2subuns_3ssd_10tps" ]; then
        compare_nto1_vtk_hdf "${name}__multi"
      fi
      ((counter++))

      run_test "${cmd}" "${name}" "1" "0" "1" "1" "0"
      non_exist_vtk_hdf
      non_exist_json
      compare_nto1_vtk_hdf "${name}"
      if [ "${name}" == "1uns_2subuns_3ssd_10tps" ]; then
        compare_nto1_vtk_hdf "${name}__multi"
      fi
      ((counter++))

      run_test "${cmd}" "${name}" "1" "0" "1" "1" "1"
      non_exist_vtk_hdf
      non_exist_json
      compare_nto1_vtk_hdf "${name}"
      if [ "${name}" == "1uns_2subuns_3ssd_10tps" ]; then
        compare_nto1_vtk_hdf "${name}__multi"
      fi
      ((counter++))

      run_test "${cmd}" "${name}" "1" "1" "1" "1" "0"
      compare_vtk_hdf "${cmd}" "${name}" "GIC"
      compare_json "${cmd}" "${name}"
      compare_nto1_vtk_hdf "${name}"
      if [ "${name}" == "1uns_2subuns_3ssd_10tps" ]; then
        compare_nto1_vtk_hdf "${name}__multi"
      fi
      ((counter++))

      run_test "${cmd}" "${name}" "1" "1" "1" "1" "1"
      compare_vtk_hdf "${cmd}" "${name}" "GIC"
      compare_json "${cmd}" "${name}"
      compare_nto1_vtk_hdf "${name}"
      if [ "${name}" == "1uns_2subuns_3ssd_10tps" ]; then
        compare_nto1_vtk_hdf "${name}__multi"
      fi
      ((counter++))
    fi

    run_test "${cmd}" "${name}" "0" "0" "0" "0" "0"
    non_exist_vtk_hdf
    non_exist_json
    non_exist_nto1_vtk_hdf
    ((counter++))

    # export KDI_VTK_HDF_TRACE="1"
    run_test "${cmd}" "${name}" "0" "1" "0" "0" "0"
    compare_vtk_hdf "${cmd}" "${name}" "NON_GIC"
    non_exist_json
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "0" "1" "0" "1" "0"
    compare_vtk_hdf "${cmd}" "${name}" "NON_GIC"
    compare_json "${cmd}" "${name}"
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "0" "1" "0" "1" "1"
    compare_vtk_hdf "${cmd}" "${name}" "NON_GIC"
    compare_json "${cmd}" "${name}"
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "1" "0" "0" "0" "0"
    non_exist_vtk_hdf
    non_exist_json
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "1" "1" "0" "0" "0"
    compare_vtk_hdf "${cmd}" "${name}" "GIC"
    non_exist_json
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "1" "1" "0" "1" "0"
    compare_vtk_hdf "${cmd}" "${name}" "GIC"
    compare_json "${cmd}" "${name}"
    non_exist_nto1_vtk_hdf
    ((counter++))

    run_test "${cmd}" "${name}" "1" "1" "0" "1" "1"
    compare_vtk_hdf "${cmd}" "${name}" "GIC"
    compare_json "${cmd}" "${name}"
    non_exist_nto1_vtk_hdf
    ((counter++))
  done
done
echo -e "\nNumber of test configurations passed: $counter (each comprising at least 4 stages with thousands of tests each)"